Attribute VB_Name = "modBillPrint"
Option Explicit
    Dim CSetPrinter As New cSetDfltPrinter


Private Sub PrintPosBill(BillId As Long)
    CSetPrinter.SetPrinterAsDefault (BillPrinterName)

    Dim TemRows As Long

With Printer

        Printer.Font = "Arial Black"
'        Printer.Print
        
        Printer.FontSize = 11
     '   Printer.Print Tab(2); InstitutionName;
   '     Printer.Print Tab(54); InstitutionName
        
'        Printer.FontSize = 9
'        Printer.Print Tab(3); InstitutionAddress;
'        Printer.Print Tab(64); InstitutionAddress
        
'        Printer.Print Tab(3); InstitutionTelephone;
'        Printer.Print Tab(64); InstitutionTelephone
        
        Printer.FontName = "Courier"
        Printer.FontSize = 11
'        Printer.Print
        
        Dim TemTab1 As Long
        Dim TemTab2 As Long
        Dim TemTab3 As Long
        Dim TemTab4 As Long
        Dim TemTab5 As Long
        Dim TemTab6 As Long
        Dim TemTab7 As Long
        Dim TemTab8 As Long
        Dim TemTab9 As Long
        Dim TemTab10 As Long
        Dim TemTab11 As Long
        Dim TemTab12 As Long
        
        
        Dim Displace As Long
        
        Displace = 10
        
        TemTab1 = 2 + Displace
        TemTab2 = 6 + Displace
        TemTab3 = 20 + Displace
        TemTab4 = 25 + Displace
        TemTab5 = 36 + Displace
        TemTab6 = 16 + Displace
        
        
        
        Displace = 88
        
        TemTab7 = 2 + Displace
        TemTab8 = 16 + Displace
        TemTab9 = 20 + Displace
        TemTab10 = 25 + Displace
        TemTab11 = 36 + Displace
        TemTab12 = 16 + Displace
        
        Printer.Font.Bold = True
        Printer.Font.Underline = True
        Printer.Print Tab(TemTab3);
        Printer.Print Tab(TemTab9);
        Printer.Font.Bold = False
        Printer.Font.Underline = False
        
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        
        
        Printer.Print Tab(TemTab1); "Patient"; ;
'        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); TemPatient
        'd
    '    Printer.Print Tab(TemTab7); "Patient";
'        Printer.Print Tab(TemTab8); " : ";
     '   Printer.Print Tab(TemTab9); TemPatient
        
'        Printer.Print
        Printer.Print Tab(TemTab1); "Consultant"; ;
        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); UCase(FindLDoctorFromID(Val(ListConsultantIDs.text)))
        'd
      '  Printer.Print Tab(TemTab7); "Consultant";
       ' Printer.Print Tab(TemTab8); " : ";
        'Printer.Print Tab(TemTab9); UCase(FindLDoctorFromID(Val(ListConsultantIDs.text)))
'        Printer.Print
        Printer.Print Tab(TemTab1); "Appo. Date "; ;
        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); Format(ListDates.text, DefaultLongDate)
        'd
 '       Printer.Print Tab(TemTab7); "Appo. Date ";
 '       Printer.Print Tab(TemTab8); " : ";
 '       Printer.Print Tab(TemTab9); Format(ListDates.text, DefaultLongDate)
        
        Printer.Print Tab(TemTab1); "Appo. Time"; ;
        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); TemAppointmentTime
        'd
 '       Printer.Print Tab(TemTab7); "Appo. Time";
 '       Printer.Print Tab(TemTab8); " : ";
 '       Printer.Print Tab(TemTab9); TemAppointmentTime
        
        Printer.Print Tab(TemTab1); "Appo. No."; ;
        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); TemDaySerial
        
 '       Printer.Print Tab(TemTab7); "Appo. No.";
 '       Printer.Print Tab(TemTab8); " : ";
 '       Printer.Print Tab(TemTab9); TemDaySerial
        
        Printer.Print Tab(TemTab1); "Room No."; ;
        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); ListRoomNo.text
        'd
        
  '      Printer.Print Tab(TemTab7); "Room No.";
  '      Printer.Print Tab(TemTab8); " : ";
  '      Printer.Print Tab(TemTab9); ListRoomNo.text
        
        
        Printer.Print Tab(TemTab1); "Appo. ID"; ;
        Printer.Print Tab(TemTab6); " : "; ;
        Printer.Print Tab(TemTab3); TemPatientFacilityID
        'd
        
   '     Printer.Print Tab(TemTab7); "Appo. ID";
   '     Printer.Print Tab(TemTab8); " : ";
   '     Printer.Print Tab(TemTab9); TemPatientFacilityID
        
'        Printer.Print
        
        If SSTab1.Tab = 0 Then
        
            Printer.Print Tab(TemTab1); "Doctor Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(TemDoctorFee, "0.00"))); Format(TemDoctorFee, "0.00")
            
            'd
   '         Printer.Print Tab(TemTab7); "Doctor Fee";
   '         Printer.Print Tab(TemTab8); " : ";
   '         Printer.Print Tab(TemTab9 + 8 - Len(Format(TemDoctorFee, "0.00"))); Format(TemDoctorFee, "0.00")
            
            
            Printer.Print Tab(TemTab1); "Hospital Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(TemInstitutionFee, "0.00"))); Format(TemInstitutionFee, "0.00")
            
     '       Printer.Print Tab(TemTab7); "Hospital Fee";
    '        Printer.Print Tab(TemTab8); " : ";
    '        Printer.Print Tab(TemTab9 + 8 - Len(Format(TemInstitutionFee, "0.00"))); Format(TemInstitutionFee, "0.00")
            
            Printer.Print Tab(TemTab1); "Total Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(TemDoctorFee + TemInstitutionFee, "0.00"))); Format(TemDoctorFee + TemInstitutionFee, "0.00")
            'd
            
   '         Printer.Print Tab(TemTab7); "Total Fee";
   '         Printer.Print Tab(TemTab8); " : ";
   '         Printer.Print Tab(TemTab9 + 8 - Len(Format(TemDoctorFee + TemInstitutionFee, "0.00"))); Format(TemDoctorFee + TemInstitutionFee, "0.00")
        
            Printer.Print Tab(TemTab1); "Payment";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3); "Cash"
            'd

    '        Printer.Print Tab(TemTab7); "Payment";
    '        Printer.Print Tab(TemTab8); " : ";
    '        Printer.Print Tab(TemTab9); "Cash"
        
        
        ElseIf SSTab1.Tab = 1 Then
            Printer.Print Tab(TemTab1); "Doctor Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(TemDoctorFee, "0.00"))); Format(TemDoctorFee, "0.00")
            
            'd
       '     Printer.Print Tab(TemTab7); "Doctor Fee";
       '     Printer.Print Tab(TemTab8); " : ";
     '       Printer.Print Tab(TemTab9 + 8 - Len(Format(TemDoctorFee, "0.00"))); Format(TemDoctorFee, "0.00")
            
            
            Printer.Print Tab(TemTab1); "Hospital Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(TemInstitutionFee, "0.00"))); Format(TemInstitutionFee, "0.00")
            
       '     Printer.Print Tab(TemTab7); "Hospital Fee";
      '      Printer.Print Tab(TemTab8); " : ";
      '      Printer.Print Tab(TemTab9 + 8 - Len(Format(TemInstitutionFee, "0.00"))); Format(TemInstitutionFee, "0.00")
            
            Printer.Print Tab(TemTab1); "Total Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(TemDoctorFee + TemInstitutionFee, "0.00"))); Format(TemDoctorFee + TemInstitutionFee, "0.00")
            'd
            
     '       Printer.Print Tab(TemTab7); "Total Fee";
    '        Printer.Print Tab(TemTab8); " : ";
    '        Printer.Print Tab(TemTab9 + 8 - Len(Format(TemDoctorFee + TemInstitutionFee, "0.00"))); Format(TemDoctorFee + TemInstitutionFee, "0.00")
        
            Printer.Print Tab(TemTab1); "Payment";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3); "Agent"
            'd

     '       Printer.Print Tab(TemTab7); "Payment";
     '       Printer.Print Tab(TemTab8); " : ";
     '       Printer.Print Tab(TemTab9); "Agent"
        
        ElseIf SSTab1.Tab = 2 Then
        
            Printer.Print Tab(TemTab1); "Doctor Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(0, "0.00"))); Format(0, "0.00")
            
            'd
   '         Printer.Print Tab(TemTab7); "Doctor Fee";
   '         Printer.Print Tab(TemTab8); " : ";
   '         Printer.Print Tab(TemTab9 + 8 - Len(Format(0, "0.00"))); Format(0, "0.00")
            
            
            Printer.Print Tab(TemTab1); "Hospital Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(0, "0.00"))); Format(0, "0.00")
            
      '      Printer.Print Tab(TemTab7); "Hospital Fee";
      '      Printer.Print Tab(TemTab8); " : ";
      '      Printer.Print Tab(TemTab9 + 8 - Len(Format(0, "0.00"))); Format(0, "0.00")
            
            Printer.Print Tab(TemTab1); "Total Fee";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3 + 8 - Len(Format(0 + 0, "0.00"))); Format(0 + 0, "0.00")
            'd
            
      '      Printer.Print Tab(TemTab7); "Total Fee";
      '      Printer.Print Tab(TemTab8); " : ";
          '  Printer.Print Tab(TemTab9 + 8 - Len(Format(0 + 0, "0.00"))); Format(TemDoctorFee + TemInstitutionFee, "0.00")
        
            Printer.Print Tab(TemTab1); "Payment";
            Printer.Print Tab(TemTab6); " : ";
            Printer.Print Tab(TemTab3); "Credit"
            'd

       '     Printer.Print Tab(TemTab7); "Payment";
       '     Printer.Print Tab(TemTab8); " : ";
      '      Printer.Print Tab(TemTab9); "Credit"
        
        End If
        
        Printer.Print
        Printer.Print
        
        Printer.Print Tab(TemTab2); "--------------------"
     '   Printer.Print Tab(TemTab8); "--------------------"
        
        Printer.Print Tab(TemTab2); UserName
     '   Printer.Print Tab(TemTab8); UserName
        
        Printer.Print Tab(TemTab2); Time
    '    Printer.Print Tab(TemTab8); Time
        
        Printer.Print Tab(TemTab2); Format(Date, DefaultShortDate)
    '    Printer.Print Tab(TemTab8); Format(Date, DefaultShortDate)
        
        Printer.EndDoc
    End With

End Sub


