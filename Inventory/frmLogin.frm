VERSION 5.00
Begin VB.Form frmLogin 
   BackColor       =   &H00C0FFFF&
   Caption         =   "Inventory Login"
   ClientHeight    =   4860
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7050
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   Picture         =   "frmLogin.frx":0000
   ScaleHeight     =   4860
   ScaleWidth      =   7050
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtPassword 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   3000
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   3360
      Width           =   2535
   End
   Begin VB.TextBox txtUserName 
      Height          =   375
      Left            =   3000
      TabIndex        =   1
      Top             =   2760
      Width           =   2535
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "C&lose"
      Height          =   375
      Left            =   4320
      TabIndex        =   5
      Top             =   3840
      Width           =   1215
   End
   Begin VB.CommandButton btnLogin 
      Caption         =   "&Login"
      Height          =   375
      Left            =   3000
      TabIndex        =   4
      Top             =   3840
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "&Password"
      Height          =   255
      Left            =   1680
      TabIndex        =   2
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "&Username"
      Height          =   255
      Left            =   1680
      TabIndex        =   0
      Top             =   2760
      Width           =   1215
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim frmResize As New clsResizer
    Dim FSys As New Scripting.FileSystemObject
    Dim isFirstLogin As Boolean
    
    Dim mySec As New clsSecurity
    Dim myUser As New clsUser

Private Sub setSecKey()
    Dim mySec As New clsSecurity
    ProgramVariable.SecurityKey = mySec.Decode(")?C{.�", "Buddhika")
End Sub


Private Sub btnCancel_Click()
    End
End Sub

Private Sub btnLogin_Click()
    If (Trim(txtUserName.text) = "") Then
        MsgBox "Please enter the user name"
        txtUserName.SetFocus
    End If
    If (Trim(txtPassword.text) = "") Then
        MsgBox "Please enter the password"
        txtPassword.SetFocus
    End If
    If isFirstLogin = True Then
        addData
    Else
        If getUserData = False Then
            MsgBox "Login Failure. Please try again"
            txtUserName.SetFocus
        Else
            Set ProgramVariable.loggedUser = myUser
            MDIMain.Show
            Unload Me
        End If
        
    End If
End Sub

Private Function getUserData() As Boolean
    Dim temSQL As String
    Dim loginSuccess As Boolean
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblUser"
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                If Trim(txtUserName.text) = mySec.Decode(!UserName, ProgramVariable.SecurityKey) And mySec.Hash(Trim(txtPassword.text)) = !UserPassword Then
                    myUser.UserID = !UserID
                    getUserData = True
                End If
                .MoveNext
            Wend
            .Close
        Else
            addData
        End If
    End With
End Function

Private Sub addData()
    Dim myPerson As New clsPerson
    Dim myRole As New clsRole

    
    With myPerson
        .AddedDate = Date
        .AddedTime = Time
        .PersonName = Trim(txtUserName.text)
        .saveData
    End With
    
    With myRole
        .AddedDate = Date
        .AddedTime = Time
        .RoleName = "Administrator"
        .saveData
    End With
    
    With myUser
        .AddedDate = Date
        .AddedTime = Time
        .personId = myPerson.personId
        .RoleID = myRole.RoleID
        .UserName = mySec.Encode(Trim(txtUserName.text), ProgramVariable.SecurityKey)
        .UserPassword = mySec.Hash(Trim(txtPassword.text))
        .saveData
        .AddedUserID = .UserID
        .saveData
    End With
    
    With myPerson
        .AddedUserID = myUser.UserID
        .saveData
    End With
    
    With myRole
        .AddedUserID = myUser.UserID
        .saveData
    End With
    
    
    Set ProgramVariable.loggedUser = myUser
    MDIMain.Show
    Unload Me
    
End Sub



Private Sub Form_Load()
    Call checkFirstLogin
    'SetColours Me
    'GetCommonSettings Me
    Call prepareResize
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    'SaveCommonSettings Me
End Sub

Private Sub Form_Resize()
  Call frmResize.FormResized(Me)
End Sub

Private Sub prepareResize()
  frmResize.KeepRatio = False
  frmResize.FontResize = True
  Call frmResize.InitializeResizer(Me)
End Sub

Private Sub checkFirstLogin()
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblUser"
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        
        If .RecordCount <= 0 Then
            isFirstLogin = True
        Else
            isFirstLogin = False
        End If
        .Close
    End With
    Set rsTem = Nothing
End Sub

Private Sub addRole()

End Sub

Private Sub txtPassword_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        If Trim(txtUserName.text) <> "" Then
            btnLogin_Click
        Else
            txtUserName.SetFocus
        End If
    End If
End Sub

Private Sub txtUserName_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtPassword.SetFocus
    Else
        
    End If
End Sub
