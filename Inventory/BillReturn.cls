VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BillReturn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varbillId As Long
    Private varreturnedAt As Date
    Private varreturnedDate As Date
    Private varreturnedUserId As Long
    Private varreturnedValue As Double
    Private vardtype As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBillReturn Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !billId = varbillId
        !returnedAt = varreturnedAt
        !returnedDate = varreturnedDate
        !returnedUserId = varreturnedUserId
        !returnedValue = varreturnedValue
        !dtype = vardtype
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBillReturn WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!billId) Then
               varbillId = !billId
            End If
            If Not IsNull(!returnedAt) Then
               varreturnedAt = !returnedAt
            End If
            If Not IsNull(!returnedDate) Then
               varreturnedDate = !returnedDate
            End If
            If Not IsNull(!returnedUserId) Then
               varreturnedUserId = !returnedUserId
            End If
            If Not IsNull(!returnedValue) Then
               varreturnedValue = !returnedValue
            End If
            If Not IsNull(!dtype) Then
               vardtype = !dtype
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varbillId = 0
    varreturnedAt = Empty
    varreturnedDate = Empty
    varreturnedUserId = 0
    varreturnedValue = 0
    vardtype = Empty
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let billId(ByVal vbillId As Long)
    varbillId = vbillId
End Property

Public Property Get billId() As Long
    billId = varbillId
End Property

Public Property Let returnedAt(ByVal vreturnedAt As Date)
    varreturnedAt = vreturnedAt
End Property

Public Property Get returnedAt() As Date
    returnedAt = varreturnedAt
End Property

Public Property Let returnedDate(ByVal vreturnedDate As Date)
    varreturnedDate = vreturnedDate
End Property

Public Property Get returnedDate() As Date
    returnedDate = varreturnedDate
End Property

Public Property Let returnedUserId(ByVal vreturnedUserId As Long)
    varreturnedUserId = vreturnedUserId
End Property

Public Property Get returnedUserId() As Long
    returnedUserId = varreturnedUserId
End Property

Public Property Let returnedValue(ByVal vreturnedValue As Double)
    varreturnedValue = vreturnedValue
End Property

Public Property Get returnedValue() As Double
    returnedValue = varreturnedValue
End Property

Public Property Let dtype(ByVal vdtype As String)
    vardtype = vdtype
End Property

Public Property Get dtype() As String
    dtype = vardtype
End Property


