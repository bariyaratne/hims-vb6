VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BillItemReturn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varbillItemId As Long
    Private varitemId As Long
    Private varbatchId As String
    Private varreturnRate As Double
    Private varreturnQuantity As Double
    Private varreturnValue As Double

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBillItemReturn Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !billItemId = varbillItemId
        !itemId = varitemId
        !batchId = varbatchId
        !returnRate = varreturnRate
        !returnQuantity = varreturnQuantity
        !returnValue = varreturnValue
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBillItemReturn WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!billItemId) Then
               varbillItemId = !billItemId
            End If
            If Not IsNull(!itemId) Then
               varitemId = !itemId
            End If
            If Not IsNull(!batchId) Then
               varbatchId = !batchId
            End If
            If Not IsNull(!returnRate) Then
               varreturnRate = !returnRate
            End If
            If Not IsNull(!returnQuantity) Then
               varreturnQuantity = !returnQuantity
            End If
            If Not IsNull(!returnValue) Then
               varreturnValue = !returnValue
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varbillItemId = 0
    varitemId = 0
    varbatchId = Empty
    varreturnRate = 0
    varreturnQuantity = 0
    varreturnValue = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let billItemId(ByVal vbillItemId As Long)
    varbillItemId = vbillItemId
End Property

Public Property Get billItemId() As Long
    billItemId = varbillItemId
End Property

Public Property Let itemId(ByVal vitemId As Long)
    varitemId = vitemId
End Property

Public Property Get itemId() As Long
    itemId = varitemId
End Property

Public Property Let batchId(ByVal vbatchId As String)
    varbatchId = vbatchId
End Property

Public Property Get batchId() As String
    batchId = varbatchId
End Property

Public Property Let returnRate(ByVal vreturnRate As Double)
    varreturnRate = vreturnRate
End Property

Public Property Get returnRate() As Double
    returnRate = varreturnRate
End Property

Public Property Let returnQuantity(ByVal vreturnQuantity As Double)
    varreturnQuantity = vreturnQuantity
End Property

Public Property Get returnQuantity() As Double
    returnQuantity = varreturnQuantity
End Property

Public Property Let returnValue(ByVal vreturnValue As Double)
    varreturnValue = vreturnValue
End Property

Public Property Get returnValue() As Double
    returnValue = varreturnValue
End Property


