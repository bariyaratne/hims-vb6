Attribute VB_Name = "modStart"
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim FSys As New Scripting.FileSystemObject

Public Sub Main()
    Call setSecKey
    Call LoadColourPreferances
    Call LoadPreferances
    Call selectDatabase
    Call updateDatabase
    If activation = False Then Exit Sub
    Call getIns
    frmLogin.Show
End Sub

Public Sub getIns()
    Dim rsTem As New ADODB.Recordset
    Dim sql As String
    Dim ins As New institution
    sql = "Select id from tblInstitution where dtype = 'owner'"
    
    With rsTem
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            ins.id = !id
        Else
            ins.institutionName = "www.divudi.com"
            ins.dtype = "owner"
            ins.saveData
        End If
        .Close
    End With
    Set ProgramVariable.institution = ins
End Sub


Public Function activation() As Boolean
    Dim TemResponce As Byte
    Dim WillExpire As Boolean
    Dim ExpiaryDate As Date
    Dim myAct As New clsActivation
    Dim hiddenDemo As Boolean
    
    WillExpire = True
    hiddenDemo = False
    ExpiaryDate = #3/31/2013#
    
    If myAct.isActivated = False Then
        If ExpiaryDate >= Date Then
            If hiddenDemo = False Then
                TemResponce = MsgBox("This is a Demo copy. This program will expire. Do you want to register the product now?" & vbNewLine & "+94 715812399", vbYesNoCancel, "Register?")
                If TemResponce = vbYes Then
                    frmActivate.Show
                    frmActivate.ZOrder 0
                    Exit Function
                ElseIf TemResponce = vbNo Then
                    activation = True
                Else
                    activation = False
                    End
                End If
            Else
                activation = True
            End If
        Else
            TemResponce = MsgBox("The Program has expiared. Please contact www.divudi.com for Assistant." & vbNewLine & "+94 715812399" & vbNewLine & "buddhika.ari@gmail.com, info@divudi.com", vbCritical, "Expired")
            frmActivate.Show
            frmActivate.ZOrder 0
            activation = False
            Exit Function
        End If
    Else
        activation = True
    End If
    
End Function



Public Sub updateDatabase()
    Dim dbVersion As New clsVersion
    dbVersion.loadData
    If dbVersion.VersionNo = 1 Then
        updateDBFromV1ToV2
    ElseIf dbVersion.VersionNo = 2 Then
    
    End If
End Sub

Private Sub updateDBFromV1ToV2()

End Sub

Private Sub setSecKey()
    Dim mySec As New clsSecurity
    ProgramVariable.SecurityKey = mySec.Decode(")?C{.�", "Buddhika")
End Sub

Private Sub selectDatabase()
    If FSys.FileExists(ProgramVariable.DatabaseName) = True Then
        openDatabase
    Else
        frmInitialPreferances.Show 1
        selectDatabase
    End If
End Sub

Public Sub openDatabase()
    openAdoDatabase
    openDeDatabase
End Sub

Public Sub closeDatabase()
    closeAdoDatabase
    closeDeDatabase
End Sub

Public Sub openAdoDatabase()
    On Error GoTo eh
    Dim cnn As New ADODB.Connection
    Dim constr As String
    constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & ProgramVariable.DatabaseName & " ;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
    With ProgramVariable.conn
        If .State = 1 Then .Close
        .Open constr
    End With
    Exit Sub
eh:
    MsgBox "Error connecting to database"
End Sub

Public Sub closeAdoDatabase()
    With ProgramVariable.conn
        If .State = 1 Then .Close
    End With
    Exit Sub
eh:
    MsgBox "Error disconnecting to database"

End Sub

Public Sub openDeDatabase()
    On Error GoTo eh
    Dim constr As String
    constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & ProgramVariable.DatabaseName & " ;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
    With DataEnvironment1.Connection1
        If .State = 1 Then .Close
        .ConnectionString = constr
        .Open
    End With
    Exit Sub
eh:
    MsgBox "Error connecting to database"
End Sub

Public Sub closeDeDatabase()
    With DataEnvironment1.Connection1
        If .State = 1 Then .Close
    End With
    Exit Sub
eh:
    MsgBox "Error disconnecting to database"
End Sub

Private Sub LoadPreferances()
'On Error Resume Next
    ProgramVariable.DatabaseName = GetSetting(App.EXEName, "Options", "Database", App.Path & "\Database.mdb")

    ProgramVariable.LongDateFormat = GetSetting(App.EXEName, "Options", "LongDateFormat", "yyyy mmmm dd")
    ProgramVariable.ShortDateFormat = GetSetting(App.EXEName, "Options", "ShortDateFormat", "dd MM yy")
    
    ProgramVariable.sqlLike = "%"

    If CBool(GetSetting(App.EXEName, "Options", "Energy", True)) = True Then
        DefaultColourScheme = Energy
    ElseIf CBool(GetSetting(App.EXEName, "Options", "Sunny", False)) = True Then
        DefaultColourScheme = Sunny
    ElseIf CBool(GetSetting(App.EXEName, "Options", "Aqua", False)) = True Then
        DefaultColourScheme = Aqua
    End If
        
    MDIImageFile = GetSetting(App.EXEName, "Options", "MDIImageFile", "")
    
    
    ProgramVariable.saleBillPrinterName = GetSetting(App.EXEName, App.EXEName, "saleBillPrinterName", "")
    ProgramVariable.saleBillPaperName = GetSetting(App.EXEName, App.EXEName, "saleBillPaperName", "")
    
    
    ProgramVariable.reportPrinterName = GetSetting(App.EXEName, App.EXEName, "reportPrinterName", "")
    ProgramVariable.reportPaperName = GetSetting(App.EXEName, App.EXEName, "reportPaperName", "")
    
    
    ProgramVariable.purchaseBillPrinterName = GetSetting(App.EXEName, App.EXEName, "purchaseBillPrinterName", "")
    ProgramVariable.purchaseBillPaperName = GetSetting(App.EXEName, App.EXEName, "purchaseBillPaperName", "")

    
End Sub


