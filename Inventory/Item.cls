VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Item"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varitemName As String
    Private varitemCode As String
    Private vardeleted As Boolean
    Private vardeletedAt As Date
    Private vardeletedUserId As Long
    Private varcategoryId As Long
    Private vardtype As String
    Private varcanChangePrice As Boolean
    Private varpriceByItem As Boolean
    Private varpriceByBatch As Boolean
    Private varmeasurmentUnitId As Long
    Private varisAService As Boolean
    Private varNoWarrenty As Boolean
    Private varwarrentyByPurchase As Boolean
    Private varwarrentyBySale As Boolean
    Private varwarrentyDaysFromSale As Long
    Private varwarrentyDaysLessFromPurchase As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblItem Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !itemName = varitemName
        !itemCode = varitemCode
        !deleted = vardeleted
        !deletedAt = vardeletedAt
        !deletedUserId = vardeletedUserId
        !categoryId = varcategoryId
        !dtype = vardtype
        !canChangePrice = varcanChangePrice
        !priceByItem = varpriceByItem
        !priceByBatch = varpriceByBatch
        !measurmentUnitId = varmeasurmentUnitId
        !isAService = varisAService
        !NoWarrenty = varNoWarrenty
        !warrentyByPurchase = varwarrentyByPurchase
        !warrentyBySale = varwarrentyBySale
        !warrentyDaysFromSale = varwarrentyDaysFromSale
        !warrentyDaysLessFromPurchase = varwarrentyDaysLessFromPurchase
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblItem WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!itemName) Then
               varitemName = !itemName
            End If
            If Not IsNull(!itemCode) Then
               varitemCode = !itemCode
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
            If Not IsNull(!categoryId) Then
               varcategoryId = !categoryId
            End If
            If Not IsNull(!dtype) Then
               vardtype = !dtype
            End If
            If Not IsNull(!canChangePrice) Then
               varcanChangePrice = !canChangePrice
            End If
            If Not IsNull(!priceByItem) Then
               varpriceByItem = !priceByItem
            End If
            If Not IsNull(!priceByBatch) Then
               varpriceByBatch = !priceByBatch
            End If
            If Not IsNull(!measurmentUnitId) Then
               varmeasurmentUnitId = !measurmentUnitId
            End If
            If Not IsNull(!isAService) Then
               varisAService = !isAService
            End If
            If Not IsNull(!NoWarrenty) Then
               varNoWarrenty = !NoWarrenty
            End If
            If Not IsNull(!warrentyByPurchase) Then
               varwarrentyByPurchase = !warrentyByPurchase
            End If
            If Not IsNull(!warrentyBySale) Then
               varwarrentyBySale = !warrentyBySale
            End If
            If Not IsNull(!warrentyDaysFromSale) Then
               varwarrentyDaysFromSale = !warrentyDaysFromSale
            End If
            If Not IsNull(!warrentyDaysLessFromPurchase) Then
               varwarrentyDaysLessFromPurchase = !warrentyDaysLessFromPurchase
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varitemName = Empty
    varitemCode = Empty
    vardeleted = False
    vardeletedAt = Empty
    vardeletedUserId = 0
    varcategoryId = 0
    vardtype = Empty
    varcanChangePrice = False
    varpriceByItem = False
    varpriceByBatch = False
    varmeasurmentUnitId = 0
    varisAService = False
    varNoWarrenty = False
    varwarrentyByPurchase = False
    varwarrentyBySale = False
    varwarrentyDaysFromSale = 0
    varwarrentyDaysLessFromPurchase = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let itemName(ByVal vitemName As String)
    varitemName = vitemName
End Property

Public Property Get itemName() As String
    itemName = varitemName
End Property

Public Property Let itemCode(ByVal vitemCode As String)
    varitemCode = vitemCode
End Property

Public Property Get itemCode() As String
    itemCode = varitemCode
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property

Public Property Let categoryId(ByVal vcategoryId As Long)
    varcategoryId = vcategoryId
End Property

Public Property Get categoryId() As Long
    categoryId = varcategoryId
End Property

Public Property Let dtype(ByVal vdtype As String)
    vardtype = vdtype
End Property

Public Property Get dtype() As String
    dtype = vardtype
End Property

Public Property Let canChangePrice(ByVal vcanChangePrice As Boolean)
    varcanChangePrice = vcanChangePrice
End Property

Public Property Get canChangePrice() As Boolean
    canChangePrice = varcanChangePrice
End Property

Public Property Let priceByItem(ByVal vpriceByItem As Boolean)
    varpriceByItem = vpriceByItem
End Property

Public Property Get priceByItem() As Boolean
    priceByItem = varpriceByItem
End Property

Public Property Let priceByBatch(ByVal vpriceByBatch As Boolean)
    varpriceByBatch = vpriceByBatch
End Property

Public Property Get priceByBatch() As Boolean
    priceByBatch = varpriceByBatch
End Property

Public Property Let measurmentUnitId(ByVal vmeasurmentUnitId As Long)
    varmeasurmentUnitId = vmeasurmentUnitId
End Property

Public Property Get measurmentUnitId() As Long
    measurmentUnitId = varmeasurmentUnitId
End Property

Public Property Let isAService(ByVal visAService As Boolean)
    varisAService = visAService
End Property

Public Property Get isAService() As Boolean
    isAService = varisAService
End Property

Public Property Let NoWarrenty(ByVal vNoWarrenty As Boolean)
    varNoWarrenty = vNoWarrenty
End Property

Public Property Get NoWarrenty() As Boolean
    NoWarrenty = varNoWarrenty
End Property

Public Property Let warrentyByPurchase(ByVal vwarrentyByPurchase As Boolean)
    varwarrentyByPurchase = vwarrentyByPurchase
End Property

Public Property Get warrentyByPurchase() As Boolean
    warrentyByPurchase = varwarrentyByPurchase
End Property

Public Property Let warrentyBySale(ByVal vwarrentyBySale As Boolean)
    varwarrentyBySale = vwarrentyBySale
End Property

Public Property Get warrentyBySale() As Boolean
    warrentyBySale = varwarrentyBySale
End Property

Public Property Let warrentyDaysFromSale(ByVal vwarrentyDaysFromSale As Long)
    varwarrentyDaysFromSale = vwarrentyDaysFromSale
End Property

Public Property Get warrentyDaysFromSale() As Long
    warrentyDaysFromSale = varwarrentyDaysFromSale
End Property

Public Property Let warrentyDaysLessFromPurchase(ByVal vwarrentyDaysLessFromPurchase As Long)
    varwarrentyDaysLessFromPurchase = vwarrentyDaysLessFromPurchase
End Property

Public Property Get warrentyDaysLessFromPurchase() As Long
    warrentyDaysLessFromPurchase = varwarrentyDaysLessFromPurchase
End Property


