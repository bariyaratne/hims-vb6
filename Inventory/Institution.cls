VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Institution"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varinstitutionName As String
    Private varinstitutionCode As String
    Private varparentInstitutionId As Long
    Private vardtype As String
    Private vardeleted As Boolean
    Private vardeletedUserId As Long
    Private vardeletedAt As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblInstitution Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !institutionName = varinstitutionName
        !institutionCode = varinstitutionCode
        !parentInstitutionId = varparentInstitutionId
        !dtype = vardtype
        !deleted = vardeleted
        !deletedUserId = vardeletedUserId
        !deletedAt = vardeletedAt
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblInstitution WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!institutionName) Then
               varinstitutionName = !institutionName
            End If
            If Not IsNull(!institutionCode) Then
               varinstitutionCode = !institutionCode
            End If
            If Not IsNull(!parentInstitutionId) Then
               varparentInstitutionId = !parentInstitutionId
            End If
            If Not IsNull(!dtype) Then
               vardtype = !dtype
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varinstitutionName = Empty
    varinstitutionCode = Empty
    varparentInstitutionId = 0
    vardtype = Empty
    vardeleted = False
    vardeletedUserId = 0
    vardeletedAt = Empty
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let institutionName(ByVal vinstitutionName As String)
    varinstitutionName = vinstitutionName
End Property

Public Property Get institutionName() As String
    institutionName = varinstitutionName
End Property

Public Property Let institutionCode(ByVal vinstitutionCode As String)
    varinstitutionCode = vinstitutionCode
End Property

Public Property Get institutionCode() As String
    institutionCode = varinstitutionCode
End Property

Public Property Let parentInstitutionId(ByVal vparentInstitutionId As Long)
    varparentInstitutionId = vparentInstitutionId
End Property

Public Property Get parentInstitutionId() As Long
    parentInstitutionId = varparentInstitutionId
End Property

Public Property Let dtype(ByVal vdtype As String)
    vardtype = vdtype
End Property

Public Property Get dtype() As String
    dtype = vardtype
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property


