VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Batch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varbatchName As String
    Private varprice As Double
    Private varbatchCode As String
    Private varitemId As Long
    Private vardom As Date
    Private vardoe As Date
    Private vardeleted As Boolean
    Private vardeletedUserId As Long
    Private vardeletedAt As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBatch Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !batchName = varbatchName
        !price = varprice
        !batchCode = varbatchCode
        !itemId = varitemId
        !dom = vardom
        !doe = vardoe
        !deleted = vardeleted
        !deletedUserId = vardeletedUserId
        !deletedAt = vardeletedAt
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBatch WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!batchName) Then
               varbatchName = !batchName
            End If
            If Not IsNull(!price) Then
               varprice = !price
            End If
            If Not IsNull(!batchCode) Then
               varbatchCode = !batchCode
            End If
            If Not IsNull(!itemId) Then
               varitemId = !itemId
            End If
            If Not IsNull(!dom) Then
               vardom = !dom
            End If
            If Not IsNull(!doe) Then
               vardoe = !doe
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varbatchName = Empty
    varprice = 0
    varbatchCode = Empty
    varitemId = 0
    vardom = Empty
    vardoe = Empty
    vardeleted = False
    vardeletedUserId = 0
    vardeletedAt = Empty
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let batchName(ByVal vbatchName As String)
    varbatchName = vbatchName
End Property

Public Property Get batchName() As String
    batchName = varbatchName
End Property

Public Property Let price(ByVal vprice As Double)
    varprice = vprice
End Property

Public Property Get price() As Double
    price = varprice
End Property

Public Property Let batchCode(ByVal vbatchCode As String)
    varbatchCode = vbatchCode
End Property

Public Property Get batchCode() As String
    batchCode = varbatchCode
End Property

Public Property Let itemId(ByVal vitemId As Long)
    varitemId = vitemId
End Property

Public Property Get itemId() As Long
    itemId = varitemId
End Property

Public Property Let dom(ByVal vdom As Date)
    vardom = vdom
End Property

Public Property Get dom() As Date
    dom = vardom
End Property

Public Property Let doe(ByVal vdoe As Date)
    vardoe = vdoe
End Property

Public Property Get doe() As Date
    doe = vardoe
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property


