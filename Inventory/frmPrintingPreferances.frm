VERSION 5.00
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmPrintingPreferances 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Printing Preferances"
   ClientHeight    =   3765
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9225
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3765
   ScaleWidth      =   9225
   ShowInTaskbar   =   0   'False
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   7560
      TabIndex        =   0
      Top             =   3000
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2775
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8805
      _ExtentX        =   15531
      _ExtentY        =   4895
      _Version        =   393216
      Tab             =   2
      TabHeight       =   520
      TabCaption(0)   =   "Sale Bills"
      TabPicture(0)   =   "frmPrintingPreferances.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmbSalePaper"
      Tab(0).Control(1)=   "cmbSalePrinter"
      Tab(0).Control(2)=   "Label1"
      Tab(0).Control(3)=   "Label2"
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Purchase Bills"
      TabPicture(1)   =   "frmPrintingPreferances.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmbPurchasePrinter"
      Tab(1).Control(1)=   "cmbPurchasePaper"
      Tab(1).Control(2)=   "Label3"
      Tab(1).Control(3)=   "Label4"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Reports"
      TabPicture(2)   =   "frmPrintingPreferances.frx":0038
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Label10"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Label9"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "cmbReportPaper"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "cmbReportPrinter"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).ControlCount=   4
      Begin VB.ComboBox cmbSalePaper 
         Height          =   360
         Left            =   -73800
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   960
         Width           =   3255
      End
      Begin VB.ComboBox cmbSalePrinter 
         Height          =   360
         Left            =   -73800
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   480
         Width           =   3255
      End
      Begin VB.ComboBox cmbPurchasePrinter 
         Height          =   360
         Left            =   -73800
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   480
         Width           =   3255
      End
      Begin VB.ComboBox cmbPurchasePaper 
         Height          =   360
         Left            =   -73800
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   960
         Width           =   3255
      End
      Begin VB.ComboBox cmbReportPrinter 
         Height          =   360
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   480
         Width           =   3255
      End
      Begin VB.ComboBox cmbReportPaper 
         Height          =   360
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   960
         Width           =   3255
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   13
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Paper"
         Height          =   375
         Left            =   -74760
         TabIndex        =   12
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Paper"
         Height          =   375
         Left            =   -74760
         TabIndex        =   11
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   10
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Paper"
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Printer"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   480
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmPrintingPreferances"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim NumForms As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private CsetPrinter As New cSetDfltPrinter
    Dim temSQL As String
    Dim rsIxList As New ADODB.Recordset
    Dim i As Integer


Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub cmbSalePrinter_Change()
    cmbSalePrinter_Click
End Sub

Private Sub cmbReportPrinter_Change()
    cmbReportPrinter_Click
End Sub

Private Sub cmbPurchasePrinter_Change()
    cmbPurchasePrinter_Click
End Sub


Private Sub cmbSalePrinter_Click()
    cmbSalePaper.Clear
    CsetPrinter.SetPrinterAsDefault (cmbSalePrinter.text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
'        With FormSize
'            .cx = PrescreptionPaperHeight
'            .cy = PrescreptionPaperWidth
'        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbSalePaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub

Private Sub cmbReportPrinter_Click()
    cmbReportPaper.Clear
    CsetPrinter.SetPrinterAsDefault (cmbReportPrinter.text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
'            .cx = PrescreptionPaperHeight
'            .cy = PrescreptionPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbReportPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub

Private Sub cmbPurchasePrinter_Click()
    cmbPurchasePaper.Clear
    CsetPrinter.SetPrinterAsDefault (cmbPurchasePrinter.text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
'        With FormSize
'            .cx = PrescreptionPaperHeight
'            .cy = PrescreptionPaperWidth
'        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPurchasePaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub

Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbSalePrinter.AddItem MyPrinter.DeviceName
        cmbReportPrinter.AddItem MyPrinter.DeviceName
        cmbPurchasePrinter.AddItem MyPrinter.DeviceName
    Next
End Sub


Private Sub Form_Load()
    frmMessage.message = "Loading printer data .... " & vbNewLine & "Please wait"
    frmMessage.title = "Printer Settings"
    frmMessage.Show
    frmMessage.ZOrder 0
    Screen.MousePointer = vbHourglass
    Call FillPrinters
    Call GetSettings
    Unload frmMessage
    Screen.MousePointer = vbDefault
End Sub

Private Sub GetSettings()
    On Error Resume Next
    GetCommonSettings Me
    
    cmbSalePrinter.text = GetSetting(App.EXEName, App.EXEName, "saleBillPrinterName", "")
    cmbSalePrinter_Click
    cmbSalePaper.text = GetSetting(App.EXEName, App.EXEName, "saleBillPaperName", "")
   
    cmbReportPrinter.text = GetSetting(App.EXEName, App.EXEName, "reportPrinterName", "")
    cmbReportPrinter_Click
    cmbReportPaper.text = GetSetting(App.EXEName, App.EXEName, "reportPaperName", "")
    
    
    cmbPurchasePrinter.text = GetSetting(App.EXEName, App.EXEName, "purchaseBillPrinterName", "")
    cmbPurchasePrinter_Click
    cmbPurchasePaper.text = GetSetting(App.EXEName, App.EXEName, "purchaseBillPaperName", "")
    
End Sub

Private Sub SaveSettings()
    SaveSetting App.EXEName, App.EXEName, "saleBillPrinterName", cmbSalePrinter.text
    SaveSetting App.EXEName, App.EXEName, "saleBillPaperName", cmbSalePaper.text

    SaveSetting App.EXEName, App.EXEName, "reportPrinterName", cmbReportPrinter.text
    SaveSetting App.EXEName, App.EXEName, "reportPaperName", cmbReportPaper.text

    SaveSetting App.EXEName, App.EXEName, "purchaseBillPrinterName", cmbPurchasePrinter.text
    SaveSetting App.EXEName, App.EXEName, "purchaseBillPaperName", cmbPurchasePaper.text
    
    ProgramVariable.saleBillPrinterName = cmbSalePrinter.text
    ProgramVariable.saleBillPaperName = cmbSalePaper.text

    ProgramVariable.reportPrinterName = cmbReportPrinter.text
    ProgramVariable.reportPaperName = cmbReportPaper.text

    ProgramVariable.purchaseBillPrinterName = cmbPurchasePrinter.text
    ProgramVariable.purchaseBillPaperName = cmbPurchasePaper.text
    
    
    SaveCommonSettings Me
    

End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

