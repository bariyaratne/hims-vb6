VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ItemRelation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varparentItemId As Long
    Private varchildItemId As Long
    Private varquantity As Double
    Private varmeasurmentUnitId As Long
    Private vardeleted As Boolean
    Private vardeletedAt As Date
    Private vardeletedUserId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblItemRelation Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !parentItemId = varparentItemId
        !childItemId = varchildItemId
        !quantity = varquantity
        !measurmentUnitId = varmeasurmentUnitId
        !deleted = vardeleted
        !deletedAt = vardeletedAt
        !deletedUserId = vardeletedUserId
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblItemRelation WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!parentItemId) Then
               varparentItemId = !parentItemId
            End If
            If Not IsNull(!childItemId) Then
               varchildItemId = !childItemId
            End If
            If Not IsNull(!quantity) Then
               varquantity = !quantity
            End If
            If Not IsNull(!measurmentUnitId) Then
               varmeasurmentUnitId = !measurmentUnitId
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varparentItemId = 0
    varchildItemId = 0
    varquantity = 0
    varmeasurmentUnitId = 0
    vardeleted = False
    vardeletedAt = Empty
    vardeletedUserId = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let parentItemId(ByVal vparentItemId As Long)
    varparentItemId = vparentItemId
End Property

Public Property Get parentItemId() As Long
    parentItemId = varparentItemId
End Property

Public Property Let childItemId(ByVal vchildItemId As Long)
    varchildItemId = vchildItemId
End Property

Public Property Get childItemId() As Long
    childItemId = varchildItemId
End Property

Public Property Let quantity(ByVal vquantity As Double)
    varquantity = vquantity
End Property

Public Property Get quantity() As Double
    quantity = varquantity
End Property

Public Property Let measurmentUnitId(ByVal vmeasurmentUnitId As Long)
    varmeasurmentUnitId = vmeasurmentUnitId
End Property

Public Property Get measurmentUnitId() As Long
    measurmentUnitId = varmeasurmentUnitId
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property


