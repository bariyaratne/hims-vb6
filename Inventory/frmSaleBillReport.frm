VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmSaleBillReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sale Bill Report"
   ClientHeight    =   7470
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   9555
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7470
   ScaleWidth      =   9555
   Begin VB.CommandButton btnPrintList 
      Caption         =   "Print List"
      Height          =   495
      Left            =   1560
      TabIndex        =   8
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnPrintBill 
      Caption         =   "Print Bill"
      Height          =   495
      Left            =   240
      TabIndex        =   7
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "&Process"
      Height          =   375
      Left            =   5760
      TabIndex        =   6
      Top             =   600
      Width           =   2055
   End
   Begin MSFlexGridLib.MSFlexGrid gridList 
      Height          =   5655
      Left            =   240
      TabIndex        =   5
      Top             =   1080
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   9975
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   2520
      TabIndex        =   3
      Top             =   120
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   50331651
      CurrentDate     =   41241
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8160
      TabIndex        =   0
      Top             =   6840
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   2520
      TabIndex        =   4
      Top             =   600
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   50331651
      CurrentDate     =   41241
   End
   Begin VB.Label Label2 
      Caption         =   "To"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "From"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   2175
   End
End
Attribute VB_Name = "frmSaleBillReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    
Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillCombos()

End Sub


Private Sub btnPrintBill_Click()
    closeAdoDatabase
    printSaleBill Val(gridList.TextMatrix(gridList.row, 0)), Me.hwnd
    openAdoDatabase
End Sub

Private Sub btnProcess_Click()
    Dim sql As String
        sql = "SELECT tblBill.id, tblInstitution.institutionName, tblBill.billAt, tblBill.billAt, tblBill.grossTotal, tblBill.discount, tblBill.netTotal, tblBill.dtype " & _
                "FROM tblBill LEFT JOIN tblInstitution ON tblBill.fromInstitutionId = tblInstitution.id " & _
                "WHERE (((tblBill.dtype)='saleBill')) OR (((tblBill.billAt) Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & " 00.00.00" & "# And #" & Format(dtpFrom.Value, "dd MMMM yyyy") & " 00.00.00" & "#))"
    
    Dim i(1) As Integer
    i(0) = 1
    FillAnyGrid sql, gridList, 0, i, i
End Sub

Private Sub Form_Load()
    SetColours Me
    GetCommonSettings Me
    Call fillCombos
    dtpFrom.Value = Date
    dtpTo.Value = Date
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub
