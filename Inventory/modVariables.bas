Attribute VB_Name = "modProgramVariables"
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence


'Classes
Public ProgramVariable As New clsVariables


'Payment Method
Public Enum paymentMethod
    Cash = 1
    Credit = 2
    Card = 3
    Cheque = 4
    BankTransfer = 5
    Other = 99
End Enum


