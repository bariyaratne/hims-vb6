VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmInstitution 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Institution"
   ClientHeight    =   7485
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   10875
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7485
   ScaleWidth      =   10875
   Begin MSDataListLib.DataList lstContact 
      Height          =   4140
      Left            =   4440
      TabIndex        =   10
      Top             =   1920
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   7303
      _Version        =   393216
   End
   Begin VB.TextBox txtContact 
      Height          =   375
      Left            =   4440
      MaxLength       =   250
      TabIndex        =   9
      Top             =   1440
      Width           =   5415
   End
   Begin MSDataListLib.DataCombo cmbContact 
      Height          =   360
      Left            =   4440
      TabIndex        =   8
      Top             =   1080
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.CommandButton btnRemove 
      Caption         =   "Delete"
      Height          =   375
      Left            =   9960
      TabIndex        =   14
      Top             =   1920
      Width           =   855
   End
   Begin VB.CommandButton btnContactAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   9960
      TabIndex        =   13
      Top             =   1440
      Width           =   855
   End
   Begin MSDataListLib.DataList lstSearch 
      Height          =   5820
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   10266
      _Version        =   393216
   End
   Begin VB.TextBox txtSearch 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3855
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   1440
      TabIndex        =   4
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   9600
      TabIndex        =   12
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   8640
      TabIndex        =   11
      Top             =   6120
      Width           =   1215
   End
   Begin VB.TextBox txtName 
      Height          =   375
      Left            =   5520
      MaxLength       =   250
      TabIndex        =   7
      Top             =   480
      Width           =   4335
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "&Delete"
      Height          =   495
      Left            =   2760
      TabIndex        =   5
      Top             =   6720
      Width           =   1215
   End
   Begin VB.Label lblEditName 
      Caption         =   "&Name"
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label lblName 
      Caption         =   "&Search"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmInstitution"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    
    Dim mySec As New clsSecurity
    Dim myUser As New clsUser
    
    Dim current As New Institution
    

Private Sub btnAdd_Click()
    Dim temStr As String
    txtSearch.text = Empty
    lstSearch.BoundText = 0
    txtName.text = temStr
    Set current = New Institution
    displayDetails
    txtName.SetFocus
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnContactAdd_Click()
    If Trim(txtContact.text) = Empty Then
        MsgBox "Please enter a contact"
        txtContact.SetFocus
        Exit Sub
    End If
    If IsNumeric(cmbContact.BoundText) = False Then
        MsgBox "Please select a contact category"
        cmbContact.SetFocus
        Exit Sub
    End If
    Dim con As New Contact
    con.contactName = txtContact.text
    con.categoryId = Val(cmbContact.BoundText)
    
    If current.id = 0 Then btnSave_Click
    con.institutionId = current.id
    con.saveData
    fillContacts
    txtContact.text = Empty
    txtContact.SetFocus
    
End Sub

Private Sub fillContacts()
    Dim sql As String
    If current.id = 0 Then
        Set lstContact.DataSource = Nothing
    End If
    sql = "SELECT tblContact.id, tblCategory.categoryName & ' - ' & tblContact.contactName as contactDetail FROM tblContact LEFT JOIN tblCategory ON tblContact.categoryId = tblCategory.id WHERE tblContact.institutionId = " & current.id & " and tblContact.deleted = false "
    Dim cons As New clsFillCombo
    cons.fillSqlList lstContact, sql
End Sub

Private Sub btnDelete_Click()
    Dim a As Integer
    a = MsgBox("Are you sure you want to delete " & lstSearch.text & "?", vbYesNo)
    If a = vbYes Then
        current.deleted = True
        current.deletedAt = Now
        current.deletedUserId = ProgramVariable.loggedUser.UserID
        current.saveData
        MsgBox "Deleted"
        txtSearch.text = Empty
        fillNameCombo
    End If
End Sub

Private Sub fillNameCombo()
    Dim sql As String
    Dim allItems As New clsFillCombo
    If txtSearch.text = "" Then
        sql = "Select id, InstitutionName from tblInstitution where  deleted = false order by InstitutionName"
    Else
        sql = "Select id, InstitutionName from tblInstitution where  deleted = false and InstitutionName like '" & ProgramVariable.sqlLike & txtSearch.text & ProgramVariable.sqlLike & "' order by InstitutionName"
    End If
    allItems.fillSqlList lstSearch, sql
End Sub

Private Sub btnRemove_Click()
    If IsNumeric(lstContact.BoundText) = True Then
        Dim temC As New Contact
        temC.id = Val(lstContact.BoundText)
        temC.deleted = True
        temC.deletedAt = Now
        temC.deletedUserId = ProgramVariable.loggedUser.UserID
        temC.saveData
    End If
    fillContacts
    txtContact.text = Empty
    txtContact.SetFocus
End Sub

Private Sub btnSave_Click()
    Dim i As Long
    If Trim(txtName.text) = Empty Then
        MsgBox "Please enter a name"
        txtName.SetFocus
    End If
    With current
        .institutionName = txtName.text
        .saveData
        i = .id
        txtSearch.text = ""
        fillNameCombo
    End With
End Sub

Private Sub lstSearch_Click()
    Call displayDetails
End Sub

Private Sub txtSearch_Change()
    fillNameCombo
    Call displayDetails
End Sub

Private Sub Form_Load()
    SetColours Me
    GetCommonSettings Me
    Call fillCombos
    Call fillNameCombo
    Call displayDetails
End Sub
Private Sub fillCombos()
    Dim cc As New clsFillCombo
    cc.fillSqlCombo cmbContact, "select id, categoryName from tblCategory where dtype = 'contactCategory' and deleted = false order by categoryName"
End Sub

Private Sub displayDetails()
    With current
        .id = Val(lstSearch.BoundText)
        txtName.text = .institutionName
    End With
    Call fillContacts
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub
