VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StockHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varitemId As Long
    Private varinstitutionId As Long
    Private varbatchId As Long
    Private varbillId As Long
    Private varbillReturnId As Long
    Private varbeforeStock As Double
    Private varafterStock As Long
    Private vardeleted As Boolean
    Private vardeletedAt As Date
    Private vardeletedUserId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStockHistory Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !itemId = varitemId
        !institutionId = varinstitutionId
        !batchId = varbatchId
        !billId = varbillId
        !billReturnId = varbillReturnId
        !beforeStock = varbeforeStock
        !afterStock = varafterStock
        !deleted = vardeleted
        !deletedAt = vardeletedAt
        !deletedUserId = vardeletedUserId
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStockHistory WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!itemId) Then
               varitemId = !itemId
            End If
            If Not IsNull(!institutionId) Then
               varinstitutionId = !institutionId
            End If
            If Not IsNull(!batchId) Then
               varbatchId = !batchId
            End If
            If Not IsNull(!billId) Then
               varbillId = !billId
            End If
            If Not IsNull(!billReturnId) Then
               varbillReturnId = !billReturnId
            End If
            If Not IsNull(!beforeStock) Then
               varbeforeStock = !beforeStock
            End If
            If Not IsNull(!afterStock) Then
               varafterStock = !afterStock
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varitemId = 0
    varinstitutionId = 0
    varbatchId = 0
    varbillId = 0
    varbillReturnId = 0
    varbeforeStock = 0
    varafterStock = 0
    vardeleted = False
    vardeletedAt = Empty
    vardeletedUserId = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let itemId(ByVal vitemId As Long)
    varitemId = vitemId
End Property

Public Property Get itemId() As Long
    itemId = varitemId
End Property

Public Property Let institutionId(ByVal vinstitutionId As Long)
    varinstitutionId = vinstitutionId
End Property

Public Property Get institutionId() As Long
    institutionId = varinstitutionId
End Property

Public Property Let batchId(ByVal vbatchId As Long)
    varbatchId = vbatchId
End Property

Public Property Get batchId() As Long
    batchId = varbatchId
End Property

Public Property Let billId(ByVal vbillId As Long)
    varbillId = vbillId
End Property

Public Property Get billId() As Long
    billId = varbillId
End Property

Public Property Let billReturnId(ByVal vbillReturnId As Long)
    varbillReturnId = vbillReturnId
End Property

Public Property Get billReturnId() As Long
    billReturnId = varbillReturnId
End Property

Public Property Let beforeStock(ByVal vbeforeStock As Double)
    varbeforeStock = vbeforeStock
End Property

Public Property Get beforeStock() As Double
    beforeStock = varbeforeStock
End Property

Public Property Let afterStock(ByVal vafterStock As Long)
    varafterStock = vafterStock
End Property

Public Property Get afterStock() As Long
    afterStock = varafterStock
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property


