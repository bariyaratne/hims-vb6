VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Contact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varcategoryId As Long
    Private varcontactName As String
    Private varinstitutionId As Long
    Private varpersonId As Long
    Private vardeleted As Boolean
    Private vardeletedAt As Date
    Private vardeletedUserId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblContact Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !categoryId = varcategoryId
        !contactName = varcontactName
        !institutionId = varinstitutionId
        !personId = varpersonId
        !deleted = vardeleted
        !deletedAt = vardeletedAt
        !deletedUserId = vardeletedUserId
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblContact WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!categoryId) Then
               varcategoryId = !categoryId
            End If
            If Not IsNull(!contactName) Then
               varcontactName = !contactName
            End If
            If Not IsNull(!institutionId) Then
               varinstitutionId = !institutionId
            End If
            If Not IsNull(!personId) Then
               varpersonId = !personId
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varcategoryId = 0
    varcontactName = Empty
    varinstitutionId = 0
    varpersonId = 0
    vardeleted = False
    vardeletedAt = Empty
    vardeletedUserId = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let categoryId(ByVal vcategoryId As Long)
    varcategoryId = vcategoryId
End Property

Public Property Get categoryId() As Long
    categoryId = varcategoryId
End Property

Public Property Let contactName(ByVal vcontactName As String)
    varcontactName = vcontactName
End Property

Public Property Get contactName() As String
    contactName = varcontactName
End Property

Public Property Let institutionId(ByVal vinstitutionId As Long)
    varinstitutionId = vinstitutionId
End Property

Public Property Get institutionId() As Long
    institutionId = varinstitutionId
End Property

Public Property Let personId(ByVal vpersonId As Long)
    varpersonId = vpersonId
End Property

Public Property Get personId() As Long
    personId = varpersonId
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property


