VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BillItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Rate"
Attribute VB_Ext_KEY = "Member1" ,"batch"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varbillId As Long
    Private varitemId As Long
    Private varbillSerial As Long
    Private varbatch As String
    Private vardoe As Date
    Private varrate As Double
    Private vardom As Date
    Private varquantity As Double
    Private varfreeQuantity As Double
    Private vartotalQuantity As Double
    Private vargrossRate As Double
    Private varitemDiscountRate As Double
    Private varbillDiscountRate As Double
    Private varnetRate As Double
    Private vargrossValue As Long
    Private varfreeValue As Double
    Private varitemDiscountValue As Double
    Private varbillDiscountValue As Double
    Private vartotalValue As Double
    Private varbatchId As Long
    Private varretailRate As Double
    Private varwholesale As Double

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBillItem Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !billId = varbillId
        !itemId = varitemId
        !billSerial = varbillSerial
        !batch = varbatch
        If vardoe = 0 Then
            !doe = Null
        Else
            !doe = vardoe
        End If
        !Rate = varrate
        !dom = vardom
        !quantity = varquantity
        !freeQuantity = varfreeQuantity
        !totalQuantity = vartotalQuantity
        !grossRate = vargrossRate
        !itemDiscountRate = varitemDiscountRate
        !billDiscountRate = varbillDiscountRate
        !netRate = varnetRate
        !grossValue = vargrossValue
        !freeValue = varfreeValue
        !itemDiscountValue = varitemDiscountValue
        !billDiscountValue = varbillDiscountValue
        !totalValue = vartotalValue
        !batchId = varbatchId
        !retailRate = varretailRate
        !wholesale = varwholesale
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBillItem WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!billId) Then
               varbillId = !billId
            End If
            If Not IsNull(!itemId) Then
               varitemId = !itemId
            End If
            If Not IsNull(!billSerial) Then
               varbillSerial = !billSerial
            End If
            If Not IsNull(!batch) Then
               varbatch = !batch
            End If
            If Not IsNull(!doe) Then
               vardoe = !doe
            End If
            If Not IsNull(!Rate) Then
               varrate = !Rate
            End If
            If Not IsNull(!dom) Then
               vardom = !dom
            End If
            If Not IsNull(!quantity) Then
               varquantity = !quantity
            End If
            If Not IsNull(!freeQuantity) Then
               varfreeQuantity = !freeQuantity
            End If
            If Not IsNull(!totalQuantity) Then
               vartotalQuantity = !totalQuantity
            End If
            If Not IsNull(!grossRate) Then
               vargrossRate = !grossRate
            End If
            If Not IsNull(!itemDiscountRate) Then
               varitemDiscountRate = !itemDiscountRate
            End If
            If Not IsNull(!billDiscountRate) Then
               varbillDiscountRate = !billDiscountRate
            End If
            If Not IsNull(!netRate) Then
               varnetRate = !netRate
            End If
            If Not IsNull(!grossValue) Then
               vargrossValue = !grossValue
            End If
            If Not IsNull(!freeValue) Then
               varfreeValue = !freeValue
            End If
            If Not IsNull(!itemDiscountValue) Then
               varitemDiscountValue = !itemDiscountValue
            End If
            If Not IsNull(!billDiscountValue) Then
               varbillDiscountValue = !billDiscountValue
            End If
            If Not IsNull(!totalValue) Then
               vartotalValue = !totalValue
            End If
            If Not IsNull(!batchId) Then
               varbatchId = !batchId
            End If
            If Not IsNull(!retailRate) Then
               varretailRate = !retailRate
            End If
            If Not IsNull(!wholesale) Then
               varwholesale = !wholesale
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varbillId = 0
    varitemId = 0
    varbillSerial = 0
    varbatch = Empty
    vardoe = Empty
    varrate = 0
    vardom = Empty
    varquantity = 0
    varfreeQuantity = 0
    vartotalQuantity = 0
    vargrossRate = 0
    varitemDiscountRate = 0
    varbillDiscountRate = 0
    varnetRate = 0
    vargrossValue = 0
    varfreeValue = 0
    varitemDiscountValue = 0
    varbillDiscountValue = 0
    vartotalValue = 0
    varbatchId = 0
    varretailRate = 0
    varwholesale = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let billId(ByVal vbillId As Long)
    varbillId = vbillId
End Property

Public Property Get billId() As Long
    billId = varbillId
End Property

Public Property Let itemId(ByVal vitemId As Long)
    varitemId = vitemId
End Property

Public Property Get itemId() As Long
    itemId = varitemId
End Property

Public Property Let billSerial(ByVal vbillSerial As Long)
    varbillSerial = vbillSerial
End Property

Public Property Get billSerial() As Long
    billSerial = varbillSerial
End Property

Public Property Let batch(ByVal vbatch As String)
    varbatch = vbatch
End Property

Public Property Get batch() As String
    batch = varbatch
End Property

Public Property Let doe(ByVal vdoe As Date)
    vardoe = vdoe
End Property

Public Property Get doe() As Date
    doe = vardoe
End Property

Public Property Let Rate(ByVal vrate As Double)
    varrate = vrate
End Property

Public Property Get Rate() As Double
    Rate = varrate
End Property

Public Property Let dom(ByVal vdom As Date)
    vardom = vdom
End Property

Public Property Get dom() As Date
    dom = vardom
End Property

Public Property Let quantity(ByVal vquantity As Double)
    varquantity = vquantity
End Property

Public Property Get quantity() As Double
    quantity = varquantity
End Property

Public Property Let freeQuantity(ByVal vfreeQuantity As Double)
    varfreeQuantity = vfreeQuantity
End Property

Public Property Get freeQuantity() As Double
    freeQuantity = varfreeQuantity
End Property

Public Property Let totalQuantity(ByVal vtotalQuantity As Double)
    vartotalQuantity = vtotalQuantity
End Property

Public Property Get totalQuantity() As Double
    totalQuantity = vartotalQuantity
End Property

Public Property Let grossRate(ByVal vgrossRate As Double)
    vargrossRate = vgrossRate
End Property

Public Property Get grossRate() As Double
    grossRate = vargrossRate
End Property

Public Property Let itemDiscountRate(ByVal vitemDiscountRate As Double)
    varitemDiscountRate = vitemDiscountRate
End Property

Public Property Get itemDiscountRate() As Double
    itemDiscountRate = varitemDiscountRate
End Property

Public Property Let billDiscountRate(ByVal vbillDiscountRate As Double)
    varbillDiscountRate = vbillDiscountRate
End Property

Public Property Get billDiscountRate() As Double
    billDiscountRate = varbillDiscountRate
End Property

Public Property Let netRate(ByVal vnetRate As Double)
    varnetRate = vnetRate
End Property

Public Property Get netRate() As Double
    netRate = varnetRate
End Property

Public Property Let grossValue(ByVal vgrossValue As Long)
    vargrossValue = vgrossValue
End Property

Public Property Get grossValue() As Long
    grossValue = vargrossValue
End Property

Public Property Let freeValue(ByVal vfreeValue As Double)
    varfreeValue = vfreeValue
End Property

Public Property Get freeValue() As Double
    freeValue = varfreeValue
End Property

Public Property Let itemDiscountValue(ByVal vitemDiscountValue As Double)
    varitemDiscountValue = vitemDiscountValue
End Property

Public Property Get itemDiscountValue() As Double
    itemDiscountValue = varitemDiscountValue
End Property

Public Property Let billDiscountValue(ByVal vbillDiscountValue As Double)
    varbillDiscountValue = vbillDiscountValue
End Property

Public Property Get billDiscountValue() As Double
    billDiscountValue = varbillDiscountValue
End Property

Public Property Let totalValue(ByVal vtotalValue As Double)
    vartotalValue = vtotalValue
End Property

Public Property Get totalValue() As Double
    totalValue = vartotalValue
End Property

Public Property Let batchId(ByVal vbatchId As Long)
    varbatchId = vbatchId
End Property

Public Property Get batchId() As Long
    batchId = varbatchId
End Property

Public Property Let retailRate(ByVal vretailRate As Double)
    varretailRate = vretailRate
End Property

Public Property Get retailRate() As Double
    retailRate = varretailRate
End Property

Public Property Let wholesale(ByVal vwholesale As Double)
    varwholesale = vwholesale
End Property

Public Property Get wholesale() As Double
    wholesale = varwholesale
End Property


