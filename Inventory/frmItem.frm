VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmItem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item"
   ClientHeight    =   7485
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   10710
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7485
   ScaleWidth      =   10710
   Begin VB.Frame Frame4 
      Caption         =   "Warrenty"
      Height          =   2175
      Left            =   4440
      TabIndex        =   23
      Top             =   4560
      Width           =   5535
      Begin VB.TextBox txtWLess 
         Height          =   375
         Left            =   1920
         MaxLength       =   255
         TabIndex        =   26
         Top             =   840
         Width           =   975
      End
      Begin VB.OptionButton optWs 
         Caption         =   "Warrenty by Sale"
         Height          =   480
         Left            =   240
         TabIndex        =   28
         Top             =   1320
         Width           =   1575
      End
      Begin VB.OptionButton optWp 
         Caption         =   "Warrenty by Purchase"
         Height          =   480
         Left            =   240
         TabIndex        =   25
         Top             =   720
         Width           =   1575
      End
      Begin VB.OptionButton optNoW 
         Caption         =   "No Warrenty"
         Height          =   240
         Left            =   240
         TabIndex        =   24
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox txtW 
         Height          =   375
         Left            =   1920
         MaxLength       =   255
         TabIndex        =   29
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Days from Sale"
         Height          =   375
         Left            =   3000
         TabIndex        =   30
         Top             =   1320
         Width           =   2295
      End
      Begin VB.Label Label4 
         Caption         =   "Days Less than from given"
         Height          =   375
         Left            =   3000
         TabIndex        =   27
         Top             =   840
         Width           =   2295
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Price by"
      Height          =   975
      Left            =   4440
      TabIndex        =   17
      Top             =   2400
      Width           =   3015
      Begin VB.OptionButton optBatch 
         Caption         =   "Batch"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   855
      End
      Begin VB.OptionButton optItem 
         Caption         =   "Item"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   600
         Width           =   975
      End
   End
   Begin VB.TextBox txtCode 
      Height          =   375
      Left            =   5520
      MaxLength       =   255
      TabIndex        =   9
      Top             =   960
      Width           =   4455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Price change during sale"
      Height          =   975
      Left            =   7560
      TabIndex        =   20
      Top             =   2400
      Width           =   2415
      Begin VB.OptionButton optNotAllowed 
         Caption         =   "Not Allowed"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   1575
      End
      Begin VB.OptionButton optAllowed 
         Caption         =   "Allowed"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type"
      Height          =   975
      Left            =   4440
      TabIndex        =   14
      Top             =   3480
      Width           =   5535
      Begin VB.TextBox txtServiceCharge 
         Alignment       =   1  'Right Justify
         Height          =   375
         Left            =   3720
         MaxLength       =   255
         TabIndex        =   33
         Top             =   480
         Width           =   1575
      End
      Begin VB.OptionButton optProduct 
         Caption         =   "&Product"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optService 
         Caption         =   "Ser&vice"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Service Charge"
         Height          =   255
         Left            =   1920
         TabIndex        =   34
         Top             =   600
         Width           =   2295
      End
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   5520
      TabIndex        =   11
      Top             =   1440
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataList lstSearch 
      Height          =   4140
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   7303
      _Version        =   393216
   End
   Begin VB.TextBox txtSearch 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3855
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print All"
      Height          =   495
      Left            =   1440
      TabIndex        =   4
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   9240
      TabIndex        =   32
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   7320
      TabIndex        =   31
      Top             =   6840
      Width           =   1215
   End
   Begin VB.TextBox txtName 
      Height          =   375
      Left            =   5520
      MaxLength       =   255
      TabIndex        =   7
      Top             =   480
      Width           =   4455
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "&Delete"
      Height          =   495
      Left            =   2760
      TabIndex        =   5
      Top             =   5160
      Width           =   1215
   End
   Begin MSDataListLib.DataCombo cmbUnit 
      Height          =   360
      Left            =   5520
      TabIndex        =   13
      Top             =   1920
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label3 
      Caption         =   "Unit"
      Height          =   375
      Left            =   4440
      TabIndex        =   12
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Cod&e"
      Height          =   375
      Left            =   4440
      TabIndex        =   8
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "&Category"
      Height          =   375
      Left            =   4440
      TabIndex        =   10
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label lblEditName 
      Caption         =   "&Name"
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label lblName 
      Caption         =   "&Search"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    
    Dim mySec As New clsSecurity
    Dim myUser As New clsUser
    
    Dim current As New Item
    

Private Sub btnAdd_Click()
    Dim temStr As String
    txtSearch.text = Empty
    lstSearch.BoundText = 0
    txtName.text = temStr
    Set current = New Item
    displayDetails
    On Error Resume Next
    txtName.SetFocus
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnDelete_Click()
    Dim a As Integer
    a = MsgBox("Are you sure you want to delete " & lstSearch.text & "?", vbYesNo)
    If a = vbYes Then
        current.deleted = True
        current.deletedAt = Now
        current.deletedUserId = ProgramVariable.loggedUser.UserID
        current.saveData
        MsgBox "Deleted"
        txtSearch.text = Empty
        fillNameCombo
    End If
End Sub

Private Sub fillNameCombo()
    Dim sql As String
    Dim allItems As New clsFillCombo
    If txtSearch.text = "" Then
        sql = "Select id, itemName from tblItem where dtype = 'amp' and  deleted = false order by itemName"
    Else
        sql = "Select id, itemName from tblItem where dtype = 'amp' and  deleted = false and itemName like '" & ProgramVariable.sqlLike & txtSearch.text & ProgramVariable.sqlLike & "' order by itemName"
    End If
    allItems.fillSqlList lstSearch, sql
    
End Sub

Private Sub fillCombo()
    Dim sql As String
    Dim cats As New clsFillCombo
    Dim mu As New clsFillCombo
    sql = "Select id, categoryName from tblCategory where dtype = 'itemCategory' and deleted = false order by categoryName"
    cats.fillSqlCombo cmbCat, sql
    sql = "Select id, categoryName from tblCategory where dtype = 'measurmentUnit' and deleted = false order by categoryName"
    mu.fillSqlCombo cmbUnit, sql
End Sub

Private Sub btnPrint_Click()
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "select itemName as displayName from tblItem where dtype = 'amp' and deleted = false order by itemName"
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockReadOnly
    End With
    With drSingleCol
        Set .DataSource = rsTem
        .Sections("Section4").Controls("lblTopic").Caption = ProgramVariable.institution.institutionName
        .Sections("Section4").Controls("lblSUbtopic").Caption = "Item List"
        .Sections("Section2").Controls("lblCaption").Caption = "Item"
        .Show
    End With

End Sub

Private Sub btnSave_Click()
    Dim i As Long
    If Trim(txtName.text) = "" Then
        MsgBox "Please enter a name"
        txtName.SetFocus
        Exit Sub
    End If
    With current
        .dtype = "amp"
        .itemName = txtName.text
        .canChangePrice = optAllowed.Value
        .categoryId = Val(cmbCat.BoundText)
        .isAService = optService.Value
        .itemCode = txtCode.text
        .priceByBatch = optBatch.Value
        .priceByItem = Not optBatch.Value
        .measurmentUnitId = Val(cmbUnit.BoundText)
        
        .NoWarrenty = optNoW.Value
        .warrentyByPurchase = optWp.Value
        .warrentyDaysLessFromPurchase = Val(txtWLess.text)
        .warrentyBySale = optWs.Value
        .warrentyDaysFromSale = Val(txtW.text)
        .saveData
        i = .id
        
        If optItem.Value = True Then
            setServiceCharge .id, Val(txtServiceCharge.text)
        End If
        
        txtSearch.text = ""
        
        fillNameCombo
    End With
End Sub



Private Sub lstSearch_Click()
    Call displayDetails
End Sub

Private Sub optService_Click()
    If optService.Value = True Then
        txtServiceCharge.Enabled = True
    Else
        txtServiceCharge.Enabled = False
        txtServiceCharge.text = 0
    End If
End Sub

Private Sub txtSearch_Change()
    fillNameCombo
    Call displayDetails
End Sub

Private Sub Form_Load()
    SetColours Me
    'GetCommonSettings Me
    Call fillNameCombo
    Call fillCombo
    btnAdd_Click
End Sub

Private Sub displayDetails()
    With current
        .id = Val(lstSearch.BoundText)
        txtName.text = .itemName
        txtCode.text = .itemCode
        cmbCat.BoundText = .categoryId
        cmbUnit.BoundText = .measurmentUnitId
        optNotAllowed.Value = Not .canChangePrice
        optAllowed.Value = .canChangePrice
        optBatch.Value = .priceByBatch
        optItem.Value = .priceByItem
        optProduct.Value = Not .isAService
        optService.Value = .isAService
        txtW.text = .warrentyDaysFromSale
        txtWLess.text = .warrentyDaysLessFromPurchase
        optWp.Value = .warrentyByPurchase
        optWs.Value = .warrentyBySale
        optNoW.Value = .NoWarrenty
        If .isAService = True Then
            txtServiceCharge.text = Format(findItemPrice(.id), "0.00")
        Else
            txtServiceCharge.text = "0.00"
        End If
    End With
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub
