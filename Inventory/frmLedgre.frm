VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmLedgre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Ledgure"
   ClientHeight    =   7470
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   9555
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7470
   ScaleWidth      =   9555
   Begin VB.CommandButton btnPrintList 
      Caption         =   "Print List"
      Height          =   495
      Left            =   240
      TabIndex        =   7
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "&Process"
      Height          =   375
      Left            =   7320
      TabIndex        =   6
      Top             =   1080
      Width           =   2055
   End
   Begin MSFlexGridLib.MSFlexGrid gridList 
      Height          =   5175
      Left            =   240
      TabIndex        =   5
      Top             =   1560
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   9128
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   2520
      TabIndex        =   3
      Top             =   120
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   359923715
      CurrentDate     =   41241
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8160
      TabIndex        =   0
      Top             =   6840
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   2520
      TabIndex        =   4
      Top             =   600
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   359923715
      CurrentDate     =   41241
   End
   Begin MSDataListLib.DataCombo cmbItem 
      Height          =   360
      Left            =   2520
      TabIndex        =   8
      Top             =   1080
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label3 
      Caption         =   "Item"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1080
      Width           =   2175
   End
   Begin VB.Label Label2 
      Caption         =   "To"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "From"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   2175
   End
End
Attribute VB_Name = "frmLedgre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    
Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillCombos()
    Dim sql As String
    Dim allItems As New clsFillCombo
    sql = "Select id, itemName from tblItem where dtype = 'amp' and deleted = false order by itemName"
    allItems.fillSqlCombo cmbItem, sql
End Sub


Private Sub btnPrintList_Click()
    Unload drItemLedgre
    DoEvents
    DataEnvironment1.cmbItemLedgre Val(cmbItem.BoundText)
    DoEvents
    drItemLedgre.Sections(1).Controls("lblItemName").Caption = cmbItem.text
    Dim a As Integer
    For a = 0 To 1000
        DoEvents
    Next
    drItemLedgre.Refresh
    drItemLedgre.Show
    If DataEnvironment1.rscmbItemLedgre.State = 1 Then DataEnvironment1.rscmbItemLedgre.Close
End Sub

Private Sub btnProcess_Click()
    Dim sql As String
    sql = "SELECT ([tblBill].[billAt] & [tblBillReturn].[returnedAt]) AS [Bill Date], tblStockHistory.billId,  tblStockHistory.beforeStock as [Stock Before] , (tblStockHistory.afterStock-tblStockHistory.beforeStock) AS [In / Out Quentiry], tblStockHistory.afterStock as [Stock After] " & _
            "FROM (tblStockHistory LEFT JOIN tblBill ON tblStockHistory.billId = tblBill.id) LEFT JOIN tblBillReturn ON tblStockHistory.billReturnId = tblBillReturn.id " & _
            "WHERE (((tblStockHistory.deleted)=False) AND ((tblStockHistory.itemId)=" & Val(cmbItem.BoundText) & "))"
    Dim i(1) As Integer
    i(0) = 1
    FillAnyGrid sql, gridList, 0, i, i
End Sub

Private Sub Form_Load()
    SetColours Me
    GetCommonSettings Me
    Call fillCombos
    dtpFrom.Value = Date
    dtpTo.Value = Date
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub
