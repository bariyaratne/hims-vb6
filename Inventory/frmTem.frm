VERSION 5.00
Begin VB.Form frmTem 
   Caption         =   "Form1"
   ClientHeight    =   7890
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   6825
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7890
   ScaleWidth      =   6825
   Begin VB.CommandButton Command2 
      Caption         =   "Command1"
      Height          =   495
      Left            =   2640
      TabIndex        =   2
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   2400
      TabIndex        =   1
      Top             =   2520
      Width           =   1215
   End
   Begin VB.ListBox List1 
      Height          =   2595
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   2055
   End
End
Attribute VB_Name = "frmTem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Command1_Click()
    Dim myP As Printer
    For Each myP In Printers
        If List1.text = myP.DeviceName Then
            Set Printer = myP
        End If
    Next
    drPosPrint.PrintReport
    
End Sub

Private Sub Command2_Click()
    drSingleCol.PrintReport
End Sub

Private Sub Form_Load()
    Dim myP As Printer
    For Each myP In Printers
        List1.AddItem myP.DeviceName
    Next
End Sub
