VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim temSQL As String
    Private varUserID As Long
    Private varUserName As String
    Private varUserPassword As String
    Private varRoleID As Long
    Private vardeleted As Boolean
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private vardeletedUserId As Date
    Private varAddedDate As Date
    Private varAddedTime As Date
    Private varAddedUserID As Long
    Private varpersonId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblUser Where UserID = " & varUserID
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !UserName = varUserName
        !UserPassword = varUserPassword
        !RoleID = varRoleID
        !deleted = vardeleted
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !deletedUserId = vardeletedUserId
        !AddedDate = varAddedDate
        !AddedTime = varAddedTime
        !AddedUserID = varAddedUserID
        !personId = varpersonId
        .Update
        varUserID = !UserID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblUser WHERE UserID = " & varUserID
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!UserID) Then
               varUserID = !UserID
            End If
            If Not IsNull(!UserName) Then
               varUserName = !UserName
            End If
            If Not IsNull(!UserPassword) Then
               varUserPassword = !UserPassword
            End If
            If Not IsNull(!RoleID) Then
               varRoleID = !RoleID
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
            If Not IsNull(!AddedDate) Then
               varAddedDate = !AddedDate
            End If
            If Not IsNull(!AddedTime) Then
               varAddedTime = !AddedTime
            End If
            If Not IsNull(!AddedUserID) Then
               varAddedUserID = !AddedUserID
            End If
            If Not IsNull(!personId) Then
               varpersonId = !personId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varUserID = 0
    varUserName = Empty
    varUserPassword = Empty
    varRoleID = 0
    vardeleted = False
    varDeletedDate = Empty
    varDeletedTime = Empty
    vardeletedUserId = Empty
    varAddedDate = Empty
    varAddedTime = Empty
    varAddedUserID = 0
    varpersonId = 0
End Sub

Public Property Let UserID(ByVal vUserID As Long)
    Call clearData
    varUserID = vUserID
    Call loadData
End Property

Public Property Get UserID() As Long
    UserID = varUserID
End Property

Public Property Let UserName(ByVal vUserName As String)
    varUserName = vUserName
End Property

Public Property Get UserName() As String
    UserName = varUserName
End Property

Public Property Let UserPassword(ByVal vUserPassword As String)
    varUserPassword = vUserPassword
End Property

Public Property Get UserPassword() As String
    UserPassword = varUserPassword
End Property

Public Property Let RoleID(ByVal vRoleID As Long)
    varRoleID = vRoleID
End Property

Public Property Get RoleID() As Long
    RoleID = varRoleID
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Date)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Date
    deletedUserId = vardeletedUserId
End Property

Public Property Let AddedDate(ByVal vAddedDate As Date)
    varAddedDate = vAddedDate
End Property

Public Property Get AddedDate() As Date
    AddedDate = varAddedDate
End Property

Public Property Let AddedTime(ByVal vAddedTime As Date)
    varAddedTime = vAddedTime
End Property

Public Property Get AddedTime() As Date
    AddedTime = varAddedTime
End Property

Public Property Let AddedUserID(ByVal vAddedUserID As Long)
    varAddedUserID = vAddedUserID
End Property

Public Property Get AddedUserID() As Long
    AddedUserID = varAddedUserID
End Property

Public Property Let personId(ByVal vpersonId As Long)
    varpersonId = vpersonId
End Property

Public Property Get personId() As Long
    personId = varpersonId
End Property


