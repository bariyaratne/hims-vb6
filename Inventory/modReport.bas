Attribute VB_Name = "modReport"
Option Explicit
    Private CsetPrinter As New cSetDfltPrinter
    Dim PrinterName As String
    Dim PrinterHandle As Long


Public Sub printPurchaseBill(billId As Long, formHwnd)
    Unload drPurchaseBill
    With DataEnvironment1
        If .rscmdPurchaseBill.State = 1 Then .rscmdPurchaseBill.Close
        .cmdPurchaseBill billId
    End With
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.purchaseBillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.purchaseBillPrinterName)
    If SelectForm(ProgramVariable.purchaseBillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    DoEvents
    Dim a As Integer
    For a = 1 To 1000
        DoEvents
    Next
    drPurchaseBill.Refresh
    drPurchaseBill.PrintReport
    With DataEnvironment1
        If .rscmdPurchaseBill.State = 1 Then .rscmdPurchaseBill.Close
    End With
    
End Sub

Public Sub previewPurchaseBill(billId As Long, formHwnd)
    Unload drPurchaseBill
    With DataEnvironment1
        If .rscmdPurchaseBill.State = 1 Then .rscmdPurchaseBill.Close
        .cmdPurchaseBill billId
    End With
    Dim a As Integer
    For a = 1 To 1000
        DoEvents
    Next
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.purchaseBillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.purchaseBillPrinterName)

    If SelectForm(ProgramVariable.purchaseBillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    'MsgBox "Previewing Bill"
    For a = 1 To 1000
        DoEvents
    Next
    drPurchaseBill.Refresh
    drPurchaseBill.Show
    With DataEnvironment1
        If .rscmdPurchaseBill.State = 1 Then .rscmdPurchaseBill.Close
    End With
    
End Sub

Public Sub printSaleBill(billId As Long, formHwnd)
    Unload drSaleBillA5
    With DataEnvironment1
        If .rscmdSaleBill.State = 1 Then .rscmdSaleBill.Close
        .cmdSaleBill billId
    End With
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.saleBillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.saleBillPrinterName)
    If SelectForm(ProgramVariable.saleBillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    DoEvents
    Dim a As Integer
    For a = 1 To 1000
        DoEvents
    Next
    drSaleBillA5.Refresh
    drSaleBillA5.PrintReport
    With DataEnvironment1
        If .rscmdSaleBill.State = 1 Then .rscmdSaleBill.Close
    End With
    
End Sub

Public Sub previewSaleBill(billId As Long, formHwnd)
    Unload drSaleBillA5
    With DataEnvironment1
        If .rscmdSaleBill.State = 1 Then .rscmdSaleBill.Close
        .cmdSaleBill billId
    End With
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.saleBillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (ProgramVariable.saleBillPrinterName)
    If SelectForm(ProgramVariable.saleBillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim a As Integer
    For a = 1 To 1000
        DoEvents
    Next
    drSaleBillA5.Refresh
    drSaleBillA5.Show
    With DataEnvironment1
        If .rscmdSaleBill.State = 1 Then .rscmdSaleBill.Close
    End With

End Sub
