Attribute VB_Name = "modSystem"
Option Explicit
    Dim rsTem As New ADODB.Recordset
    Dim sql As String
    
Public Function findItemStock(itemId As Long) As Double
    findItemStock = 0
    With rsTem
        If .State = 1 Then .Close
        sql = "SELECT Sum(tblStock.stock) AS SumOfstock FROM tblStock WHERE (((tblStock.itemId)=" & itemId & " ))"
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!SumOfStock) = False Then
                findItemStock = !SumOfStock
            End If
        End If
        .Close
    End With
End Function

Public Function findBatchStock(batchId As Long) As Double
    findBatchStock = 0
    With rsTem
        If .State = 1 Then .Close
        sql = "SELECT Sum(tblStock.stock) AS SumOfstock FROM tblStock WHERE (((tblStock.batchId)=" & batchId & " ))"
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!SumOfStock) = False Then
                findBatchStock = !SumOfStock
            End If
        End If
        .Close
    End With
End Function

Public Function findBatchPrice(batchId As Long) As Double
    findBatchPrice = 0
    With rsTem
        If .State = 1 Then .Close
        sql = "SELECT tblStock.price  FROM tblStock WHERE (((tblStock.batchId)=" & batchId & " )) order by id desc"
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!price) = False Then
                findBatchPrice = !price
            End If
        End If
        .Close
    End With
End Function

Public Function findItemPrice(itemId As Long) As Double
    findItemPrice = 0
    With rsTem
        If .State = 1 Then .Close
        sql = "SELECT tblStock.price  FROM tblStock WHERE (((tblStock.itemId)=" & itemId & " )) order by id desc"
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!price) = False Then
                findItemPrice = !price
            End If
        End If
        .Close
    End With
End Function

Public Sub setServiceCharge(itemId As Long, serviceCharge As Double)
    With rsTem
        If .State = 1 Then .Close
        sql = "SELECT tblStock.*  FROM tblStock WHERE (((tblStock.itemId)=" & itemId & " )) order by id desc"
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !price = serviceCharge
            .Update
        Else
            .AddNew
            !itemId = itemId
            !price = serviceCharge
            .Update
        End If
        .Close
    End With
End Sub

Public Function getBatchId(itemId As Long, batch As String, price As Double, dom As Date, doe As Date)
    Dim b As New batch
    With rsTem
        If .State = 1 Then .Close
        sql = "select id from tblBatch where itemId = " & itemId & " and deleted = false and batchName = '" & batch & "' and price = " & price
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            b.id = !id
        End If
        b.batchName = batch
        If doe <> Date Then b.doe = doe
        If dom <> Date Then b.dom = dom
        b.itemId = itemId
        b.price = price
        b.saveData
    End With
    getBatchId = b.id
End Function

Public Sub addToStock(batchId As Long, qty As Double, insId As Long)
    Dim temStock As New stock
    Dim temBatch As New batch
    temBatch.id = batchId
    With rsTem
        If .State = 1 Then .Close
        sql = "select id from tblStock where batchId = " & batchId & " and price = " & temBatch.price & " AND deleted = false and institutionId = " & insId
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temStock.id = !id
        End If
        temStock.batchId = batchId
        temStock.institutionId = insId
        temStock.itemId = temBatch.itemId
        temStock.price = temBatch.price
        temStock.stock = temStock.stock + qty
        temStock.saveData
    End With
End Sub

Public Sub addToStockHistry(batchId As Long, beforeStock As Double, afterStock As Double, insId As Long, billId As Long, billReturnId As Long, batchStock As Boolean)
    Dim temStock As New StockHistory
    Dim temBatch As New batch
    temBatch.id = batchId
    With rsTem
        If .State = 1 Then .Close
        If batchStock = True Then
            sql = "select id from tblStockHistory where batchId = " & batchId & " AND deleted = false and institutionId = " & insId & " and billId = " & billId & " and billReturnId = " & billReturnId
        Else
            sql = "select id from tblStockHistory where itemId = " & temBatch.itemId & " AND deleted = false and institutionId = " & insId & " and billId = " & billId & " and billReturnId = " & billReturnId
        End If
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temStock.id = !id
        End If
        If batchStock = True Then
            temStock.batchId = batchId
        Else
            temStock.itemId = temBatch.itemId
        End If
        temStock.institutionId = insId
        temStock.billId = billId
        temStock.billReturnId = billReturnId
        If beforeStock <> 0 Then temStock.beforeStock = beforeStock
        If afterStock <> 0 Then temStock.afterStock = afterStock
        temStock.saveData
    End With
End Sub

Public Sub removeFromStock(batchId As Long, qty As Double, insId As Long)
    addToStock batchId, 0 - qty, insId
End Sub

Public Function getItemStocks(itemId As Long) As Collection
    Dim temCol As New Collection
    Dim b As stock
    With rsTem
        If .State = 1 Then .Close
        sql = "select id from tblStock where itemId = " & itemId & " and stock >0 and deleted = false and institutionId = " & ProgramVariable.institution.id & " order by id"
        .Open sql, ProgramVariable.conn, adOpenStatic, adLockReadOnly
        While .EOF = False
            Set b = New stock
            b.id = !id
            temCol.Add b
            .MoveNext
        Wend
        .Close
    End With
    Set getItemStocks = temCol
End Function

