VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Category"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varcategoryName As String
    Private varcode As String
    Private vardtype As String
    Private vardeleted As Boolean
    Private vardeletedAt As Date
    Private vardeletedUserId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblCategory Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !categoryName = varcategoryName
        !code = varcode
        !dtype = vardtype
        !deleted = vardeleted
        !deletedAt = vardeletedAt
        !deletedUserId = vardeletedUserId
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblCategory WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!categoryName) Then
               varcategoryName = !categoryName
            End If
            If Not IsNull(!code) Then
               varcode = !code
            End If
            If Not IsNull(!dtype) Then
               vardtype = !dtype
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varcategoryName = Empty
    varcode = Empty
    vardtype = Empty
    vardeleted = False
    vardeletedAt = Empty
    vardeletedUserId = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let categoryName(ByVal vcategoryName As String)
    varcategoryName = vcategoryName
End Property

Public Property Get categoryName() As String
    categoryName = varcategoryName
End Property

Public Property Let code(ByVal vcode As String)
    varcode = vcode
End Property

Public Property Get code() As String
    code = varcode
End Property

Public Property Let dtype(ByVal vdtype As String)
    vardtype = vdtype
End Property

Public Property Get dtype() As String
    dtype = vardtype
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property


