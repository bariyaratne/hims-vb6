VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmContactCategory 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contact Category"
   ClientHeight    =   5400
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   9555
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5400
   ScaleWidth      =   9555
   Begin MSDataListLib.DataList lstSearch 
      Height          =   3180
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   5609
      _Version        =   393216
   End
   Begin VB.TextBox txtSearch 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3855
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   1440
      TabIndex        =   4
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8160
      TabIndex        =   9
      Top             =   4800
      Width           =   1215
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   7800
      TabIndex        =   8
      Top             =   1440
      Width           =   1215
   End
   Begin VB.TextBox txtName 
      Height          =   375
      Left            =   5520
      TabIndex        =   7
      Top             =   480
      Width           =   3495
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "&Delete"
      Height          =   495
      Left            =   2760
      TabIndex        =   5
      Top             =   4320
      Width           =   1215
   End
   Begin VB.Label lblEditName 
      Caption         =   "&Name"
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label lblName 
      Caption         =   "&Search"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmContactCategory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    
    Dim mySec As New clsSecurity
    Dim myUser As New clsUser
    
    Dim current As New Category
    

Private Sub btnAdd_Click()
    Dim temStr As String
    txtSearch.text = Empty
    lstSearch.BoundText = 0
    txtName.text = temStr
    Set current = New Category
    displayDetails
    txtName.SetFocus
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnDelete_Click()
    Dim a As Integer
    a = MsgBox("Are you sure you want to delete " & lstSearch.text & "?", vbYesNo)
    If a = vbYes Then
        current.deleted = True
        current.deletedAt = Now
        current.deletedUserId = ProgramVariable.loggedUser.UserID
        current.saveData
        MsgBox "Deleted"
        txtSearch.text = Empty
        fillNameCombo
    End If
End Sub

Private Sub fillNameCombo()
    Dim sql As String
    Dim allItems As New clsFillCombo
    If txtSearch.text = "" Then
        sql = "Select id, categoryName from tblCategory where dtype = 'contactCategory' and  deleted = false order by categoryName"
    Else
        sql = "Select id, categoryName from tblCategory where  dtype = 'contactCategory' and   deleted = false and categoryName like '" & ProgramVariable.sqlLike & txtSearch.text & ProgramVariable.sqlLike & "' order by categoryName"
    End If
    allItems.fillSqlList lstSearch, sql
    
End Sub

Private Sub btnSave_Click()
    Dim i As Long
    With current
        .dtype = "contactCategory"
        .categoryName = txtName.text
        .saveData
        i = .id
        txtSearch.text = ""
        fillNameCombo
        
    End With
End Sub

Private Sub lstSearch_Click()
    Call displayDetails
End Sub

Private Sub txtSearch_Change()
    fillNameCombo
    Call displayDetails
End Sub

Private Sub Form_Load()
    SetColours Me
    GetCommonSettings Me
    Call fillNameCombo
End Sub

Private Sub displayDetails()
    With current
        .id = Val(lstSearch.BoundText)
        txtName.text = .categoryName
    End With
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub
