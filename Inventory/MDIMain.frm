VERSION 5.00
Begin VB.MDIForm MDIMain 
   BackColor       =   &H00FFFFFF&
   Caption         =   "POS system"
   ClientHeight    =   4770
   ClientLeft      =   -150
   ClientTop       =   540
   ClientWidth     =   7515
   LinkTopic       =   "MDIForm1"
   Picture         =   "MDIMain.frx":0000
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileBackUp 
         Caption         =   "Back up"
      End
      Begin VB.Menu mnuFileRestore 
         Caption         =   "Restore"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "Edit"
      Begin VB.Menu mnuEditItemCategory 
         Caption         =   "Item Categories"
      End
      Begin VB.Menu mnuMeasurmentUnit 
         Caption         =   "Measurement Unit"
      End
      Begin VB.Menu mnuEditItem 
         Caption         =   "Item"
      End
      Begin VB.Menu mnuEditCantactCategory 
         Caption         =   "Contact Category"
      End
      Begin VB.Menu mnuEditPersons 
         Caption         =   "Persons"
      End
      Begin VB.Menu mnuEditSupplier 
         Caption         =   "Supplier"
      End
      Begin VB.Menu mnuEditUsers 
         Caption         =   "Users"
      End
      Begin VB.Menu mnuEditMyDetails 
         Caption         =   "My Details"
      End
      Begin VB.Menu mnuEditLeaveTypes 
         Caption         =   "Leave Types"
      End
   End
   Begin VB.Menu mnuTransaction 
      Caption         =   "Transactions"
      Begin VB.Menu mnuSale 
         Caption         =   "Sale"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuPurchase 
         Caption         =   "Purchase"
         Shortcut        =   {F3}
      End
   End
   Begin VB.Menu mnuBackOffice 
      Caption         =   "Back Office"
      Begin VB.Menu mnuBackOfficeItemStock 
         Caption         =   "Item Stock"
      End
      Begin VB.Menu mnuBackOfficeSaleBills 
         Caption         =   "Sale Bills"
      End
      Begin VB.Menu mnuBackOfficePurchaseBills 
         Caption         =   "Purchase Bills"
      End
      Begin VB.Menu mnuBackOfficeLedgre 
         Caption         =   "Ledgre"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "Tools"
      Begin VB.Menu mnuToolsPrintingPreferences 
         Caption         =   "Printing Preferences "
      End
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit

Private Sub mnuBackOfficeItemStock_Click()
    drItemStock.Show
    drItemStock.ZOrder 0
End Sub

Private Sub mnuBackOfficeLedgre_Click()
    frmLedgre.Show
    frmLedgre.ZOrder 0
End Sub

Private Sub mnuBackOfficePurchaseBills_Click()
    frmPurchaseBillReport.Show
    frmPurchaseBillReport.ZOrder 0
End Sub

Private Sub mnuBackOfficeSaleBills_Click()
    frmSaleBillReport.Show
    frmSaleBillReport.ZOrder 0
End Sub

Private Sub mnuEditCantactCategory_Click()
    frmContactCategory.Show
    frmContactCategory.ZOrder 0
End Sub

Private Sub mnuEditItem_Click()
    frmItem.Show
    frmItem.ZOrder 0
End Sub

Private Sub mnuEditItemCategory_Click()
    frmItemCategory.Show
    frmItemCategory.ZOrder 0
End Sub

Private Sub mnuEditMyDetails_Click()
    frmMyDetails.Show
    frmMyDetails.ZOrder 0
End Sub

Private Sub mnuEditPersons_Click()
    frmPerson.Show
    frmPerson.ZOrder 0
End Sub

Private Sub mnuEditSupplier_Click()
    frmSupplier.Show
    frmSupplier.ZOrder 0
End Sub

Private Sub mnuEditUsers_Click()
    frmUsers.Show
    frmUsers.ZOrder 0
End Sub

Private Sub mnuFileBackUp_Click()
    frmBackUp.Show
    frmBackUp.ZOrder 0
End Sub

Private Sub mnuFileExit_Click()
    End
End Sub

Private Sub mnuFileRestore_Click()
    frmRestore.Show
    frmRestore.ZOrder 0
End Sub


Private Sub mnuMeasurmentUnit_Click()
    frmMeasurmentUnit.Show
    frmMeasurmentUnit.ZOrder 0
End Sub

Private Sub mnuPurchase_Click()
    frmPurchase.Show
    frmPurchase.ZOrder 0
End Sub

Private Sub mnuSale_Click()
    frmSale.Show
    frmSale.ZOrder 0
End Sub

Private Sub mnuToolsPrintingPreferences_Click()
    frmPrintingPreferances.Show
    frmPrintingPreferances.ZOrder 0
End Sub
