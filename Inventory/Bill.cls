VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Bill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private vardtype As String
    Private varinvoiceNo As String
    Private varbillAt As Date
    Private varbillDate As Date
    Private vargrossTotal As Double
    Private vardiscount As Double
    Private varfreeTotal As Double
    Private varnetTotal As Double
    Private varcost As Double
    Private varbillUserId As Long
    Private varreferanceBillId As Long
    Private varfromInstitutionId As Long
    Private vartoInstitutionId As Long
    Private varpaymentMethodId As Long
    Private varfromPersonId As Long
    Private vartoPersonId As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBill Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !dtype = vardtype
        !invoiceNo = varinvoiceNo
        !billAt = varbillAt
        !billDate = varbillDate
        !grossTotal = vargrossTotal
        !discount = vardiscount
        !freeTotal = varfreeTotal
        !netTotal = varnetTotal
        !cost = varcost
        !billUserId = varbillUserId
        !referanceBillId = varreferanceBillId
        !fromInstitutionId = varfromInstitutionId
        !toInstitutionId = vartoInstitutionId
        !paymentMethodId = varpaymentMethodId
        !fromPersonId = varfromPersonId
        !toPersonId = vartoPersonId
        .Update
        varid = !id
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBill WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, ProgramVariable.conn, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!dtype) Then
               vardtype = !dtype
            End If
            If Not IsNull(!invoiceNo) Then
               varinvoiceNo = !invoiceNo
            End If
            If Not IsNull(!billAt) Then
               varbillAt = !billAt
            End If
            If Not IsNull(!billDate) Then
               varbillDate = !billDate
            End If
            If Not IsNull(!grossTotal) Then
               vargrossTotal = !grossTotal
            End If
            If Not IsNull(!discount) Then
               vardiscount = !discount
            End If
            If Not IsNull(!freeTotal) Then
               varfreeTotal = !freeTotal
            End If
            If Not IsNull(!netTotal) Then
               varnetTotal = !netTotal
            End If
            If Not IsNull(!cost) Then
               varcost = !cost
            End If
            If Not IsNull(!billUserId) Then
               varbillUserId = !billUserId
            End If
            If Not IsNull(!referanceBillId) Then
               varreferanceBillId = !referanceBillId
            End If
            If Not IsNull(!fromInstitutionId) Then
               varfromInstitutionId = !fromInstitutionId
            End If
            If Not IsNull(!toInstitutionId) Then
               vartoInstitutionId = !toInstitutionId
            End If
            If Not IsNull(!paymentMethodId) Then
               varpaymentMethodId = !paymentMethodId
            End If
            If Not IsNull(!fromPersonId) Then
               varfromPersonId = !fromPersonId
            End If
            If Not IsNull(!toPersonId) Then
               vartoPersonId = !toPersonId
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    vardtype = Empty
    varinvoiceNo = Empty
    varbillAt = Empty
    varbillDate = Empty
    vargrossTotal = 0
    vardiscount = 0
    varfreeTotal = 0
    varnetTotal = 0
    varcost = 0
    varbillUserId = 0
    varreferanceBillId = 0
    varfromInstitutionId = 0
    vartoInstitutionId = 0
    varpaymentMethodId = 0
    varfromPersonId = 0
    vartoPersonId = 0
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let dtype(ByVal vdtype As String)
    vardtype = vdtype
End Property

Public Property Get dtype() As String
    dtype = vardtype
End Property

Public Property Let invoiceNo(ByVal vinvoiceNo As String)
    varinvoiceNo = vinvoiceNo
End Property

Public Property Get invoiceNo() As String
    invoiceNo = varinvoiceNo
End Property

Public Property Let billAt(ByVal vbillAt As Date)
    varbillAt = vbillAt
End Property

Public Property Get billAt() As Date
    billAt = varbillAt
End Property

Public Property Let billDate(ByVal vbillDate As Date)
    varbillDate = vbillDate
End Property

Public Property Get billDate() As Date
    billDate = varbillDate
End Property

Public Property Let grossTotal(ByVal vgrossTotal As Double)
    vargrossTotal = vgrossTotal
End Property

Public Property Get grossTotal() As Double
    grossTotal = vargrossTotal
End Property

Public Property Let discount(ByVal vdiscount As Double)
    vardiscount = vdiscount
End Property

Public Property Get discount() As Double
    discount = vardiscount
End Property

Public Property Let freeTotal(ByVal vfreeTotal As Double)
    varfreeTotal = vfreeTotal
End Property

Public Property Get freeTotal() As Double
    freeTotal = varfreeTotal
End Property

Public Property Let netTotal(ByVal vnetTotal As Double)
    varnetTotal = vnetTotal
End Property

Public Property Get netTotal() As Double
    netTotal = varnetTotal
End Property

Public Property Let cost(ByVal vcost As Double)
    varcost = vcost
End Property

Public Property Get cost() As Double
    cost = varcost
End Property

Public Property Let billUserId(ByVal vbillUserId As Long)
    varbillUserId = vbillUserId
End Property

Public Property Get billUserId() As Long
    billUserId = varbillUserId
End Property

Public Property Let referanceBillId(ByVal vreferanceBillId As Long)
    varreferanceBillId = vreferanceBillId
End Property

Public Property Get referanceBillId() As Long
    referanceBillId = varreferanceBillId
End Property

Public Property Let fromInstitutionId(ByVal vfromInstitutionId As Long)
    varfromInstitutionId = vfromInstitutionId
End Property

Public Property Get fromInstitutionId() As Long
    fromInstitutionId = varfromInstitutionId
End Property

Public Property Let toInstitutionId(ByVal vtoInstitutionId As Long)
    vartoInstitutionId = vtoInstitutionId
End Property

Public Property Get toInstitutionId() As Long
    toInstitutionId = vartoInstitutionId
End Property

Public Property Let paymentMethodId(ByVal vpaymentMethodId As Long)
    varpaymentMethodId = vpaymentMethodId
End Property

Public Property Get paymentMethodId() As Long
    paymentMethodId = varpaymentMethodId
End Property

Public Property Let fromPersonId(ByVal vfromPersonId As Long)
    varfromPersonId = vfromPersonId
End Property

Public Property Get fromPersonId() As Long
    fromPersonId = varfromPersonId
End Property

Public Property Let toPersonId(ByVal vtoPersonId As Long)
    vartoPersonId = vtoPersonId
End Property

Public Property Get toPersonId() As Long
    toPersonId = vartoPersonId
End Property


