VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmSale 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sale"
   ClientHeight    =   7260
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   12045
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   12045
   Begin MSComCtl2.DTPicker dtpW 
      Height          =   375
      Left            =   5640
      TabIndex        =   34
      Top             =   1680
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   49020931
      CurrentDate     =   41244
   End
   Begin MSFlexGridLib.MSFlexGrid gridBatch 
      Height          =   2415
      Left            =   7920
      TabIndex        =   28
      Top             =   120
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   4260
      _Version        =   393216
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.TextBox txtBalance 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   375
      Left            =   9720
      TabIndex        =   27
      Top             =   5640
      Width           =   2175
   End
   Begin VB.TextBox txtPaid 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   9720
      TabIndex        =   26
      Top             =   5160
      Width           =   2175
   End
   Begin VB.TextBox txtNetTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   375
      Left            =   9720
      TabIndex        =   25
      Top             =   4680
      Width           =   2175
   End
   Begin VB.TextBox txtDiscount 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   9720
      TabIndex        =   24
      Top             =   4200
      Width           =   2175
   End
   Begin VB.TextBox txtGrossTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   375
      Left            =   9720
      TabIndex        =   23
      Top             =   3720
      Width           =   2175
   End
   Begin VB.ComboBox cmbPayment 
      Height          =   360
      Left            =   9720
      TabIndex        =   22
      Top             =   3120
      Width           =   2175
   End
   Begin MSDataListLib.DataCombo cmbCustomer 
      Height          =   360
      Left            =   9720
      TabIndex        =   21
      Top             =   2640
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   635
      _Version        =   393216
      Text            =   ""
   End
   Begin MSFlexGridLib.MSFlexGrid gridBill 
      Height          =   3495
      Left            =   120
      TabIndex        =   13
      Top             =   3120
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   6165
      _Version        =   393216
   End
   Begin VB.TextBox txtValue 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6600
      TabIndex        =   11
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox txtRate 
      Height          =   375
      Left            =   6600
      TabIndex        =   9
      Top             =   600
      Width           =   1215
   End
   Begin MSDataListLib.DataList lstSearch 
      Height          =   2460
      Left            =   120
      TabIndex        =   2
      Top             =   555
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   4339
      _Version        =   393216
   End
   Begin VB.TextBox txtSearch 
      Height          =   375
      Left            =   720
      TabIndex        =   1
      Top             =   120
      Width           =   4695
   End
   Begin VB.CommandButton btnUpdate 
      Caption         =   "&Update"
      Height          =   495
      Left            =   8040
      TabIndex        =   4
      Top             =   6120
      Width           =   3855
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   10680
      TabIndex        =   8
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   375
      Left            =   5640
      TabIndex        =   7
      Top             =   2160
      Width           =   2175
   End
   Begin VB.TextBox txtQty 
      Height          =   375
      Left            =   6600
      TabIndex        =   6
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton btnRemove 
      Caption         =   "&Remove"
      Height          =   375
      Left            =   5640
      TabIndex        =   3
      Top             =   2640
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   6600
      TabIndex        =   30
      Top             =   6600
      Width           =   3855
      Begin VB.OptionButton optPrint 
         Caption         =   "Print"
         Height          =   240
         Left            =   120
         TabIndex        =   33
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton optPreview 
         Caption         =   "Preview"
         Height          =   240
         Left            =   1320
         TabIndex        =   32
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optNone 
         Caption         =   "None"
         Height          =   240
         Left            =   2640
         TabIndex        =   31
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Label lblBatchStock 
      Caption         =   "Label3"
      Height          =   495
      Left            =   8040
      TabIndex        =   29
      Top             =   6720
      Width           =   1935
   End
   Begin VB.Label Label10 
      Caption         =   "Balance"
      Height          =   375
      Left            =   8040
      TabIndex        =   20
      Top             =   5640
      Width           =   1095
   End
   Begin VB.Label Label9 
      Caption         =   "Paid"
      Height          =   375
      Left            =   8040
      TabIndex        =   19
      Top             =   5160
      Width           =   1095
   End
   Begin VB.Label Label8 
      Caption         =   "Net Total"
      Height          =   375
      Left            =   8040
      TabIndex        =   18
      Top             =   4680
      Width           =   1095
   End
   Begin VB.Label Label7 
      Caption         =   "Discount"
      Height          =   375
      Left            =   8040
      TabIndex        =   17
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "Gross Total"
      Height          =   375
      Left            =   8040
      TabIndex        =   16
      Top             =   3720
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Payment Method"
      Height          =   375
      Left            =   8040
      TabIndex        =   15
      Top             =   3120
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Customer"
      Height          =   375
      Left            =   8040
      TabIndex        =   14
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Value"
      Height          =   375
      Left            =   5640
      TabIndex        =   12
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "&Rate"
      Height          =   375
      Left            =   5640
      TabIndex        =   10
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label lblEditName 
      Caption         =   "&Quantity"
      Height          =   375
      Left            =   5640
      TabIndex        =   5
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblName 
      Caption         =   "&Item"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmSale"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    
    Dim mySec As New clsSecurity
    Dim myUser As New clsUser
    
    Dim current As New Bill
    Dim billItems As New Collection
    Dim currentItem As New Item
    Dim currentStocks As New Collection
    Dim currentStock As New stock
    
    
Private Sub fillGrid()
    Dim temBi As BillItem
    Dim temItem As Item
    Dim i As Integer
    Dim grosValue As Double
    
    With gridBill
        .Clear
        .Rows = billItems.Count + 1
        .Cols = 9
        .ColWidth(0) = 0
        .ColWidth(2) = 2500
        .FixedCols = 2
        .TextMatrix(0, 0) = "id"
        .TextMatrix(0, 1) = "No"
        .TextMatrix(0, 2) = "Item"
        .TextMatrix(0, 3) = "Quantity"
        .TextMatrix(0, 4) = "Rate"
        .TextMatrix(0, 5) = "Value"
        .TextMatrix(0, 6) = "Batch"
        .TextMatrix(0, 7) = "DOM"
        .TextMatrix(0, 8) = "Warrenty"
        i = 1
        For Each temBi In billItems
            Set temItem = New Item
            temItem.id = temBi.itemId
            .TextMatrix(i, 0) = temBi.id
            .TextMatrix(i, 1) = i
            .TextMatrix(i, 2) = temItem.itemName & " (" & temItem.itemCode & ")"
            .TextMatrix(i, 3) = temBi.quantity
            .TextMatrix(i, 4) = temBi.grossRate
            .TextMatrix(i, 5) = temBi.grossValue
            .TextMatrix(i, 6) = temBi.batch
            .TextMatrix(i, 7) = temBi.dom
            .TextMatrix(i, 8) = temBi.doe
            grosValue = grosValue + temBi.grossValue
            i = i + 1
        Next
    End With
    
    current.grossTotal = grosValue
    current.discount = Val(txtDiscount.text)
    current.netTotal = current.grossTotal - current.discount
    
    txtGrossTotal.text = Format(current.grossTotal, "#,#0.00")
    txtNetTotal.text = Format(current.netTotal, "#,#0.00")
    

End Sub



Private Sub btnAdd_Click()
    If IsNumeric(lstSearch.BoundText) = False Then
        MsgBox "Please select an item"
        txtSearch.SetFocus
        Exit Sub
    End If
    If IsNumeric(txtQty.text) = False Then
        MsgBox "Please enter the quantity"
        txtQty.SetFocus
        Exit Sub
    End If
    If IsNumeric(txtRate.text) = False Then
        MsgBox "Please enter a rate"
        txtRate.Enabled = True
        txtRate.SetFocus
        Exit Sub
    End If
    Dim addingBillItem As New BillItem
    With addingBillItem
        .itemId = currentItem.id
        .batchId = currentStock.batchId
        .quantity = Val(txtQty.text)
        .Rate = Val(txtRate.text)
        .grossValue = Val(txtValue.text)
        If dtpW.Value = Date Then
            .doe = 0
        Else
            .doe = dtpW.Value
        End If
        If currentItem.isAService = True Then
            billItems.Add addingBillItem
        Else
            If .quantity > currentStock.stock Then
                MsgBox "Not sufficient stocks"
                txtQty.SetFocus
                Exit Sub
            Else
                billItems.Add addingBillItem
            End If
        End If
    End With
    fillGrid
    clearBillItemValues
    txtSearch.SetFocus
    txtSearch_Change
End Sub



Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillNameCombo()
    Dim sql As String
    Dim allItems As New clsFillCombo
    If txtSearch.text = "" Then
        sql = "Select id, itemName & ' (' & itemCode & ')' as itemDisplay  from tblItem where dtype = 'amp' and  deleted = false order by itemName"
    Else
        sql = "Select id, itemName & ' (' & itemCode & ')' as itemDisplay  from tblItem where dtype = 'amp' and  deleted = false and (itemName like '" & ProgramVariable.sqlLike & txtSearch.text & ProgramVariable.sqlLike & "' or itemCode like '" & ProgramVariable.sqlLike & txtSearch.text & ProgramVariable.sqlLike & "' ) order by itemName"
    End If
    allItems.fillSqlList lstSearch, sql
    
End Sub

Private Sub fillCombo()
    cmbPayment.AddItem "Cash"
    cmbPayment.AddItem "Credit"
    cmbPayment.AddItem "Card"
    cmbPayment.AddItem "Cheque"
    cmbPayment.AddItem "Bank"
    cmbPayment.AddItem "Other"
    cmbPayment.text = "Cash"
End Sub


Private Sub btnRemove_Click()
    If gridBill.row < 1 Then Exit Sub
    If gridBill.row > billItems.Count Then Exit Sub
    billItems.Remove (gridBill.row)
    clearBillItemValues
    fillGrid
End Sub

Private Sub btnUpdate_Click()
    Dim person As New clsPerson
    person.PersonName = cmbCustomer.text
    If person.PersonName <> "" Then
        person.saveData
    End If
    If gridBill.Rows = 1 Then
        MsgBox "Nothing to sale"
        Exit Sub
    End If
    current.billAt = Now
    current.billDate = Date
    current.billUserId = ProgramVariable.loggedUser.UserID
    current.toPersonId = person.personId
    
    current.dtype = "saleBill"
    current.fromInstitutionId = ProgramVariable.institution.id
    
    Select Case cmbPayment.text
        Case "Cash": current.paymentMethodId = paymentMethod.Cash
        Case "Credit": current.paymentMethodId = paymentMethod.Cash
        Case "Card": current.paymentMethodId = paymentMethod.Cash
        Case "Cheque": current.paymentMethodId = paymentMethod.Cash
        Case "Bank": current.paymentMethodId = paymentMethod.Cash
        Case "Other": current.paymentMethodId = paymentMethod.Cash
    End Select
    current.saveData
    
    Dim bi As BillItem
    Dim temBatch As batch
    
    Dim billSerial As Integer
    billSerial = 1
    
    For Each bi In billItems
        Set temBatch = New batch
        temBatch.id = bi.batchId
        bi.totalQuantity = bi.quantity
        bi.batch = temBatch.batchName
        bi.grossRate = bi.Rate
        bi.netRate = bi.Rate
        bi.billSerial = billSerial
        
        addToStockHistry temBatch.id, findItemStock(bi.itemId), 0, ProgramVariable.institution.id, current.id, 0, False
        addToStockHistry temBatch.id, findBatchStock(bi.batchId), 0, ProgramVariable.institution.id, current.id, 0, True
        
        removeFromStock temBatch.id, bi.totalQuantity, ProgramVariable.institution.id
        bi.billDiscountRate = (bi.grossRate * (current.discount * 100 / current.grossTotal)) / 100
        bi.billDiscountValue = bi.billDiscountRate * bi.quantity
        bi.billId = current.id
        bi.netRate = bi.grossRate - bi.billDiscountRate
        bi.saveData
        
        addToStockHistry temBatch.id, 0, findItemStock(bi.itemId), ProgramVariable.institution.id, current.id, 0, False
        addToStockHistry temBatch.id, 0, findBatchStock(bi.batchId), ProgramVariable.institution.id, current.id, 0, True
        
        
        Set bi = Nothing
        
        
        billSerial = billSerial + 1
    Next

    closeAdoDatabase
    
    If optPrint.Value = True Then
        printSaleBill current.id, Me.hwnd
    ElseIf optPreview.Value = True Then
        previewSaleBill current.id, Me.hwnd
    Else
   
    End If
    
    openAdoDatabase

    
    
    
    
    Call clearBillItemValues
    Call clearBillValues
    Call fillGrid
    MsgBox "Bill added"
    txtSearch.SetFocus
    txtSearch_Change

End Sub

Private Sub dtpW_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        btnAdd_Click
        KeyCode = Empty
    ElseIf KeyCode = vbKeyEscape Then
        dtpW.Value = Date
    End If
End Sub

Private Sub gridBatch_Click()
    With gridBatch
        If .Rows < 2 Or .row < 1 Then
            currentStock = Nothing
            lblBatchStock.Caption = ""
        Else
            Set currentStock = currentStocks(.row)
            lblBatchStock.Caption = currentStock.stock
        End If
    End With
End Sub

Private Sub lstSearch_Click()
    itemChanged
End Sub

Private Sub lstSearch_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtQty.SetFocus
        KeyCode = Empty
    ElseIf KeyCode = vbKeyEscape Then
        txtSearch.SetFocus
    End If
End Sub

Private Sub txtDiscount_Change()
    current.discount = Val(txtDiscount.text)
    current.netTotal = current.grossTotal - current.discount
    txtNetTotal.text = Format(current.netTotal, "#,##0.00")
End Sub

Private Sub txtPaid_Change()
    txtBalance.text = Format(Val(txtPaid.text) - Val(txtNetTotal.text), "#,##0.00")
End Sub

Private Sub txtQty_Change()
    Call calculateValue
End Sub

Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        If txtRate.Enabled = True Then
            txtRate.SetFocus
        Else
            btnAdd_Click
        End If
    End If
End Sub

Private Sub calculateValue()
    txtValue.text = Format(Val(txtQty.text) * Val(txtRate.text), "0.00")
End Sub


Private Sub txtRate_Change()
    Call calculateValue
End Sub

Private Sub txtRate_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        dtpW.SetFocus
    End If
End Sub

Private Sub txtSearch_Change()
    fillNameCombo
    itemChanged
End Sub

Private Sub Form_Load()
    SetColours Me
    GetCommonSettings Me
    dtpW.Value = Date
    Call fillNameCombo
    Call clearBillValues
    Call clearBillItemValues
    Call itemChanged
    Call fillStocks
End Sub

Private Sub clearBillValues()
    Set current = New Bill
    Set currentItem = New Item
    Set currentStock = New stock
    Set currentStocks = New Collection
    Set billItems = New Collection
    txtGrossTotal.text = Empty
    txtNetTotal.text = Empty
    txtDiscount.text = Empty
    txtPaid.text = Empty
    txtBalance.text = Empty
    cmbCustomer.text = Empty
    cmbPayment.text = "Cash"
End Sub

Private Sub clearBillItemValues()
    txtSearch.text = ""
    txtQty.text = ""
    txtRate.text = ""
    txtValue.text = ""
    dtpW.MaxDate = Date
    dtpW.Value = Date
End Sub

Private Sub itemChanged()
    Dim temB As New batch
    If currentItem.id = Val(lstSearch.BoundText) Then Exit Sub
    currentItem.id = Val(lstSearch.BoundText)
    If currentItem.canChangePrice = True Then
        txtRate.Enabled = True
    Else
        txtRate.Enabled = False
    End If
    With currentItem
        
        If currentItem.isAService = False Then
            Call fillStocks
            temB.id = currentStock.batchId
            If .priceByBatch = True Then
                txtRate.text = findBatchPrice(currentStock.batchId)
                If Val(txtRate.text) = 0 Then
                    txtRate.text = findItemPrice(currentItem.id)
                End If
            Else
                txtRate.text = findItemPrice(currentItem.id)
            End If
        Else
            txtRate.text = findItemPrice(currentItem.id)
            Set currentStocks = New Collection
            Set currentStock = New stock
            temB.id = 0
        End If
        
        
        If .NoWarrenty = True Then
            dtpW.MaxDate = Date
            dtpW.Value = Date
        Else
            If .warrentyBySale = True Then
                dtpW.MaxDate = Date + .warrentyDaysFromSale
                dtpW.Value = Date + .warrentyDaysFromSale
            ElseIf .warrentyByPurchase = True Then
                dtpW.MaxDate = temB.doe - .warrentyDaysLessFromPurchase
                dtpW.Value = temB.doe - .warrentyDaysLessFromPurchase
            Else
                dtpW.MaxDate = Date
                dtpW.Value = Date
            End If
        End If
        dtpW.MaxDate = dtpW.Value
        
    End With
    
End Sub

Private Sub fillStocks()
    Dim tB As batch
    Dim temS As stock
    Dim i As Integer
    With gridBatch
        .Clear
        .Cols = 4
        .Rows = 1
        .row = 0
        .col = 0
        .text = "Batch/Serial"
        .col = 1
        .text = "Price"
        .col = 2
        .text = "Stock"
        
        Set currentStocks = getItemStocks(currentItem.id)
        
        .Visible = False
        
        For Each temS In currentStocks
        
            If stockExistsInBill(currentItem.id) = False Then
                Set tB = New batch
                Set currentStock = temS
                tB.id = temS.batchId
                .Rows = .Rows + 1
                .row = .Rows - 1
                .col = 0
                .text = tB.batchName
                .col = 1
                .text = temS.stock
                .col = 2
                If currentItem.priceByBatch = True Then
                    .text = Format(temS.price, "0.00")
                Else
                    .text = Format(findItemPrice(currentItem.id), "0.00")
                End If
            End If
            .row = 1
            .col = 0
            .ColSel = .Cols - 1
        Next
        If currentStocks.Count > 0 Then
            Set currentStock = currentStocks(1)
        Else
            Set currentStock = Nothing
        End If
        .Visible = True
        
        
    End With
End Sub

Private Function stockExistsInBill(stockId As Long) As Boolean
    stockExistsInBill = False
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub

Private Sub txtSearch_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        txtSearch.text = Empty
    ElseIf KeyCode = vbKeyReturn Then
        txtQty.SetFocus
        KeyCode = Empty
    ElseIf KeyCode = vbKeyDown Then
        lstSearch.SetFocus
    ElseIf KeyCode = vbKeyUp Then
        cmbCustomer.SetFocus
        KeyCode = Empty
    ElseIf KeyCode = vbKeyLeft Then
        txtQty.SetFocus
        KeyCode = Empty
    End If
End Sub

