VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsVariables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"institution"
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence


Option Explicit
    Private mvarSecurityKey As String
    Private mvarDatabaseName As String 'local copy
    Private mvarDatabaseLocation As String 'local copy
    Private mvarconn As New ADODB.Connection 'local copy
    Private mvarShortDateFormat As String 'local copy
    Private mvarLongDateFormat As String 'local copy
    Private mvarloggedUser As clsUser 'local copy
    Private mvarsqlLike As String 'local copy
    Private mvarinstitution As institution 'local copy
    Private mvarsaleBillPrinterName As String 'local copy
    Private mvarsaleBillPaperName As String 'local copy
    Private mvarpurchaseBillPaperName As String 'local copy
    Private mvarpurchaseBillPrinterName As String 'local copy
    Private mvarreportPaperName As String 'local copy
    Private mvarreportPrinterName As String 'local copy

Public Property Let reportPrinterName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.reportPrinterName = 5
    mvarreportPrinterName = vData
End Property


Public Property Get reportPrinterName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.reportPrinterName
    reportPrinterName = mvarreportPrinterName
End Property



Public Property Let reportPaperName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.reportPaperName = 5
    mvarreportPaperName = vData
End Property


Public Property Get reportPaperName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.reportPaperName
    reportPaperName = mvarreportPaperName
End Property



Public Property Let purchaseBillPrinterName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.purchaseBillPrinterName = 5
    mvarpurchaseBillPrinterName = vData
End Property


Public Property Get purchaseBillPrinterName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.purchaseBillPrinterName
    purchaseBillPrinterName = mvarpurchaseBillPrinterName
End Property



Public Property Let purchaseBillPaperName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.purchaseBillPaperName = 5
    mvarpurchaseBillPaperName = vData
End Property


Public Property Get purchaseBillPaperName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.purchaseBillPaperName
    purchaseBillPaperName = mvarpurchaseBillPaperName
End Property



Public Property Let saleBillPaperName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.saleBillPaperName = 5
    mvarsaleBillPaperName = vData
End Property


Public Property Get saleBillPaperName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.saleBillPaperName
    saleBillPaperName = mvarsaleBillPaperName
End Property



Public Property Let saleBillPrinterName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.saleBillPrinterName = 5
    mvarsaleBillPrinterName = vData
End Property


Public Property Get saleBillPrinterName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.saleBillPrinterName
    saleBillPrinterName = mvarsaleBillPrinterName
End Property



    
Public Property Set institution(ByVal vData As institution)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.institution = Form1
    Set mvarinstitution = vData
End Property

Public Property Get institution() As institution
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.institution
    Set institution = mvarinstitution
End Property




Public Property Let sqlLike(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sqlLike = 5
    mvarsqlLike = vData
End Property


Public Property Get sqlLike() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sqlLike
    sqlLike = mvarsqlLike
End Property




Public Property Set loggedUser(ByVal vData As clsUser)
    Set mvarloggedUser = vData
End Property


Public Property Get loggedUser() As clsUser
    Set loggedUser = mvarloggedUser
End Property

Public Property Let LongDateFormat(ByVal vData As String)
    mvarLongDateFormat = vData
End Property


Public Property Get LongDateFormat() As String
    LongDateFormat = mvarLongDateFormat
End Property

Public Property Let ShortDateFormat(ByVal vData As String)
    mvarShortDateFormat = vData
End Property


Public Property Get ShortDateFormat() As String
    ShortDateFormat = mvarShortDateFormat
End Property

Public Property Set conn(ByVal vData As ADODB.Connection)
    Set mvarconn = vData
End Property


Public Property Get conn() As ADODB.Connection
    Set conn = mvarconn
End Property

Public Property Let DatabaseLocation(ByVal vData As String)
    mvarDatabaseLocation = vData
End Property

Public Property Get DatabaseLocation() As String
    DatabaseLocation = mvarDatabaseLocation
End Property

Public Property Let DatabaseName(ByVal vData As String)
    mvarDatabaseName = vData
End Property

Public Property Get DatabaseName() As String
    DatabaseName = mvarDatabaseName
End Property

Public Property Let SecurityKey(ByVal vData As String)
    mvarSecurityKey = vData
End Property


Public Property Get SecurityKey() As String
    SecurityKey = mvarSecurityKey
End Property



