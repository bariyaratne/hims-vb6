VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmPurchase 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Purchase"
   ClientHeight    =   7260
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   14070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   14070
   Begin VB.CommandButton btnRemove 
      Caption         =   "&Remove"
      Height          =   375
      Left            =   8640
      TabIndex        =   21
      Top             =   2040
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   10080
      TabIndex        =   36
      Top             =   5400
      Width           =   3855
      Begin VB.OptionButton optNone 
         Caption         =   "None"
         Height          =   240
         Left            =   2640
         TabIndex        =   39
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optPreview 
         Caption         =   "Preview"
         Height          =   240
         Left            =   1320
         TabIndex        =   38
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optPrint 
         Caption         =   "Print"
         Height          =   240
         Left            =   120
         TabIndex        =   37
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin VB.TextBox txtRetail 
      Height          =   375
      Left            =   6360
      TabIndex        =   12
      Top             =   2040
      Width           =   2055
   End
   Begin MSComCtl2.DTPicker dtpDom 
      Height          =   375
      Left            =   11160
      TabIndex        =   16
      Top             =   600
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   50266115
      CurrentDate     =   41240
   End
   Begin VB.TextBox txtFree 
      Height          =   375
      Left            =   6360
      TabIndex        =   6
      Top             =   600
      Width           =   2055
   End
   Begin VB.TextBox txtBatch 
      Height          =   375
      Left            =   11160
      TabIndex        =   14
      Top             =   120
      Width           =   2535
   End
   Begin VB.TextBox txtInvoice 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   11760
      TabIndex        =   33
      Top             =   5040
      Width           =   2175
   End
   Begin VB.TextBox txtNetTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   375
      Left            =   11760
      TabIndex        =   31
      Top             =   4560
      Width           =   2175
   End
   Begin VB.TextBox txtDiscount 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   11760
      TabIndex        =   29
      Top             =   4080
      Width           =   2175
   End
   Begin VB.TextBox txtGrossTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   375
      Left            =   11760
      TabIndex        =   27
      Top             =   3600
      Width           =   2175
   End
   Begin VB.ComboBox cmbPayment 
      Height          =   360
      Left            =   11760
      Style           =   2  'Dropdown List
      TabIndex        =   25
      Top             =   3000
      Width           =   2175
   End
   Begin MSDataListLib.DataCombo cmbSupplier 
      Height          =   360
      Left            =   11760
      TabIndex        =   23
      Top             =   2520
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSFlexGridLib.MSFlexGrid gridBill 
      Height          =   4455
      Left            =   120
      TabIndex        =   20
      Top             =   2640
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   7858
      _Version        =   393216
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.TextBox txtValue 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6360
      TabIndex        =   10
      Top             =   1560
      Width           =   2055
   End
   Begin VB.TextBox txtRate 
      Height          =   375
      Left            =   6360
      TabIndex        =   8
      Top             =   1080
      Width           =   2055
   End
   Begin MSDataListLib.DataList lstSearch 
      Height          =   1980
      Left            =   120
      TabIndex        =   2
      Top             =   555
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   3493
      _Version        =   393216
   End
   Begin VB.TextBox txtSearch 
      Height          =   375
      Left            =   720
      TabIndex        =   1
      Top             =   120
      Width           =   3615
   End
   Begin VB.CommandButton btnUpdate 
      Caption         =   "&Update"
      Height          =   495
      Left            =   10080
      TabIndex        =   34
      Top             =   6120
      Width           =   3855
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   12720
      TabIndex        =   35
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   375
      Left            =   8640
      TabIndex        =   19
      Top             =   1560
      Width           =   5055
   End
   Begin VB.TextBox txtQty 
      Height          =   375
      Left            =   6360
      TabIndex        =   4
      Top             =   120
      Width           =   2055
   End
   Begin MSComCtl2.DTPicker dtpDoe 
      Height          =   375
      Left            =   11160
      TabIndex        =   18
      Top             =   1080
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   136118275
      CurrentDate     =   41240
   End
   Begin VB.Label Label10 
      Caption         =   "Retail Sale Price"
      Height          =   375
      Left            =   4680
      TabIndex        =   11
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Free"
      Height          =   375
      Left            =   4680
      TabIndex        =   5
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "Batch No or Serial No"
      Height          =   375
      Left            =   9000
      TabIndex        =   13
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label Label11 
      Caption         =   "Date of Manufacture"
      Height          =   375
      Left            =   9000
      TabIndex        =   15
      Top             =   600
      Width           =   2175
   End
   Begin VB.Label Label3 
      Caption         =   "Date of Expiary"
      Height          =   375
      Left            =   9000
      TabIndex        =   17
      Top             =   1080
      Width           =   2175
   End
   Begin VB.Label Label9 
      Caption         =   "Invoice No"
      Height          =   375
      Left            =   10080
      TabIndex        =   32
      Top             =   5040
      Width           =   1095
   End
   Begin VB.Label Label8 
      Caption         =   "Net Total"
      Height          =   375
      Left            =   10080
      TabIndex        =   30
      Top             =   4560
      Width           =   1095
   End
   Begin VB.Label Label7 
      Caption         =   "Discount"
      Height          =   375
      Left            =   10080
      TabIndex        =   28
      Top             =   4080
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "Gross Total"
      Height          =   375
      Left            =   10080
      TabIndex        =   26
      Top             =   3600
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Payment Method"
      Height          =   375
      Left            =   10080
      TabIndex        =   24
      Top             =   3000
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Supplier"
      Height          =   375
      Left            =   10080
      TabIndex        =   22
      Top             =   2520
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Value"
      Height          =   375
      Left            =   4680
      TabIndex        =   9
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "&Rate"
      Height          =   375
      Left            =   4680
      TabIndex        =   7
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label lblEditName 
      Caption         =   "&Quantity"
      Height          =   375
      Left            =   4680
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblName 
      Caption         =   "&Item"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmPurchase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Auther : Dr. M. H. B. Ariyaratne
'          buddhika.ari@gmail.com
'          buddhika_ari@yahoo.com
'          +94 71 58 12399
'          GPL Licence

Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    
    Dim mySec As New clsSecurity
    Dim myUser As New clsUser
    
    Dim current As New Bill
    Dim billItems As New Collection
    Dim currentItem As New Item
    Dim currentStocks As New Collection
    
Private Sub btnAdd_Click()
    Dim temBi As New BillItem
    With temBi
        .itemId = Val(lstSearch.BoundText)
        .quantity = Val(txtQty.text)
        .freeQuantity = Val(txtFree.text)
        .totalQuantity = Val(txtQty.text) + Val(txtFree.text)
        .grossRate = Val(txtRate.text)
        .grossValue = .quantity * .grossRate
        .retailRate = Val(txtRetail.text)
        .Rate = Val(txtRate.text)
        If dtpDoe.Value <> Date Then
            .doe = dtpDoe.Value
        End If
        If dtpDom.Value <> Date Then
            .dom = dtpDom.Value
        End If
        .batch = txtBatch.text
    End With
    billItems.Add temBi
    fillGrid
    clearBillItem
    txtSearch.SetFocus
End Sub

Private Sub clearBillItem()
    txtSearch.text = Empty
    txtQty.text = ""
    txtFree.text = ""
    txtRate.text = ""
    txtValue.text = ""
    txtRetail.text = Empty
    txtBatch.text = Empty
    dtpDom.Value = Date
    dtpDoe.Value = Date
    
End Sub

Private Sub clearBill()
    cmbPayment.text = "Cash"
    txtInvoice.text = Empty
    txtGrossTotal.text = Empty
    txtDiscount.text = Empty
    txtNetTotal.text = Empty
    Set current = New Bill
    Set billItems = New Collection
    Set currentItem = New Item
    Set currentStocks = New Collection
End Sub

Private Sub fillGrid()
    Dim temBi As BillItem
    Dim temItem As Item
    Dim i As Integer
    Dim grosValue As Double
    
    With gridBill
        .Clear
        .Rows = billItems.Count + 1
        .Cols = 10
        .ColWidth(0) = 0
        .ColWidth(2) = 2500
        .FixedCols = 2
        .TextMatrix(0, 0) = "id"
        .TextMatrix(0, 1) = "No"
        .TextMatrix(0, 2) = "Item"
        .TextMatrix(0, 3) = "Quantity"
        .TextMatrix(0, 4) = "Free"
        .TextMatrix(0, 5) = "Rate"
        .TextMatrix(0, 6) = "Value"
        .TextMatrix(0, 7) = "Batch"
        .TextMatrix(0, 8) = "DOM"
        .TextMatrix(0, 9) = "DOE"
        i = 1
        For Each temBi In billItems
            Set temItem = New Item
            temItem.id = temBi.itemId
            .TextMatrix(i, 0) = temBi.id
            .TextMatrix(i, 1) = i
            .TextMatrix(i, 2) = temItem.itemName & " (" & temItem.itemCode & ")"
            .TextMatrix(i, 3) = temBi.quantity
            .TextMatrix(i, 4) = temBi.freeQuantity
            .TextMatrix(i, 5) = temBi.grossRate
            .TextMatrix(i, 6) = temBi.grossValue
            .TextMatrix(i, 7) = temBi.batch
            .TextMatrix(i, 8) = temBi.dom
            .TextMatrix(i, 9) = temBi.doe
            
            grosValue = grosValue + temBi.grossValue
            i = i + 1
        Next
    End With
    
    current.grossTotal = grosValue
    current.discount = Val(txtDiscount.text)
    current.netTotal = current.grossTotal - current.discount
    
    txtGrossTotal.text = Format(current.grossTotal, "#,#0.00")
    txtNetTotal.text = Format(current.netTotal, "#,#0.00")
    

End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillNameCombo()
    Dim sql As String
    Dim temStr As String
    temStr = txtSearch.text
    temStr = Replace(temStr, Chr(39), "")
    temStr = Replace(temStr, Chr(34), "")
    Dim allItems As New clsFillCombo
    If txtSearch.text = "" Then
        sql = "Select id, itemName & ' (' & itemCode & ')' as itemDisplay  from tblItem where dtype = 'amp' and  deleted = false order by itemName"
    Else
        sql = "Select id, itemName & ' (' & itemCode & ')' as itemDisplay  from tblItem where dtype = 'amp' and  deleted = false and (itemName like '" & ProgramVariable.sqlLike & temStr & ProgramVariable.sqlLike & "' or itemCode like '" & ProgramVariable.sqlLike & temStr & ProgramVariable.sqlLike & "' ) order by itemName"
    End If
    allItems.fillSqlList lstSearch, sql
    
End Sub

Private Sub btnRemove_Click()
    If gridBill.row < 1 Then Exit Sub
    If gridBill.row > billItems.Count Then Exit Sub
    billItems.Remove (gridBill.row)
    clearBillItem
    fillGrid
End Sub

Private Sub btnUpdate_Click()
    If gridBill.Rows = 1 Then
        MsgBox "Nothing to sale"
        Exit Sub
    End If
    current.billAt = Now
    current.billDate = Date
    current.billUserId = ProgramVariable.loggedUser.UserID

    current.dtype = "purchaseBill"
    current.invoiceNo = txtInvoice.text
    current.fromInstitutionId = Val(cmbSupplier.BoundText)
    current.toInstitutionId = ProgramVariable.institution.id
    Select Case cmbPayment.text
        Case "Cash": current.paymentMethodId = paymentMethod.Cash
        Case "Credit": current.paymentMethodId = paymentMethod.Cash
        Case "Card": current.paymentMethodId = paymentMethod.Cash
        Case "Cheque": current.paymentMethodId = paymentMethod.Cash
        Case "Bank": current.paymentMethodId = paymentMethod.Cash
        Case "Other": current.paymentMethodId = paymentMethod.Cash
    End Select
    current.saveData
    
    Dim bi As BillItem
    Dim temBatch As batch
    
    For Each bi In billItems
        Set temBatch = New batch
        temBatch.id = getBatchId(bi.itemId, bi.batch, bi.retailRate, bi.dom, bi.doe)
        
        addToStockHistry temBatch.id, findItemStock(bi.itemId), 0, ProgramVariable.institution.id, current.id, 0, False
        addToStockHistry temBatch.id, findBatchStock(bi.batchId), 0, ProgramVariable.institution.id, current.id, 0, True
        
        
        addToStock temBatch.id, bi.totalQuantity, ProgramVariable.institution.id
        bi.batchId = temBatch.id
        bi.billDiscountRate = (bi.grossRate * (current.discount * 100 / current.grossTotal)) / 100
        bi.billDiscountValue = bi.billDiscountRate * bi.quantity
        bi.billId = current.id
        bi.netRate = bi.grossRate - bi.billDiscountRate
        bi.saveData
        
        addToStockHistry temBatch.id, 0, findItemStock(bi.itemId), ProgramVariable.institution.id, current.id, 0, False
        addToStockHistry temBatch.id, 0, findBatchStock(bi.batchId), ProgramVariable.institution.id, current.id, 0, True
        
        
        Set bi = Nothing
    Next
    
    closeAdoDatabase
   
    MsgBox "Bill added"
    
    
    
    If optPrint.Value = True Then
        printPurchaseBill current.id, Me.hwnd
    ElseIf optPreview.Value = True Then
        previewPurchaseBill current.id, Me.hwnd
    Else

    End If
    
    openAdoDatabase

    Call clearBill
    Call fillGrid

    txtSearch.SetFocus
    
End Sub

Private Sub calValue()
    txtValue.text = Format(Val(txtQty.text) * Val(txtRate.text), "0.00")
End Sub


Private Sub cmbPayment_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtDiscount.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        cmbPayment.text = "Cash"
    End If

End Sub

Private Sub cmbSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbPayment.SetFocus
    End If
End Sub

Private Sub dtpDoe_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        btnAdd_Click
    ElseIf KeyCode = vbKeyEscape Then
        dtpDoe.Value = Date
    End If
End Sub

Private Sub dtpDom_KeyDown(KeyCode As Integer, Shift As Integer)
   If KeyCode = vbKeyReturn Then
        dtpDoe.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        dtpDom.Value = Date
    End If
End Sub

Private Sub lstSearch_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtQty.SetFocus
        KeyCode = Empty
    ElseIf KeyCode = vbKeyEscape Then
        txtSearch.SetFocus
    End If
End Sub

Private Sub txtBatch_KeyDown(KeyCode As Integer, Shift As Integer)
   If KeyCode = vbKeyReturn Then
        dtpDom.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtBatch.text = Empty
    ElseIf KeyCode = vbKeyRight Then
        dtpDoe.SetFocus
    ElseIf KeyCode = vbKeyLeft Then
        txtBatch.SetFocus
    End If
End Sub

Private Sub txtDiscount_Change()
    current.discount = Val(txtDiscount.text)
    current.netTotal = current.grossTotal - current.discount
    txtNetTotal.text = Format(current.netTotal, "#,##0.00")
End Sub

Private Sub txtDiscount_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtInvoice.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtDiscount.text = Empty
    End If
    
End Sub

Private Sub txtFree_KeyDown(KeyCode As Integer, Shift As Integer)
   If KeyCode = vbKeyReturn Then
        txtRate.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtFree.text = Empty
    ElseIf KeyCode = vbKeyRight Then
        txtRate.SetFocus
    ElseIf KeyCode = vbKeyLeft Then
        txtQty.SetFocus
    End If
End Sub

Private Sub txtInvoice_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        btnUpdate_Click
    ElseIf KeyCode = vbKeyEscape Then
        txtInvoice.text = Empty
    End If
End Sub

Private Sub txtQty_Change()
    Call calValue
End Sub

Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtFree.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtQty.text = Empty
    ElseIf KeyCode = vbKeyRight Then
        txtFree.SetFocus
    ElseIf KeyCode = vbKeyLeft Then
        txtSearch.SetFocus
    End If
End Sub

Private Sub txtRate_Change()
    Call calValue
End Sub

Private Sub txtRate_KeyDown(KeyCode As Integer, Shift As Integer)
   If KeyCode = vbKeyReturn Then
        txtRetail.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtRate.text = Empty
    ElseIf KeyCode = vbKeyRight Then
        txtRetail.SetFocus
    ElseIf KeyCode = vbKeyLeft Then
        txtFree.SetFocus
    End If
End Sub

Private Sub txtRetail_KeyDown(KeyCode As Integer, Shift As Integer)
   If KeyCode = vbKeyReturn Then
        txtBatch.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtRetail.text = Empty
    ElseIf KeyCode = vbKeyRight Then
        txtBatch.SetFocus
    ElseIf KeyCode = vbKeyLeft Then
        txtRate.SetFocus
    End If
End Sub

Private Sub txtSearch_Change()
    fillNameCombo
End Sub

Private Sub Form_Load()
    SetColours Me
'    GetCommonSettings Me
    Call fillNameCombo
    Call fillCombos
    Call clearBillItem
    Call clearBill
End Sub

Private Sub fillCombos()
    Dim sup As New clsFillCombo
    Dim sql As String
    sql = "Select id, InstitutionName from tblInstitution where dtype='supplier' and deleted = false order by InstitutionName"
    sup.fillSqlCombo cmbSupplier, sql
    cmbPayment.AddItem "Cash"
    cmbPayment.AddItem "Credit"
    cmbPayment.AddItem "Card"
    cmbPayment.AddItem "Cheque"
    cmbPayment.AddItem "Bank"
    cmbPayment.AddItem "Other"
    cmbPayment.text = "Cash"
End Sub

Private Sub itemChanged()
    If current.id = Val(lstSearch.BoundText) Then Exit Sub
End Sub

Private Sub fillStocks()

End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub

Private Sub txtSearch_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        txtSearch.text = Empty
    ElseIf KeyCode = vbKeyReturn Then
        txtQty.SetFocus
        KeyCode = Empty
    ElseIf KeyCode = vbKeyDown Then
        lstSearch.SetFocus
    ElseIf KeyCode = vbKeyUp Then
        cmbSupplier.SetFocus
        KeyCode = Empty
    ElseIf KeyCode = vbKeyLeft Then
        txtQty.SetFocus
        KeyCode = Empty
    End If
End Sub
