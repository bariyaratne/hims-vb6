VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmIxValues 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigations"
   ClientHeight    =   5460
   ClientLeft      =   2130
   ClientTop       =   1635
   ClientWidth     =   10920
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   10920
   Begin VB.Frame Frame2 
      Height          =   4695
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   3615
      Begin MSDataListLib.DataCombo dtcIxValue 
         Height          =   3780
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   6668
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   4200
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1320
         TabIndex        =   2
         Top             =   4200
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   2520
         TabIndex        =   3
         Top             =   4200
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   3960
      TabIndex        =   11
      Top             =   120
      Width           =   6855
      Begin VB.TextBox txtValueID 
         Enabled         =   0   'False
         Height          =   360
         Left            =   5640
         TabIndex        =   13
         Top             =   480
         Width           =   615
      End
      Begin MSDataListLib.DataCombo dtcCategory 
         Height          =   360
         Left            =   1920
         TabIndex        =   7
         Top             =   960
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.TextBox txtName 
         Height          =   360
         Left            =   1920
         TabIndex        =   5
         Top             =   480
         Width           =   3615
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   2880
         TabIndex        =   8
         Top             =   4200
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   3960
         TabIndex        =   9
         Top             =   4200
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label3 
         Caption         =   "Categ&ory"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "&Investigation"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   9840
      TabIndex        =   10
      Top             =   4920
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmIxValues"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsViewIxValue As New ADODB.Recordset
    Dim rsViewCategory As New ADODB.Recordset
    Dim rsViewDept As New ADODB.Recordset
    Dim rsIxValue As New ADODB.Recordset

Private Sub bttnCancel_Click()
    Call SelectMode
    Call Clearvalues
    dtcIxValue.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnDelete_Click()
    With rsIxValue
        If .State = 1 Then .Close
        temSql = "Select * from tblIxValue where IxValueID = " & Val(dtcIxValue.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
        End If
        If .State = 1 Then .Close
    End With
    Call Clearvalues
    Call FillCOmbos
    dtcIxValue.Text = Empty
    dtcIxValue.SetFocus
End Sub

Private Sub dtcIxValue_Change()
    If IsNumeric(dtcIxValue.BoundText) = True Then
        Call DisplaySelected
        bttnEdit.Enabled = True
        bttnDelete.Enabled = True
    Else
        Clearvalues
        bttnEdit.Enabled = False
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub dtcIxValue_Click(Area As Integer)
    dtcIxValue_Change
End Sub

Private Sub Form_Load()
    FillCOmbos
    Call SelectMode
    Clearvalues
End Sub

Private Sub bttnAdd_Click()
    Clearvalues
    Call EditMode
    txtName.Text = dtcIxValue.Text
    txtValueID.Visible = False
    dtcIxValue.Text = Empty
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    Call EditMode
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub ChangeDate()
    Dim i As Integer
    With rsIxValue
        On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        temSql = "Select * From tblIxValue Where IxValueID = " & Val(dtcIxValue.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !IxValue = Trim(txtName.Text)
            !IxValueCategoryID = Val(dtcCategory.BoundText)
            .Update
        Else
            Call SaveData
        End If
        If .State = 1 Then .Close
        Exit Sub
ErrorHandler:
        i = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub

Private Sub bttnSave_Click()
    If Trim(txtName.Text) = "" Then
        MsgBox "Please enter a Name"
        txtName.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcCategory.BoundText) = False Then
        MsgBox "Please select a category"
        dtcCategory.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcIxValue.BoundText) = True Then
        Call ChangeDate
    Else
        Call SaveData
    End If
    FillCOmbos
    Call SelectMode
    Call Clearvalues
    dtcIxValue.Text = Empty
    dtcIxValue.SetFocus
End Sub

Private Sub SaveData()
    Dim i As Integer
    With rsIxValue
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblIxValue", cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !IxValue = Trim(txtName.Text)
        !IxValueCategoryID = Val(dtcCategory.BoundText)
        .Update
        If .State = 1 Then .Close
        Exit Sub
    
ErrorHandler:
        i = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub

Private Sub FillCOmbos()
    With rsViewCategory
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIxValueCategory  WHERE Deleted = False order by IxValueCategory"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcCategory
        Set .RowSource = rsViewCategory
        .ListField = "IxValueCategory"
        .BoundColumn = "IxValueCategoryID"
    End With
    With rsViewIxValue
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIxValue  WHERE Deleted = False order by IxValue"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcIxValue
        Set .RowSource = rsViewIxValue
        .ListField = "IxValue"
        .BoundColumn = "IxValueID"
    End With
End Sub

Private Sub EditMode()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcIxValue.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    txtName.Enabled = True
    
    dtcCategory.Enabled = True
End Sub

Private Sub SelectMode()
    
    bttnAdd.Enabled = True
    bttnEdit.Enabled = False
    dtcIxValue.Enabled = True
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    txtName.Enabled = False
    
    dtcCategory.Enabled = False
End Sub

Private Sub Clearvalues()
    txtName.Text = Empty
    dtcCategory.Text = Empty
    txtValueID.Text = Empty
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcIxValue.BoundText) Then Exit Sub
    With rsIxValue
        If .State = 1 Then .Close
        .Open "Select * From tblIxValue Where (IxValueID = " & dtcIxValue.BoundText & ")", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        txtName.Text = !IxValue
        If IsNull(!IxValueCategoryID) = False Then
            dtcCategory.BoundText = !IxValueCategoryID
        Else
            dtcCategory.Text = Empty
        End If
        txtValueID.Visible = True
        txtValueID.Text = !IxValueID
        If .State = 1 Then .Close
    End With
End Sub

