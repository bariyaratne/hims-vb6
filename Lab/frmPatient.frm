VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmPatient 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Patient"
   ClientHeight    =   5685
   ClientLeft      =   2130
   ClientTop       =   1635
   ClientWidth     =   10920
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5685
   ScaleWidth      =   10920
   Begin VB.Frame Frame2 
      Height          =   4815
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   3615
      Begin MSDataListLib.DataCombo dtcPatient 
         Height          =   4020
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   7091
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   4320
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1320
         TabIndex        =   2
         Top             =   4320
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   2520
         TabIndex        =   3
         Top             =   4320
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4935
      Left            =   3960
      TabIndex        =   27
      Top             =   120
      Width           =   6855
      Begin MSComCtl2.DTPicker dtpDOB 
         Height          =   375
         Left            =   120
         TabIndex        =   29
         Top             =   1680
         Visible         =   0   'False
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   103350275
         CurrentDate     =   39773
      End
      Begin VB.TextBox txtD 
         Height          =   375
         Left            =   5760
         TabIndex        =   18
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox txtM 
         Height          =   375
         Left            =   3840
         TabIndex        =   16
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox txtPatientID 
         Enabled         =   0   'False
         Height          =   375
         Left            =   5760
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin MSDataListLib.DataCombo cmbTitle 
         Height          =   360
         Left            =   1920
         TabIndex        =   8
         Top             =   720
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin VB.TextBox txtPhone 
         Height          =   375
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   20
         Top             =   3120
         Width           =   4815
      End
      Begin VB.TextBox txtY 
         Height          =   375
         Left            =   1920
         TabIndex        =   14
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox txtAddress 
         Height          =   1320
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   12
         Top             =   1200
         Width           =   4815
      End
      Begin VB.TextBox txtName 
         Height          =   360
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   3735
      End
      Begin VB.TextBox txtcomment 
         Height          =   615
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   22
         Top             =   3600
         Width           =   4815
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   3120
         TabIndex        =   23
         Top             =   4320
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4200
         TabIndex        =   24
         Top             =   4320
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo cmbSex 
         Height          =   360
         Left            =   5040
         TabIndex        =   10
         Top             =   720
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label10 
         Caption         =   "Days"
         Height          =   255
         Left            =   5160
         TabIndex        =   17
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label Label9 
         Caption         =   "Months"
         Height          =   255
         Left            =   3120
         TabIndex        =   15
         Top             =   2640
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Years"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   2640
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Sex"
         Height          =   255
         Left            =   3960
         TabIndex        =   9
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Title"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "&Telephone"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   3120
         Width           =   2055
      End
      Begin VB.Label Label5 
         Caption         =   "Date of Birth"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   4440
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Add&ress"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1200
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "&Patient Name"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "C&omments"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   3600
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   9840
      TabIndex        =   26
      Top             =   5160
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPatient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsPatient As New ADODB.Recordset
    Dim rsViewPatient As New ADODB.Recordset
    Dim temSql As String
    Dim PtID As Long

Private Sub bttnCancel_Click()
    Call SelectMode
    Call Clearvalues
    dtcPatient.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnDelete_Click()
    With rsPatient
        If .State = 1 Then .Close
        temSql = "Select * from tblPatient where PatientID = " & Val(dtcPatient.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
        End If
        If .State = 1 Then .Close
    End With
    Call FillCOmbos
    dtcPatient.Text = Empty
End Sub

Private Sub cmbSex_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtY.SetFocus
    Else
    
    End If
End Sub

Private Sub cmbTitle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbSex.SetFocus
    End If
End Sub

Private Sub dtcPatient_Change()
    If IsNumeric(dtcPatient.BoundText) = True Then
        Call DisplaySelected
        bttnEdit.Enabled = True
        bttnAdd.Enabled = False
        bttnDelete.Enabled = True
    Else
        Clearvalues
        bttnAdd.Enabled = True
        bttnEdit.Enabled = False
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub dtpDOB_Change()
'    txtAge.Text = DateDiff("yyyy", dtpDOB.Value, Date)
End Sub

Private Sub Form_Load()
    FillCOmbos
    SelectMode
    Clearvalues
End Sub

Private Sub bttnAdd_Click()
    Dim temText As String
    If IsNumeric(dtcPatient.BoundText) = False Then
        temText = dtcPatient.Text
    Else
        temText = Empty
    End If
    dtcPatient.Text = Empty
    txtPatientID.Visible = False
    Clearvalues
    EditMode
    txtName.SetFocus
    txtName.Text = temText
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    EditMode
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub ChangeData()
    Dim TemResponce As Integer
    If txtName.Text = "" Then
        MsgBox ("Please enter a Name")
        txtName.SetFocus
        Exit Sub
    End If
    With rsPatient
        If .State = 1 Then .Close
        temSql = "Select * From tblPatient Where PatientID = " & Val(dtcPatient.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount = 0 Then Exit Sub
        !Name = Trim(txtName.Text)
        !Address = txtAddress.Text
        !DateOfBirth = dtpDOB.Value
        !Phone = txtPhone.Text
        !Comments = txtcomment.Text
        !SexID = Val(cmbSex.BoundText)
        !TitleID = Val(cmbTitle.BoundText)
        .Update
        PtID = !PatientID
        If .State = 1 Then .Close
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub

Private Sub bttnSave_Click()
    If IsNumeric(dtcPatient.BoundText) = True Then
        Call ChangeData
    Else
        Call SaveData
    End If
    Call Clearvalues
    Call SelectMode
    Call FillCOmbos
    frmPatientIx.FillCOmbos
    frmPatientIx.txtID.Text = PtID
    dtcPatient.Text = Empty
    dtcPatient.SetFocus
    dtcPatient.BoundText = PtID
    'Me.ZOrder 0
    frmPatientIx.ZOrder 0
End Sub
    
Private Sub SaveData()
    Dim TemResponce As Integer
    If Trim(txtName.Text) = "" Then
        MsgBox "Please enter a name"
        txtName.SetFocus
        Exit Sub
    End If
    With rsPatient
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        temSql = "Select * From tblPatient"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Name = Trim(txtName.Text)
        !Address = txtAddress.Text
        !DateOfBirth = dtpDOB.Value
        !Phone = txtPhone.Text
        !Comments = txtcomment.Text
        !SexID = Val(cmbSex.BoundText)
        !TitleID = Val(cmbTitle.BoundText)
        .Update
'        MsgBox "Patient No. " & !PatientID
        PtID = !PatientID
        If .State = 1 Then .Close
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub

Private Sub FillCOmbos()
    With rsViewPatient
        If .State = 1 Then .Close
        temSql = "SELECT tblPatient.* FROM tblPatient where Deleted = False Order by Name"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcPatient
        Set .RowSource = rsViewPatient
        .ListField = "Name"
        .BoundColumn = "PatientID"
    End With
    
    Dim Sex As New clsFillCombos
    Dim Title As New clsFillCombos
    
    Sex.FillAnyCombo cmbSex, "Sex", False
    Title.FillAnyCombo cmbTitle, "Title", False
    
End Sub

Private Sub EditMode()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcPatient.Enabled = False
    bttnDelete.Enabled = False
    
    txtcomment.Enabled = True
    txtName.Enabled = True
    txtAddress.Enabled = True
    txtPhone.Enabled = True
    txtY.Enabled = True
    txtM.Enabled = True
    txtD.Enabled = True
    
    dtpDOB.Enabled = True
    cmbSex.Enabled = True
    cmbTitle.Enabled = True
    
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    
End Sub

Private Sub SelectMode()
    bttnAdd.Enabled = True
    bttnEdit.Enabled = True
    dtcPatient.Enabled = True
    bttnDelete.Enabled = True
    
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    
    txtcomment.Enabled = False
    txtName.Enabled = False
    txtAddress.Enabled = False
    txtPhone.Enabled = False
    txtY.Enabled = False
    txtM.Enabled = False
    txtD.Enabled = False
    dtpDOB.Enabled = False
    cmbSex.Enabled = False
    cmbTitle.Enabled = False
    
End Sub

Private Sub Clearvalues()
    txtName.Text = Empty
    txtcomment.Text = Empty
    txtAddress.Text = Empty
    txtPhone.Text = Empty
    txtY.Text = Empty
    txtM.Text = Empty
    txtD.Text = Empty
    dtpDOB.Value = Date
    cmbSex.Text = Empty
    cmbTitle.Text = Empty
    txtPatientID.Text = Empty
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If rsPatient.State = 1 Then rsPatient.Close: Set rsPatient = Nothing
    If rsViewPatient.State = 1 Then rsViewPatient.Close: Set rsViewPatient = Nothing
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcPatient.BoundText) Then Exit Sub
    With rsPatient
        If .State = 1 Then .Close
        temSql = "Select * From tblPatient Where (PatientID = " & dtcPatient.BoundText & ")"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        txtName.Text = !Name
        If Not IsNull(!Address) Then txtAddress.Text = !Address
        If Not IsNull(!DateOfBirth) Then dtpDOB.Value = !DateOfBirth
        If Not IsNull(!Phone) Then txtPhone.Text = !Phone
        If Not IsNull(!Comments) Then txtcomment.Text = !Comments
        If Not IsNull(!SexID) Then cmbSex.BoundText = !SexID
        If Not IsNull(!TitleID) Then cmbTitle.BoundText = !TitleID
        txtPatientID.Text = !PatientID
        txtPatientID.Visible = True
'        txtAge.Text = DateDiff("yyyy", dtpDOB, Date)
        If .State = 1 Then .Close
    End With
End Sub

'Private Sub txtAge_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyReturn Then
'        dtpDOB.Value = DateSerial(Year(Date) - Val(txtAge.Text), Month(Date), Day(Date))
'    End If
'End Sub
'
Private Sub CalculateAge()
        dtpDOB.Value = DateSerial(Year(Date) - Val(txtY.Text), Month(Date) - Val(txtM.Text), Day(Date) - Val(txtD.Text))
End Sub

Private Sub txtD_Change()
    CalculateAge
End Sub

Private Sub txtD_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        bttnSave_Click
    End If
End Sub

Private Sub txtM_Change()
    CalculateAge
End Sub

Private Sub txtM_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtD.SetFocus
    End If
End Sub

Private Sub txtName_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbTitle.SetFocus
    End If
End Sub

Private Sub txtY_Change()
    CalculateAge
End Sub

Private Sub txtY_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtM.SetFocus
    End If
End Sub
