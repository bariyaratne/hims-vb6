VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmShiftEndSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Shift End Summery"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9225
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleWidth      =   9225
   Begin VB.TextBox txtTIncome 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   4320
      TabIndex        =   17
      Top             =   8160
      Width           =   2655
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   120
      TabIndex        =   13
      Top             =   8160
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   1440
      TabIndex        =   12
      Top             =   8160
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.OptionButton optAll 
      Caption         =   "&All"
      Height          =   240
      Left            =   360
      TabIndex        =   11
      Top             =   1800
      Width           =   1215
   End
   Begin VB.OptionButton optWard 
      Caption         =   "&Ward"
      Height          =   240
      Left            =   4920
      TabIndex        =   10
      Top             =   1800
      Width           =   1215
   End
   Begin VB.OptionButton optCredit 
      Caption         =   "C&redit"
      Height          =   240
      Left            =   3405
      TabIndex        =   9
      Top             =   1800
      Width           =   1215
   End
   Begin VB.OptionButton optCash 
      Caption         =   "&Cash"
      Height          =   240
      Left            =   1875
      TabIndex        =   8
      Top             =   1800
      Value           =   -1  'True
      Width           =   1215
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   7800
      TabIndex        =   3
      Top             =   8160
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   1695
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   7695
      Begin btButtonEx.ButtonEx btnProcess 
         Height          =   375
         Left            =   5760
         TabIndex        =   14
         Top             =   1200
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Process"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo dtcUser 
         Height          =   360
         Left            =   1920
         TabIndex        =   2
         Top             =   1200
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   1920
         TabIndex        =   6
         Top             =   240
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   16580611
         CurrentDate     =   39773
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   1920
         TabIndex        =   7
         Top             =   720
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   16580611
         CurrentDate     =   39773
      End
      Begin VB.Label Label2 
         Caption         =   "&User"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "&From"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "&To"
         Height          =   255
         Left            =   240
         TabIndex        =   0
         Top             =   720
         Width           =   1575
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5895
      Left            =   120
      TabIndex        =   15
      Top             =   2160
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   10398
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Billing"
      TabPicture(0)   =   "frmShiftEndSummery.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "gridBill"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Cancellations"
      TabPicture(1)   =   "frmShiftEndSummery.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "gridCancel"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Refunds"
      TabPicture(2)   =   "frmShiftEndSummery.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "gridRefund"
      Tab(2).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid gridBill 
         Height          =   5415
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   9551
         _Version        =   393216
      End
      Begin MSFlexGridLib.MSFlexGrid gridCancel 
         Height          =   5415
         Left            =   -74880
         TabIndex        =   19
         Top             =   360
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   9551
         _Version        =   393216
      End
      Begin MSFlexGridLib.MSFlexGrid gridRefund 
         Height          =   5415
         Left            =   -74880
         TabIndex        =   20
         Top             =   360
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   9551
         _Version        =   393216
      End
   End
   Begin VB.Label Label3 
      Caption         =   "Total  &Income"
      Height          =   375
      Left            =   2760
      TabIndex        =   18
      Top             =   8160
      Width           =   1215
   End
End
Attribute VB_Name = "frmShiftEndSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim rsBill As New ADODB.Recordset
    Dim rsViewStaff As New ADODB.Recordset
    Dim csetPrinter As New cSetDfltPrinter
    Dim temTopic As String
    Dim temSubTopic As String
    
    
Private Sub btnExcel_Click()
    GridToExcel gridBill, temTopic, temTopic
End Sub

Private Sub btnPrint_Click()
    Dim setDF As cSetDfltPrinter
    setDF ""
    
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = ReportPrinterName Then
            Set Printer = MyPrinter
        End If
    Next
    
    DoEvents
    
    
    setDF ReportPrinterName



    csetPrinter.SetPrinterAsDefault ReportPrinterName
    If SelectForm(ReportPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim myPR As PrintReport
    GetPrintDefaults myPR
    
    GridPrint gridBill, myPR, temTopic, temSubTopic
End Sub

Private Sub btnProcess_Click()
    Call FormatGrid
    Call FillGrid

End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnPrint_Click()
    
End Sub

Private Sub dtcUser_Change()
'    Call FormatGrid
'    Call FillGrid
End Sub

Private Sub dtcUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        dtcUser.Text = Empty
    End If
End Sub

Private Sub dtpFrom_Change()
'    Call FormatGrid
'    Call FillGrid
End Sub

Private Sub Form_Load()
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call FormatGrid
'    Call FillGrid
    Call FillCombos
    dtcUser.BoundText = UserID
    GetCommonSettings Me
End Sub

Private Sub FormatGrid()

    '   0   BillID
    '   1   Date
    '   2   Time
    '   3   Value
    With gridBill
        .Rows = 1
        .Cols = 5
        .Row = 0
        .col = 0
        .CellAlignment = 4
        .Text = "Bill ID."
        .col = 1
        .CellAlignment = 4
        .Text = "Date"
        .col = 2
        .CellAlignment = 4
        .Text = "Type"
        .col = 3
        .CellAlignment = 4
        .Text = "Time"
        .col = 4
        .CellAlignment = 4
        .Text = "Value"
'        .Col = 4
'        .Text = "IxID"
        .ColWidth(0) = 700
        .ColWidth(1) = 1700
        .ColWidth(2) = 1700
        .ColWidth(3) = 1300
        .ColWidth(4) = 1300
'        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 100)
    End With
End Sub

Private Sub FillGrid()
    temTopic = "Shift End Summery"
    Dim temCancelSQL As String
    
    Dim D(1) As Integer
    Dim p(0) As Integer
    
     
    If dtpFrom.Value = dtpTo.Value Then
        temSubTopic = "On " & Format(dtpFrom.Value, "dd MMMM yyyy")
    Else
        temSubTopic = "From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpTo.Value, "dd MMMM yyyy")
    End If
    
    If IsNumeric(dtcUser.BoundText) = True Then
        temSubTopic = temSubTopic & " User - " & dtcUser.Text
    Else
        temSubTopic = temSubTopic & " All Users"
    End If
    
    temSQL = "SELECT PatientIxBillID as [Bill No], BillType  as [Bill Type], "
    temCancelSQL = "SELECT PatientIxBillID as [Bill No], BillType  as [Bill Type], "
    
    If dtpFrom.Value <> dtpTo.Value Then
        temSQL = temSQL & " format(Date, 'yyyy MMMM dd') as [Bill Date] , "
        temCancelSQL = temCancelSQL & " format(CancelledDate, 'yyyy MMMM dd') as [Cancelled Date] , "
        D(0) = 4
    Else
        D(0) = 3
    End If
    
    temSQL = temSQL & " format(Time , 'hh:mm AMPM') as [Bill Time], NetValue as [Bill Value] FROM tblPatientIxBill "
    temCancelSQL = temCancelSQL & " format(CancelledTime , 'hh:mm AMPM') as [Cancelled Time], NetValue as [Bill Value] FROM tblPatientIxBill "
    
    temSQL = temSQL & " Where Date Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "# "
    temCancelSQL = temCancelSQL & " Where CancelledDate Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "# "
    
    If IsNumeric(dtcUser.BoundText) = True Then
        temSQL = temSQL & " And UserID = " & Val(dtcUser.BoundText)
        temCancelSQL = temCancelSQL & " And CancelledUserID = " & Val(dtcUser.BoundText)
    End If
    
    If optCash.Value = True Then
        temSQL = temSQL & " AND BillType = 'Cash' "
        temCancelSQL = temCancelSQL & " AND BillType = 'Cash' "
    
    ElseIf optCredit.Value = True Then
        temSQL = temSQL & " AND BillType = 'Company' "
        temCancelSQL = temCancelSQL & " AND BillType = 'Company' "
    
    ElseIf optWard.Value = True Then
        temSQL = temSQL & " AND BillType = 'Inward' "
        temCancelSQL = temCancelSQL & " AND BillType = 'Inward' "
    
    End If
    
    
    Dim temDblArray() As Double
    Dim temRtnArray() As Double
    
    temDblArray = FillAnyGrid(temSQL, gridBill, 1, D, p)
    temRtnArray = FillAnyGrid(temCancelSQL, gridCancel, 1, D, p)
    
    If dtpFrom.Value = dtpTo.Value Then
        txtTIncome.Text = Format(temDblArray(3) - temRtnArray(3), "0.00")
    Else
        txtTIncome.Text = Format(temDblArray(4) - temRtnArray(4), "0.00")
    End If
    
    With gridBill
        If .Rows > 8 Then
            .TopRow = .Rows - 8
        End If
    End With
End Sub

Private Sub FillCombos()
    With rsViewStaff
        If .State = 1 Then .Close
        temSQL = "SELECT tblStaff.* FROM tblStaff"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcUser
        Set .RowSource = rsViewStaff
        .ListField = "Name"
        .BoundColumn = "StaffID"
    End With
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub

Private Sub GridBill_Click()
    Dim newViewForm As New frmViewPatientIx
    With gridBill
        .col = 0
        If IsNumeric(.TextMatrix(.Row, 0)) = True Then
            newViewForm.txtPatientIxBillID = Val(.TextMatrix(.Row, 0))
            newViewForm.Show
        End If
    End With
End Sub

Private Sub optAll_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub optCash_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub optCredit_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub optWard_Click()
    Call FormatGrid
    Call FillGrid
End Sub
