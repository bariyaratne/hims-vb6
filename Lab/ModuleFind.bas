Attribute VB_Name = "ModuleFind"
Option Explicit
'    Dim rsPrice As New ADODB.Recordset
    Dim temSQL As String

    Public Enum ControlType
        TextBox = 1
        ComboBox = 2
        ListBox = 3
        DataCombo = 4
        DataList = 5
        Grid = 6
        Button = 7
        MenuItem = 8
        CheckBox = 9
        OptionButton = 10
        DateTimePicker = 11
        Label = 12
        SSTab = 13
        Unknown = 100
    End Enum
    
Public Sub DrawBorder()
'    Printer.Line (0, 0)-(Printer.Width, 0)
    Printer.Line (0, 0)-(0, Printer.Height)
    Printer.Line (Printer.Width, 0)-(Printer.Width, Printer.Height)
    'Printer.Line (0, Printer.Height)-(Printer.Width, Printer.Height)
End Sub

        


Public Function SupplierPaymentMethodID(SupplierID As Long)
    Dim rsSupplierPM As New ADODB.Recordset
    With rsSupplierPM
        If .State = 1 Then .Close
        temSQL = "Select * from tblSupplier where SupplierID = " & SupplierID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!PaymentMethodID) = False Then
                SupplierPaymentMethodID = !PaymentMethodID
            Else
                SupplierPaymentMethodID = 0
            End If
        Else
            SupplierPaymentMethodID = 0
        End If
        .Close
    End With
    Set rsSupplierPM = Nothing
End Function

Public Sub VisibleControls(MyForm As Form)
    Dim MyControl As Control
    Dim temText As String
    Dim rsTem As New ADODB.Recordset
    On Error Resume Next
    For Each MyControl In MyForm.Controls
        With rsTem
            If .State = 1 Then .Close
            temSQL = "Select * from tblUserAuthorityControl where AuthorityID = " & UserPositionID & " AND ControlID = " & GetControlID(GetFormID(MyForm.Name, MyForm.Caption), MyControl)
            .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
            If .RecordCount > 0 Then
                MyControl.Visible = !Visible
            Else
                MyControl.Visible = True
            End If
            .Close
        End With
    Next
    For Each MyControl In MyForm.Controls
        With rsTem
            If isMainMenu(GetControlID(GetFormID(MyForm.Name, MyForm.Caption), MyControl)) = True Then
                VisibleMainControl GetControlID(GetFormID(MyForm.Name, MyForm.Caption), MyControl), MyForm
            End If
        End With
    Next

End Sub

Private Function isMainMenu(ControlID As Long) As Boolean
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblControl where ControlID = " & ControlID & " And Deleted = False"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If !MainMenu = True Then
                isMainMenu = True
            Else
                isMainMenu = False
            End If
        Else
            isMainMenu = False
        End If
        .Close
    End With
End Function

Private Sub VisibleMainControl(ControlID As Long, MyForm As Form)
    Dim MyControl As Control
    Dim temText As String
    Dim rsTem As New ADODB.Recordset
    'On Error Resume Next
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT Count(tblControl.ControlID) AS CountOfControlID " & _
                    "FROM tblControl RIGHT JOIN tblUserAuthorityControl ON tblControl.ControlID = tblUserAuthorityControl.ControlID " & _
                    "WHERE (((tblControl.MainMenuID)=" & ControlID & ") AND ((tblUserAuthorityControl.AuthorityID)=" & UserPositionID & ") AND ((tblUserAuthorityControl.Visible)=True))"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If !CountOfControlID > 0 Then
            MyControl.Visible = True
        Else
            MyControl.Visible = False
        End If
        .Close
    End With
End Sub

Private Function GetControlID(FormID As Long, MyControl As Control) As Long
    Dim i As Integer
    GetControlID = 0
    Dim rsForm As New ADODB.Recordset
    Dim rsTem As New ADODB.Recordset
            With rsForm
                If TypeOf MyControl Is SSTab Then
                    For i = 0 To MyControl.Tabs - 1
                        If .State = 1 Then .Close
                        temSQL = "Select * from tblCOntrol where FormID = " & FormID & " AND COntrol = '" & MyControl.Name & "' AND ControlIndex = " & i
                        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
                        MyControl.Tab = i
                        If .RecordCount > 0 Then
                            !ControlText = GetControlText(MyControl)
                        Else
                            .AddNew
                            !FormID = FormID
                            !Control = MyControl.Name
                            !ControlType = GetControlType(MyControl)
                            !ControlText = GetControlText(MyControl)
                            !ControlIndex = i
                        End If
                        .Update
                        GetControlID = !ControlID
                        .Close
                    Next i
                Else
                    If .State = 1 Then .Close
                    temSQL = "Select * from tblCOntrol where FormID = " & FormID & " AND COntrol = '" & MyControl.Name & "'"
                    .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
                    If .RecordCount > 0 Then
                        !ControlText = GetControlText(MyControl)
                    Else
                        .AddNew
                        !FormID = FormID
                        !Control = MyControl.Name
                        !ControlType = GetControlType(MyControl)
                        !ControlText = GetControlText(MyControl)
                    End If
                    .Update
                    GetControlID = !ControlID
                    .Close
                End If
            End With
End Function


Public Function GetControlType(ByRef MyControl As Control) As ControlType
    GetControlType = Unknown
    If TypeOf MyControl Is TextBox Then
        GetControlType = TextBox
    ElseIf TypeOf MyControl Is ComboBox Then
        GetControlType = ComboBox
    ElseIf TypeOf MyControl Is Button Then
        GetControlType = Button
    ElseIf TypeOf MyControl Is CheckBox Then
        GetControlType = CheckBox
    ElseIf TypeOf MyControl Is DataCombo Then
        GetControlType = DataCombo
    ElseIf TypeOf MyControl Is DataList Then
        GetControlType = DataList
    ElseIf TypeOf MyControl Is DTPicker Then
        GetControlType = DateTimePicker
    ElseIf TypeOf MyControl Is MSFlexGrid Then
        GetControlType = Grid
    ElseIf TypeOf MyControl Is Label Then
        GetControlType = Label
    ElseIf TypeOf MyControl Is ListBox Then
        GetControlType = ListBox
    ElseIf TypeOf MyControl Is Menu Then
        GetControlType = MenuItem
    ElseIf TypeOf MyControl Is OptionButton Then
        GetControlType = OptionButton
    ElseIf TypeOf MyControl Is SSTab Then
        GetControlType = SSTab
    End If

End Function

Public Function GetControlText(MyControl As Control) As String
    GetControlText = Empty
    If TypeOf MyControl Is TextBox Then
        GetControlText = MyControl.Text
    ElseIf TypeOf MyControl Is ComboBox Then
        GetControlText = MyControl.Text
    ElseIf TypeOf MyControl Is Button Then
        GetControlText = MyControl.Caption
    ElseIf TypeOf MyControl Is CheckBox Then
        GetControlText = MyControl.Caption
    ElseIf TypeOf MyControl Is DataCombo Then
        GetControlText = MyControl.Text
    ElseIf TypeOf MyControl Is DataList Then
        GetControlText = MyControl.Text
    ElseIf TypeOf MyControl Is DTPicker Then
        GetControlText = Right(MyControl.Name, Len(MyControl.Name) - 3)
    ElseIf TypeOf MyControl Is MSFlexGrid Then
        GetControlText = Right(MyControl.Name, Len(MyControl.Name) - 4)
    ElseIf TypeOf MyControl Is Label Then
        GetControlText = MyControl.Caption
    ElseIf TypeOf MyControl Is ListBox Then
        GetControlText = MyControl.Text
    ElseIf TypeOf MyControl Is Menu Then
        GetControlText = MyControl.Caption
    ElseIf TypeOf MyControl Is OptionButton Then
        GetControlText = MyControl.Caption
    ElseIf TypeOf MyControl Is SSTab Then
        GetControlText = MyControl.Caption
    End If
End Function

Public Function GetFormID(FormName As String, FormText As String) As Long
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblForm where Form = '" & FormName & "'"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !FormText = FormText
        Else
            .AddNew
            !FormText = FormText
            !Form = FormName
        End If
        .Update
        GetFormID = !FormID
        .Close
    End With
End Function
