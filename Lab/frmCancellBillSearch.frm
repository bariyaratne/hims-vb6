VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmCancellBillSearch 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Bill Search to Cancell"
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10530
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   10530
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   120
      TabIndex        =   14
      Top             =   1200
      Width           =   10335
      Begin VB.OptionButton optNonCash 
         Caption         =   "&Non Cash"
         Height          =   240
         Left            =   3360
         TabIndex        =   20
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton optAll 
         Caption         =   "&All"
         Height          =   240
         Left            =   240
         TabIndex        =   18
         Top             =   360
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optWard 
         Caption         =   "&Ward"
         Height          =   240
         Left            =   8760
         TabIndex        =   17
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton optCredit 
         Caption         =   "C&redit"
         Height          =   240
         Left            =   7320
         TabIndex        =   16
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton optCash 
         Caption         =   "&Cash"
         Height          =   240
         Left            =   1800
         TabIndex        =   15
         Top             =   360
         Width           =   855
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   9480
      TabIndex        =   7
      Top             =   8280
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   8280
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame2 
      Height          =   6255
      Left            =   120
      TabIndex        =   10
      Top             =   1920
      Width           =   10335
      Begin MSFlexGridLib.MSFlexGrid GridBill 
         Height          =   5895
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   10398
         _Version        =   393216
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   8
      Top             =   0
      Width           =   10335
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   960
         TabIndex        =   12
         Top             =   240
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   16580611
         CurrentDate     =   39773
      End
      Begin MSDataListLib.DataCombo dtcUser 
         Height          =   360
         Left            =   1920
         TabIndex        =   2
         Top             =   1200
         Visible         =   0   'False
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         _Version        =   393216
         Enabled         =   0   'False
         Text            =   ""
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   5880
         TabIndex        =   13
         Top             =   240
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   16580611
         CurrentDate     =   39773
      End
      Begin VB.Label Label2 
         Caption         =   "&User"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   1200
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "&From"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "&To"
         Height          =   255
         Left            =   5520
         TabIndex        =   0
         Top             =   240
         Width           =   1575
      End
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   1200
      TabIndex        =   19
      Top             =   8280
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   360
      TabIndex        =   11
      Top             =   7200
      Width           =   7695
      Begin VB.TextBox txtTIncome 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   525
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   5
         Text            =   "0.00"
         Top             =   210
         Width           =   3735
      End
      Begin VB.Label Label3 
         Caption         =   "Total  &Income"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmCancellBillSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim rsBill As New ADODB.Recordset
    Dim csetPrinter As New cSetDfltPrinter
    Dim temTopic As String
    Dim temSubTopic As String
    
Private Sub btnExcel_Click()
    GridToExcel GridBill, temTopic, temSubTopic
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnPrint_Click()
    Dim myPR As PrintReport
    GetPrintDefaults myPR
    GridPrint GridBill, myPR, temTopic, temSubTopic
End Sub

Private Sub dtpFrom_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub Form_Load()
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call FormatGrid
    Call FillGrid
    
End Sub

Private Sub FormatGrid()

    '   0   BillID
    '   1   Date
    '   2   Time
    '   3   Value
    With GridBill
        .Rows = 1
        .Cols = 6
        .Row = 0
        .col = 0
        .CellAlignment = 4
        .Text = "Bill ID."
        .col = 1
        .CellAlignment = 4
        .Text = "Type"
        .col = 2
        .CellAlignment = 4
        .Text = "Date"
        .col = 3
        .CellAlignment = 4
        .Text = "Time"
        .col = 4
        .Text = "Value"
        .col = 5
        .Text = "Remarks"
                
        
'        .Col = 4
'        .Text = "IxID"
        .ColWidth(0) = 1700
        .ColWidth(1) = 1900
        .ColWidth(2) = 1900
        .ColWidth(3) = 1350
        .ColWidth(4) = 1350
        .ColWidth(4) = 1550
'        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 100)
    End With
End Sub

Private Sub FillGrid()
    Dim TotalValue As Double
    With rsBill
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblPatientIxBill "
        temSQL = temSQL & " Where Date Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "# "
        If IsNumeric(dtcUser.BoundText) = True Then
            temSQL = temSQL & " And UserID = " & Val(dtcUser.BoundText)
        End If
        
        If optCash.Value = True Then
            temSQL = temSQL & " AND BillType = 'Cash' "
        ElseIf optNonCash.Value = True Then
            temSQL = temSQL & " AND ( BillType = 'Company' OR BillType = 'Inward' ) "
        ElseIf optCredit.Value = True Then
            temSQL = temSQL & " AND BillType = 'Company' "
        ElseIf optWard.Value = True Then
            temSQL = temSQL & " AND BillType = 'Inward' "
        End If
        
        temSQL = temSQL & " Order By tblPatientIxBill.PatientIxBillID"
        
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridBill.Rows = GridBill.Rows + 1
                GridBill.Row = GridBill.Rows - 1
                GridBill.col = 0
                GridBill.Text = !PatientIxBillID
                GridBill.col = 1
                GridBill.Text = Format(!BillType, "")
                GridBill.col = 2
                GridBill.Text = !Date
                GridBill.col = 3
                GridBill.Text = !time
                GridBill.col = 4
                If !Cancelled = False Then
                    GridBill.Text = Format(!NetValue, "0.00")
                    TotalValue = TotalValue + !NetValue
                Else
                    GridBill.col = 5
                    GridBill.Text = "Cancelled"
                End If
                
                .MoveNext
            Wend
        End If
        txtTIncome.Text = Format(TotalValue, "0.00")
    End With
End Sub

Private Sub GridBill_Click()
    Dim newViewForm As New frmCancellBill
    With GridBill
        .col = 0
        If IsNumeric(.TextMatrix(.Row, 0)) = True Then
            newViewForm.txtPatientIxBillID = Val(.TextMatrix(.Row, 0))
            newViewForm.Show
        End If
    End With
End Sub

Private Sub optAll_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub optCash_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub optCredit_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub optNonCash_Click()
    Call FormatGrid
    Call FillGrid

End Sub

Private Sub optWard_Click()
    Call FormatGrid
    Call FillGrid
End Sub
