VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim temSQL As String
    
    Dim IDValue As Long
    Dim IxValue As String
    Dim IxTubeIDValue As Long
    Dim IxTubeValue As String
    Dim IxSpecimanIDValue As Long
    Dim IxSpecimanValue As String
    Dim IxDepartmentIDValue As Long
    Dim IxDepartmentValue As String
    Dim IxCommentValue As String
    
Public Property Let ID(ByVal IxID As Long)
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT tblIx.*, tblDept.Dept, tblTube.Tube, tblIxCategory.IxCategory, tblSpeciman.Speciman " & _
                    "FROM tblDept RIGHT JOIN ((tblTube RIGHT JOIN (tblIxCategory RIGHT JOIN tblIx ON tblIxCategory.IxCategoryID = tblIx.IxCategoryID) ON tblTube.TubeID = tblIx.TubeID) LEFT JOIN tblSpeciman ON tblIx.SpecimanID = tblSpeciman.SpecimanID) ON tblDept.DeptID = tblIx.DeptID " & _
                    "WHERE (((tblIx.IxID)=" & IxID & "))"

        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            IDValue = IxID
            IxValue = !Ix
            If IsNull(!TubeID) = False Then IxTubeIDValue = !TubeID
            If IsNull(!Tube) = False Then IxTubeValue = !Tube
            If IsNull(!SpecimanID) = False Then IxSpecimanIDValue = !SpecimanID
            If IsNull(!Speciman) = False Then IxSpecimanValue = !Speciman
            If IsNull(!DeptID) = False Then IxDepartmentIDValue = !DeptID
            If IsNull(!Dept) = False Then IxDepartmentValue = !Dept
            If IsNull(!Comments) = False Then IxCommentValue = !Comments
        Else
            IDValue = IxID
            IxValue = Empty
            IxTubeIDValue = Empty
            IxTubeValue = Empty
            IxSpecimanIDValue = Empty
            IxSpecimanValue = Empty
            IxDepartmentIDValue = Empty
            IxDepartmentValue = Empty
            IxCommentValue = Empty
        End If
        .Close
    End With
End Property

Public Property Get IxID()
    IxID = IDValue
End Property

Public Property Get Ix()
    Ix = IxValue
End Property

Public Property Get Tube()
    Tube = IxTubeValue
End Property

Public Property Get TubeID()
    TubeID = IxTubeIDValue
End Property

Public Property Get Speciman()
    Speciman = IxSpecimanValue
End Property

Public Property Get SpecimanID()
    SpecimanID = IxSpecimanIDValue
End Property

Public Property Get Department()
    Department = IxDepartmentValue
End Property

Public Property Get DepartmentID()
    DepartmentID = IxDepartmentIDValue
End Property

Public Property Get Comments()
    Comments = IxCommentValue
End Property
