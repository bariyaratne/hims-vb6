VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmTube 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tube"
   ClientHeight    =   4665
   ClientLeft      =   2130
   ClientTop       =   1635
   ClientWidth     =   10920
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4665
   ScaleWidth      =   10920
   Begin VB.Frame Frame2 
      Height          =   3975
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   3615
      Begin MSDataListLib.DataCombo dtcTube 
         Height          =   2820
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   4974
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1320
         TabIndex        =   2
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   2520
         TabIndex        =   3
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3975
      Left            =   3960
      TabIndex        =   11
      Top             =   120
      Width           =   6855
      Begin VB.TextBox txtTube 
         Height          =   360
         Left            =   1920
         TabIndex        =   5
         Top             =   480
         Width           =   4455
      End
      Begin VB.TextBox txtcomment 
         Height          =   2175
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   960
         Width           =   4455
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   3360
         TabIndex        =   8
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4440
         TabIndex        =   9
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "&Tube"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "C&omments"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   9840
      TabIndex        =   10
      Top             =   4200
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmTube"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsTube As New ADODB.Recordset
    Dim rsViewTube As New ADODB.Recordset
    Dim temSql As String

Private Sub bttnCancel_Click()
    Call SelectMode
    Call Clearvalues
    dtcTube.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnDelete_Click()
    With rsTube
        If .State = 1 Then .Close
        temSql = "Select * from tblTube where TubeID = " & Val(dtcTube.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
        End If
        If .State = 1 Then .Close
    End With
    Call FillCOmbos
    dtcTube.Text = Empty
End Sub

Private Sub dtcTube_Change()
    If IsNumeric(dtcTube.BoundText) = True Then
        Call DisplaySelected
        bttnEdit.Enabled = True
        bttnAdd.Enabled = False
        bttnDelete.Enabled = True
    Else
        Clearvalues
        bttnAdd.Enabled = True
        bttnEdit.Enabled = False
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub Form_Load()
    FillCOmbos
    SelectMode
    Clearvalues
End Sub

Private Sub bttnAdd_Click()
    Dim temText As String
    If IsNumeric(dtcTube.Text) = True Then
        temText = Empty
    Else
        temText = dtcTube.Text
    End If
    EditMode
    Clearvalues
    dtcTube.Text = Empty
    txtTube.Text = temText
    txtTube.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    EditMode
    txtTube.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub ChangeData()
    Dim TemResponce As Integer
    If txtTube.Text = "" Then
        MsgBox "No Name for the tube"
        txtTube.SetFocus
        Exit Sub
    End If
    With rsTube
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblTube Where TubeID = " & Val(dtcTube.BoundText), cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount = 0 Then Exit Sub
        !Tube = Trim(txtTube.Text)
        !Comments = txtcomment.Text
        .Update
        If .State = 1 Then .Close
        FillCOmbos
        SelectMode
        Clearvalues
        dtcTube.Text = Empty
        dtcTube.SetFocus
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        SelectMode
        dtcTube.Text = Empty
        dtcTube.SetFocus
        If .State = 1 Then .Close
    End With
End Sub

Private Sub bttnSave_Click()
    Dim TemResponce As Integer
    If Trim(txtTube.Text) = "" Then
        MsgBox "Please enter a name"
        txtTube.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcTube.BoundText) = True Then
        Call ChangeData
    Else
        Call SaveData
    End If
    FillCOmbos
    SelectMode
    Clearvalues
    dtcTube.Text = Empty
    dtcTube.SetFocus
End Sub

Private Sub SaveData()
    Dim i As Integer
    With rsTube
        On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblTube", cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Tube = Trim(txtTube.Text)
        !Comments = txtcomment.Text
        .Update
        If .State = 1 Then .Close
        Exit Sub
    
ErrorHandler:
        i = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
    
End Sub


Private Sub FillCOmbos()
    With rsViewTube
        If .State = 1 Then .Close
        temSql = "SELECT tblTube.* FROM tblTube WHERE Deleted = False Order by Tube"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcTube
        Set .RowSource = rsViewTube
        .ListField = "Tube"
        .BoundColumn = "TubeID"
    End With
End Sub

Private Sub EditMode()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcTube.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    txtcomment.Enabled = True
    txtTube.Enabled = True
End Sub

Private Sub SelectMode()
    
    bttnAdd.Enabled = True
    bttnEdit.Enabled = False
    dtcTube.Enabled = True
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    txtcomment.Enabled = False
    txtTube.Enabled = False
End Sub

Private Sub Clearvalues()
    txtTube.Text = Empty
    txtcomment.Text = Empty
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If rsTube.State = 1 Then rsTube.Close: Set rsTube = Nothing
    If rsViewTube.State = 1 Then rsViewTube.Close: Set rsViewTube = Nothing
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcTube.BoundText) Then Exit Sub
    With rsTube
        If .State = 1 Then .Close
        .Open "Select * From tblTube Where (TubeID = " & dtcTube.BoundText & ")", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        txtTube.Text = !Tube
        If Not IsNull(!Comments) Then txtcomment.Text = !Comments
        If .State = 1 Then .Close
    End With
End Sub

