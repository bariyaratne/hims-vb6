VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1215
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   1215
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnBackup 
      Caption         =   "Backup"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4335
   End
   Begin VB.Timer frmBackup 
      Interval        =   60000
      Left            =   120
      Top             =   720
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim myCount As Integer
    Dim bc As Integer
    
Public Sub backupDbs()
    bc = bc + 1
    If bc Mod 4 = 0 Then
        backUpDbsToPath "\\PHARMACY\SharedDocs\Backups"
    ElseIf bc Mod 4 = 1 Then
         backUpDbsToPath "\\CEO\SharedDocs\Backups"
    ElseIf bc Mod 4 = 2 Then
        backUpDbsToPath "\\RECEPTION\SharedDocs\Backups"
    ElseIf bc Mod 4 = 3 Then
    
    End If
End Sub

Public Sub backUpDbsToPath(desPath As String)
    On Error Resume Next
    Dim fSys As New FileSystemObject
    Dim strFolder As String
    strFolder = desPath & "\" & Format(Date, "dd-MMMM-yyyy-") & Format(Time, "mm-hh-ss")
    fSys.CreateFolder (strFolder)
    fSys.CopyFile App.Path & "\eLab.mdb", strFolder & "\eLab.mdb", True
    fSys.CopyFile App.Path & "\Hospital.mdb", strFolder & "\Hospital.mdb", True
End Sub

Private Sub btnBackup_Click()
    backupDbs
End Sub

Private Sub frmBackup_Timer()
    If myCount > 1000 Then
        backupDbs
        myCount = 0
    Else
        myCount = myCount + 1
    End If
End Sub
