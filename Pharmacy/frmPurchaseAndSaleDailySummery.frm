VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPurchaseAndSaleDailySummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Summery Report - Purchase & Sale"
   ClientHeight    =   7410
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10095
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   10095
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1560
      TabIndex        =   3
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8760
      TabIndex        =   2
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   6720
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridSum 
      Height          =   6615
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   11668
      _Version        =   393216
   End
End
Attribute VB_Name = "frmPurchaseAndSaleDailySummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim saleCats As New Collection


Private Sub btnExcel_Click()
    GridToExcel gridSum, "Daily Purchase and Sale Summery - " & UserStore, Format(Date, LongDateFormat)
End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    GridPrint gridSum, ThisReportFormat, "Daily Purchase and Sale Summery - " & UserStore, Format(Date, LongDateFormat)
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    DisplayDetails
End Sub

Private Sub Form_Load()
    GetSettings
    Date = Date
    DisplayDetails
End Sub

Private Sub GetSettings()
    GetCommonSettings Me
    Set saleCats = findAllSaleCategories
End Sub



Private Sub SaveSettings()
    SaveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub

Private Sub DisplayDetails()
    Dim temCat As SaleCategory
    Dim Col As Integer
    Dim Row As Integer
    Dim temRow As Integer
    
    
    Dim todayTotalSale As Double
    Dim todayTotalPurchase As Double
    Dim ydayTotalSale As Double
    Dim ydayTotalPurchase As Double
    
    
    With gridSum
        Col = 1
        .Rows = 18 + saleCats.Count
        .Cols = 4
        
        .TextMatrix(0, 1) = "Upto Day Before"
        .TextMatrix(0, 2) = "This Date"
        .TextMatrix(0, 3) = "Upto today"

        .TextMatrix(1, 0) = "Received From Units"
        .TextMatrix(2, 0) = "Purchases"
        .TextMatrix(3, 0) = "Total"
        
        .TextMatrix(4, 0) = "Price Change"
        .TextMatrix(5, 0) = "Transfer"
        
        .TextMatrix(6, 0) = "Total"
        
        .TextMatrix(7, 0) = "Opening Balance"
        .TextMatrix(8, 0) = "Grand Total"
        
        .TextMatrix(9, 0) = "Less"

        temRow = 11
        For Each temCat In saleCats
            .TextMatrix(temRow, 0) = temCat.SaleCategory
            temRow = temRow + 1
        Next
        
        .TextMatrix(temRow, 0) = "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Returns"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Discard"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Remainng Stock Value"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Total"



'    *********************************************************

        .TextMatrix(1, 1) = findGrossPurchase(Date - 1, Date - 1, UserStoreID, False, True)
        .TextMatrix(2, 1) = findGrossPurchase(Date - 1, Date - 1, UserStoreID, True, False)
        ydayTotalPurchase = Val(.TextMatrix(1, 2)) + Val(.TextMatrix(2, 2))
        .TextMatrix(3, 1) = ydayTotalPurchase
        
        .TextMatrix(4, 1) = ""
        .TextMatrix(5, 1) = ""
        
        .TextMatrix(6, 1) = "" '
        
        .TextMatrix(7, 1) = "" ' "Opening Balance"
        .TextMatrix(8, 1) = ydayTotalPurchase
        
        .TextMatrix(9, 1) = "Less"

        temRow = 11
        
        Dim temTot As Double
        Dim temVal As Double
        
        temTot = 0
        For Each temCat In saleCats
            temVal = findNetSale(Date - 1, Date - 1, temCat.SaleCategoryID, UserStoreID)
            temTot = temTot + temVal
            .TextMatrix(temRow, 1) = temVal
            temRow = temRow + 1
        Next
        
        ydayTotalSale = temTot
        
        .TextMatrix(temRow, 1) = temTot
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" 'to do
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = ""
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" ' "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" ' "Remainng Stock Value"
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" '"Total"

    ' **********************************************


'    *********************************************************

        .TextMatrix(1, 2) = findGrossPurchase(Date, Date, UserStoreID, False, True)
        .TextMatrix(2, 2) = findGrossPurchase(Date, Date, UserStoreID, True, False)
        .TextMatrix(3, 2) = Val(.TextMatrix(1, 1)) + Val(.TextMatrix(2, 1))
        
        .TextMatrix(4, 2) = 0
        .TextMatrix(5, 2) = 0
        
        .TextMatrix(6, 2) = "" '
        
        .TextMatrix(7, 2) = "" ' "Opening Balance"
        .TextMatrix(8, 2) = "" ' "Grand Total"
        
        .TextMatrix(9, 2) = "Less"

        temRow = 11
        
        
        temTot = 0
        For Each temCat In saleCats
            temVal = findNetSale(Date, Date, temCat.SaleCategoryID, UserStoreID)
            temTot = temTot + temVal
            .TextMatrix(temRow, 2) = temVal
            temRow = temRow + 1
        Next
        
        
        .TextMatrix(temRow, 2) = temTot
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = 0 'to do
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = 0
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = 0 ' "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = findStockValue(UserStoreID, 0) ' "Remainng Stock Value"
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = "" '"Total"

    ' **********************************************
    
    
    
    
    
'    *********************************************************

        Dim startDate As Date

        startDate = DateSerial(Year(Date), 1, 1)

        .TextMatrix(1, 3) = findGrossPurchase(startDate, Date, UserStoreID, False, True)
        .TextMatrix(2, 3) = findGrossPurchase(startDate, Date, UserStoreID, True, False)
        .TextMatrix(3, 3) = Val(.TextMatrix(1, 1)) + Val(.TextMatrix(2, 1))
        
        .TextMatrix(4, 3) = 0
        .TextMatrix(5, 3) = 0
        
        .TextMatrix(6, 3) = "" '
        
        .TextMatrix(7, 3) = "" ' "Opening Balance"
        .TextMatrix(8, 3) = "" ' "Grand Total"
        
        .TextMatrix(9, 3) = "Less"

        temRow = 11
        
        
        temTot = 0
        For Each temCat In saleCats
            temVal = findNetSale(startDate, Date, temCat.SaleCategoryID, UserStoreID)
            temTot = temTot + temVal
            .TextMatrix(temRow, 3) = temVal
            temRow = temRow + 1
        Next
        
        
        .TextMatrix(temRow, 3) = temTot
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = 0 'to do
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = 0
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = 0 ' "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = findStockValue(UserStoreID, 0) ' "Remainng Stock Value"
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = "" '"Total"

    ' **********************************************
    
    End With
End Sub
