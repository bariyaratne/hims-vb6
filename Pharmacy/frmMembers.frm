VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMembers 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Members"
   ClientHeight    =   9585
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12165
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9585
   ScaleWidth      =   12165
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4200
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame frameSearch 
      Height          =   8775
      Left            =   120
      TabIndex        =   40
      Top             =   120
      Width           =   4575
      Begin MSDataListLib.DataCombo dtcStaff 
         Height          =   7380
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   13018
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   495
         Left            =   240
         TabIndex        =   1
         Top             =   8040
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   873
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   495
         Left            =   2520
         TabIndex        =   2
         Top             =   8040
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   873
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   495
         Left            =   2520
         TabIndex        =   31
         Top             =   8040
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   873
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frameDetails 
      Height          =   8775
      Left            =   4920
      TabIndex        =   37
      Top             =   120
      Width           =   6975
      Begin TabDlg.SSTab SSTabMain 
         Height          =   7695
         Left            =   120
         TabIndex        =   36
         Top             =   240
         Width           =   6765
         _ExtentX        =   11933
         _ExtentY        =   13573
         _Version        =   393216
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Personal"
         TabPicture(0)   =   "frmMembers.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label21"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label20"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label11"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label12"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label13"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Label18"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Label2"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Label3"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "Label4"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "Label5"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "Label27"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcSex"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtcTitle"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtName"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtQualifications"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtRegistation"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtDesignation"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtListedName"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtPrivateAddress"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtPrivateTel"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtPrivateFax"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtPrivateEmail"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtPrivateMobile"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Photos"
         TabPicture(1)   =   "frmMembers.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label14"
         Tab(1).Control(1)=   "Label8"
         Tab(1).Control(2)=   "imgSignature"
         Tab(1).Control(3)=   "imgPhoto"
         Tab(1).Control(4)=   "bttnSigDelete"
         Tab(1).Control(5)=   "bttnSigLoad"
         Tab(1).Control(6)=   "bttnPhotoDelete"
         Tab(1).Control(7)=   "txtPhoto"
         Tab(1).Control(8)=   "txtSignature"
         Tab(1).Control(9)=   "bttnPhotoLoad"
         Tab(1).ControlCount=   10
         Begin VB.TextBox txtPrivateMobile 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   22
            Top             =   4920
            Width           =   4695
         End
         Begin VB.TextBox txtPrivateEmail 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   26
            Top             =   6120
            Width           =   4695
         End
         Begin VB.TextBox txtPrivateFax 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   24
            Top             =   5520
            Width           =   4695
         End
         Begin VB.TextBox txtPrivateTel 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   20
            Top             =   4440
            Width           =   4695
         End
         Begin VB.TextBox txtPrivateAddress 
            Height          =   735
            Left            =   1920
            MultiLine       =   -1  'True
            TabIndex        =   18
            Top             =   3480
            Width           =   4695
         End
         Begin btButtonEx.ButtonEx bttnPhotoLoad 
            Height          =   255
            Left            =   -74520
            TabIndex        =   32
            Top             =   900
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   450
            Appearance      =   3
            Caption         =   "Load"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.TextBox txtSignature 
            Height          =   360
            Left            =   -74640
            TabIndex        =   42
            Top             =   6060
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtPhoto 
            Height          =   360
            Left            =   -74520
            TabIndex        =   41
            Top             =   2820
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtListedName 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   10
            Top             =   1680
            Width           =   3975
         End
         Begin VB.TextBox txtDesignation 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   16
            Top             =   3060
            Width           =   3975
         End
         Begin VB.TextBox txtRegistation 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   14
            Top             =   2580
            Width           =   3975
         End
         Begin VB.TextBox txtQualifications 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   12
            Top             =   2100
            Width           =   3975
         End
         Begin VB.TextBox txtName 
            Height          =   375
            Left            =   1920
            MaxLength       =   100
            TabIndex        =   8
            Top             =   1140
            Width           =   3975
         End
         Begin MSDataListLib.DataCombo dtcTitle 
            Height          =   360
            Left            =   1920
            TabIndex        =   4
            Top             =   660
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   635
            _Version        =   393216
            Style           =   2
            ListField       =   ""
            BoundColumn     =   ""
            Text            =   ""
            Object.DataMember      =   ""
         End
         Begin MSDataListLib.DataCombo dtcSex 
            Height          =   360
            Left            =   4560
            TabIndex        =   6
            Top             =   600
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   635
            _Version        =   393216
            Style           =   2
            ListField       =   ""
            BoundColumn     =   ""
            Text            =   ""
            Object.DataMember      =   ""
         End
         Begin btButtonEx.ButtonEx bttnPhotoDelete 
            Height          =   255
            Left            =   -74520
            TabIndex        =   33
            Top             =   1260
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   450
            Appearance      =   3
            Caption         =   "Delete"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx bttnSigLoad 
            Height          =   255
            Left            =   -74640
            TabIndex        =   34
            Top             =   5220
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   450
            Appearance      =   3
            Caption         =   "Load"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx bttnSigDelete 
            Height          =   255
            Left            =   -74640
            TabIndex        =   35
            Top             =   5580
            Width           =   975
            _ExtentX        =   1720
            _ExtentY        =   450
            Appearance      =   3
            Caption         =   "Delete"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label27 
            BackStyle       =   0  'Transparent
            Caption         =   "Mobile"
            Height          =   375
            Left            =   240
            TabIndex        =   21
            Top             =   4920
            Width           =   2175
         End
         Begin VB.Label Label5 
            BackStyle       =   0  'Transparent
            Caption         =   "E-Mail"
            Height          =   375
            Left            =   240
            TabIndex        =   25
            Top             =   6120
            Width           =   2175
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "Fax"
            Height          =   375
            Left            =   240
            TabIndex        =   23
            Top             =   5640
            Width           =   2175
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Telephone"
            Height          =   375
            Left            =   240
            TabIndex        =   19
            Top             =   4440
            Width           =   2175
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Address"
            Height          =   375
            Left            =   240
            TabIndex        =   17
            Top             =   3720
            Width           =   2175
         End
         Begin VB.Image imgPhoto 
            BorderStyle     =   1  'Fixed Single
            Height          =   4575
            Left            =   -73320
            Top             =   180
            Width           =   4815
         End
         Begin VB.Image imgSignature 
            BorderStyle     =   1  'Fixed Single
            Height          =   855
            Left            =   -73320
            Top             =   4980
            Width           =   4815
         End
         Begin VB.Label Label18 
            BackStyle       =   0  'Transparent
            Caption         =   "Code"
            Height          =   375
            Left            =   240
            TabIndex        =   11
            Top             =   1560
            Width           =   2175
         End
         Begin VB.Label Label13 
            BackStyle       =   0  'Transparent
            Caption         =   "Designation"
            Height          =   375
            Left            =   240
            TabIndex        =   15
            Top             =   3060
            Width           =   2175
         End
         Begin VB.Label Label12 
            BackStyle       =   0  'Transparent
            Caption         =   "Registation"
            Height          =   375
            Left            =   240
            TabIndex        =   13
            Top             =   2580
            Width           =   2175
         End
         Begin VB.Label Label11 
            BackStyle       =   0  'Transparent
            Caption         =   "Qualifications"
            Height          =   375
            Left            =   240
            TabIndex        =   9
            Top             =   2040
            Width           =   2175
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Name"
            Height          =   375
            Left            =   240
            TabIndex        =   7
            Top             =   1140
            Width           =   2175
         End
         Begin VB.Label Label20 
            BackStyle       =   0  'Transparent
            Caption         =   "Title"
            Height          =   375
            Left            =   240
            TabIndex        =   3
            Top             =   660
            Width           =   2175
         End
         Begin VB.Label Label21 
            Caption         =   "Sex"
            Height          =   375
            Left            =   3840
            TabIndex        =   5
            Top             =   600
            Width           =   855
         End
         Begin VB.Label Label8 
            BackStyle       =   0  'Transparent
            Caption         =   "Signature"
            Height          =   375
            Left            =   -74880
            TabIndex        =   39
            Top             =   4860
            Width           =   2175
         End
         Begin VB.Label Label14 
            BackStyle       =   0  'Transparent
            Caption         =   "Photo"
            Height          =   375
            Left            =   -74640
            TabIndex        =   38
            Top             =   300
            Width           =   2175
         End
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   495
         Left            =   120
         TabIndex        =   27
         Top             =   8040
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Cancel          =   -1  'True
         Height          =   495
         Left            =   4800
         TabIndex        =   29
         Top             =   8040
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnChange 
         Height          =   495
         Left            =   1440
         TabIndex        =   28
         Top             =   8040
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin btButtonEx.ButtonEx ButtonEx1 
      Height          =   495
      Left            =   10680
      TabIndex        =   30
      Top             =   9000
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "E&xit"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmMembers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsStaff As New ADODB.Recordset
    Dim rsTitle As New ADODB.Recordset
    Dim rsAuthority As New ADODB.Recordset
    Dim rsSex As New ADODB.Recordset
    Dim rsSpeciality As New ADODB.Recordset
    Dim rsTemStaff As New ADODB.Recordset
    Dim temSQL As String
    Dim TemUserName As String
    Dim TemStaffID As Long

Private Sub BeforeAddEdit()
    frameSearch.Enabled = True
    frameDetails.Enabled = False
    bttnSave.Visible = False
    bttnChange.Visible = False
    bttnCancel.Visible = False
End Sub

Private Sub AfterAdd()
    frameSearch.Enabled = False
    frameDetails.Enabled = True
    bttnSave.Visible = True
    bttnChange.Visible = False
    bttnCancel.Visible = True
End Sub

Private Sub AfterEdit()
    frameSearch.Enabled = False
    frameDetails.Enabled = True
    bttnSave.Visible = False
    bttnChange.Visible = True
    bttnCancel.Visible = True
End Sub

Private Sub ClearValues()
    
    Me.txtDesignation.text = Empty
    Me.txtListedName.text = Empty
    Me.txtName.text = Empty
    Me.txtPrivateAddress.text = Empty
    Me.txtPrivateEmail.text = Empty
    Me.txtPrivateFax.text = Empty
    Me.txtPrivateMobile.text = Empty
    Me.txtPrivateTel.text = Empty
    Me.txtQualifications.text = Empty
    Me.txtRegistation.text = Empty
    Me.dtcSex.text = Empty
    Me.dtcStaff.text = Empty
    Me.dtcTitle.text = Empty
    Me.txtPhoto.text = Empty
    Me.txtSignature.text = Empty
    imgPhoto.Picture = LoadPicture()
    imgSignature.Picture = LoadPicture()
End Sub

Private Function CanSave() As Boolean
    Dim tr As Integer
    CanSave = False
        If Trim(Me.txtListedName.text) = Empty Then
            txtListedName.text = txtName.text
        End If
        If Trim(Me.txtName.text) = Empty Then
            tr = MsgBox("You have not entered tha Name", vbCritical, "Name")
            SSTabMain.Tab = 0
            txtName.SetFocus
            Exit Function
        End If
    CanSave = True
End Function

Private Sub LocateDetails()
    With rsTemStaff
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblstaff where staffID = " & dtcStaff.BoundText
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If Not IsNull(!TitleID) Then dtcTitle.BoundText = !TitleID
            If Not IsNull(!SexID) Then dtcSex.BoundText = !SexID
            If Not IsNull(!Name) Then txtName.text = !Name
            If Not IsNull(!Code) Then txtListedName.text = !Code
            If Not IsNull(!Qualifications) Then txtQualifications.text = !Qualifications
            If Not IsNull(!Registation) Then txtRegistation.text = !Registation
            If Not IsNull(!Designation) Then txtDesignation.text = !Designation
            If Not IsNull(!PrivateAddress) Then txtPrivateAddress.text = !PrivateAddress
            If Not IsNull(!PrivatePhone) Then txtPrivateTel.text = !PrivatePhone
            If Not IsNull(!PrivateFax) Then txtPrivateFax.text = !PrivateFax
            If Not IsNull(!PrivateEmail) Then txtPrivateEmail.text = !PrivateEmail
            If Not IsNull(!MobilePhone) Then txtPrivateMobile.text = !MobilePhone
        End If
        .Close
    End With
End Sub

Private Sub SaveDetails()
    On Error Resume Next
    With rsTemStaff
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblstaff where StaffId = 0"
        .Open temSQL, cnnStores, adOpenDynamic, adLockOptimistic
        .AddNew
        If IsNumeric(dtcTitle.BoundText) Then !TitleID = dtcTitle.BoundText
        If IsNumeric(dtcSex.BoundText) Then !SexID = dtcSex.BoundText
        !Name = txtName.text
        !ListedName = txtName.text
        !Code = txtListedName.text
        !Qualifications = txtQualifications.text
        !IsMember = 1
        !IsStaff = 1
        !Registation = txtRegistation.text
        !Designation = txtDesignation.text
        !PrivateAddress = txtPrivateAddress.text
        !PrivatePhone = txtPrivateTel.text
        !PrivateFax = txtPrivateFax.text
        !PrivateEmail = txtPrivateEmail.text
        !MobilePhone = txtPrivateMobile.text
        !Photo = txtPhoto.text
        !Signature = txtSignature.text
        .Update
    End With
End Sub

Private Sub ChangeDetails()
    On Error Resume Next
    With rsTemStaff
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblstaff where staffID = " & dtcStaff.BoundText
        .Open temSQL, cnnStores, adOpenDynamic, adLockOptimistic
        If IsNumeric(dtcTitle.BoundText) Then !TitleID = dtcTitle.BoundText
        If IsNumeric(dtcSex.BoundText) Then !SexID = dtcSex.BoundText
        !Name = txtName.text
        !IsMember = 1
        !IsStaff = 0
        !ListedName = txtName.text
        !Code = txtListedName.text
        !Qualifications = txtQualifications.text
        !Registation = txtRegistation.text
        !Designation = txtDesignation.text
        !PrivateAddress = txtPrivateAddress.text
        !PrivatePhone = txtPrivateTel.text
        !PrivateFax = txtPrivateFax.text
        !PrivateEmail = txtPrivateEmail.text
        !MobilePhone = txtPrivateMobile.text
        !Photo = txtPhoto.text
        !Signature = txtSignature.text
        .Update
    End With
End Sub

Private Sub LoadPhoto(StaffID As Long)

End Sub

Private Sub SavePhoto(StaffID As Long)

End Sub


Private Sub bttnAdd_Click()
    TemUserName = Empty
    Call ClearValues
    Call AfterAdd
End Sub

Private Sub bttnCancel_Click()
    Call ClearValues
    Call BeforeAddEdit
End Sub

Private Sub bttnChange_Click()
    Call ChangeDetails
    Call ClearValues
    Call FillLists
    Call BeforeAddEdit
End Sub

Private Sub bttnEdit_Click()
    Dim tr As Integer
    If Not IsNumeric(dtcStaff.BoundText) Then
        tr = MsgBox("You have not selected a staff member to edit", vbCritical, "Staff?")
        dtcStaff.SetFocus
        Exit Sub
    End If
    Call AfterEdit
End Sub

Private Sub bttnPhotoDelete_Click()
    imgPhoto.Picture = LoadPicture()
    txtPhoto.text = Empty
End Sub

Private Sub bttnPhotoLoad_Click()
    Dim tr As Integer
    imgPhoto.Stretch = True
    CommonDialog1.Filter = "BMP|*.BMP|JPG|*.JPG;JPE;JPEG|GIF|*.GIF|All Images|*.BMP;*.JPG;*.JPE;*.JPGE;*.GIF|All Files|*.*"
    CommonDialog1.ShowOpen
    On Error GoTo PhotoError:
    imgPhoto.Picture = LoadPicture(CommonDialog1.FileName)
    txtPhoto.text = CommonDialog1.FileName
    Exit Sub
PhotoError:
    If Err.Number = 481 Then
        tr = MsgBox("The Photo you choose is not suitable, try using a medium size BMP, JPG or GIF file", vbOKOnly, "Photo Error")
    ElseIf Err.Number = 53 Then
        tr = MsgBox("No photo exist to selected, try to select again correctly.", vbOKOnly, "Photo Error")
    Else
        tr = MsgBox("An unknown error has occured, try again," & Chr(13) & Err.Description, vbOKOnly, "Photo Error")
    End If
End Sub

Private Sub bttnSave_Click()
    If CanSave = False Then Exit Sub
    Call SaveDetails
    Call ClearValues
    Call BeforeAddEdit
    Call FillLists
End Sub

Private Sub bttnSigDelete_Click()
    imgSignature.Picture = LoadPicture()
    txtSignature.text = Empty
End Sub

Private Sub bttnSigLoad_Click()
    Dim tr As Integer
    imgSignature.Stretch = True
    CommonDialog1.Filter = "BMP|*.BMP|JPG|*.JPG;JPE;JPEG|GIF|*.GIF|All Images|*.BMP;*.JPG;*.JPE;*.JPGE;*.GIF|All Files|*.*"
    CommonDialog1.ShowOpen
    On Error GoTo PhotoError:
    imgSignature.Picture = LoadPicture(CommonDialog1.FileName)
    txtSignature.text = CommonDialog1.FileName
    Exit Sub
PhotoError:
    If Err.Number = 481 Then
        tr = MsgBox("The Photo you choose is not suitable, try using a medium size BMP, JPG or GIF file", vbOKOnly, "Photo Error")
    ElseIf Err.Number = 53 Then
        tr = MsgBox("No photo exist to selected, try to select again correctly.", vbOKOnly, "Photo Error")
    Else
        tr = MsgBox("An unknown error has occured, try again," & Chr(13) & Err.Description, vbOKOnly, "Photo Error")
    End If
End Sub

Private Sub ButtonEx1_Click()
    Unload Me
End Sub

Private Sub dtcStaff_Click(Area As Integer)
    If IsNumeric(dtcStaff.BoundText) Then LocateDetails
End Sub

Private Sub Form_Load()
    
    Call FillLists
    Call BeforeAddEdit
    If UserAuthority = 6 Then
        bttnAdd.Enabled = False
        bttnEdit.Enabled = False
    End If
    SSTabMain.Tab = 0
End Sub

Private Sub FillLists()
    With rsStaff
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblstaff where IsMember =1 order by name"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcStaff
        Set .RowSource = rsStaff
        .ListField = "ListedName"
        .BoundColumn = "StaffID"
    End With
    With rsTitle
        If .State = 1 Then .Close
        temSQL = "SELECT * from tbltitle order by title"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcTitle
        Set dtcTitle.RowSource = rsTitle
        .ListField = "Title"
        .BoundColumn = "TitleID"
    End With
    With rsSex
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblsex"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcSex
        Set .RowSource = rsSex
        .ListField = "Sex"
        .BoundColumn = "SexID"
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If rsAuthority.State = 1 Then rsAuthority.Close
    If rsSex.State = 1 Then rsSex.Close
    If rsSpeciality.State = 1 Then rsSpeciality.Close
    If rsStaff.State = 1 Then rsStaff.Close
    If rsTitle.State = 1 Then rsTitle.Close
    If rsTemStaff.State = 1 Then rsTemStaff.Close
    Set rsAuthority = Nothing
    Set rsSex = Nothing
    Set rsStaff = Nothing
    Set rsTitle = Nothing
    Set rsSpeciality = Nothing
    Set rsTemStaff = Nothing
End Sub
