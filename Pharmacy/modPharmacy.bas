Attribute VB_Name = "modPharmacy"
Option Explicit
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Public lastDailyStockDate As Date


Public Sub recordDailyStock()
    If Date = lastDailyStockDate Then Exit Sub
    Dim rsSv As New ADODB.Recordset
    With rsSv
        If .State = 1 Then .Close
        temSQL = "select * from tblStockValueHx where valueDate = '" & Format(Date - 1, "dd MMMM yyyy") & "'"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            lastDailyStockDate = Date
        Else
            temSQL = "select * from tblStore"
            If .State = 1 Then .Close
            .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While .EOF = False
                recordStoreDailyStock !storeId
                .MoveNext
            Wend
            .Close
            lastDailyStockDate = Date
        End If
    End With
End Sub

Public Sub recordStoreDailyStock(storeId As Long)
    Dim rsHx As New ADODB.Recordset
    temSQL = "select * from tblStockValueHx where StoreId = " & storeId & " and valueDate = '" & Format(Date - 1, "dd MMMM yyyy") & "'"
    With rsHx
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !stockValue = findStockValue(storeId, 0)
        Else
            .AddNew
            !storeId = storeId
            !ValueDate = Date - 1
            !stockValue = findStockValue(storeId, 0)
        End If
        .Update
        .Close
    End With
End Sub

Public Sub fillSaleCats(combo As DataCombo)
    Dim rsCat As New ADODB.Recordset
    With rsCat
        If .State = 1 Then .Close
        temSQL = "Select * From tblSaleCategory Order by SaleCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Set combo.RowSource = rsCat
        combo.BoundColumn = "SaleCategoryID"
        combo.ListField = "SaleCategory"
    End With
End Sub

Public Sub fillBhts(combo As DataCombo)
    Dim rsCat As New ADODB.Recordset
    With rsCat
        If .State = 1 Then .Close
        temSQL = "Select * From tblBht Order by Bht"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Set combo.RowSource = rsCat
        combo.BoundColumn = "BhtId"
        combo.ListField = "Bht"
    End With
End Sub

Public Sub FillItems(combo As DataCombo)
    Dim rsDept As New ADODB.Recordset
    With rsDept
        If .State = 1 Then .Close
        .Open "Select tblItem.* From tblItem Order By Display", cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Set combo.RowSource = rsDept
        combo.ListField = "Display"
        combo.BoundColumn = "ItemID"
    End With
End Sub

Public Sub fillDepts(combo As DataCombo)
    Dim rsDept As New ADODB.Recordset
 With rsDept
        If .State = 1 Then .Close
        .Open "Select tblStore.* From tblStore Order By Store", cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Set combo.RowSource = rsDept
        combo.ListField = "Store"
        combo.BoundColumn = "StoreID"
    End With
End Sub

Public Function findSalePrice(ByVal ItemID As Long, Optional BatchID As Long) As Double
    With rsTem
        findSalePrice = 0
        If .State = 1 Then .Close
        
        
        If BatchID = 0 Then
            temSQL = "SELECT tblCurrentSalePrice.SPrice FROM tblCurrentSalePrice WHERE tblCurrentSalePrice.ItemID=" & ItemID & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentSalePrice.SPrice FROM tblCurrentSalePrice WHERE tblCurrentSalePrice.ItemID=" & ItemID & " And BatchId = " & BatchID & "Order By SetDate Desc, SetTime DESC"
        End If
        
        
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findSalePrice = !sprice
        End If
        If findSalePrice = 0 And BatchID <> 0 Then
            findSalePrice = findSalePrice(ItemID)
        End If
        End With
End Function

Public Function findPurchasePrice(ByVal ItemID As Long, Optional BatchID As Long) As Double
    With rsTem
        findPurchasePrice = 0
        If .State = 1 Then .Close
        If BatchID = 0 Then
            temSQL = "SELECT tblCurrentPurchasePrice.PPrice FROM tblCurrentPurchasePrice WHERE tblCurrentPurchasePrice.ItemID=" & ItemID & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentPurchasePrice.PPrice FROM tblCurrentPurchasePrice WHERE tblCurrentPurchasePrice.ItemID=" & ItemID & " And BatchId = " & BatchID & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findPurchasePrice = !pprice
        End If
        If findPurchasePrice = 0 And BatchID <> 0 Then
            findPurchasePrice = findPurchasePrice(ItemID)
        End If
    End With
End Function

Public Function findWholeSalePrice(ByVal ItemID As Long, Optional BatchID As Long) As Double
    With rsTem
        findWholeSalePrice = 0
        If .State = 1 Then .Close
        If BatchID = 0 Then
            temSQL = "SELECT tblCurrentWholeSalePrice.WPrice FROM tblCurrentWholeSalePrice WHERE tblCurrentWholeSalePrice.ItemID=" & ItemID & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentWholeSalePrice.WPrice FROM tblCurrentWholeSalePrice WHERE tblCurrentWholeSalePrice.ItemID=" & ItemID & " And BatchId = " & BatchID & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findWholeSalePrice = !wprice
        End If
        If findWholeSalePrice = 0 And BatchID <> 0 Then
            findWholeSalePrice = findWholeSalePrice(ItemID)
        End If
        End With
End Function

Public Function findAllSaleCategories() As Collection
    Dim cats As New Collection
    Dim temCat As SaleCategory
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblSaleCategory order by SaleCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            Set temCat = New SaleCategory
            temCat.SaleCategoryID = !SaleCategoryID
            cats.Add temCat
            .MoveNext
        Wend
        .Close
    End With
    Set findAllSaleCategories = cats
End Function


Public Function findNetSale(fromDate As Date, toDate As Date, CatId As Long, deptId As Long) As Double
    findNetSale = findGrossSale(fromDate, toDate, CatId, deptId) - findCancellAndReturn(fromDate, toDate, CatId, deptId)
End Function

Public Function findGrossSale(fromDate As Date, toDate As Date, CatId As Long, deptId As Long) As Double
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT sum(tblSaleBill.NetPrice) as sumNetTotal " & _
                    "FROM tblSaleBill " & _
                    "WHERE  "
        If CatId <> 0 Then
            temSQL = temSQL & " tblSaleBill.salecategoryid = " & CatId & " AND "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & "  tblSaleBill.Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumNetTotal) = False Then
            findGrossSale = !sumNetTotal
        Else
            findGrossSale = 0
        End If
    End With
End Function

Public Function findCancellAndReturn(fromDate As Date, toDate As Date, CatId As Long, deptId As Long) As Double
    With rsTem
        If .State = 1 Then .Close
         temSQL = "SELECT sum(tblReturnBill.NetPrice) AS sumReturn " & _
                    "FROM dbo.tblReturnBill LEFT OUTER JOIN dbo.tblSaleBill ON dbo.tblReturnBill.SaleBillID = dbo.tblSaleBill.SaleBillID "
        If CatId = 0 Then
            temSQL = temSQL & "WHERE tblsalebill.salecategoryid = " & CatId & " AND "
        Else
            temSQL = temSQL & "WHERE "
        End If
        If CatId <> 0 Then
            temSQL = temSQL & " tblSaleBill.salecategoryid = " & CatId & " AND "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & deptId & " AND "
        End If
        
        temSQL = temSQL & " ( tblReturnBill.Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' ) "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumReturn) = False Then
            findCancellAndReturn = !sumReturn
        Else
            findCancellAndReturn = 0
        End If
    End With
End Function


Public Function findGrossPurchase(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(NetPrice) AS SumOfPurchases " & _
                    "FROM dbo.tblRefillBill " & _
                    "WHERE  "
        If Purchase = True And unitReceive = True Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =1) AND "
        ElseIf Purchase = False And unitReceive = True Then
            temSQL = temSQL & " (purchase = 0 or ReceivedIssue =1) AND "
        ElseIf Purchase = True And unitReceive = False Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =0) AND "
        Else
            temSQL = temSQL & " "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & " Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!SumOfPurchases) = False Then
            findGrossPurchase = !SumOfPurchases
        Else
            findGrossPurchase = 0
        End If
    End With
End Function

Public Function findStockValue(deptId As Long, ItemID As Long) As Double
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(dbo.tblBatchStock.sprice * dbo.tblBatchStock.Stock)AS valueOfStock FROM dbo.tblBatch RIGHT OUTER JOIN dbo.tblBatchStock ON dbo.tblBatch.BatchID = dbo.tblBatchStock.BatchID " & _
                    "WHERE  "
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & "  "
        End If
        If ItemID <> 0 And deptId <> 0 Then
            temSQL = temSQL & " and "
        End If
        If ItemID <> 0 Then
            temSQL = temSQL & " ItemId = " & ItemID & " "
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!valueOfStock) = False Then
            findStockValue = !valueOfStock
        Else
            findStockValue = 0
        End If
    End With
End Function

Public Sub addBatchPrice()
    On Error Resume Next
    Dim temBatch As New Batch
    Dim temBs As BatchStock
    Dim rsTem1 As New ADODB.Recordset
    
    With rsTem1
        If .State = 1 Then .Close
        temSQL = "SELECT tblBatchStock.* FROM tblBatchStock"
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        .MoveLast
        .MoveFirst
        Dim i As Integer
        
        For i = 0 To .RecordCount - 1
            Set temBs = New BatchStock
            temBs.BatchStockID = !BatchStockID
            temBatch.BatchID = !BatchID
            temBs.sprice = findSalePrice(temBatch.ItemID, temBatch.BatchID)
            temBs.pprice = findPurchasePrice(temBatch.ItemID, temBatch.BatchID)
            temBs.wprice = findWholeSalePrice(temBatch.ItemID, temBatch.BatchID)
            temBs.saveData
            .MoveNext
        Next
        .Close
'        While .EOF = False
'            temBs.BatchStockID = !BatchStockID
'            temBatch.BatchID = !BatchID
'            temBs.sprice = findSalePrice(temBatch.ItemID, temBatch.BatchID)
'            temBs.pprice = findPurchasePrice(temBatch.ItemID, temBatch.BatchID)
'            temBs.wprice = findWholeSalePrice(temBatch.ItemID, temBatch.BatchID)
'            .MoveNext
'        Wend
'        .Close
    End With
End Sub



