VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaleDept"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varsaleDeptId As Long
    Private vardeptId As Long
    Private varsaleCatId As Long
    Private vardeleted As Boolean
    Private vardeletedUserId As Long
    Private vardeletedAt As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblSaleDept Where saleDeptId = " & varsaleDeptId
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !deptId = vardeptId
        !saleCatId = varsaleCatId
        !deleted = vardeleted
        !deletedUserId = vardeletedUserId
        !deletedAt = vardeletedAt
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varsaleDeptId = !NewID
        Else
            varsaleDeptId = !saleDeptId
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSaleDept WHERE saleDeptId = " & varsaleDeptId
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!saleDeptId) Then
               varsaleDeptId = !saleDeptId
            End If
            If Not IsNull(!deptId) Then
               vardeptId = !deptId
            End If
            If Not IsNull(!saleCatId) Then
               varsaleCatId = !saleCatId
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
            If Not IsNull(!deletedUserId) Then
               vardeletedUserId = !deletedUserId
            End If
            If Not IsNull(!deletedAt) Then
               vardeletedAt = !deletedAt
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varsaleDeptId = 0
    vardeptId = 0
    varsaleCatId = 0
    vardeleted = False
    vardeletedUserId = 0
    vardeletedAt = Empty
End Sub

Public Property Let saleDeptId(ByVal vsaleDeptId As Long)
    Call clearData
    varsaleDeptId = vsaleDeptId
    Call loadData
End Property

Public Property Get saleDeptId() As Long
    saleDeptId = varsaleDeptId
End Property

Public Property Let deptId(ByVal vdeptId As Long)
    vardeptId = vdeptId
End Property

Public Property Get deptId() As Long
    deptId = vardeptId
End Property

Public Property Let saleCatId(ByVal vsaleCatId As Long)
    varsaleCatId = vsaleCatId
End Property

Public Property Get saleCatId() As Long
    saleCatId = varsaleCatId
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property

Public Property Let deletedUserId(ByVal vdeletedUserId As Long)
    vardeletedUserId = vdeletedUserId
End Property

Public Property Get deletedUserId() As Long
    deletedUserId = vardeletedUserId
End Property

Public Property Let deletedAt(ByVal vdeletedAt As Date)
    vardeletedAt = vdeletedAt
End Property

Public Property Get deletedAt() As Date
    deletedAt = vardeletedAt
End Property


