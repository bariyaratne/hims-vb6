VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Begin VB.Form frmSaleDept 
   Caption         =   "Sale Departments"
   ClientHeight    =   5160
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8670
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5160
   ScaleWidth      =   8670
   Begin VB.CommandButton btnRemove 
      Caption         =   "Remove"
      Height          =   375
      Left            =   7320
      TabIndex        =   7
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "&Close"
      Height          =   375
      Left            =   6000
      TabIndex        =   6
      Top             =   4680
      Width           =   1215
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   375
      Left            =   7320
      TabIndex        =   5
      Top             =   840
      Width           =   1215
   End
   Begin MSDataListLib.DataList cmbUnitCat 
      Height          =   3180
      Left            =   1920
      TabIndex        =   4
      Top             =   1440
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   5609
      _Version        =   393216
   End
   Begin MSDataListLib.DataCombo cmbUnit 
      Height          =   360
      Left            =   1920
      TabIndex        =   2
      Top             =   360
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   635
      _Version        =   393216
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   1920
      TabIndex        =   3
      Top             =   840
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   635
      _Version        =   393216
      Text            =   ""
   End
   Begin VB.Label Label2 
      Caption         =   "Unit"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Sale Category"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   1215
   End
End
Attribute VB_Name = "frmSaleDept"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsCat As New ADODB.Recordset
    Dim rsDept As New ADODB.Recordset
    Dim rsDeptCat As New ADODB.Recordset
    Dim tsmSql As String
    Dim temSQL As String

Private Sub btnAdd_Click()
    If IsNumeric(cmbCat.BoundText) = False Then
        MsgBox "Please select a Sale category"
        cmbCat.SetFocus
        Exit Sub
    End If
    If IsNumeric(cmbUnit.BoundText) = False Then
        MsgBox "Please select a Unit"
        cmbUnit.SetFocus
        Exit Sub
    End If
    Dim mySaleCat As New SaleDept
    With mySaleCat
        .deptId = Val(cmbUnit.BoundText)
        .saleCatId = Val(cmbCat.BoundText)
        .saveData
    End With
    fillList
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnRemove_Click()
    If IsNumeric(cmbUnitCat.BoundText) = False Then
        MsgBox "Please select one to remove"
        cmbUnitCat.SetFocus
        Exit Sub
    End If
    Dim mySaleCat As New SaleDept
    With mySaleCat
        .saleDeptId = Val(cmbUnitCat.BoundText)
        .Deleted = True
        .deletedAt = Now
        .deletedUserId = UserID
        .saveData
    End With
    fillList
End Sub

Private Sub cmbUnit_Change()
    Call fillList
End Sub

Private Sub cmbUnit_Click(Area As Integer)
    Call fillList
End Sub

Private Sub Form_Load()
    fillCombos
    cmbUnit.BoundText = UserStoreID
    fillList
End Sub

Private Sub fillCombos()
    With rsCat
        If .State = 1 Then .Close
        temSQL = "Select * From tblSaleCategory Order by SaleCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Set cmbCat.RowSource = rsCat
        cmbCat.BoundColumn = "SaleCategoryID"
        cmbCat.ListField = "SaleCategory"
    End With
    With rsDept
        If .State = 1 Then .Close
        .Open "Select tblStore.* From tblStore Order By Store", cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Set cmbUnit.RowSource = rsDept
        cmbUnit.ListField = "Store"
        cmbUnit.BoundColumn = "StoreID"
    End With
End Sub

Private Sub fillList()
    With rsDeptCat
        If .State = 1 Then .Close
        temSQL = "SELECT dbo.tblSaleDept.saleDeptId, dbo.tblSaleCategory.SaleCategory " & _
                    "FROM dbo.tblSaleCategory LEFT OUTER JOIN dbo.tblSaleDept ON dbo.tblSaleCategory.SaleCategoryID = dbo.tblSaleDept.saleCatId " & _
                    "Where (dbo.tblSaleDept.deptId = " & Val(cmbUnit.BoundText) & ") And (dbo.tblSaleDept.deleted = 0) " & _
                    "ORDER BY dbo.tblSaleCategory.SaleCategory "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Set cmbUnitCat.RowSource = rsDeptCat
        cmbUnitCat.ListField = "SaleCategory"
        cmbUnitCat.BoundColumn = "saleDeptId"
    End With
End Sub
