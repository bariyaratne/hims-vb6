VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsStore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varStoreID As Long
    Private varStore As String
    Private varStoreDescreption As String
    Private varStoreCatogery As Long
    Private varupsize_ts

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblStore Where StoreID = " & varStoreID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !Store = varStore
        !StoreDescreption = varStoreDescreption
        !StoreCatogery = varStoreCatogery
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varStoreID = !NewID
        Else
            varStoreID = !StoreID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStore WHERE StoreID = " & varStoreID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!Store) Then
               varStore = !Store
            End If
            If Not IsNull(!StoreDescreption) Then
               varStoreDescreption = !StoreDescreption
            End If
            If Not IsNull(!StoreCatogery) Then
               varStoreCatogery = !StoreCatogery
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varStoreID = 0
    varStore = Empty
    varStoreDescreption = Empty
    varStoreCatogery = 0
    varupsize_ts = Empty
End Sub

Public Property Let StoreID(ByVal vStoreID As Long)
    Call clearData
    varStoreID = vStoreID
    Call loadData
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let Store(ByVal vStore As String)
    varStore = vStore
End Property

Public Property Get Store() As String
    Store = varStore
End Property

Public Property Let StoreDescreption(ByVal vStoreDescreption As String)
    varStoreDescreption = vStoreDescreption
End Property

Public Property Get StoreDescreption() As String
    StoreDescreption = varStoreDescreption
End Property

Public Property Let StoreCatogery(ByVal vStoreCatogery As Long)
    varStoreCatogery = vStoreCatogery
End Property

Public Property Get StoreCatogery() As Long
    StoreCatogery = varStoreCatogery
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property


