VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmReceivedBillsList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Rceived Items"
   ClientHeight    =   6300
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13350
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6300
   ScaleWidth      =   13350
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   2040
      Top             =   3480
   End
   Begin VB.CommandButton btnToReceive 
      Caption         =   "&To Receive"
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   5640
      Width           =   1575
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "&Close"
      Height          =   495
      Left            =   11880
      TabIndex        =   1
      Top             =   5640
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridList 
      Height          =   5295
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   9340
      _Version        =   393216
   End
End
Attribute VB_Name = "frmReceivedBillsList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnToReceive_Click()
    TxSaleBillID = Val(gridList.TextMatrix(gridList.Row, 0))
    Unload frmSaleReceive
    On Error Resume Next
    frmSaleReceive.Show
    frmSaleReceive.ZOrder 0
End Sub

Private Sub Form_Load()
    GetCommonSettings Me
    fillGrid
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub

Private Sub fillGrid()
    temSQL = "SELECT      TOP 100 PERCENT dbo.tblSaleBill.SaleBillID AS [Bill Id], dbo.tblSaleBill.Date AS [Billed Date], dbo.tblSaleBill.Time AS [Billed Time], dbo.tblStore.Store AS [From Unit], dbo.tblSaleBill.NetPrice AS [Bill Value] " & _
                "FROM          dbo.tblSaleBill LEFT OUTER JOIN dbo.tblStore ON dbo.tblSaleBill.StoreID = dbo.tblStore.StoreId " & _
                "WHERE      (dbo.tblSaleBill.BilledUnitID = " & UserStoreID & ") AND (dbo.tblSaleBill.Cancelled = 0 or dbo.tblSaleBill.Cancelled is null) AND (dbo.tblSaleBill.Received IS NULL OR dbo.tblSaleBill.Received = 0) AND (dbo.tblSaleBill.Refused IS NULL OR dbo.tblSaleBill.Refused = 0) " & _
                "ORDER BY dbo.tblSaleBill.SaleBillID"
    FillAnyGrid temSQL, gridList
    formatGridString gridList, 1, "dd MMMM yyyy"
    formatGridString gridList, 2, "mm:hh AMPM"
    formatGridString gridList, 4, "#,##0.00"
End Sub

Private Sub gridList_Click()
    btnToReceive_Click
End Sub

Private Sub Timer1_Timer()
    fillGrid
End Sub
