VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaleBill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Private varSaleBillID As Long
    Private varSaleCategoryID As Long
    Private varDate As Date
    Private varTime As Date
    Private varStaffID As Long
    Private varStoreID As Long
    Private varPrice As Double
    Private varDiscount As Double
    Private varDiscountPercent
    Private varTotalMedicineIncome As Double
    Private varOtherIncome As Double
    Private varNetPrice As Double
    Private varNetCost As Double
    Private varCheckedStaffID As Long
    Private varBilledBHTID As Long
    Private varBilledOutPatientID As Long
    Private varBilledStaffID As Long
    Private varPaymentMethodID As Long
    Private varFullCashPrice As Double
    Private varCashAdvancePrice As Double
    Private varCreditPrice As Double
    Private varChequePrice As Double
    Private varCreditCardPrice As Double
    Private varReceivedChequeID As Long
    Private varReceivedCreditCardID As Long
    Private varReceivedCashID As Long
    Private varReceivedCreditID As Long
    Private varPaymentMethod As String
    Private varCancelled As Boolean
    Private varCancelledUserID As Long
    Private varCancelledDate As Date
    Private varCancelledTime As Date
    Private varCancelledCUserID As Long
    Private varRepayPaymentMethodID As Long
    Private varIssuedCashID As Long
    Private varIssuedCreditID As Long
    Private varIssuedChequeID As Long
    Private varReturned As Boolean
    Private varReturnedUserID As Long
    Private varReturnedCUserID As Long
    Private varReturnedDate As Date
    Private varReturnedTime As Date
    Private varCancelledValue As Double
    Private varReturnedValue As Double
    Private varBilledUnitID As Long
    Private varReceivedOtherID As Long
    Private varPaidAtCashier As Boolean
    Private varPaidAtCashierUserID As Long
    Private varPaidAtCashierDate As Date
    Private varPaidAtCashierTime As Date
    Private varPaidCancelledAtCashier As Boolean
    Private varPaidCancelledAtCashierUserID As Long
    Private varPaidCancelledAtCashierDate As Date
    Private varPaidCancelledAtCashierTime As Date
    Private varPaidReturnedAtCashier As Boolean
    Private varPaidReturnedAtCashierUserID As Long
    Private varPaidReturnedAtCashierDate As Date
    Private varPaidReturnedAtCashierTime As Date
    Private varPaidReturnedValue As Double
    Private varupsize_ts
    Private varWSale As Boolean
    Private varRSale As Boolean
    Private varReceived As Boolean
    Private varReceivedUserID As Long
    Private varReceivedDate As Date
    Private varReceivedTime As Date
    Private varReceivedComments As String
    Private varRefused As Boolean
    Private varRefesedUserID As Long
    Private varRefusedDate As Date
    Private varRefusedTime As Date
    Private varRefusedComments As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSql = "SELECT * FROM tblSaleBill Where SaleBillID = " & varSaleBillID
        If .State = 1 Then .Close
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !SaleCategoryID = varSaleCategoryID
        !Date = varDate
        !Time = varTime
        !StaffID = varStaffID
        !StoreID = varStoreID
        !Price = varPrice
        !Discount = varDiscount
        !DiscountPercent = varDiscountPercent
        !TotalMedicineIncome = varTotalMedicineIncome
        !OtherIncome = varOtherIncome
        !NetPrice = varNetPrice
        !NetCost = varNetCost
        !CheckedStaffID = varCheckedStaffID
        !BilledBHTID = varBilledBHTID
        !BilledOutPatientID = varBilledOutPatientID
        !BilledStaffID = varBilledStaffID
        !PaymentMethodID = varPaymentMethodID
        !FullCashPrice = varFullCashPrice
        !CashAdvancePrice = varCashAdvancePrice
        !CreditPrice = varCreditPrice
        !ChequePrice = varChequePrice
        !CreditCardPrice = varCreditCardPrice
        !ReceivedChequeID = varReceivedChequeID
        !ReceivedCreditCardID = varReceivedCreditCardID
        !ReceivedCashID = varReceivedCashID
        !ReceivedCreditID = varReceivedCreditID
        !PaymentMethod = varPaymentMethod
        !Cancelled = varCancelled
        !CancelledUserID = varCancelledUserID
        !CancelledDate = varCancelledDate
        !CancelledTime = varCancelledTime
        !CancelledCUserID = varCancelledCUserID
        !RepayPaymentMethodID = varRepayPaymentMethodID
        !IssuedCashID = varIssuedCashID
        !IssuedCreditID = varIssuedCreditID
        !IssuedChequeID = varIssuedChequeID
        !Returned = varReturned
        !ReturnedUserID = varReturnedUserID
        !ReturnedCUserID = varReturnedCUserID
        !ReturnedDate = varReturnedDate
        !ReturnedTime = varReturnedTime
        !CancelledValue = varCancelledValue
        !ReturnedValue = varReturnedValue
        !BilledUnitID = varBilledUnitID
        !ReceivedOtherID = varReceivedOtherID
        !PaidAtCashier = varPaidAtCashier
        !PaidAtCashierUserID = varPaidAtCashierUserID
        !PaidAtCashierDate = varPaidAtCashierDate
        !PaidAtCashierTime = varPaidAtCashierTime
        !PaidCancelledAtCashier = varPaidCancelledAtCashier
        !PaidCancelledAtCashierUserID = varPaidCancelledAtCashierUserID
        !PaidCancelledAtCashierDate = varPaidCancelledAtCashierDate
        !PaidCancelledAtCashierTime = varPaidCancelledAtCashierTime
        !PaidReturnedAtCashier = varPaidReturnedAtCashier
        !PaidReturnedAtCashierUserID = varPaidReturnedAtCashierUserID
        !PaidReturnedAtCashierDate = varPaidReturnedAtCashierDate
        !PaidReturnedAtCashierTime = varPaidReturnedAtCashierTime
        !PaidReturnedValue = varPaidReturnedValue
        !WSale = varWSale
        !RSale = varRSale
        !Received = varReceived
        !ReceivedUserID = varReceivedUserID
        !ReceivedDate = varReceivedDate
        !ReceivedTime = varReceivedTime
        !ReceivedComments = varReceivedComments
        !Refused = varRefused
        !RefesedUserID = varRefesedUserID
        !RefusedDate = varRefusedDate
        !RefusedTime = varRefusedTime
        !RefusedComments = varRefusedComments
        .Update
        If newEntry = True Then
            .Close
            temSql = "SELECT @@IDENTITY AS NewID"
           .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
            varSaleBillID = !NewID
        Else
            varSaleBillID = !SaleBillID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "SELECT * FROM tblSaleBill WHERE SaleBillID = " & varSaleBillID
        If .State = 1 Then .Close
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!SaleBillID) Then
               varSaleBillID = !SaleBillID
            End If
            If Not IsNull(!SaleCategoryID) Then
               varSaleCategoryID = !SaleCategoryID
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!Price) Then
               varPrice = !Price
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!DiscountPercent) Then
               varDiscountPercent = !DiscountPercent
            End If
            If Not IsNull(!TotalMedicineIncome) Then
               varTotalMedicineIncome = !TotalMedicineIncome
            End If
            If Not IsNull(!OtherIncome) Then
               varOtherIncome = !OtherIncome
            End If
            If Not IsNull(!NetPrice) Then
               varNetPrice = !NetPrice
            End If
            If Not IsNull(!NetCost) Then
               varNetCost = !NetCost
            End If
            If Not IsNull(!CheckedStaffID) Then
               varCheckedStaffID = !CheckedStaffID
            End If
            If Not IsNull(!BilledBHTID) Then
               varBilledBHTID = !BilledBHTID
            End If
            If Not IsNull(!BilledOutPatientID) Then
               varBilledOutPatientID = !BilledOutPatientID
            End If
            If Not IsNull(!BilledStaffID) Then
               varBilledStaffID = !BilledStaffID
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!FullCashPrice) Then
               varFullCashPrice = !FullCashPrice
            End If
            If Not IsNull(!CashAdvancePrice) Then
               varCashAdvancePrice = !CashAdvancePrice
            End If
            If Not IsNull(!CreditPrice) Then
               varCreditPrice = !CreditPrice
            End If
            If Not IsNull(!ChequePrice) Then
               varChequePrice = !ChequePrice
            End If
            If Not IsNull(!CreditCardPrice) Then
               varCreditCardPrice = !CreditCardPrice
            End If
            If Not IsNull(!ReceivedChequeID) Then
               varReceivedChequeID = !ReceivedChequeID
            End If
            If Not IsNull(!ReceivedCreditCardID) Then
               varReceivedCreditCardID = !ReceivedCreditCardID
            End If
            If Not IsNull(!ReceivedCashID) Then
               varReceivedCashID = !ReceivedCashID
            End If
            If Not IsNull(!ReceivedCreditID) Then
               varReceivedCreditID = !ReceivedCreditID
            End If
            If Not IsNull(!PaymentMethod) Then
               varPaymentMethod = !PaymentMethod
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!CancelledUserID) Then
               varCancelledUserID = !CancelledUserID
            End If
            If Not IsNull(!CancelledDate) Then
               varCancelledDate = !CancelledDate
            End If
            If Not IsNull(!CancelledTime) Then
               varCancelledTime = !CancelledTime
            End If
            If Not IsNull(!CancelledCUserID) Then
               varCancelledCUserID = !CancelledCUserID
            End If
            If Not IsNull(!RepayPaymentMethodID) Then
               varRepayPaymentMethodID = !RepayPaymentMethodID
            End If
            If Not IsNull(!IssuedCashID) Then
               varIssuedCashID = !IssuedCashID
            End If
            If Not IsNull(!IssuedCreditID) Then
               varIssuedCreditID = !IssuedCreditID
            End If
            If Not IsNull(!IssuedChequeID) Then
               varIssuedChequeID = !IssuedChequeID
            End If
            If Not IsNull(!Returned) Then
               varReturned = !Returned
            End If
            If Not IsNull(!ReturnedUserID) Then
               varReturnedUserID = !ReturnedUserID
            End If
            If Not IsNull(!ReturnedCUserID) Then
               varReturnedCUserID = !ReturnedCUserID
            End If
            If Not IsNull(!ReturnedDate) Then
               varReturnedDate = !ReturnedDate
            End If
            If Not IsNull(!ReturnedTime) Then
               varReturnedTime = !ReturnedTime
            End If
            If Not IsNull(!CancelledValue) Then
               varCancelledValue = !CancelledValue
            End If
            If Not IsNull(!ReturnedValue) Then
               varReturnedValue = !ReturnedValue
            End If
            If Not IsNull(!BilledUnitID) Then
               varBilledUnitID = !BilledUnitID
            End If
            If Not IsNull(!ReceivedOtherID) Then
               varReceivedOtherID = !ReceivedOtherID
            End If
            If Not IsNull(!PaidAtCashier) Then
               varPaidAtCashier = !PaidAtCashier
            End If
            If Not IsNull(!PaidAtCashierUserID) Then
               varPaidAtCashierUserID = !PaidAtCashierUserID
            End If
            If Not IsNull(!PaidAtCashierDate) Then
               varPaidAtCashierDate = !PaidAtCashierDate
            End If
            If Not IsNull(!PaidAtCashierTime) Then
               varPaidAtCashierTime = !PaidAtCashierTime
            End If
            If Not IsNull(!PaidCancelledAtCashier) Then
               varPaidCancelledAtCashier = !PaidCancelledAtCashier
            End If
            If Not IsNull(!PaidCancelledAtCashierUserID) Then
               varPaidCancelledAtCashierUserID = !PaidCancelledAtCashierUserID
            End If
            If Not IsNull(!PaidCancelledAtCashierDate) Then
               varPaidCancelledAtCashierDate = !PaidCancelledAtCashierDate
            End If
            If Not IsNull(!PaidCancelledAtCashierTime) Then
               varPaidCancelledAtCashierTime = !PaidCancelledAtCashierTime
            End If
            If Not IsNull(!PaidReturnedAtCashier) Then
               varPaidReturnedAtCashier = !PaidReturnedAtCashier
            End If
            If Not IsNull(!PaidReturnedAtCashierUserID) Then
               varPaidReturnedAtCashierUserID = !PaidReturnedAtCashierUserID
            End If
            If Not IsNull(!PaidReturnedAtCashierDate) Then
               varPaidReturnedAtCashierDate = !PaidReturnedAtCashierDate
            End If
            If Not IsNull(!PaidReturnedAtCashierTime) Then
               varPaidReturnedAtCashierTime = !PaidReturnedAtCashierTime
            End If
            If Not IsNull(!PaidReturnedValue) Then
               varPaidReturnedValue = !PaidReturnedValue
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!WSale) Then
               varWSale = !WSale
            End If
            If Not IsNull(!RSale) Then
               varRSale = !RSale
            End If
            If Not IsNull(!Received) Then
               varReceived = !Received
            End If
            If Not IsNull(!ReceivedUserID) Then
               varReceivedUserID = !ReceivedUserID
            End If
            If Not IsNull(!ReceivedDate) Then
               varReceivedDate = !ReceivedDate
            End If
            If Not IsNull(!ReceivedTime) Then
               varReceivedTime = !ReceivedTime
            End If
            If Not IsNull(!ReceivedComments) Then
               varReceivedComments = !ReceivedComments
            End If
            If Not IsNull(!Refused) Then
               varRefused = !Refused
            End If
            If Not IsNull(!RefesedUserID) Then
               varRefesedUserID = !RefesedUserID
            End If
            If Not IsNull(!RefusedDate) Then
               varRefusedDate = !RefusedDate
            End If
            If Not IsNull(!RefusedTime) Then
               varRefusedTime = !RefusedTime
            End If
            If Not IsNull(!RefusedComments) Then
               varRefusedComments = !RefusedComments
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varSaleBillID = 0
    varSaleCategoryID = 0
    varDate = Empty
    varTime = Empty
    varStaffID = 0
    varStoreID = 0
    varPrice = 0
    varDiscount = 0
    varDiscountPercent = Empty
    varTotalMedicineIncome = 0
    varOtherIncome = 0
    varNetPrice = 0
    varNetCost = 0
    varCheckedStaffID = 0
    varBilledBHTID = 0
    varBilledOutPatientID = 0
    varBilledStaffID = 0
    varPaymentMethodID = 0
    varFullCashPrice = 0
    varCashAdvancePrice = 0
    varCreditPrice = 0
    varChequePrice = 0
    varCreditCardPrice = 0
    varReceivedChequeID = 0
    varReceivedCreditCardID = 0
    varReceivedCashID = 0
    varReceivedCreditID = 0
    varPaymentMethod = Empty
    varCancelled = False
    varCancelledUserID = 0
    varCancelledDate = Empty
    varCancelledTime = Empty
    varCancelledCUserID = 0
    varRepayPaymentMethodID = 0
    varIssuedCashID = 0
    varIssuedCreditID = 0
    varIssuedChequeID = 0
    varReturned = False
    varReturnedUserID = 0
    varReturnedCUserID = 0
    varReturnedDate = Empty
    varReturnedTime = Empty
    varCancelledValue = 0
    varReturnedValue = 0
    varBilledUnitID = 0
    varReceivedOtherID = 0
    varPaidAtCashier = False
    varPaidAtCashierUserID = 0
    varPaidAtCashierDate = Empty
    varPaidAtCashierTime = Empty
    varPaidCancelledAtCashier = False
    varPaidCancelledAtCashierUserID = 0
    varPaidCancelledAtCashierDate = Empty
    varPaidCancelledAtCashierTime = Empty
    varPaidReturnedAtCashier = False
    varPaidReturnedAtCashierUserID = 0
    varPaidReturnedAtCashierDate = Empty
    varPaidReturnedAtCashierTime = Empty
    varPaidReturnedValue = 0
    varupsize_ts = Empty
    varWSale = False
    varRSale = False
    varReceived = False
    varReceivedUserID = 0
    varReceivedDate = Empty
    varReceivedTime = Empty
    varReceivedComments = Empty
    varRefused = False
    varRefesedUserID = 0
    varRefusedDate = Empty
    varRefusedTime = Empty
    varRefusedComments = Empty
End Sub

Public Property Let SaleBillID(ByVal vSaleBillID As Long)
    Call clearData
    varSaleBillID = vSaleBillID
    Call loadData
End Property

Public Property Get SaleBillID() As Long
    SaleBillID = varSaleBillID
End Property

Public Property Let SaleCategoryID(ByVal vSaleCategoryID As Long)
    varSaleCategoryID = vSaleCategoryID
End Property

Public Property Get SaleCategoryID() As Long
    SaleCategoryID = varSaleCategoryID
End Property

Public Property Let BillDate(ByVal vDate As Date)
    varDate = vDate
End Property

Public Property Get BillDate() As Date
    BillDate = varDate
End Property

Public Property Let Time(ByVal vTime As Date)
    varTime = vTime
End Property

Public Property Get Time() As Date
    Time = varTime
End Property

Public Property Let StaffID(ByVal vStaffID As Long)
    varStaffID = vStaffID
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let StoreID(ByVal vStoreID As Long)
    varStoreID = vStoreID
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let Price(ByVal vPrice As Double)
    varPrice = vPrice
End Property

Public Property Get Price() As Double
    Price = varPrice
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let DiscountPercent(ByVal vDiscountPercent)
    varDiscountPercent = vDiscountPercent
End Property

Public Property Get DiscountPercent()
    DiscountPercent = varDiscountPercent
End Property

Public Property Let TotalMedicineIncome(ByVal vTotalMedicineIncome As Double)
    varTotalMedicineIncome = vTotalMedicineIncome
End Property

Public Property Get TotalMedicineIncome() As Double
    TotalMedicineIncome = varTotalMedicineIncome
End Property

Public Property Let OtherIncome(ByVal vOtherIncome As Double)
    varOtherIncome = vOtherIncome
End Property

Public Property Get OtherIncome() As Double
    OtherIncome = varOtherIncome
End Property

Public Property Let NetPrice(ByVal vNetPrice As Double)
    varNetPrice = vNetPrice
End Property

Public Property Get NetPrice() As Double
    NetPrice = varNetPrice
End Property

Public Property Let NetCost(ByVal vNetCost As Double)
    varNetCost = vNetCost
End Property

Public Property Get NetCost() As Double
    NetCost = varNetCost
End Property

Public Property Let CheckedStaffID(ByVal vCheckedStaffID As Long)
    varCheckedStaffID = vCheckedStaffID
End Property

Public Property Get CheckedStaffID() As Long
    CheckedStaffID = varCheckedStaffID
End Property

Public Property Let BilledBHTID(ByVal vBilledBHTID As Long)
    varBilledBHTID = vBilledBHTID
End Property

Public Property Get BilledBHTID() As Long
    BilledBHTID = varBilledBHTID
End Property

Public Property Let BilledOutPatientID(ByVal vBilledOutPatientID As Long)
    varBilledOutPatientID = vBilledOutPatientID
End Property

Public Property Get BilledOutPatientID() As Long
    BilledOutPatientID = varBilledOutPatientID
End Property

Public Property Let BilledStaffID(ByVal vBilledStaffID As Long)
    varBilledStaffID = vBilledStaffID
End Property

Public Property Get BilledStaffID() As Long
    BilledStaffID = varBilledStaffID
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let FullCashPrice(ByVal vFullCashPrice As Double)
    varFullCashPrice = vFullCashPrice
End Property

Public Property Get FullCashPrice() As Double
    FullCashPrice = varFullCashPrice
End Property

Public Property Let CashAdvancePrice(ByVal vCashAdvancePrice As Double)
    varCashAdvancePrice = vCashAdvancePrice
End Property

Public Property Get CashAdvancePrice() As Double
    CashAdvancePrice = varCashAdvancePrice
End Property

Public Property Let CreditPrice(ByVal vCreditPrice As Double)
    varCreditPrice = vCreditPrice
End Property

Public Property Get CreditPrice() As Double
    CreditPrice = varCreditPrice
End Property

Public Property Let ChequePrice(ByVal vChequePrice As Double)
    varChequePrice = vChequePrice
End Property

Public Property Get ChequePrice() As Double
    ChequePrice = varChequePrice
End Property

Public Property Let CreditCardPrice(ByVal vCreditCardPrice As Double)
    varCreditCardPrice = vCreditCardPrice
End Property

Public Property Get CreditCardPrice() As Double
    CreditCardPrice = varCreditCardPrice
End Property

Public Property Let ReceivedChequeID(ByVal vReceivedChequeID As Long)
    varReceivedChequeID = vReceivedChequeID
End Property

Public Property Get ReceivedChequeID() As Long
    ReceivedChequeID = varReceivedChequeID
End Property

Public Property Let ReceivedCreditCardID(ByVal vReceivedCreditCardID As Long)
    varReceivedCreditCardID = vReceivedCreditCardID
End Property

Public Property Get ReceivedCreditCardID() As Long
    ReceivedCreditCardID = varReceivedCreditCardID
End Property

Public Property Let ReceivedCashID(ByVal vReceivedCashID As Long)
    varReceivedCashID = vReceivedCashID
End Property

Public Property Get ReceivedCashID() As Long
    ReceivedCashID = varReceivedCashID
End Property

Public Property Let ReceivedCreditID(ByVal vReceivedCreditID As Long)
    varReceivedCreditID = vReceivedCreditID
End Property

Public Property Get ReceivedCreditID() As Long
    ReceivedCreditID = varReceivedCreditID
End Property

Public Property Let PaymentMethod(ByVal vPaymentMethod As String)
    varPaymentMethod = vPaymentMethod
End Property

Public Property Get PaymentMethod() As String
    PaymentMethod = varPaymentMethod
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledUserID(ByVal vCancelledUserID As Long)
    varCancelledUserID = vCancelledUserID
End Property

Public Property Get CancelledUserID() As Long
    CancelledUserID = varCancelledUserID
End Property

Public Property Let CancelledDate(ByVal vCancelledDate As Date)
    varCancelledDate = vCancelledDate
End Property

Public Property Get CancelledDate() As Date
    CancelledDate = varCancelledDate
End Property

Public Property Let CancelledTime(ByVal vCancelledTime As Date)
    varCancelledTime = vCancelledTime
End Property

Public Property Get CancelledTime() As Date
    CancelledTime = varCancelledTime
End Property

Public Property Let CancelledCUserID(ByVal vCancelledCUserID As Long)
    varCancelledCUserID = vCancelledCUserID
End Property

Public Property Get CancelledCUserID() As Long
    CancelledCUserID = varCancelledCUserID
End Property

Public Property Let RepayPaymentMethodID(ByVal vRepayPaymentMethodID As Long)
    varRepayPaymentMethodID = vRepayPaymentMethodID
End Property

Public Property Get RepayPaymentMethodID() As Long
    RepayPaymentMethodID = varRepayPaymentMethodID
End Property

Public Property Let IssuedCashID(ByVal vIssuedCashID As Long)
    varIssuedCashID = vIssuedCashID
End Property

Public Property Get IssuedCashID() As Long
    IssuedCashID = varIssuedCashID
End Property

Public Property Let IssuedCreditID(ByVal vIssuedCreditID As Long)
    varIssuedCreditID = vIssuedCreditID
End Property

Public Property Get IssuedCreditID() As Long
    IssuedCreditID = varIssuedCreditID
End Property

Public Property Let IssuedChequeID(ByVal vIssuedChequeID As Long)
    varIssuedChequeID = vIssuedChequeID
End Property

Public Property Get IssuedChequeID() As Long
    IssuedChequeID = varIssuedChequeID
End Property

Public Property Let Returned(ByVal vReturned As Boolean)
    varReturned = vReturned
End Property

Public Property Get Returned() As Boolean
    Returned = varReturned
End Property

Public Property Let ReturnedUserID(ByVal vReturnedUserID As Long)
    varReturnedUserID = vReturnedUserID
End Property

Public Property Get ReturnedUserID() As Long
    ReturnedUserID = varReturnedUserID
End Property

Public Property Let ReturnedCUserID(ByVal vReturnedCUserID As Long)
    varReturnedCUserID = vReturnedCUserID
End Property

Public Property Get ReturnedCUserID() As Long
    ReturnedCUserID = varReturnedCUserID
End Property

Public Property Let ReturnedDate(ByVal vReturnedDate As Date)
    varReturnedDate = vReturnedDate
End Property

Public Property Get ReturnedDate() As Date
    ReturnedDate = varReturnedDate
End Property

Public Property Let ReturnedTime(ByVal vReturnedTime As Date)
    varReturnedTime = vReturnedTime
End Property

Public Property Get ReturnedTime() As Date
    ReturnedTime = varReturnedTime
End Property

Public Property Let CancelledValue(ByVal vCancelledValue As Double)
    varCancelledValue = vCancelledValue
End Property

Public Property Get CancelledValue() As Double
    CancelledValue = varCancelledValue
End Property

Public Property Let ReturnedValue(ByVal vReturnedValue As Double)
    varReturnedValue = vReturnedValue
End Property

Public Property Get ReturnedValue() As Double
    ReturnedValue = varReturnedValue
End Property

Public Property Let BilledUnitID(ByVal vBilledUnitID As Long)
    varBilledUnitID = vBilledUnitID
End Property

Public Property Get BilledUnitID() As Long
    BilledUnitID = varBilledUnitID
End Property

Public Property Let ReceivedOtherID(ByVal vReceivedOtherID As Long)
    varReceivedOtherID = vReceivedOtherID
End Property

Public Property Get ReceivedOtherID() As Long
    ReceivedOtherID = varReceivedOtherID
End Property

Public Property Let PaidAtCashier(ByVal vPaidAtCashier As Boolean)
    varPaidAtCashier = vPaidAtCashier
End Property

Public Property Get PaidAtCashier() As Boolean
    PaidAtCashier = varPaidAtCashier
End Property

Public Property Let PaidAtCashierUserID(ByVal vPaidAtCashierUserID As Long)
    varPaidAtCashierUserID = vPaidAtCashierUserID
End Property

Public Property Get PaidAtCashierUserID() As Long
    PaidAtCashierUserID = varPaidAtCashierUserID
End Property

Public Property Let PaidAtCashierDate(ByVal vPaidAtCashierDate As Date)
    varPaidAtCashierDate = vPaidAtCashierDate
End Property

Public Property Get PaidAtCashierDate() As Date
    PaidAtCashierDate = varPaidAtCashierDate
End Property

Public Property Let PaidAtCashierTime(ByVal vPaidAtCashierTime As Date)
    varPaidAtCashierTime = vPaidAtCashierTime
End Property

Public Property Get PaidAtCashierTime() As Date
    PaidAtCashierTime = varPaidAtCashierTime
End Property

Public Property Let PaidCancelledAtCashier(ByVal vPaidCancelledAtCashier As Boolean)
    varPaidCancelledAtCashier = vPaidCancelledAtCashier
End Property

Public Property Get PaidCancelledAtCashier() As Boolean
    PaidCancelledAtCashier = varPaidCancelledAtCashier
End Property

Public Property Let PaidCancelledAtCashierUserID(ByVal vPaidCancelledAtCashierUserID As Long)
    varPaidCancelledAtCashierUserID = vPaidCancelledAtCashierUserID
End Property

Public Property Get PaidCancelledAtCashierUserID() As Long
    PaidCancelledAtCashierUserID = varPaidCancelledAtCashierUserID
End Property

Public Property Let PaidCancelledAtCashierDate(ByVal vPaidCancelledAtCashierDate As Date)
    varPaidCancelledAtCashierDate = vPaidCancelledAtCashierDate
End Property

Public Property Get PaidCancelledAtCashierDate() As Date
    PaidCancelledAtCashierDate = varPaidCancelledAtCashierDate
End Property

Public Property Let PaidCancelledAtCashierTime(ByVal vPaidCancelledAtCashierTime As Date)
    varPaidCancelledAtCashierTime = vPaidCancelledAtCashierTime
End Property

Public Property Get PaidCancelledAtCashierTime() As Date
    PaidCancelledAtCashierTime = varPaidCancelledAtCashierTime
End Property

Public Property Let PaidReturnedAtCashier(ByVal vPaidReturnedAtCashier As Boolean)
    varPaidReturnedAtCashier = vPaidReturnedAtCashier
End Property

Public Property Get PaidReturnedAtCashier() As Boolean
    PaidReturnedAtCashier = varPaidReturnedAtCashier
End Property

Public Property Let PaidReturnedAtCashierUserID(ByVal vPaidReturnedAtCashierUserID As Long)
    varPaidReturnedAtCashierUserID = vPaidReturnedAtCashierUserID
End Property

Public Property Get PaidReturnedAtCashierUserID() As Long
    PaidReturnedAtCashierUserID = varPaidReturnedAtCashierUserID
End Property

Public Property Let PaidReturnedAtCashierDate(ByVal vPaidReturnedAtCashierDate As Date)
    varPaidReturnedAtCashierDate = vPaidReturnedAtCashierDate
End Property

Public Property Get PaidReturnedAtCashierDate() As Date
    PaidReturnedAtCashierDate = varPaidReturnedAtCashierDate
End Property

Public Property Let PaidReturnedAtCashierTime(ByVal vPaidReturnedAtCashierTime As Date)
    varPaidReturnedAtCashierTime = vPaidReturnedAtCashierTime
End Property

Public Property Get PaidReturnedAtCashierTime() As Date
    PaidReturnedAtCashierTime = varPaidReturnedAtCashierTime
End Property

Public Property Let PaidReturnedValue(ByVal vPaidReturnedValue As Double)
    varPaidReturnedValue = vPaidReturnedValue
End Property

Public Property Get PaidReturnedValue() As Double
    PaidReturnedValue = varPaidReturnedValue
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let WSale(ByVal vWSale As Boolean)
    varWSale = vWSale
End Property

Public Property Get WSale() As Boolean
    WSale = varWSale
End Property

Public Property Let RSale(ByVal vRSale As Boolean)
    varRSale = vRSale
End Property

Public Property Get RSale() As Boolean
    RSale = varRSale
End Property

Public Property Let Received(ByVal vReceived As Boolean)
    varReceived = vReceived
End Property

Public Property Get Received() As Boolean
    Received = varReceived
End Property

Public Property Let ReceivedUserID(ByVal vReceivedUserID As Long)
    varReceivedUserID = vReceivedUserID
End Property

Public Property Get ReceivedUserID() As Long
    ReceivedUserID = varReceivedUserID
End Property

Public Property Let ReceivedDate(ByVal vReceivedDate As Date)
    varReceivedDate = vReceivedDate
End Property

Public Property Get ReceivedDate() As Date
    ReceivedDate = varReceivedDate
End Property

Public Property Let ReceivedTime(ByVal vReceivedTime As Date)
    varReceivedTime = vReceivedTime
End Property

Public Property Get ReceivedTime() As Date
    ReceivedTime = varReceivedTime
End Property

Public Property Let ReceivedComments(ByVal vReceivedComments As String)
    varReceivedComments = vReceivedComments
End Property

Public Property Get ReceivedComments() As String
    ReceivedComments = varReceivedComments
End Property

Public Property Let Refused(ByVal vRefused As Boolean)
    varRefused = vRefused
End Property

Public Property Get Refused() As Boolean
    Refused = varRefused
End Property

Public Property Let RefesedUserID(ByVal vRefesedUserID As Long)
    varRefesedUserID = vRefesedUserID
End Property

Public Property Get RefesedUserID() As Long
    RefesedUserID = varRefesedUserID
End Property

Public Property Let RefusedDate(ByVal vRefusedDate As Date)
    varRefusedDate = vRefusedDate
End Property

Public Property Get RefusedDate() As Date
    RefusedDate = varRefusedDate
End Property

Public Property Let RefusedTime(ByVal vRefusedTime As Date)
    varRefusedTime = vRefusedTime
End Property

Public Property Get RefusedTime() As Date
    RefusedTime = varRefusedTime
End Property

Public Property Let RefusedComments(ByVal vRefusedComments As String)
    varRefusedComments = vRefusedComments
End Property

Public Property Get RefusedComments() As String
    RefusedComments = varRefusedComments
End Property


