VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaleCategory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varSaleCategoryID As Long
    Private varSaleCategory As String
    Private varSaleDiscountPercent As Double
    Private varProfitMargin As Double
    Private varPaymentMethodID As Long
    Private varCash As Boolean
    Private varCredit As Boolean
    Private varCheque As Boolean
    Private varSlips As Boolean
    Private varCreditCard As Boolean
    Private varOther As Boolean
    Private varOutPatient As Boolean
    Private varInPatient As Boolean
    Private varStaff As Boolean
    Private varUnit As Boolean
    Private varupsize_ts
    Private varRetail As Boolean
    Private varWholeSale As Boolean
    Private varbillPrint As Boolean
    Private varreportPrint As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblSaleCategory Where SaleCategoryID = " & varSaleCategoryID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !SaleCategory = varSaleCategory
        !SaleDiscountPercent = varSaleDiscountPercent
        !ProfitMargin = varProfitMargin
        !PaymentMethodID = varPaymentMethodID
        !Cash = varCash
        !Credit = varCredit
        !Cheque = varCheque
        !Slips = varSlips
        !CreditCard = varCreditCard
        !Other = varOther
        !OutPatient = varOutPatient
        !InPatient = varInPatient
        !Staff = varStaff
        !Unit = varUnit
        !Retail = varRetail
        !WholeSale = varWholeSale
        !billPrint = varbillPrint
        !reportPrint = varreportPrint
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varSaleCategoryID = !NewID
        Else
            varSaleCategoryID = !SaleCategoryID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSaleCategory WHERE SaleCategoryID = " & varSaleCategoryID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!SaleCategoryID) Then
               varSaleCategoryID = !SaleCategoryID
            End If
            If Not IsNull(!SaleCategory) Then
               varSaleCategory = !SaleCategory
            End If
            If Not IsNull(!SaleDiscountPercent) Then
               varSaleDiscountPercent = !SaleDiscountPercent
            End If
            If Not IsNull(!ProfitMargin) Then
               varProfitMargin = !ProfitMargin
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!Cash) Then
               varCash = !Cash
            End If
            If Not IsNull(!Credit) Then
               varCredit = !Credit
            End If
            If Not IsNull(!Cheque) Then
               varCheque = !Cheque
            End If
            If Not IsNull(!Slips) Then
               varSlips = !Slips
            End If
            If Not IsNull(!CreditCard) Then
               varCreditCard = !CreditCard
            End If
            If Not IsNull(!Other) Then
               varOther = !Other
            End If
            If Not IsNull(!OutPatient) Then
               varOutPatient = !OutPatient
            End If
            If Not IsNull(!InPatient) Then
               varInPatient = !InPatient
            End If
            If Not IsNull(!Staff) Then
               varStaff = !Staff
            End If
            If Not IsNull(!Unit) Then
               varUnit = !Unit
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!Retail) Then
               varRetail = !Retail
            End If
            If Not IsNull(!WholeSale) Then
               varWholeSale = !WholeSale
            End If
            If Not IsNull(!billPrint) Then
               varbillPrint = !billPrint
            End If
            If Not IsNull(!reportPrint) Then
               varreportPrint = !reportPrint
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varSaleCategoryID = 0
    varSaleCategory = Empty
    varSaleDiscountPercent = 0
    varProfitMargin = 0
    varPaymentMethodID = 0
    varCash = False
    varCredit = False
    varCheque = False
    varSlips = False
    varCreditCard = False
    varOther = False
    varOutPatient = False
    varInPatient = False
    varStaff = False
    varUnit = False
    varupsize_ts = Empty
    varRetail = False
    varWholeSale = False
    varbillPrint = False
    varreportPrint = False
End Sub

Public Property Let SaleCategoryID(ByVal vSaleCategoryID As Long)
    Call clearData
    varSaleCategoryID = vSaleCategoryID
    Call loadData
End Property

Public Property Get SaleCategoryID() As Long
    SaleCategoryID = varSaleCategoryID
End Property

Public Property Let SaleCategory(ByVal vSaleCategory As String)
    varSaleCategory = vSaleCategory
End Property

Public Property Get SaleCategory() As String
    SaleCategory = varSaleCategory
End Property

Public Property Let SaleDiscountPercent(ByVal vSaleDiscountPercent As Double)
    varSaleDiscountPercent = vSaleDiscountPercent
End Property

Public Property Get SaleDiscountPercent() As Double
    SaleDiscountPercent = varSaleDiscountPercent
End Property

Public Property Let ProfitMargin(ByVal vProfitMargin As Double)
    varProfitMargin = vProfitMargin
End Property

Public Property Get ProfitMargin() As Double
    ProfitMargin = varProfitMargin
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let Cash(ByVal vCash As Boolean)
    varCash = vCash
End Property

Public Property Get Cash() As Boolean
    Cash = varCash
End Property

Public Property Let Credit(ByVal vCredit As Boolean)
    varCredit = vCredit
End Property

Public Property Get Credit() As Boolean
    Credit = varCredit
End Property

Public Property Let Cheque(ByVal vCheque As Boolean)
    varCheque = vCheque
End Property

Public Property Get Cheque() As Boolean
    Cheque = varCheque
End Property

Public Property Let Slips(ByVal vSlips As Boolean)
    varSlips = vSlips
End Property

Public Property Get Slips() As Boolean
    Slips = varSlips
End Property

Public Property Let CreditCard(ByVal vCreditCard As Boolean)
    varCreditCard = vCreditCard
End Property

Public Property Get CreditCard() As Boolean
    CreditCard = varCreditCard
End Property

Public Property Let Other(ByVal vOther As Boolean)
    varOther = vOther
End Property

Public Property Get Other() As Boolean
    Other = varOther
End Property

Public Property Let OutPatient(ByVal vOutPatient As Boolean)
    varOutPatient = vOutPatient
End Property

Public Property Get OutPatient() As Boolean
    OutPatient = varOutPatient
End Property

Public Property Let InPatient(ByVal vInPatient As Boolean)
    varInPatient = vInPatient
End Property

Public Property Get InPatient() As Boolean
    InPatient = varInPatient
End Property

Public Property Let Staff(ByVal vStaff As Boolean)
    varStaff = vStaff
End Property

Public Property Get Staff() As Boolean
    Staff = varStaff
End Property

Public Property Let Unit(ByVal vUnit As Boolean)
    varUnit = vUnit
End Property

Public Property Get Unit() As Boolean
    Unit = varUnit
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let Retail(ByVal vRetail As Boolean)
    varRetail = vRetail
End Property

Public Property Get Retail() As Boolean
    Retail = varRetail
End Property

Public Property Let WholeSale(ByVal vWholeSale As Boolean)
    varWholeSale = vWholeSale
End Property

Public Property Get WholeSale() As Boolean
    WholeSale = varWholeSale
End Property

Public Property Let billPrint(ByVal vbillPrint As Boolean)
    varbillPrint = vbillPrint
End Property

Public Property Get billPrint() As Boolean
    billPrint = varbillPrint
End Property

Public Property Let reportPrint(ByVal vreportPrint As Boolean)
    varreportPrint = vreportPrint
End Property

Public Property Get reportPrint() As Boolean
    reportPrint = varreportPrint
End Property


