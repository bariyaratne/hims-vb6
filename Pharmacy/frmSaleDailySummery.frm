VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmSaleDailySummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Summery Report - Sale"
   ClientHeight    =   6090
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   14685
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6090
   ScaleWidth      =   14685
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1560
      TabIndex        =   6
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8760
      TabIndex        =   5
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   240
      TabIndex        =   4
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   5400
      TabIndex        =   3
      Top             =   240
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid gridSum 
      Height          =   4695
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   14295
      _ExtentX        =   25215
      _ExtentY        =   8281
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1200
      TabIndex        =   1
      Top             =   240
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   122748931
      CurrentDate     =   41234
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "frmSaleDailySummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim saleCats As New Collection


Private Sub btnExcel_Click()
    GridToExcel gridSum, "Daily Sale Summery - " & UserStore, Format(dtpDate.Value, LongDateFormat)
End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    ThisReportFormat.ReportPrintOrientation = Landscape
    GridPrint gridSum, ThisReportFormat, "Daily Sale Summery - " & UserStore, Format(dtpDate.Value, LongDateFormat)
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    DisplayDetails
End Sub

Private Sub Form_Load()
    getSettings
    dtpDate.Value = Date
    DisplayDetails
End Sub

Private Sub getSettings()
    GetCommonSettings Me
    Set saleCats = findAllSaleCategories
End Sub



Private Sub saveSettings()
    SaveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    saveSettings
End Sub

Private Sub DisplayDetails()
    Dim temCat As SaleCategory
    Dim Col As Integer
    Dim Row As Integer
    
    Dim cashSum1 As Double
    Dim nonCashSum1 As Double
    Dim unitSum1 As Double
    
    Dim cashSum2 As Double
    Dim nonCashSum2 As Double
    Dim unitSum2 As Double
    
    Dim sum1 As Double
    Dim sum2 As Double
    
    With gridSum
        Col = 1
        .Rows = 5
        .Cols = 6
        
        .TextMatrix(0, 0) = "Date"
        .TextMatrix(1, 0) = Format(dtpDate.Value, "dd MMMM yyyy")
        .TextMatrix(2, 0) = Format(dtpDate.Value - 1, "dd MMMM yyyy")
        .TextMatrix(3, 0) = "Total"

        
        
        For Each temCat In saleCats
            If (findNetSale(dtpDate.Value - 1, dtpDate.Value, temCat.SaleCategoryID, UserStoreID)) > 0 Then
                
                .Cols = .Cols + 1
                
                .TextMatrix(0, Col) = temCat.SaleCategory
                sum1 = findNetSale(dtpDate.Value, dtpDate.Value, temCat.SaleCategoryID, UserStoreID)
                .TextMatrix(1, Col) = Format(sum1, "#,##0.00")
                sum2 = findNetSale(DateSerial(Year(dtpDate.Value), Month(dtpDate.Value), 1), dtpDate.Value - 1, temCat.SaleCategoryID, UserStoreID)
                .TextMatrix(2, Col) = Format(sum2, "#,##0.00")
                                
                If temCat.Cash = True Then
                    cashSum1 = cashSum1 + sum1
                    cashSum2 = cashSum2 + sum2
                ElseIf temCat.Unit = False Then
                    nonCashSum1 = nonCashSum1 + sum1
                    nonCashSum2 = nonCashSum2 + sum2
                ElseIf temCat.Unit = True Then
                    unitSum1 = unitSum1 + sum1
                    unitSum2 = unitSum2 + sum2
                Else
                
                End If
                
                .TextMatrix(3, Col) = Format(sum1 + sum2, "#,##0.00")
                
                
                Col = Col + 1
            End If
        Next
        
        .TextMatrix(0, Col) = "Cash Sale"
        .TextMatrix(1, Col) = Format(cashSum1, "#,##0.00")
        .TextMatrix(2, Col) = Format(cashSum2, "#,##0.00")
        .TextMatrix(3, Col) = Format(cashSum1 + cashSum2, "#,##0.00")
        
        
        .TextMatrix(0, Col + 1) = "Other Sale"
        .TextMatrix(1, Col + 1) = Format(nonCashSum1, "#,##0.00")
        .TextMatrix(2, Col + 1) = Format(nonCashSum2, "#,##0.00")
        .TextMatrix(3, Col + 1) = Format(nonCashSum1 + nonCashSum2, "#,##0.00")
        
        .TextMatrix(0, Col + 2) = "All Sale"
        .TextMatrix(1, Col + 2) = Format(nonCashSum1 + cashSum1, "#,##0.00")
        .TextMatrix(2, Col + 2) = Format(nonCashSum2 + cashSum2, "#,##0.00")
        .TextMatrix(3, Col + 2) = Format(nonCashSum1 + nonCashSum2 + cashSum1 + cashSum2, "#,##0.00")
        
        .TextMatrix(0, Col + 3) = "Unit Trasnfer"
        .TextMatrix(1, Col + 3) = Format(unitSum1, "#,##0.00")
        .TextMatrix(2, Col + 3) = Format(unitSum2, "#,##0.00")
        .TextMatrix(3, Col + 3) = Format(unitSum1 + unitSum2, "#,##0.00")
        
        .TextMatrix(0, Col + 4) = "Current Stock Value"
        .TextMatrix(1, Col + 4) = Format(findStockValue(UserStoreID, 0), "#,##0.00")
        
    End With
End Sub
