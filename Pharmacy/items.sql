USE [coop]
GO
/****** Object:  Table [dbo].[tblItem]    Script Date: 02/18/2013 17:23:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblItem](
	[ItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[VTM] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VMP] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AMP] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AMPP] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VMPP] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Display] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Code] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vtmId] [bigint] NULL,
	[vmpId] [bigint] NULL,
	[vmppId] [bigint] NULL,
	[isAtm] [bit] NULL,
	[isVtm] [bit] NULL,
	[isAmp] [bit] NULL,
	[isVmp] [bit] NULL,
	[isAmpp] [bit] NULL,
	[isVmpp] [bit] NULL,
	[UnitId] [bigint] NULL,
	[TradeNameID] [bigint] NULL CONSTRAINT [DF__tblItem__TradeNa__32767D0B]  DEFAULT ((0)),
	[GenericNameID] [bigint] NULL CONSTRAINT [DF__tblItem__Generic__336AA144]  DEFAULT ((0)),
	[ItemCategoryID] [bigint] NULL CONSTRAINT [DF__tblItem__ItemCat__345EC57D]  DEFAULT ((0)),
	[StrengthUnitID] [bigint] NULL CONSTRAINT [DF__tblItem__Strengt__3552E9B6]  DEFAULT ((0)),
	[IssueUnitID] [bigint] NULL CONSTRAINT [DF__tblItem__IssueUn__36470DEF]  DEFAULT ((0)),
	[PackUnitID] [bigint] NULL CONSTRAINT [DF__tblItem__PackUni__373B3228]  DEFAULT ((0)),
	[StrengthOfIssueUnit] [float] NULL CONSTRAINT [DF__tblItem__Strengt__382F5661]  DEFAULT ((0)),
	[IssueUnitsPerPack] [float] NULL CONSTRAINT [DF__tblItem__IssueUn__39237A9A]  DEFAULT ((0)),
	[ROL] [float] NULL CONSTRAINT [DF__tblItem__ROL__3A179ED3]  DEFAULT ((0)),
	[ROQ] [float] NULL CONSTRAINT [DF__tblItem__ROQ__3B0BC30C]  DEFAULT ((0)),
	[MinQty] [float] NULL CONSTRAINT [DF__tblItem__MinQty__3BFFE745]  DEFAULT ((0)),
	[ManufacturerID] [bigint] NULL CONSTRAINT [DF__tblItem__Manufac__3CF40B7E]  DEFAULT ((0)),
	[ImporterID] [bigint] NULL CONSTRAINT [DF__tblItem__Importe__3DE82FB7]  DEFAULT ((0)),
	[Comments] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[upsize_ts] [timestamp] NULL,
	[priceByItem] [bit] NOT NULL CONSTRAINT [DF_tblItem_priceByItem]  DEFAULT ((1)),
	[priceByBatch] [bit] NOT NULL CONSTRAINT [DF_tblItem_priceByBatch]  DEFAULT ((0)),
 CONSTRAINT [aaaaatblItem_PK] PRIMARY KEY NONCLUSTERED 
(
	[ItemID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'17' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ⴋ橳紩䴛➬ỗꙓꑆ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ItemID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ItemID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'6870' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'뎫ꙁ蜍䷘ڄⳔ攮ࠪ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=6870 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'VTM' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'VTM' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VTM'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2895' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'捿部ｫ䒏傓꒥㸺' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2895 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'VMP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'VMP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMP'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'4440' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㝸ᚣ랸乮↪嚪渖' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=4440 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'AMP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'AMP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMP'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2925' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ㇴ孍伥㎛᳸' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2925 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'AMPP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'AMPP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'AMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1665' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ﯛ䀻疈儮Ꜣ䴰' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1665 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'VMPP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'VMPP' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'VMPP'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'3225' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'�뙿ⴷ䮍֤汧胱屖' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=3225 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Display' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Display' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Display'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㢪�栭伦릺┭懿㋤' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Code' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Code' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Code'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vtmId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vtmId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vtmId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vtmId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vtmId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmpId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmpId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmpId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmpId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmpId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmppId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmppId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmppId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmppId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'vmppId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVtm'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isAmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'isVmpp'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'UnitId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'UnitId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'UnitId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'UnitId'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'UnitId'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'蠟䓚䔦즅ჶ�ꙫ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TradeNameID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'TradeNameID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'TradeNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'차䒦ڭ䴵좬䮛Ἕ嬐' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'GenericNameID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'GenericNameID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'GenericNameID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'1770' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'焆妛䘺䒌鵓♦ﮑ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1770 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ItemCategoryID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ItemCategoryID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ItemCategoryID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'㩱莊뫬䯭재弴￵喽' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'StrengthUnitID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'StrengthUnitID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'緞냋ዚ䘑讀䓑繟' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'IssueUnitID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'IssueUnitID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'皭ᠪឧ仗떂䶝﵀�' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PackUnitID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PackUnitID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'PackUnitID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ﮀ嬘论䉊욭켖㊍땇' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'StrengthOfIssueUnit' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'14' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'StrengthOfIssueUnit' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'7' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'StrengthOfIssueUnit'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'2160' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꞇѩ䐼䦢붇鲠ᐽ給' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2160 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'IssueUnitsPerPack' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'15' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'IssueUnitsPerPack' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'7' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'IssueUnitsPerPack'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'냻꤃䤖ྸT薵ｖ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ROL' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'16' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ROL' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'7' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROL'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'䰽䯐䕆熞᯿漗' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ROQ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'17' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ROQ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'7' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ROQ'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꙴ혬䚑榬鋝퓽皰' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MinQty' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'18' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'8' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MinQty' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'7' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'MinQty'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'༈�伂䮨ⶩ�⥰뭣' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ManufacturerID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'19' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ManufacturerID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ManufacturerID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'DefaultValue', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'⋫�뷀䳀삠٧䵃ᓗ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DecimalPlaces', @value=N'255' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'ImporterID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'20' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'ImporterID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'4' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'ImporterID'

GO
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1033' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'GUID', @value=N'ꥌ궖�䰓厬誟⽡쳡' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Comments' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'21' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Comments' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'12' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'Comments'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'upsize_ts'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'upsize_ts'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'upsize_ts'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'upsize_ts'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'upsize_ts'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByBatch'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByBatch'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByBatch'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByBatch'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem', @level2type=N'COLUMN', @level2name=N'priceByBatch'

GO
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'6/23/2008 7:58:57 PM' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'6/20/2009 3:57:05 PM' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=0x02 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Filter', @value=N'((tblItem.Code="3860"))' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_FilterOnLoad', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_HideNewField', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderBy', @value=N'[tblItem].[Display], [tblItem].[VTM], [tblItem].[isAmp], tblItem.ItemID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=True ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOnLoad', @value=True ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=0x00 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TableMaxRecords', @value=10000 ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_TotalsRow', @value=False ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'tblItem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'3295' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'

GO
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'tblItem'
