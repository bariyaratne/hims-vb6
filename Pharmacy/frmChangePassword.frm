VERSION 5.00
Begin VB.Form frmChangePassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Change My Password"
   ClientHeight    =   2370
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5400
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   5400
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   3960
      TabIndex        =   7
      Top             =   1680
      Width           =   1215
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "Save"
      Height          =   495
      Left            =   2400
      TabIndex        =   6
      Top             =   1680
      Width           =   1215
   End
   Begin VB.TextBox txtRP 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2400
      PasswordChar    =   "*"
      TabIndex        =   5
      Top             =   1200
      Width           =   2775
   End
   Begin VB.TextBox txtNewP 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2400
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   720
      Width           =   2775
   End
   Begin VB.TextBox txtOldP 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2400
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   240
      Width           =   2775
   End
   Begin VB.Label Label3 
      Caption         =   "Re-enter New Password"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   2295
   End
   Begin VB.Label Label2 
      Caption         =   "Enter New Password"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "Enter Old Password"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   2295
   End
End
Attribute VB_Name = "frmChangePassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnSave_Click()
    If Trim(txtOldP.text) = "" Then
        MsgBox "Enter the Old Password"
        txtOldP.SetFocus
        Exit Sub
    End If
    If Trim(txtNewP.text) = "" Then
        MsgBox "Enter the a new Password"
        txtNewP.SetFocus
        Exit Sub
    End If
    If Trim(txtRP.text) <> Trim(txtNewP.text) Then
        MsgBox "New password and Rentered passwords are not matching"
        txtRP.SetFocus
        Exit Sub
    End If
    Dim myUser As New clsStaff
    myUser.StaffID = UserID
    
    If Trim(txtOldP.text) <> DecreptedWord(myUser.Password) Then
        MsgBox "Wrong the Old Password"
        txtOldP.SetFocus
        Exit Sub
    End If
    
    myUser.Password = EncreptedWord(txtNewP.text)
    myUser.saveData
    
    MsgBox "Saved"
    
    Unload Me
    
End Sub
