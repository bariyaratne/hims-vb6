VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Refill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varRefillID As Long
    Private varCatogeryID As Long
    Private varItemID As Long
    Private varBatchID As Long
    Private varStoreID As Long
    Private varSaleBillID As Long
    Private varSaleId As Long
    Private varAmount As Double
    Private varFreeAmount As Double
    Private varDate As Date
    Private varTime As Date
    Private varStaffID As Long
    Private varCheckedStaffID As Long
    Private varDistributorID As Long
    Private varPrice As Double
    Private varDiscount As Double
    Private varDiscountPercent As Double
    Private varNetPrice As Double
    Private varRefillBillID As Long
    Private varComments As String
    Private varOrderID As Long
    Private varOrderBillID As Long
    Private varPurchase As Boolean
    Private varAutoRequest As Boolean
    Private varManualRequest As Boolean
    Private varIssueReceive As Boolean
    Private varDOM As Date
    Private varDOE As Date
    Private varLastPPrice As Double
    Private varLastSPrice As Double
    Private varLastWPrice As Double
    Private varSPrice As Double
    Private varPPrice As Double
    Private varWPrice As Double
    Private varPackPPrice As Double
    Private varReturned As Boolean
    Private varReturnedUserID As Long
    Private varReturnedCUserID As Long
    Private varReturnedDate As Date
    Private varReturnedTime As Date
    Private varReturnedAmount As Double
    Private varReturnedValue As Double
    Private varupsize_ts

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblRefill Where RefillID = " & varRefillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !CatogeryID = varCatogeryID
        !ItemID = varItemID
        !BatchID = varBatchID
        !StoreID = varStoreID
        !SaleBillID = varSaleBillID
        !SaleId = varSaleId
        !Amount = varAmount
        !FreeAmount = varFreeAmount
        !Date = varDate
        !Time = varTime
        !StaffID = varStaffID
        !CheckedStaffID = varCheckedStaffID
        !DistributorID = varDistributorID
        !Price = varPrice
        !Discount = varDiscount
        !DiscountPercent = varDiscountPercent
        !NetPrice = varNetPrice
        !RefillBillID = varRefillBillID
        !Comments = varComments
        !OrderID = varOrderID
        !OrderBillID = varOrderBillID
        !Purchase = varPurchase
        !AutoRequest = varAutoRequest
        !ManualRequest = varManualRequest
        !IssueReceive = varIssueReceive
        !DOM = varDOM
        !DOE = varDOE
        !LastPPrice = varLastPPrice
        !LastSPrice = varLastSPrice
        !LastWPrice = varLastWPrice
        !SPrice = varSPrice
        !PPrice = varPPrice
        !WPrice = varWPrice
        !PackPPrice = varPackPPrice
        !Returned = varReturned
        !ReturnedUserID = varReturnedUserID
        !ReturnedCUserID = varReturnedCUserID
        !ReturnedDate = varReturnedDate
        !ReturnedTime = varReturnedTime
        !ReturnedAmount = varReturnedAmount
        !ReturnedValue = varReturnedValue
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varRefillID = !NewID
        Else
            varRefillID = !RefillID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblRefill WHERE RefillID = " & varRefillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!RefillID) Then
               varRefillID = !RefillID
            End If
            If Not IsNull(!CatogeryID) Then
               varCatogeryID = !CatogeryID
            End If
            If Not IsNull(!ItemID) Then
               varItemID = !ItemID
            End If
            If Not IsNull(!BatchID) Then
               varBatchID = !BatchID
            End If
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!SaleBillID) Then
               varSaleBillID = !SaleBillID
            End If
            If Not IsNull(!SaleId) Then
               varSaleId = !SaleId
            End If
            If Not IsNull(!Amount) Then
               varAmount = !Amount
            End If
            If Not IsNull(!FreeAmount) Then
               varFreeAmount = !FreeAmount
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!CheckedStaffID) Then
               varCheckedStaffID = !CheckedStaffID
            End If
            If Not IsNull(!DistributorID) Then
               varDistributorID = !DistributorID
            End If
            If Not IsNull(!Price) Then
               varPrice = !Price
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!DiscountPercent) Then
               varDiscountPercent = !DiscountPercent
            End If
            If Not IsNull(!NetPrice) Then
               varNetPrice = !NetPrice
            End If
            If Not IsNull(!RefillBillID) Then
               varRefillBillID = !RefillBillID
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!OrderID) Then
               varOrderID = !OrderID
            End If
            If Not IsNull(!OrderBillID) Then
               varOrderBillID = !OrderBillID
            End If
            If Not IsNull(!Purchase) Then
               varPurchase = !Purchase
            End If
            If Not IsNull(!AutoRequest) Then
               varAutoRequest = !AutoRequest
            End If
            If Not IsNull(!ManualRequest) Then
               varManualRequest = !ManualRequest
            End If
            If Not IsNull(!IssueReceive) Then
               varIssueReceive = !IssueReceive
            End If
            If Not IsNull(!DOM) Then
               varDOM = !DOM
            End If
            If Not IsNull(!DOE) Then
               varDOE = !DOE
            End If
            If Not IsNull(!LastPPrice) Then
               varLastPPrice = !LastPPrice
            End If
            If Not IsNull(!LastSPrice) Then
               varLastSPrice = !LastSPrice
            End If
            If Not IsNull(!LastWPrice) Then
               varLastWPrice = !LastWPrice
            End If
            If Not IsNull(!SPrice) Then
               varSPrice = !SPrice
            End If
            If Not IsNull(!PPrice) Then
               varPPrice = !PPrice
            End If
            If Not IsNull(!WPrice) Then
               varWPrice = !WPrice
            End If
            If Not IsNull(!PackPPrice) Then
               varPackPPrice = !PackPPrice
            End If
            If Not IsNull(!Returned) Then
               varReturned = !Returned
            End If
            If Not IsNull(!ReturnedUserID) Then
               varReturnedUserID = !ReturnedUserID
            End If
            If Not IsNull(!ReturnedCUserID) Then
               varReturnedCUserID = !ReturnedCUserID
            End If
            If Not IsNull(!ReturnedDate) Then
               varReturnedDate = !ReturnedDate
            End If
            If Not IsNull(!ReturnedTime) Then
               varReturnedTime = !ReturnedTime
            End If
            If Not IsNull(!ReturnedAmount) Then
               varReturnedAmount = !ReturnedAmount
            End If
            If Not IsNull(!ReturnedValue) Then
               varReturnedValue = !ReturnedValue
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varRefillID = 0
    varCatogeryID = 0
    varItemID = 0
    varBatchID = 0
    varStoreID = 0
    varSaleBillID = 0
    varSaleId = 0
    varAmount = 0
    varFreeAmount = 0
    varDate = Empty
    varTime = Empty
    varStaffID = 0
    varCheckedStaffID = 0
    varDistributorID = 0
    varPrice = 0
    varDiscount = 0
    varDiscountPercent = 0
    varNetPrice = 0
    varRefillBillID = 0
    varComments = Empty
    varOrderID = 0
    varOrderBillID = 0
    varPurchase = False
    varAutoRequest = False
    varManualRequest = False
    varIssueReceive = False
    varDOM = Empty
    varDOE = Empty
    varLastPPrice = 0
    varLastSPrice = 0
    varLastWPrice = 0
    varSPrice = 0
    varPPrice = 0
    varWPrice = 0
    varPackPPrice = 0
    varReturned = False
    varReturnedUserID = 0
    varReturnedCUserID = 0
    varReturnedDate = Empty
    varReturnedTime = Empty
    varReturnedAmount = 0
    varReturnedValue = 0
    varupsize_ts = Empty
End Sub

Public Property Let RefillID(ByVal vRefillID As Long)
    Call clearData
    varRefillID = vRefillID
    Call loadData
End Property

Public Property Get RefillID() As Long
    RefillID = varRefillID
End Property

Public Property Let CatogeryID(ByVal vCatogeryID As Long)
    varCatogeryID = vCatogeryID
End Property

Public Property Get CatogeryID() As Long
    CatogeryID = varCatogeryID
End Property

Public Property Let ItemID(ByVal vItemID As Long)
    varItemID = vItemID
End Property

Public Property Get ItemID() As Long
    ItemID = varItemID
End Property

Public Property Let BatchID(ByVal vBatchID As Long)
    varBatchID = vBatchID
End Property

Public Property Get BatchID() As Long
    BatchID = varBatchID
End Property

Public Property Let StoreID(ByVal vStoreID As Long)
    varStoreID = vStoreID
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let SaleBillID(ByVal vSaleBillID As Long)
    varSaleBillID = vSaleBillID
End Property

Public Property Get SaleBillID() As Long
    SaleBillID = varSaleBillID
End Property

Public Property Let SaleId(ByVal vSaleId As Long)
    varSaleId = vSaleId
End Property

Public Property Get SaleId() As Long
    SaleId = varSaleId
End Property

Public Property Let Amount(ByVal vAmount As Double)
    varAmount = vAmount
End Property

Public Property Get Amount() As Double
    Amount = varAmount
End Property

Public Property Let FreeAmount(ByVal vFreeAmount As Double)
    varFreeAmount = vFreeAmount
End Property

Public Property Get FreeAmount() As Double
    FreeAmount = varFreeAmount
End Property

Public Property Let refillDate(ByVal vDate As Date)
    varDate = vDate
End Property

Public Property Get refillDate() As Date
    Date = varDate
End Property

Public Property Let Time(ByVal vTime As Date)
    varTime = vTime
End Property

Public Property Get Time() As Date
    Time = varTime
End Property

Public Property Let StaffID(ByVal vStaffID As Long)
    varStaffID = vStaffID
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let CheckedStaffID(ByVal vCheckedStaffID As Long)
    varCheckedStaffID = vCheckedStaffID
End Property

Public Property Get CheckedStaffID() As Long
    CheckedStaffID = varCheckedStaffID
End Property

Public Property Let DistributorID(ByVal vDistributorID As Long)
    varDistributorID = vDistributorID
End Property

Public Property Get DistributorID() As Long
    DistributorID = varDistributorID
End Property

Public Property Let Price(ByVal vPrice As Double)
    varPrice = vPrice
End Property

Public Property Get Price() As Double
    Price = varPrice
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let DiscountPercent(ByVal vDiscountPercent As Double)
    varDiscountPercent = vDiscountPercent
End Property

Public Property Get DiscountPercent() As Double
    DiscountPercent = varDiscountPercent
End Property

Public Property Let NetPrice(ByVal vNetPrice As Double)
    varNetPrice = vNetPrice
End Property

Public Property Get NetPrice() As Double
    NetPrice = varNetPrice
End Property

Public Property Let RefillBillID(ByVal vRefillBillID As Long)
    varRefillBillID = vRefillBillID
End Property

Public Property Get RefillBillID() As Long
    RefillBillID = varRefillBillID
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let OrderID(ByVal vOrderID As Long)
    varOrderID = vOrderID
End Property

Public Property Get OrderID() As Long
    OrderID = varOrderID
End Property

Public Property Let OrderBillID(ByVal vOrderBillID As Long)
    varOrderBillID = vOrderBillID
End Property

Public Property Get OrderBillID() As Long
    OrderBillID = varOrderBillID
End Property

Public Property Let Purchase(ByVal vPurchase As Boolean)
    varPurchase = vPurchase
End Property

Public Property Get Purchase() As Boolean
    Purchase = varPurchase
End Property

Public Property Let AutoRequest(ByVal vAutoRequest As Boolean)
    varAutoRequest = vAutoRequest
End Property

Public Property Get AutoRequest() As Boolean
    AutoRequest = varAutoRequest
End Property

Public Property Let ManualRequest(ByVal vManualRequest As Boolean)
    varManualRequest = vManualRequest
End Property

Public Property Get ManualRequest() As Boolean
    ManualRequest = varManualRequest
End Property

Public Property Let IssueReceive(ByVal vIssueReceive As Boolean)
    varIssueReceive = vIssueReceive
End Property

Public Property Get IssueReceive() As Boolean
    IssueReceive = varIssueReceive
End Property

Public Property Let DOM(ByVal vDOM As Date)
    varDOM = vDOM
End Property

Public Property Get DOM() As Date
    DOM = varDOM
End Property

Public Property Let DOE(ByVal vDOE As Date)
    varDOE = vDOE
End Property

Public Property Get DOE() As Date
    DOE = varDOE
End Property

Public Property Let LastPPrice(ByVal vLastPPrice As Double)
    varLastPPrice = vLastPPrice
End Property

Public Property Get LastPPrice() As Double
    LastPPrice = varLastPPrice
End Property

Public Property Let LastSPrice(ByVal vLastSPrice As Double)
    varLastSPrice = vLastSPrice
End Property

Public Property Get LastSPrice() As Double
    LastSPrice = varLastSPrice
End Property

Public Property Let LastWPrice(ByVal vLastWPrice As Double)
    varLastWPrice = vLastWPrice
End Property

Public Property Get LastWPrice() As Double
    LastWPrice = varLastWPrice
End Property

Public Property Let SPrice(ByVal vSPrice As Double)
    varSPrice = vSPrice
End Property

Public Property Get SPrice() As Double
    SPrice = varSPrice
End Property

Public Property Let PPrice(ByVal vPPrice As Double)
    varPPrice = vPPrice
End Property

Public Property Get PPrice() As Double
    PPrice = varPPrice
End Property

Public Property Let WPrice(ByVal vWPrice As Double)
    varWPrice = vWPrice
End Property

Public Property Get WPrice() As Double
    WPrice = varWPrice
End Property

Public Property Let PackPPrice(ByVal vPackPPrice As Double)
    varPackPPrice = vPackPPrice
End Property

Public Property Get PackPPrice() As Double
    PackPPrice = varPackPPrice
End Property

Public Property Let Returned(ByVal vReturned As Boolean)
    varReturned = vReturned
End Property

Public Property Get Returned() As Boolean
    Returned = varReturned
End Property

Public Property Let ReturnedUserID(ByVal vReturnedUserID As Long)
    varReturnedUserID = vReturnedUserID
End Property

Public Property Get ReturnedUserID() As Long
    ReturnedUserID = varReturnedUserID
End Property

Public Property Let ReturnedCUserID(ByVal vReturnedCUserID As Long)
    varReturnedCUserID = vReturnedCUserID
End Property

Public Property Get ReturnedCUserID() As Long
    ReturnedCUserID = varReturnedCUserID
End Property

Public Property Let ReturnedDate(ByVal vReturnedDate As Date)
    varReturnedDate = vReturnedDate
End Property

Public Property Get ReturnedDate() As Date
    ReturnedDate = varReturnedDate
End Property

Public Property Let ReturnedTime(ByVal vReturnedTime As Date)
    varReturnedTime = vReturnedTime
End Property

Public Property Get ReturnedTime() As Date
    ReturnedTime = varReturnedTime
End Property

Public Property Let ReturnedAmount(ByVal vReturnedAmount As Double)
    varReturnedAmount = vReturnedAmount
End Property

Public Property Get ReturnedAmount() As Double
    ReturnedAmount = varReturnedAmount
End Property

Public Property Let ReturnedValue(ByVal vReturnedValue As Double)
    varReturnedValue = vReturnedValue
End Property

Public Property Get ReturnedValue() As Double
    ReturnedValue = varReturnedValue
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property


