VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSaleBills 
   Caption         =   "Sale Bills"
   ClientHeight    =   8715
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   17565
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8715
   ScaleWidth      =   17565
   Begin VB.CommandButton btnBillPrint 
      Caption         =   "Print"
      Height          =   375
      Left            =   8280
      TabIndex        =   14
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton btnPreview 
      Caption         =   "Preview"
      Height          =   375
      Left            =   6960
      TabIndex        =   13
      Top             =   1200
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridBills 
      Height          =   6375
      Left            =   120
      TabIndex        =   12
      Top             =   1680
      Width           =   17295
      _ExtentX        =   30506
      _ExtentY        =   11245
      _Version        =   393216
      SelectionMode   =   1
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   16200
      TabIndex        =   11
      Top             =   8160
      Width           =   1215
   End
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1440
      TabIndex        =   10
      Top             =   8160
      Width           =   1215
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   120
      TabIndex        =   9
      Top             =   8160
      Width           =   1215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   6960
      TabIndex        =   8
      Top             =   240
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   840
      TabIndex        =   6
      Top             =   1200
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   171114499
      CurrentDate     =   41272
   End
   Begin MSDataListLib.DataCombo cmbDept 
      Height          =   360
      Left            =   2160
      TabIndex        =   1
      Top             =   240
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   2160
      TabIndex        =   3
      Top             =   720
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   4440
      TabIndex        =   7
      Top             =   1200
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   171114499
      CurrentDate     =   41272
   End
   Begin VB.Label Label4 
      Caption         =   "To"
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "From"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Sale Category"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Department"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "frmSaleBills"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnBillPrint_Click()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        printBillPos id, 1
    End If
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillCombos()
    fillSaleCats cmbCat
    fillDepts cmbDept
End Sub

Private Sub fillGrid()
    Dim sql As String
    sql = "SELECT dbo.tblSaleBill.SaleBillID, dbo.tblSaleBill.Date AS [Bill Date], dbo.tblSaleBill.Time AS [Bill Time], IssuedFrom.Store AS [Issued From], dbo.tblSaleCategory.SaleCategory AS [Sale Category], Cashier.Name AS [Billed User], OutCustomer.FirstName AS Customer, OutCustomer.Phone AS [Phone No], dbo.tblBHT.BHT AS [Bht No], BilledStaff.Name AS [Billed Staff or Member], BhtPatient.FirstName AS [Inward Patient], IssuedTo.Store AS [Issued To], dbo.tblSaleBill.Price AS [Gross Total], dbo.tblSaleBill.Discount, dbo.tblSaleBill.NetPrice AS [Net Total], dbo.tblSaleBill.Cancelled, dbo.tblSaleBill.CancelledDate AS [Cancelled Date], dbo.tblSaleBill.CancelledTime AS [Cancelled Time], dbo.tblSaleBill.Returned, dbo.tblSaleBill.ReturnedDate AS [Returned Date], dbo.tblSaleBill.ReturnedTime AS [Returned Time], dbo.tblSaleBill.ReturnedValue AS [Return Value] " & _
            "FROM dbo.tblSaleCategory RIGHT OUTER JOIN dbo.tblPatientMainDetails OutCustomer RIGHT OUTER JOIN                dbo.tblStore IssuedFrom RIGHT OUTER JOIN dbo.tblStaff Cashier RIGHT OUTER JOIN dbo.tblSaleBill ON Cashier.StaffID = dbo.tblSaleBill.StaffID LEFT OUTER JOIN dbo.tblStore IssuedTo ON dbo.tblSaleBill.BilledUnitID = IssuedTo.StoreID LEFT OUTER JOIN dbo.tblStaff BilledStaff ON dbo.tblSaleBill.StaffID = BilledStaff.StaffID ON IssuedFrom.StoreID = dbo.tblSaleBill.StoreID LEFT OUTER JOIN dbo.tblBHT LEFT OUTER JOIN dbo.tblPatientMainDetails BhtPatient ON dbo.tblBHT.PatientID = BhtPatient.PatientID ON dbo.tblSaleBill.BilledBHTID = dbo.tblBHT.BHTID ON  OutCustomer.PatientID = dbo.tblSaleBill.BilledOutPatientID ON dbo.tblSaleCategory.SaleCategoryID = dbo.tblSaleBill.SaleCategoryID " & _
            "WHERE (dbo.tblSaleBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFrom.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTo.Value, "dd MMMM yyyy") & "', 102)) "
    If IsNumeric(cmbCat.BoundText) = True Then
        sql = sql & "AND (dbo.tblSaleBill.SaleCategoryID = " & Val(cmbCat.BoundText) & ") "
    End If
    If IsNumeric(cmbDept.BoundText) = True Then
        sql = sql & "AND (dbo.tblSaleBill.StoreID = " & Val(cmbDept.BoundText) & ")"
    End If
    sql = sql & " ORDER BY dbo.tblSaleBill.SaleBillID"
    FillAnyGrid sql, gridBills
End Sub

Private Sub saveSettings()
    SaveCommonSettings Me
End Sub

Private Sub getSettings()
    GetCommonSettings Me
    dtpFrom.Value = Date
    dtpTo.Value = Date
End Sub

Private Sub btnExcel_Click()
    GridToExcel1 gridBills, "All Bills - " & cmbCat.text & vbTab & cmbDept.text, Format(dtpFrom.Value, LongDateFormat) & " - " & Format(dtpTo.Value, LongDateFormat)
End Sub

Private Sub btnPreview_Click()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            previewSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            previewSaleBillA4 id, Me.hwnd
        End If
    End If

End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim csetPrinter As New cSetDfltPrinter
    csetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    GridPrint gridBills, ThisReportFormat, "All Bills - " & cmbCat.text & vbTab & cmbDept.text, Format(dtpFrom.Value, LongDateFormat) & " - " & Format(dtpTo.Value, LongDateFormat)
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    Call fillGrid
End Sub

Private Sub cmbDept_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        btnProcess_Click
    ElseIf KeyCode = vbKeyEscape Then
        cmbDept.text = Empty
    End If
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        btnProcess_Click
    ElseIf KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
    End If
End Sub

Private Sub Form_Load()
    fillCombos
    getSettings
    cmbDept.BoundText = UserStoreID
    btnProcess_Click
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    saveSettings
End Sub

Private Sub Form_Resize()
    gridBills.Width = Me.Width - 500
    gridBills.Height = Me.Height - 3000
    btnClose.Top = Me.Height - 1250
    btnPrint.Top = btnClose.Top
    btnExcel.Top = btnClose.Top
End Sub

Private Sub gridBills_DblClick()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            previewSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            previewSaleBillA4 id, Me.hwnd
        End If
    End If
End Sub
