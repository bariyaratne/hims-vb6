Attribute VB_Name = "modPrintBill"
Option Explicit

Public Sub printBillPos(BillID As Long, copyNo As Integer)
    Dim csetPrinter     As New cSetDfltPrinter
    
    Dim MyBill As New SaleBill
    Dim MyPatient As New Patient
    Dim myStaff As New clsStaff
    
    Dim mySaleItem As SaleItem
    Dim MyItem As New Item
    
    
    
    MyBill.SaleBillID = BillID
    MyPatient.PatientID = MyBill.BilledOutPatientID
    myStaff.StaffID = MyBill.StaffID
    
   
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = BillPrinterName Then
            Set Printer = myP
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Dim temStrCopy As String
    
    If copyNo = 1 Then
    
        Printer.PaintPicture LoadPicture(App.Path & "\logo.bmp"), 1000, 0, 1600, 560
        
        
        
        Dim leftXDis As Long
        
        leftXDis = -500
        
    
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print

    ElseIf copyNo = 2 Then
            
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalName) / 2) + leftXDis
        Printer.Print DecreptedWord(HospitalName)
        
        temStrCopy = "Pharmacy Copy"
        
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
        
    Else
        
        
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalName) / 2) + leftXDis
        Printer.Print DecreptedWord(HospitalName)
        
        temStrCopy = "Cashier Copy"
        
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
    
    End If
    
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalAddress) / 2) + leftXDis
    Printer.Print (HospitalAddress)
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(institutiontelephone) / 2) + leftXDis
    Printer.Print (institutiontelephone)
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(institutionWebSite) / 2) + leftXDis
    Printer.Print (institutionWebSite)
    
    
    Printer.Print
    
    
    Printer.FontSize = 10
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Pharmacy Receipt") / 2) + leftXDis
    Printer.Print "Pharmacy Receipt"

    Dim displacement As Integer
    
    displacement = -4
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 9
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & " " & MyPatient.FirstName
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.SaleBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.BillDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.BillDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 9
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 32 + displacement
    Tab3 = 40 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    
    Printer.Print Tab(Tab1); Left("Item", 308); Tab(Tab2); Right((Space(4)) & "Qty", 4); Tab(Tab3); Right((Space(10)) & "Price", 10)
    
    With rsTem
        temSQL = "Select * from tblSale where  SaleBillId = " & MyBill.SaleBillID
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            Set mySaleItem = New SaleItem
            Set MyItem = New Item
        
            mySaleItem.SaleId = !SaleId
            MyItem.id = mySaleItem.ItemID
            
            Printer.Print Tab(Tab1); Left(MyItem.Display, 308); Tab(Tab2); Right((Space(4)) & mySaleItem.Amount, 4); Tab(Tab3); Right((Space(10)) & Format(mySaleItem.Price, "0.00"), 10)
            
            .MoveNext
        Wend
        .Close
    End With
    
    Printer.Print
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        
        'Printer.Print Tab(Tab1); MyLine
        
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Price, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetPrice)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        Printer.Print
        
        .EndDoc
    End With
End Sub


