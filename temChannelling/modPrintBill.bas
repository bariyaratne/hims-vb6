Attribute VB_Name = "modPrintBill"
Option Explicit
    Dim cSetDPrinter As New cSetDfltPrinter
    
    
Public Sub PrintPosBillOld(BillID As Long)

    Dim myPf As New clsPatientFacility
    Dim myPt As New clsPatient
    Dim mySp As New clsSpeciality
    Dim myDoc As New clsDoctor
    Dim SetPrinter As Boolean
    
    myPf.PatientFacility_ID = BillID
    myPt.Patient_ID = myPf.patientid
    myDoc.Doctor_ID = myPf.Staff_ID
    mySp.Speciality_ID = myDoc.DoctorSpeciality_ID

    SetPrinter = False
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = BillPrinterName Then
            Set Printer = MyPrinter
            SetPrinter = True
        End If
    Next
    
    cSetDPrinter.SetPrinterAsDefault (BillPrinterName)

    

    Dim TemRows As Long

    Dim LineStart As Integer
    Dim ColonPos As Integer
    Dim VariableStart As Integer
    
    LineStart = 8 + 7
    ColonPos = 14 + 7 + 7
    VariableStart = 16 + 7 + 7

    With Printer
        
    Printer.Font = "Tw Cen MT Condensed Extra Bold"
    Printer.FontSize = 13
    Printer.ForeColor = vbRed
    Printer.Print InstitutionName
    Printer.FontName = "Tw Cen MT Condensed"
    Printer.FontSize = 12
    Printer.Print InstitutionDescreption
    Printer.ForeColor = vbBlack
    Printer.FontSize = 11
    Printer.Print InstitutionAddress
    
    Printer.Print InstitutionTelephone
             
             Printer.Print
        
        Printer.Print Tab(LineStart); "Patient ";
        Printer.Print Tab(ColonPos); ":";
        Printer.Print Tab(VariableStart); myPt.FirstName
        
        Printer.Print
        
        Printer.Print Tab(LineStart); "Consultant"; ;
        Printer.Print Tab(ColonPos); " : ";
        Printer.Print Tab(VariableStart); UCase(FindDoctorFromID(Val(myPf.Staff_ID)))
        
        'Printer.Print
        
        Printer.Print Tab(LineStart); "Speciality"; ;
        Printer.Print Tab(ColonPos); " : ";
        Printer.Print Tab(VariableStart); UCase(mySp.Speciality)
        
        Printer.Print
        
        Printer.FontSize = 11
        
        Printer.Print Tab(LineStart); "Appo. No."; ;
        Printer.Print Tab(ColonPos); " : "; ;
        Printer.Print Tab(VariableStart); myPf.DaySerial
        
        
        Printer.FontSize = 11
        
        Printer.Print
        
        Printer.Print Tab(LineStart); "Date "; ;
        Printer.Print Tab(ColonPos); " : "; ;
        Printer.Print Tab(VariableStart); Format(myPf.AppointmentDate, DefaultLongDate)
        
        Printer.Print Tab(LineStart); "Time"; ;
        Printer.Print Tab(ColonPos); " : "; ;
        Printer.Print Tab(VariableStart); Format(myPf.AppointmentTime, "hh:mm AMPM")
        
        Printer.Print
        Printer.Print "     "
    
        Printer.Print Tab(LineStart); "Doctor Fee";
        Printer.Print Tab(ColonPos); " : ";
        Printer.Print Tab(VariableStart + 8 - Len(Format(myPf.PersonalFee, "0.00"))); Format(myPf.PersonalFee, "0.00")
        
        Printer.Print Tab(LineStart); "Hospital Fee";
        Printer.Print Tab(ColonPos); " : ";
        Printer.Print Tab(VariableStart + 8 - Len(Format(myPf.InstitutionFee, "0.00"))); Format(myPf.InstitutionFee, "0.00")
            
        If myPf.OtherFee <> 0 Then
            Printer.Print Tab(LineStart); "Other Fee";
            Printer.Print Tab(ColonPos); " : ";
            Printer.Print Tab(VariableStart + 8 - Len(Format(myPf.OtherFee, "0.00"))); Format(myPf.OtherFee, "0.00")
        
        End If
            
            
        Printer.Print "     "
            
        Printer.Print Tab(LineStart); "Total Fee";
        Printer.Print Tab(ColonPos); " : ";
        Printer.Print Tab(VariableStart + 8 - Len(Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00"))); Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00")
        Printer.Print "     "
        Printer.Print Tab(LineStart); "Payment";
        Printer.Print Tab(ColonPos); " : ";
        
        Printer.Print Tab(VariableStart); myPf.PaymentMode
        
        Printer.Print "     "
        Printer.Print "      "
        
        Printer.Print Tab(LineStart); "--------------------"
        
        Printer.Print Tab(LineStart); FindStaffFromID(myPf.User_ID)
        
        Printer.Print Tab(LineStart); Format(myPf.BookingTime, "hh:mm AMPM")
        
        Printer.Print Tab(LineStart); Format(myPf.BookingTime, DefaultShortDate);
        
        Printer.EndDoc
    End With

End Sub


Public Sub PrintPosBill(BillID As Long, copyNo As Integer)
        Dim csetPrinter     As New cSetDfltPrinter
    
    Dim myPf As New clsPatientFacility
    Dim myPt As New clsPatient
    Dim mySp As New clsSpeciality
    Dim myDoc As New clsDoctor
    Dim myStaff As New clsStaff
    
    
    Do
    
        myPf.PatientFacility_ID = BillID
        myPt.Patient_ID = myPf.patientid
        myStaff.Staff_ID = myPf.User_ID
        myDoc.Doctor_ID = myPf.Staff_ID
        mySp.Speciality_ID = myDoc.DoctorSpeciality_ID
    
    Loop Until myPf.InstitutionFee <> 0 Or myPt.FirstName <> ""
    
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = BillPrinterName Then
            Set Printer = myP
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    'ShowPrinter MyForm, cdlPDHidePrintToFile Or cdlPDNoSelection Or cdlPDNoPageNums
    
'
'    If SelectForm(PaperName, formHdc) <> 1 Then
'        MsgBox "Printer Error"
'        Exit Sub
'    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Dim temStrCopy As String
    
     
    If copyNo = 1 Then
    
        Printer.PaintPicture LoadPicture(App.Path & "\logo.bmp"), 1000, 0, 1600, 560
        
        
        
        Dim leftXDis As Long
        
        leftXDis = -500
        
    
        'Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
 
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionAddress) / 2) + leftXDis
    Printer.Print (InstitutionAddress)
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionTelephone) / 2) + leftXDis
    Printer.Print InstitutionTelephone
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionDescreption) / 2) + leftXDis
    Printer.Print InstitutionDescreption



    ElseIf copyNo = 2 Then
            
'        Printer.FontSize = 9
'        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionName) / 2) + leftXDis
'        Printer.Print InstitutionName
        
        temStrCopy = "Cashier Copy"
        
        Printer.FontSize = 9
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
        
    Else
        
        
'        Printer.FontSize = 9
'        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionName) / 2) + leftXDis
'        Printer.Print DecreptedWord(InstitutionName)
        
        temStrCopy = "Doctor Copy"
        
        Printer.FontSize = 9
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
    
    End If
    
   

    Printer.Print
    
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Channelling Receipt") / 2) + leftXDis
    Printer.Print "Channelling Receipt"

    Dim displacement As Integer
    
    displacement = -4
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 9
    
    Printer.Print
    
        
        
        
        Printer.Print Tab(Tab1); "Patient : ";
        Printer.Print myPt.FirstName
        
        Printer.Print Tab(Tab1); "Doctor : "; ;
        Printer.Print UCase(FindDoctorFromID(Val(myPf.Staff_ID)))
        
        Printer.Print Tab(Tab1); "Speciality : "; ;
        Printer.Print UCase(mySp.Speciality)
        
        
        Printer.Print Tab(Tab1); "Appointment No. : "; ;
        Printer.Print myPf.DaySerial
       
        Printer.Print Tab(Tab1); "Appointment Date : "; ;
        Printer.Print Format(myPf.AppointmentDate, DefaultLongDate)
        
        Printer.Print Tab(Tab1); "Appointment Time : "; ;
        Printer.Print Format(myPf.AppointmentTime, "hh:mm AMPM")
    
        Printer.Print
    
        Printer.Print Tab(Tab1); "Physicians' Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.PersonalFee, "0.00"))); Format(myPf.PersonalFee, "0.00")
        
        Printer.Print Tab(Tab1); "Administration Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.InstitutionFee, "0.00"))); Format(myPf.InstitutionFee, "0.00")
            
            If myPf.InstitutionFee = 0 Then
                DoEvents
            End If
            
            
        If myPf.OtherFee <> 0 Then
            Printer.Print Tab(Tab1); "Other Charge";
            Printer.Print Tab(Tab1 + 24); " : ";
            Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.OtherFee, "0.00"))); Format(myPf.OtherFee, "0.00")
        
        End If
            
            
        Printer.Print "     "
            
        Printer.Print Tab(Tab1); "Total Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00"))); Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00")
        Printer.Print "     "
        Printer.Print Tab(Tab1); "Payment";
        Printer.Print Tab(Tab1 + 24); " : ";
        
        Printer.Print myPf.PaymentMode
        
       ' Printer.Print "     "
        Printer.Print "      "
        
'        Printer.Print Tab(Tab1); "--------------------"
        
        Printer.Print Tab(Tab1); "Receipt No.: " & myPf.PatientFacility_ID
        Printer.Print Tab(Tab1); Format(myPf.BookingDate, "dd MMM yyyy"); "   ";
        Printer.Print Format(myPf.BookingTime, "hh:mm AMPM");
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.StaffUserName)
             
        
        Printer.EndDoc

End Sub


Public Sub PrintPosBillRefund(BillID As Long, copyNo As Integer)
    Dim csetPrinter     As New cSetDfltPrinter
    
    Dim myPf As New clsPatientFacility
    Dim myPt As New clsPatient
    Dim mySp As New clsSpeciality
    Dim myDoc As New clsDoctor
    Dim myStaff As New clsStaff
    
    
    
    myPf.PatientFacility_ID = BillID
    myPt.Patient_ID = myPf.patientid
    myStaff.Staff_ID = myPf.User_ID
    myDoc.Doctor_ID = myPf.FacilityStaff_ID
    
    
    
    
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = BillPrinterName Then
            Set Printer = myP
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    'ShowPrinter MyForm, cdlPDHidePrintToFile Or cdlPDNoSelection Or cdlPDNoPageNums
    
'
'    If SelectForm(PaperName, formHdc) <> 1 Then
'        MsgBox "Printer Error"
'        Exit Sub
'    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Dim temStrCopy As String
    
     
    If copyNo = 1 Then
    
        Printer.PaintPicture LoadPicture(App.Path & "\logo.bmp"), 1000, 0, 1600, 560
        
        
        
        Dim leftXDis As Long
        
        leftXDis = -500
        
    
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print

    ElseIf copyNo = 2 Then
            
        Printer.FontSize = 9
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionName) / 2) + leftXDis
        Printer.Print InstitutionName
        
        temStrCopy = "Cashier Copy"
        
        Printer.FontSize = 9
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
        
    Else
        
        
        Printer.FontSize = 9
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionName) / 2) + leftXDis
        Printer.Print DecreptedWord(InstitutionName)
        
        temStrCopy = "Doctor Copy"
        
        Printer.FontSize = 9
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
    
    End If
    
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionAddress) / 2) + leftXDis
    Printer.Print (InstitutionAddress)
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionTelephone) / 2) + leftXDis
    Printer.Print InstitutionTelephone
    
'    Printer.FontSize = 9
'    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionDescreption) / 2) + leftXDis
'    Printer.Print InstitutionDescreption
'
'
'    Printer.Print
    
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Channelling Receipt") / 2) + leftXDis
    Printer.Print "Channelling Refund Receipt"

    Dim displacement As Integer
    
    displacement = -4
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 9
    
    Printer.Print
    
        
        
        
        Printer.Print Tab(Tab1); "Patient : ";
        Printer.Print myPt.FirstName
        
        
        Printer.Print Tab(Tab1); "Speciality : "; ;
        Printer.Print UCase(mySp.Speciality)

        
        Printer.Print Tab(Tab1); "Consultant : "; ;
        Printer.Print UCase(FindDoctorFromID(Val(myPf.Staff_ID)))
        
        Printer.Print Tab(Tab1); "Appo. No. : "; ;
        Printer.Print myPf.DaySerial
       
        Printer.Print Tab(Tab1); "Date : "; ;
        Printer.Print Format(myPf.AppointmentDate, DefaultLongDate)
        
        Printer.Print Tab(Tab1); "Time : "; ;
        Printer.Print Format(myPf.AppointmentTime, "hh:mm AMPM")
    
        Printer.Print Tab(Tab1); "Physicians Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.PersonalFee, "0.00"))); Format(myPf.PersonalFee, "0.00")
        
        Printer.Print Tab(Tab1); "Administration Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.InstitutionFee, "0.00"))); Format(myPf.InstitutionFee, "0.00")
            
        If myPf.OtherFee <> 0 Then
            Printer.Print Tab(Tab1); "Other Charge";
            Printer.Print Tab(Tab1 + 24); " : ";
            Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.OtherFee, "0.00"))); Format(myPf.OtherFee, "0.00")
        
        End If
            
            
        Printer.Print "     "
            
        Printer.Print Tab(Tab1); "Total Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00"))); Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00")
        Printer.Print "     "
        Printer.Print Tab(Tab1); "Payment";
        Printer.Print Tab(Tab1 + 24); " : ";
        
        Printer.Print myPf.PaymentMode
        
        Printer.Print "     "
        Printer.Print "      "
        
        Printer.Print Tab(Tab1); "--------------------"
        
        Printer.Print Tab(Tab1); "Receipt No. : " & vbTab & myPf.PatientFacility_ID & vbTab;
        Printer.Print Format(myPf.BookingTime, DefaultShortDate);
        Printer.Print Format(myPf.BookingTime, "hh:mm AMPM")
        Printer.Print Tab(Tab1); FindStaffFromID(myPf.User_ID)
             
        
        Printer.EndDoc
    
End Sub



Public Sub PrintPosBillCancell(BillID As Long, copyNo As Integer)
    Dim csetPrinter     As New cSetDfltPrinter
    
    Dim myPf As New clsPatientFacility
    Dim myPt As New clsPatient
    Dim mySp As New clsSpeciality
    Dim myDoc As New clsDoctor
    Dim myStaff As New clsStaff
    
    
    
    myPf.PatientFacility_ID = BillID
    myPt.Patient_ID = myPf.patientid
    myStaff.Staff_ID = myPf.User_ID
    myDoc.Doctor_ID = myPf.FacilityStaff_ID
    
    
    
    
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = BillPrinterName Then
            Set Printer = myP
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    
    'ShowPrinter MyForm, cdlPDHidePrintToFile Or cdlPDNoSelection Or cdlPDNoPageNums
    
'
'    If SelectForm(PaperName, formHdc) <> 1 Then
'        MsgBox "Printer Error"
'        Exit Sub
'    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Dim temStrCopy As String
    
     
    If copyNo = 1 Then
    
        Printer.PaintPicture LoadPicture(App.Path & "\logo.bmp"), 1000, 0, 1600, 560
        
        
        
        Dim leftXDis As Long
        
        leftXDis = -500
        
    
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print

    ElseIf copyNo = 2 Then
            
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionName) / 2) + leftXDis
        Printer.Print InstitutionName
        
        temStrCopy = "Cashier Copy"
        
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
        
    Else
        
        
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionName) / 2) + leftXDis
        Printer.Print DecreptedWord(InstitutionName)
        
        temStrCopy = "Doctor Copy"
        
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temStrCopy) / 2) + leftXDis
        Printer.Print temStrCopy
    
    End If
    
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionAddress) / 2) + leftXDis
    Printer.Print (InstitutionAddress)
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionTelephone) / 2) + leftXDis
    Printer.Print InstitutionTelephone
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(InstitutionDescreption) / 2) + leftXDis
    Printer.Print InstitutionDescreption
    
    
    Printer.Print
    
    
    Printer.FontSize = 9
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Channelling Receipt") / 2) + leftXDis
    Printer.Print "Channelling Cancellation Receipt"

    Dim displacement As Integer
    
    displacement = -4
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 9
    
    Printer.Print
    
        
        
        
        Printer.Print Tab(Tab1); "Patient ";
        Printer.Print Tab(Tab1 + 24); ":";
        Printer.Print myPt.FirstName
        
        Printer.Print
        
        Printer.Print Tab(Tab1); "Consultant"; ;
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print UCase(FindDoctorFromID(Val(myPf.Staff_ID)))
        
        'Printer.Print
        
        Printer.Print Tab(Tab1); "Speciality"; ;
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print UCase(mySp.Speciality)
        
        Printer.Print
        
        Printer.FontSize = 11
        
        Printer.Print Tab(Tab1); "Appo. No."; ;
        
        Printer.Print myPf.DaySerial
        
        
        Printer.FontSize = 11
        
        Printer.Print
        
        Printer.Print Tab(Tab1); "Date "; ;
        
        Printer.Print Format(myPf.AppointmentDate, DefaultLongDate)
        
        Printer.Print Tab(Tab1); "Time"; ;
        
        Printer.Print Format(myPf.AppointmentTime, "hh:mm AMPM")
        
        Printer.Print
        Printer.Print "     "
    
        Printer.Print Tab(Tab1); "Physicians Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.PersonalFee, "0.00"))); Format(myPf.PersonalFee, "0.00")
        
        Printer.Print Tab(Tab1); "Administration Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.InstitutionFee, "0.00"))); Format(myPf.InstitutionFee, "0.00")
            
        If myPf.OtherFee <> 0 Then
            Printer.Print Tab(Tab1); "Other Charge";
            Printer.Print Tab(Tab1 + 24); " : ";
            Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.OtherFee, "0.00"))); Format(myPf.OtherFee, "0.00")
        
        End If
            
            
        Printer.Print "     "
            
        Printer.Print Tab(Tab1); "Total Charge";
        Printer.Print Tab(Tab1 + 24); " : ";
        Printer.Print Tab(Tab1 + 28 + 8 - Len(Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00"))); Format(myPf.InstitutionFee + myPf.PersonalFee + myPf.OtherFee, "0.00")
        Printer.Print "     "
        Printer.Print Tab(Tab1); "Payment";
        Printer.Print Tab(Tab1 + 24); " : ";
        
        Printer.Print myPf.PaymentMode
        
        Printer.Print "     "
        Printer.Print "      "
        
        Printer.Print Tab(Tab1); "--------------------"
        
        Printer.Print Tab(Tab1); FindStaffFromID(myPf.User_ID)
        
        Printer.Print Tab(Tab1); Format(myPf.BookingTime, "hh:mm AMPM")
        
        Printer.Print Tab(Tab1); Format(myPf.BookingTime, DefaultShortDate);
        
             
        
        Printer.EndDoc
    
End Sub



