VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmAgentDaySummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Agent Day Summery"
   ClientHeight    =   8415
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10755
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8415
   ScaleWidth      =   10755
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Tag             =   "SS"
      Top             =   7800
      Width           =   5415
   End
   Begin MSFlexGridLib.MSFlexGrid gridBooking 
      Height          =   7095
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   12515
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin btButtonEx.ButtonEx bttnCancel 
      Height          =   495
      Left            =   9480
      TabIndex        =   3
      Top             =   7800
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   495
      Left            =   8160
      TabIndex        =   2
      Top             =   7800
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   135725059
      CurrentDate     =   39656
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   6840
      TabIndex        =   5
      Top             =   7800
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "frmAgentDaySummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim rsTemAgentReport As New ADODB.Recordset
    Dim csetPrinter As New cSetDfltPrinter
    Dim temTopic As String
    Dim temSubTopic As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temGroupBy As String
    Dim temOrderBy As String

Private Sub btnExcel_Click()
    GridToExcel gridBooking, temTopic, temSubTopic
End Sub

Private Sub bttnCancel_Click()
    Unload Me
End Sub

Private Sub bttnPrintOld_Click()
'    Dim TemResponce As Integer
'    csetPrinter.SetPrinterAsDefault (ReportPrinterName)
'
'    Dim RetVal As Integer
'    RetVal = SelectForm(BillPaperName, Me.hwnd)
'    Select Case RetVal
'        Case FORM_NOT_SELECTED   ' 0
'            TemResponce = MsgBox("You have not selected a printer form to print, Please goto Preferances and Printing preferances to set a valid printer form.", vbExclamation, "Bill Not Printed")
'        Case FORM_SELECTED   ' 1
'            With DataEnvironment1.rscmmdNewAgentBookings
'                If .State = 1 Then .Close
'                temSQL = " SELECT tblDoctor.DoctorName, tblDoctor.DoctorName, tblInstitutions.InstitutionName, tblPatientMainDetails.FirstName, tblPatientFacility.*, tblPatientFacility.AppointmentDate " & _
'                            "FROM ((tblPatientFacility LEFT JOIN tblDoctor ON tblPatientFacility.Staff_ID = tblDoctor.Doctor_ID) LEFT JOIN tblPatientMainDetails ON tblPatientFacility.PatientID = tblPatientMainDetails.Patient_ID) LEFT JOIN tblInstitutions ON tblPatientFacility.Agent_ID = tblInstitutions.Institution_ID " & _
'                            "WHERE ((Not (tblInstitutions.InstitutionName) Is Null) AND ((tblPatientFacility.AppointmentDate) = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "#) AND ((tblInstitutions.CashAgent) = True)   ) " & _
'                            "Order by tblInstitutions.InstitutionName, tblDoctor.DoctorName"
'                .Source = temSQL
'                .Open
'            End With
'            With dtrNewAgentBookings
'                Set .DataSource = DataEnvironment1.rscmmdNewAgentBookings
'                .Sections("ReportHeader").Controls("lblName").Caption = InstitutionName
'                .Sections("ReportHeader").Controls("lblAddress").Caption = InstitutionAddress
'                .Sections("ReportHeader").Controls("lblTopic").Caption = "Daily Agent Summery"
'                .Sections("ReportHeader").Controls("lblSubTopic").Caption = Format(dtpDate.Value, "dd MMMM yyyy")
'                .Show
'            End With
'
'        Case FORM_ADDED   ' 2
'            TemResponce = MsgBox("New paper size added.", vbExclamation, "New Paper size")
'    End Select
'



End Sub


Private Sub bttnPrint_Click()
    'On Error Resume Next

    Dim SetPrinter As New cSetDfltPrinter
    
    SetPrinter.SetPrinterAsDefault ""
    
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = cmbPrinter.Text Then
            Set Printer = MyPrinter
        End If
    Next
    
    temTopic = "Agent Day Summery"
    temSubTopic = Format(dtpDate.Value, "yyyy MMMM dd")
    
    SetPrinter.SetPrinterAsDefault cmbPrinter.Text
    
    Dim myPR As GridPrintSettings
    
    myPR = getDefaultGridPrint
    
    myPR.Topic = temTopic
    myPR.Subtopic = temSubTopic
    
    PrintGrid gridBooking, myPR
    
End Sub

Private Sub dtpDate_Change()
    Dim D(2) As Integer
    Dim p(0) As Integer
    temTopic = "Daily Agent Summery"
    temTopic = "Date " & Format(dtpDate.Value, "dd MMM yyyy")
    
    
    
    D(0) = 5: D(1) = 6
    temSQL = " SELECT  Left(tblInstitutions.InstitutionName ,  20) as [Agent], Left( tblDoctor.DoctorName, 15) as [Doctor],  tblPatientFacility.DaySerial as [No],  Left( tblPatientMainDetails.FirstName ,10) as [Patient], iif(Cancelled , 'Canc' , '') + iif(Refund , 'Refu' , '') as [Remarks], format(tblPatientFacility.InstitutionDue, '0.00') as [Hos Fee],  format( tblPatientFacility.PersonalDue, '0.00') as [Doc Fee] " & _
                            "FROM ((tblPatientFacility LEFT JOIN tblDoctor ON tblPatientFacility.Staff_ID = tblDoctor.Doctor_ID) LEFT JOIN tblPatientMainDetails ON tblPatientFacility.PatientID = tblPatientMainDetails.Patient_ID) LEFT JOIN tblInstitutions ON tblPatientFacility.Agent_ID = tblInstitutions.Institution_ID " & _
                            "WHERE ((Not (tblInstitutions.InstitutionName) Is Null) AND ((tblPatientFacility.AppointmentDate) = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "#) AND ((tblInstitutions.CashAgent) = True)   ) " & _
                            "Order by tblInstitutions.InstitutionName, tblDoctor.DoctorName"
    FillAnyGrid temSQL, gridBooking, 0, D, p
    gridBooking.TextMatrix(gridBooking.Rows - 1, 1) = gridBooking.Rows - 2
End Sub

Private Sub Form_Load()
    dtpDate.Value = Date
    dtpDate_Change
    Call fillPrinters
    Call GetSettings
End Sub


Private Sub GetSettings()
    GetCommonSettings Me
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
End Sub

Private Sub fillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub
