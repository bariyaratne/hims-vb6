Attribute VB_Name = "modGridPrint"
Option Explicit

Public Type GridFont
    Bold As Boolean
    Italic As Boolean
    FontName As String
    FontSize As Integer
    Underline As Boolean
    StrikeThrough As Boolean
    Color As Double
End Type

Public Enum GridAlignment
    AlignLeft
    AlignRight
    AlignCentre
    AlignJustified
End Enum

Public Type GridPrintSettings
    LeftMargin As Double
    RightMargin As Double
    TopMargin As Double
    BottomMargin As Double
    TopicFont As GridFont
    TopicAlignment As GridAlignment
    SubTopicFont As GridFont
    SubTopicAlignment As GridAlignment
    ColHeaderFont As GridFont
    ColHeaderAlignment As GridAlignment
    ColFont As GridFont
    ColAlignment As GridAlignment
    ColSpace As Double
    Topic As String
    Subtopic As String
End Type

Public Function getDefaultGridPrint() As GridPrintSettings
    With getDefaultGridPrint
        .LeftMargin = 720
        .RightMargin = 720
        .TopMargin = 720
        .BottomMargin = 720
        .ColSpace = 200
        
        .TopicFont.FontName = "Arial"
        .TopicFont.FontSize = 13
        .TopicFont.Bold = True
        .TopicFont.Italic = False
        .TopicFont.Underline = True
        .TopicAlignment = AlignCentre
        
        .SubTopicFont.FontName = "Arial"
        .SubTopicFont.FontSize = 12
        .SubTopicFont.Bold = True
        .SubTopicFont.Italic = False
        .SubTopicFont.Underline = False
        .SubTopicAlignment = AlignCentre
        
        .ColHeaderFont.FontName = "Arial"
        .ColHeaderFont.FontSize = 11
        .ColHeaderFont.Bold = True
        .ColHeaderFont.Italic = False
        .ColHeaderFont.Underline = True
        .ColHeaderAlignment = AlignLeft
        
        .ColFont.FontName = "Arial"
        .ColFont.FontSize = 10
        .ColFont.Bold = False
        .ColFont.Italic = False
        .ColFont.Underline = True
        .ColAlignment = AlignCentre
                
        .Topic = InstitutionName
        .Subtopic = InstitutionAddress
        
    End With
End Function

Public Sub PrintGrid(grid As MSFlexGrid, Settings As GridPrintSettings)
    Dim Cols As Integer
    Dim ColStartX() As Double
    Dim ColEndX() As Double
    Dim CurrentX As Double
    Dim i As Integer
    Dim n As Integer
    Dim pgNo As Integer
    Dim temY As Double
    Dim temWidth As Double
    Dim temAlignment As GridAlignment
    Dim TemText As String
    Dim temFont As GridFont
    
    
    temY = Settings.TopMargin
    PrintAlignedText Settings.Topic, Settings.TopicAlignment, Settings.TopicFont, Settings.LeftMargin, Printer.Width - Settings.RightMargin, temY, temY
    temY = Printer.CurrentY
    PrintAlignedText Settings.Subtopic, Settings.SubTopicAlignment, Settings.SubTopicFont, Settings.LeftMargin, Printer.Width - Settings.RightMargin, temY, temY
    
    With Printer
        .FontName = Settings.ColFont.FontName
        .FontSize = Settings.ColFont.FontSize
        .FontBold = Settings.ColFont.Bold
        .FontItalic = Settings.ColFont.Italic
        .FontStrikethru = Settings.ColFont.StrikeThrough
    End With
    
    With grid
        Cols = .Cols
        
        ReDim ColStartX(Cols) As Double
        ReDim ColEndX(Cols) As Double
                
        CurrentX = Settings.LeftMargin
        
        
        For i = 0 To .Cols - 1
            temWidth = (.ColWidth(i) / .Width) * (Printer.Width - (Settings.RightMargin + Settings.LeftMargin))
            ColStartX(i) = CurrentX
            ColEndX(i) = CurrentX + temWidth
            CurrentX = CurrentX + temWidth + Settings.ColSpace
        Next
    
        pgNo = 1
    
        temFont = Settings.ColFont
    
        For n = 0 To .Rows - 1
        
            temY = Printer.CurrentY
             If temY + Printer.TextHeight("A") > Printer.Height - (Settings.TopMargin + Settings.BottomMargin) Then
                PrintAlignedText "Page " & pgNo, AlignRight, temFont, Settings.LeftMargin, Settings.RightMargin, Printer.Height - Settings.BottomMargin, Printer.Height - Settings.BottomMargin
                
                Printer.NewPage
                Printer.CurrentY = Settings.TopMargin
                temY = Settings.TopMargin
                pgNo = pgNo + 1
             End If
            
            
            For i = 0 To .Cols - 1
                
                            
                
                .Row = n
                .col = i
                
              '  Debug.Print .CellAlignment
'
'                If .CellAlignment = 0 Or .CellAlignment = 1 Or .CellAlignment = 2 Then
'                    temAlignment = AlignLeft
'                ElseIf .CellAlignment = 3 Or .CellAlignment = 4 Or .CellAlignment = 5 Then
'                    temAlignment = AlignCentre
'                ElseIf .CellAlignment = 6 Or .CellAlignment = 7 Or .CellAlignment = 8 Then
'                    temAlignment = AlignRight
'                Else
'                    temAlignment = AlignLeft
'                End If
                
                If IsNumeric(.Text) = True Then
                    temAlignment = AlignRight
                Else
                    temAlignment = AlignLeft
                End If
                
                TemText = .Text
                
                If n = 0 Then
                    temFont = Settings.ColHeaderFont
                Else
                    temFont = Settings.ColFont
                End If
                
                PrintAlignedText TemText, temAlignment, temFont, ColStartX(i), ColEndX(i), temY, temY
                
            Next i
        
        
        Next n
        
        PrintAlignedText "Page " & pgNo, AlignRight, temFont, Settings.LeftMargin, Settings.RightMargin, Printer.Height - Settings.BottomMargin, Printer.Height - Settings.BottomMargin
    
        Printer.EndDoc
        
    End With
End Sub

Private Sub PrintAlignedText(TextToPrint As String, TextAlignment As GridAlignment, TextFont As GridFont, X1, X2, Y1, Y2)
    Dim temStr As String
    Dim temX1 As Double
    Dim temY1 As Double
    Dim temX2 As Double
    Dim texY2 As Double
    temStr = getMaxPrintText(TextToPrint, TextFont, X1, X2)
    With Printer
        .FontName = TextFont.FontName
        .FontSize = TextFont.FontSize
        .FontBold = TextFont.Bold
        .FontItalic = TextFont.Italic
        .FontStrikethru = TextFont.StrikeThrough
        If TextAlignment = AlignLeft Then
            temX1 = X1
        ElseIf TextAlignment = AlignRight Then
'            Debug.Print X1
'            Debug.Print X2
'            Debug.Print (Printer.TextWidth(temStr))
            temX1 = X2 - Printer.TextWidth(temStr)
        ElseIf TextAlignment = AlignCentre Then
            temX1 = ((X1 + X2) / 2) - (Printer.TextWidth(temStr) / 2)
        ElseIf TextAlignment = AlignJustified Then
            temX1 = X1
        Else
            temX1 = X1
        End If
        .CurrentX = temX1
        .CurrentY = Y1
        Printer.Print temStr
    End With
End Sub

Private Function getMaxPrintText(TextToTrim As String, TextFont As GridFont, X1, X2) As String
    With Printer
        .FontName = TextFont.FontName
        .FontSize = TextFont.FontSize
        .FontBold = TextFont.Bold
        .FontItalic = TextFont.Italic
        .FontStrikethru = TextFont.StrikeThrough
        If (X2 - X1) > .TextWidth(TextToTrim) Then
            getMaxPrintText = TextToTrim
            Exit Function
        End If
        While (X2 - X1) < .TextWidth(TextToTrim)
            TextToTrim = Left(TextToTrim, Len(TextToTrim) - 1)
        Wend
        getMaxPrintText = TextToTrim
    End With
End Function
