VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmTodaysMyAllDoctorViceBookings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Today's My All Bookings - Doctor-vice"
   ClientHeight    =   2475
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6225
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   6225
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   4800
      TabIndex        =   5
      Top             =   1800
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   495
      Left            =   3480
      TabIndex        =   4
      Top             =   1800
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Caption         =   "Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DataComboStaff 
      Bindings        =   "frmTodaysMyAllDoctorViceBookings.frx":0000
      Height          =   360
      Left            =   1920
      TabIndex        =   2
      Top             =   240
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1920
      TabIndex        =   3
      Top             =   720
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   52363267
      CurrentDate     =   39442
   End
   Begin MSComCtl2.DTPicker dtpApp 
      Height          =   375
      Left            =   1920
      TabIndex        =   6
      Top             =   1200
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   52363267
      CurrentDate     =   39442
   End
   Begin VB.Label Label3 
      Caption         =   "Appointment Date"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Booking Date"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   720
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "User"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   2175
   End
End
Attribute VB_Name = "frmTodaysMyAllDoctorViceBookings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim csetPrinter As New cSetDfltPrinter

Private Sub bttnPrint_Click()

Const PreSHape = "SHAPE {"
Const Sql = "SELECT tblPatientFacility.*, tblDoctor.DoctorName, tblTitle.Title FROM tblTitle RIGHT JOIN (tblPatientFacility LEFT JOIN tblDoctor ON tblPatientFacility.Staff_ID = tblDoctor.Doctor_ID) ON tblTitle.Title_ID = tblDoctor.DoctorTitle_ID Where "
Const PostSHape = "(((tblPatientFacility.HospitalFacility_ID)=10))}  AS newMyAllDocterBookings COMPUTE newMyAllDocterBookings, COUNT(newMyAllDocterBookings.'PatientFacility_ID') AS TotalPatientCount, SUM(newMyAllDocterBookings.'CancelledNull') AS TotalCancellations, SUM(newMyAllDocterBookings.'RefundNull') AS TotalRefunds, SUM(newMyAllDocterBookings.'FullyPaidNull') AS TotalFullyPaid, SUM(newMyAllDocterBookings.'PatientAbsentNull') AS TotalAbsent, ANY(newMyAllDocterBookings.'Title') AS DoctorTitle, SUM(newMyAllDocterBookings.'PersonalDue') AS DoctorFee BY 'DoctorName'"

csetPrinter.SetPrinterAsDefault (ReportPrinterName)

    With DataEnvironment1
    
        
        If .rsnewMyAllDocterBookings_Grouping.State = 1 Then .rsnewMyAllDocterBookings_Grouping.Close
        
        'If .rscmmdAllDoctorPatients_Grouping.State = 1 Then .rscmmdAllDoctorPatients_Grouping.Close
        
        If DetailedCount = False Then
            If PayToDoctor = True Then
                .Commands!cmmdAllDoctorPatients_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "# and FullyPaid = True and Cancelled = False and Refund = False  and  ( User_ID = " & Val(DataComboStaff.BoundText) & " OR CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  and " & PostSHape
            Else
                .Commands!cmmdAllDoctorPatients_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#  and FullyPaid = True and Cancelled = False and Refund = False and patientabsent = false  and  ( User_ID = " & Val(DataComboStaff.BoundText) & " OR CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )   and " & PostSHape
            End If
            .cmmdAllDoctorPatients_Grouping
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl1").Visible = False
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl2").Visible = True
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl3").Visible = False
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl4").Visible = False
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl10").Visible = False
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl5").Visible = False
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl7").Visible = False
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl8").Visible = False
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl9").Visible = False
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Caption = "Total Patients"
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function1").Visible = False
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function2").Visible = True
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function3").Visible = False
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function4").Visible = False
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function5").Visible = False
        Else
            .Commands!cmmdAllDoctorPatients_Grouping.CommandText = PreSHape & Sql & "  BookingDate = #" & Format(dtpDate.Value, "dd MMMM yyyy") & "# AND appointmentdate = #" & Format(dtpApp.Value, "dd MMMM yyyy") & "#   and  ( User_ID = " & Val(DataComboStaff.BoundText) & " OR CreditSettleUser_ID = " & Val(DataComboStaff.BoundText) & " )  and " & PostSHape
            .cmmdAllDoctorPatients_Grouping
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl1").Visible = True
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl2").Visible = True
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl3").Visible = True
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl4").Visible = True
            DataReportAllPatients.Sections("Section1").Controls.Item("lbl10").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl5").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl7").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl8").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl9").Visible = True
            DataReportAllPatients.Sections("PageHeader").Controls.Item("lbl6").Caption = "Fully Paid"
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function1").Visible = True
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function2").Visible = True
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function3").Visible = True
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function4").Visible = True
            DataReportAllPatients.Sections("ReportFooter").Controls.Item("Function5").Visible = True
        End If
    End With
    With DataReportAllPatients
        If HospitalDetails = True Then
            .Sections("ReportHeader").Controls.Item("InstitutionName").Caption = InstitutionName
            .Sections("ReportHeader").Controls.Item("InstitutionAddress").Caption = InstitutionAddress
            .Sections("ReportHeader").Controls.Item("lbldate").Caption = "By " & DataComboStaff.Text & " /  Booking " & Format(dtpDate.Value, "dd MMMM yyyy") & "  /  Appointment : " & Format(dtpApp.Value, "dd MMMM yyyy")
            .Sections("ReportFooter").Controls.Item("ad1").Caption = LongAd
        Else
            .Sections("ReportHeader").Controls.Item("InstitutionName").Caption = Empty
            .Sections("ReportHeader").Controls.Item("InstitutionAddress").Caption = Empty
            .Sections("ReportHeader").Controls.Item("lbldate").Caption = Format(dtpDate.Value, "dd MMMM yyyy")
            .Sections("ReportFooter").Controls.Item("ad1").Caption = LongAd
        End If
        Set .DataSource = DataEnvironment1
        .Show
    End With

End Sub

Private Sub Form_Load()
    dtpDate.Value = Date
    dtpApp.Value = Date
    With DataComboStaff
        .RowMember = Empty
        .ListField = Empty
        .BoundColumn = Empty
    End With
    With DataEnvironment1.rsStaff
        If .State = 1 Then .Close
        .Source = "SELECT tblStaff.* FROM tblStaff ORDER BY StaffName"
        .Open
    End With
    With DataComboStaff
        .RowMember = "staff"
        .ListField = "StaffName"
        .BoundColumn = "Staff_ID"
    End With
    DataComboStaff.BoundText = UserID
End Sub
