VERSION 5.00
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmCancellationAgentPayments 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cancellation Agent Payment"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8475
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCancellationAgentPayments.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   8475
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   6240
      TabIndex        =   1
      Top             =   5280
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   4815
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   7695
      Begin btButtonEx.ButtonEx bttnSerch 
         Height          =   375
         Left            =   3240
         TabIndex        =   15
         Top             =   600
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Serch"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame2 
         Height          =   2175
         Left            =   360
         TabIndex        =   4
         Top             =   1200
         Width           =   6855
         Begin VB.Label lblUserName 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   375
            Left            =   3360
            TabIndex        =   14
            Top             =   1320
            Width           =   2895
         End
         Begin VB.Label lblAmount 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   375
            Left            =   1680
            TabIndex        =   13
            Top             =   1320
            Width           =   1455
         End
         Begin VB.Label lblDate 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   375
            Left            =   120
            TabIndex        =   12
            Top             =   1320
            Width           =   1335
         End
         Begin VB.Label lblAgentCode 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   375
            Left            =   4560
            TabIndex        =   11
            Top             =   480
            Width           =   1695
         End
         Begin VB.Label lblAgentName 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   375
            Left            =   120
            TabIndex        =   10
            Top             =   480
            Width           =   4215
         End
         Begin VB.Label Label5 
            Caption         =   "Agent Code"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   4560
            TabIndex        =   9
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label Label4 
            Alignment       =   2  'Center
            Caption         =   "Cash Received User Name"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3360
            TabIndex        =   8
            Top             =   1080
            Width           =   3135
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Caption         =   "Paid Amount"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1680
            TabIndex        =   7
            Top             =   1080
            Width           =   1455
         End
         Begin VB.Label Label2 
            Caption         =   "Date"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   6
            Top             =   1080
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Agent Name"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   5
            Top             =   240
            Width           =   1815
         End
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   495
         Left            =   480
         TabIndex        =   3
         Top             =   3600
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   873
         Appearance      =   3
         Enabled         =   0   'False
         Caption         =   "Cancel"
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtReceiptNo 
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   600
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmCancellationAgentPayments"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TemAgentCashsettleId As Long
Dim TemAgentId As Long

Private Sub bttnCancel_Click()
Dim A
A = MsgBox("Are you Sure, Cancel This Payment?", vbInformation + vbYesNo, "Verify")
If A = vbNo Then: Exit Sub

With DataEnvironment1.rssqlTem11
    If .State = 1 Then .Close
    .Open "SELECT* From tblAgentCashSettle Where AgentCashSettle_ID = " & TemAgentCashsettleId & ""
    If .RecordCount = 0 Then Exit Sub
    If !Cash = 0 Then A = MsgBox("This Recipt no already cancel Or Error please check", vbInformation + vbOKOnly, "No Payment"): Exit Sub
    
    !Cash = 0
    .Update
    
    If .State = 1 Then .Close
    
    .Open "Select* From tblInstitutions Where Institution_ID = " & TemAgentId & ""
    If .RecordCount = 0 Then Exit Sub
    
    !InstitutionCredit = Val(!InstitutionCredit) - Val(lblAmount.Caption)
    .Update
    
    If .State = 1 Then .Close

    
End With
Call ClearValues
bttnCancel.Enabled = False
End Sub

Private Sub bttnClose_Click()
Unload Me
End Sub

Private Sub bttnSerch_Click()
Call FindReceiptNo
End Sub

Private Sub FindReceiptNo()

With DataEnvironment1.rssqlTem11
    If .State = 1 Then .Close
    .Open "SELECT tblAgentCashSettle.*, tblInstitutions.InstitutionName,tblInstitutions.InstitutionCode, tblStaff.StaffListedName FROM tblStaff RIGHT JOIN (tblAgentCashSettle LEFT JOIN tblInstitutions ON tblAgentCashSettle.Institution_ID = tblInstitutions.Institution_ID) ON tblStaff.Staff_ID = tblAgentCashSettle.User_ID Where ReceiptNo = '" & txtReceiptNo.Text & "'"
    If .RecordCount = 0 Then Call ClearValues: Exit Sub
    
    lblAgentName.Caption = !InstitutionName
    lblAgentCode.Caption = !InstitutionCode
    lblDate.Caption = !SettledDate
    lblAmount.Caption = Format(!Cash, "#0.00")
    lblUserName.Caption = !StaffListedName
    TemAgentCashsettleId = !AgentCashSettle_ID
    TemAgentId = !Institution_ID
    bttnCancel.Enabled = True

    If .State = 1 Then .Close
End With

End Sub

Private Sub ClearValues()
    lblAgentName.Caption = Empty
    lblAgentCode.Caption = Empty
    lblDate.Caption = Empty
    lblAmount.Caption = Empty
    lblUserName.Caption = Empty
    TemAgentCashsettleId = Empty
    TemAgentId = Empty

End Sub

Private Sub txtReceiptNo_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Call FindReceiptNo
End Sub
