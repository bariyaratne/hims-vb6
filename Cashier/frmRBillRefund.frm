VERSION 5.00
Begin VB.Form frmRBillRefund 
   Caption         =   "Refund"
   ClientHeight    =   6285
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   6465
   LinkTopic       =   "Form1"
   ScaleHeight     =   6285
   ScaleWidth      =   6465
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnRefund 
      Caption         =   "Refund"
      Height          =   495
      Left            =   5160
      TabIndex        =   1
      Top             =   5640
      Width           =   1215
   End
   Begin VB.TextBox txtBillId 
      Height          =   375
      Left            =   5520
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmRBillRefund"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String

Private Sub btnRefund_Click()
    Dim MyCombo As Control
    Dim temId As Long
    Dim ProTot As Double
    Dim HosTot As Double
    Dim Tot As Double
    
    For Each MyCombo In Controls
        If Left(MyCombo.Name, 2) = "hf" Then
            temId = Val(Right(MyCombo.Name, Len(MyCombo.Name) - 2))
            Dim temPtSv As New PatientService
            temPtSv.PatientServiceID = temId
            temPtSv.HospitaChargelReturned = Val(MyCombo.text)
            temPtSv.ChargeReturned = temPtSv.ChargeReturned + Val(MyCombo.text)
            temPtSv.ChargeToGive = temPtSv.ChargeToGive - Val(MyCombo.text)
            temPtSv.HospitalChargeToGive = temPtSv.HospitalChargeToGive - Val(MyCombo.text)
            temPtSv.HospitalChargeToPay = temPtSv.HospitalChargeToPay - Val(MyCombo.text)
            temPtSv.ChargePaid = temPtSv.ChargePaid - Val(MyCombo.text)
            temPtSv.HospitalChargePaid = temPtSv.HospitalChargePaid - Val(MyCombo.text)
            temPtSv.saveData
            HosTot = HosTot + Val(MyCombo.text)
            Tot = Tot + Val(MyCombo.text)
        ElseIf Left(MyCombo.Name, 2) = "pf" Then
            temId = Val(Right(MyCombo.Name, Len(MyCombo.Name) - 2))
            Dim temProChg As New ProfessionalCharge
            temProChg.ProfessionalChargesID = temId
            temProChg.FeePaid = temProChg.Fee - Val(MyCombo.text)
            
            temProChg.saveData
            
'            Dim temPtSv As New PatientService
            temPtSv.PatientServiceID = temProChg.PatientServiceID
            temPtSv.ProfessionalChargeReturned = Val(MyCombo.text)
            temPtSv.ChargeReturned = temPtSv.ChargeReturned + Val(MyCombo.text)
            temPtSv.ChargeToGive = temPtSv.ChargeToGive - Val(MyCombo.text)
            temPtSv.ProfessionalChargeToGive = temPtSv.HospitalChargeToGive - Val(MyCombo.text)
            temPtSv.ProfessionalChargeToPay = temPtSv.HospitalChargeToPay - Val(MyCombo.text)
            temPtSv.ProfessionalChargePaid = temPtSv.ProfessionalChargePaid - Val(MyCombo.text)
            temPtSv.saveData
            
            ProTot = ProTot + Val(MyCombo.text)
            Tot = Tot + Val(MyCombo.text)
        End If
    Next

    Dim temBill As New IncomeBill

 With temBill
        .IncomeBillID = Val(txtBillId.text)
        .Returned = True
        .ReturnedDate = Date
        .ReturnedTime = Time
        .ReturnedPaymentComments = frmRServiceBills.txtPaymentMethod.text
        .ReturnedPaymentMethodID = Val(frmRServiceBills.cmbPaymentMethod.BoundText)
        .ReturnedUserID = UserID
        .ReturnedValue = Tot
        .NetTotalToPay = .NetTotal - Tot
        .GrossTotalToPay = .GrossTotal - Tot
        .saveData
    End With
    
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblIncomeReturnBill"
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        .AddNew
        !ReturnDate = Date
        !ReturnTime = Now
        !ReturnUserID = UserID
        !ReturnValue = Tot
        !IncomeBillID = Val(frmRServiceBills.txtOPDBillID.text)
        !PaymentMethodID = Val(frmRServiceBills.cmbPaymentMethod.BoundText)
        .Update
        .Close
    End With
    
    MsgBox "Refunded"
    Unload Me
End Sub


Private Sub FillGrid()


    Dim MyCombo As Control
    Dim MyLabel As Label
    
    
    Dim LeftMargin As Long
    Dim TopMargin As Long
    Dim BottomMargin As Long
    Dim ComboHeight As Long
    Dim LabelWidth As Long
    Dim ComboWidth As Long
    Dim VerticalSpace As Long
    Dim HorizentalSpace As Long
    
    BottomMargin = 9250
    LeftMargin = 120
    TopMargin = 160
    ComboHeight = 360
    LabelWidth = 2055
    ComboWidth = 3000
    VerticalSpace = 120
    HorizentalSpace = 400
    
    
    Dim IxItemHCount As Long
    Dim IxItemVCount As Long
    
    IxItemHCount = 1
    IxItemVCount = 0
    


    Dim TemString As String
    Dim temBillId As Long
    temBillId = Val(txtBillId.text)
    If temBillId = 0 Then Exit Sub
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT IncomeBillID, DisplayBillID, BillSettled, Cancelled, BHTID, PatientID, CompletedDate, NetTotal, NetTotalToPay " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE IncomeBillID = " & temBillId
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            Dim strTem  As String
            Dim rsTem1 As New ADODB.Recordset
            Dim rsTem2 As New ADODB.Recordset
            Dim dblNo1 As Double
            strTem = ""
            dblNo1 = 0
            If rsTem1.State = 1 Then rsTem1.Close
            
            
            temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & temBillId
            rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While rsTem1.EOF = False
                Dim temPtSv As New PatientService
                temPtSv.PatientServiceID = rsTem1!PatientServiceID
                
                IxItemVCount = IxItemVCount + 1
                
                If TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace)) > BottomMargin Then
                    IxItemHCount = IxItemHCount + 1
                    IxItemVCount = 1
                End If
                
                Set MyCombo = Me.Controls.Add("VB.TextBox", "hf" & temPtSv.PatientServiceID, Me)
                MyCombo.Visible = True
                MyCombo.Top = TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace))
                MyCombo.Left = LeftMargin + LabelWidth + ((IxItemHCount - 1) * (LeftMargin + LabelWidth + ComboWidth + HorizentalSpace))
                MyCombo.Width = ComboWidth
                
                Set MyLabel = Me.Controls.Add("VB.label", "lblhf" & temPtSv.PatientServiceID, Me)
                MyLabel.Visible = True
                MyLabel.Top = TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace))
                MyLabel.Left = LeftMargin + ((IxItemHCount - 1) * (LeftMargin + LabelWidth + ComboWidth + HorizentalSpace))
                MyLabel.Width = LabelWidth
                MyLabel.Height = ComboHeight
                Dim temSvSub As New ServiceSubcategory
                temSvSub.ServiceSubcategoryID = temPtSv.ServiceSubcategoryID
                MyLabel.Caption = "Hos. Refund for " & temSvSub.ServiceSubcategory
                
                
                temSQL = "SELECT * From tblProfessionalCharges WHERE  PatientServiceID = " & rsTem1!PatientServiceID
                If rsTem2.State = 1 Then rsTem2.Close
                rsTem2.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
                Dim myStaff As New clsStaff
                While rsTem2.EOF = False
                    Dim temProChg As New ProfessionalCharge
                    temProChg.ProfessionalChargesID = rsTem2!ProfessionalChargesID
                    
                    
                        
                    IxItemVCount = IxItemVCount + 1
                    
                    If TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace)) > BottomMargin Then
                        IxItemHCount = IxItemHCount + 1
                        IxItemVCount = 1
                    End If
                    
                    Set MyCombo = Me.Controls.Add("VB.TextBox", "pf" & temProChg.ProfessionalChargesID, Me)
                    MyCombo.Visible = True
                    MyCombo.Top = TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace))
                    MyCombo.Left = LeftMargin + LabelWidth + ((IxItemHCount - 1) * (LeftMargin + LabelWidth + ComboWidth + HorizentalSpace))
                    MyCombo.Width = ComboWidth
                    MyCombo.text = Format(temProChg.Fee, "0.00")
                    
                    Set MyLabel = Me.Controls.Add("VB.label", "lblpro" & temProChg.ProfessionalChargesID, Me)
                    MyLabel.Visible = True
                    MyLabel.Top = TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace))
                    MyLabel.Left = LeftMargin + ((IxItemHCount - 1) * (LeftMargin + LabelWidth + ComboWidth + HorizentalSpace))
                    MyLabel.Width = LabelWidth
                    MyLabel.Height = ComboHeight
                    Dim TemStaff As New Staff
                    Dim temSpaci As New Speciality
                    If temProChg.StaffID = 0 Then
                        MyLabel.Caption = "Refund for Professioanl"
                    Else
                        TemStaff.StaffID = temProChg.StaffID
                        MyLabel.Caption = "Refund for " & temSvSub.ServiceSubcategory
                    End If
                    
                    rsTem2.MoveNext
                Wend
                rsTem2.Close
                rsTem1.MoveNext
            Wend
            rsTem1.Close
            
            
        
                
        End If
        
        .Close
        
    End With

End Sub


Private Sub txtBillID_Change()
    FillGrid
End Sub
