VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSummerySub 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Subcategory Summery"
   ClientHeight    =   8910
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13545
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8910
   ScaleWidth      =   13545
   Begin MSFlexGridLib.MSFlexGrid gridSummery 
      Height          =   6255
      Left            =   360
      TabIndex        =   14
      Top             =   2520
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   11033
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   5760
      TabIndex        =   13
      Top             =   2040
      Width           =   1215
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   6360
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   8400
      Width           =   4695
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   840
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   8400
      Width           =   4695
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   12120
      TabIndex        =   2
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   2040
      TabIndex        =   5
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   115933187
      CurrentDate     =   39969
   End
   Begin MSDataListLib.DataCombo cmbSc 
      Height          =   360
      Left            =   2040
      TabIndex        =   6
      Top             =   1080
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   2040
      TabIndex        =   10
      Top             =   600
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbPm 
      Height          =   360
      Left            =   2040
      TabIndex        =   11
      Top             =   2040
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      ListField       =   "cmbPm"
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   375
      Left            =   8400
      TabIndex        =   15
      Top             =   2040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   7080
      TabIndex        =   16
      Top             =   2040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   2040
      TabIndex        =   17
      Top             =   1560
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      ListField       =   "cmbUser"
      Text            =   ""
   End
   Begin VB.Label Label3 
      Caption         =   "User"
      Height          =   255
      Left            =   360
      TabIndex        =   18
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Payment Method"
      Height          =   255
      Left            =   360
      TabIndex        =   12
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   255
      Left            =   360
      TabIndex        =   9
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label8 
      Caption         =   "Category"
      Height          =   255
      Left            =   360
      TabIndex        =   8
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label Label11 
      Caption         =   "Subcategory"
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label cccc 
      Caption         =   "Paper"
      Height          =   255
      Left            =   5760
      TabIndex        =   4
      Top             =   8400
      Width           =   1815
   End
   Begin VB.Label bbbb 
      Caption         =   "Printer"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   8400
      Width           =   1815
   End
End
Attribute VB_Name = "frmSummerySub"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsSC As New ADODB.Recordset

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridSummery, "Summery - " & cmbUser.text & " - " & cmbCat.text & " - " & cmbSc.text & " - " & cmbPm.text, "Date : " & Format(dtpDate.Value, "dd MM yy"), True
End Sub

Private Sub btnPrint_Click()
    Dim myPrinter As Printer
    For Each myPrinter In Printers
        If myPrinter.DeviceName = ReportPrinterName Then
            Set Printer = myPrinter
        End If
    Next
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    With ThisReportFormat
        .ColSpace = 500
        .SubTopicFontSize = 8
    End With
    GridPrint gridSummery, ThisReportFormat, "Detailed Summery - " & cmbUser.text & " - " & cmbCat.text & cmbSc.text & " - " & cmbPm.text, "Date : " & Format(dtpDate.Value, "dd MM yy")
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    Dim temSQL As String
    
    Dim billRowStart As Long
    Dim billRowEnd As Long
    
    Dim cancalRowStart As Long
    Dim cancelRowEnd As Long
    
    Dim returnRowStart As Long
    Dim returnRowEnd As Long
    
    
    gridSummery.Visible = False
    
    gridSummery.Clear
    gridSummery.Row = 0
    gridSummery.Col = 0
    With gridSummery
        .text = "Billed"
        .Rows = 2
        .Row = 1
        billRowStart = 1
    End With
    
    
    temSQL = "SELECT      dbo.tblStaff.Name AS [User], dbo.tblPaymentMethod.PaymentMethod as [Payment], dbo.tblServiceSubCategory.ServiceSubCategory  as [Service],  " & _
"                       SUM(dbo.tblPatientService.HospitalCharge) AS [Hospital Fee], SUM(dbo.tblPatientService.ProfessionalCharge) AS [Professional Fee],  " & _
"                        SUM(dbo.tblPatientService.Charge) AS [Total Fee] " & _
"FROM          dbo.tblServiceSubCategory RIGHT OUTER JOIN " & _
"                        dbo.tblPatientService ON dbo.tblServiceSubCategory.ServiceSubCategoryID = dbo.tblPatientService.ServiceSubCategoryID RIGHT OUTER JOIN " & _
"                        dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN " & _
"                        dbo.tblPaymentMethod ON dbo.tblIncomeBill.PaymentMethodID = dbo.tblPaymentMethod.PaymentMethodID LEFT OUTER JOIN " & _
"                        dbo.tblStaff ON dbo.tblIncomeBill.CompletedUserID = dbo.tblStaff.StaffID  " & _
"WHERE (dbo.tblIncomeBill.Completed = 1) AND (dbo.tblPatientService.Deleted = 0) AND (((dbo.tblIncomeBill.CompletedDate = CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) AND (dbo.tblPaymentMethod.FinalPay = 1) ) OR ((dbo.tblIncomeBill.CreditDate = CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) AND (dbo.tblPaymentMethod.FinalPay <> 1) )) " & _
"GROUP BY dbo.tblPaymentMethod.PaymentMethod, dbo.tblStaff.Name, dbo.tblServiceSubCategory.ServiceSubCategory, dbo.tblPaymentMethod.FinalPay " & _
"ORDER BY dbo.tblStaff.Name, dbo.tblServiceSubCategory.ServiceSubCategory, dbo.tblPaymentMethod.PaymentMethod "
    FillAnyGrid temSQL, gridSummery, True



temSQL = "SELECT      dbo.tblStaff.Name AS [User], dbo.tblPaymentMethod.PaymentMethod as [Payment], dbo.tblServiceSubCategory.ServiceSubCategory  as [Service],  " & _
"                        SUM(dbo.tblPatientService.HospitalChargeCancelled) AS [Hospital Fee], SUM(dbo.tblPatientService.ProfessionalChargeCancelled)  " & _
"                        AS [Professional Fee], SUM(dbo.tblPatientService.ChargeCancelled) AS [Total Fee] " & _
"FROM          dbo.tblPaymentMethod RIGHT OUTER JOIN " & _
"                        dbo.tblIncomeBill ON dbo.tblPaymentMethod.PaymentMethodID = dbo.tblIncomeBill.CancelledPaymentMethodID LEFT OUTER JOIN " & _
"                        dbo.tblServiceSubCategory RIGHT OUTER JOIN " & _
"                        dbo.tblPatientService ON dbo.tblServiceSubCategory.ServiceSubCategoryID = dbo.tblPatientService.ServiceSubCategoryID ON  " & _
"                        dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN " & _
"                        dbo.tblStaff ON dbo.tblIncomeBill.CancelledUserID = dbo.tblStaff.StaffID " & _
"WHERE      (dbo.tblPatientService.Deleted = 0) AND (dbo.tblIncomeBill.Cancelled = 1) AND (dbo.tblIncomeBill.CancelledDate BETWEEN CONVERT(DATETIME,  " & _
"                        '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) " & _
"GROUP BY dbo.tblPaymentMethod.PaymentMethod, dbo.tblStaff.Name, dbo.tblServiceSubCategory.ServiceSubCategory " & _
"ORDER BY dbo.tblStaff.Name, dbo.tblServiceSubCategory.ServiceSubCategory, dbo.tblPaymentMethod.PaymentMethod"
    With gridSummery
        billRowEnd = .Row
        .Rows = .Rows + 2
        .Row = .Rows - 1
        .Col = 0
        .text = "Cancellations"
        .Rows = .Rows + 1
        .Row = .Rows - 1
        cancalRowStart = .Row
    End With
    
    FillAnyGrid temSQL, gridSummery, True
    
temSQL = "SELECT      dbo.tblStaff.Name AS [User], dbo.tblPaymentMethod.PaymentMethod as [Payment], dbo.tblServiceSubCategory.ServiceSubCategory  as [Service],  " & _
"                        SUM(dbo.tblPatientService.HospitaChargelReturned) AS [Hospital Fee], SUM(dbo.tblPatientService.ProfessionalChargeReturned)  " & _
"                        AS [Professional Fee], SUM(dbo.tblPatientService.ChargeReturned) AS [Total Fee] " & _
"FROM          dbo.tblPaymentMethod RIGHT OUTER JOIN " & _
"                        dbo.tblIncomeBill ON dbo.tblPaymentMethod.PaymentMethodID = dbo.tblIncomeBill.ReturnedPaymentMethodID LEFT OUTER JOIN " & _
"                        dbo.tblServiceSubCategory RIGHT OUTER JOIN " & _
"                        dbo.tblPatientService ON dbo.tblServiceSubCategory.ServiceSubCategoryID = dbo.tblPatientService.ServiceSubCategoryID ON  " & _
"                        dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN " & _
"                        dbo.tblStaff ON dbo.tblIncomeBill.ReturnedUserID = dbo.tblStaff.StaffID " & _
"WHERE      (dbo.tblPatientService.Deleted = 0) AND (dbo.tblIncomeBill.Returned = 1) AND (dbo.tblIncomeBill.ReturnedDate BETWEEN CONVERT(DATETIME,  " & _
"                        '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) " & _
"GROUP BY dbo.tblPaymentMethod.PaymentMethod, dbo.tblStaff.Name, dbo.tblServiceSubCategory.ServiceSubCategory " & _
"ORDER BY dbo.tblStaff.Name, dbo.tblServiceSubCategory.ServiceSubCategory, dbo.tblPaymentMethod.PaymentMethod"
    With gridSummery
       cancelRowEnd = .Row
        .Rows = .Rows + 2
        .Row = .Rows - 1
       
        .Col = 0
        .text = "Returns"
        .Rows = .Rows + 2
        .Row = .Rows - 1
        returnRowStart = .Row
    End With
    
    FillAnyGrid temSQL, gridSummery, True
    
    returnRowEnd = gridSummery.Row
    
    gridReset gridSummery
    
    If IsNumeric(cmbSc.BoundText) = True Then
        hideRows gridSummery, 2, cmbSc.text, "Service"
    End If
    
    If IsNumeric(cmbPm.BoundText) = True Then
        hideRows gridSummery, 1, cmbPm.text, "Payment"
    End If
    
    If IsNumeric(cmbUser.BoundText) = True Then
        hideRows gridSummery, 0, cmbUser.text, "User"
    End If
    
    With gridSummery
        .Rows = .Rows + 2
        .Row = .Rows - 1
        .Col = 0
        .text = "Total"
        
        .Col = 3
        .text = rowTotal(gridSummery, 3, Val(billRowStart), Val(billRowEnd), True) - rowTotal(gridSummery, 3, Val(cancalRowStart), Val(cancelRowEnd), True) - rowTotal(gridSummery, 3, Val(returnRowStart), Val(returnRowEnd), True)

        .Col = 4
        .text = rowTotal(gridSummery, 4, Val(billRowStart), Val(billRowEnd), True) - rowTotal(gridSummery, 4, Val(cancalRowStart), Val(cancelRowEnd), True) - rowTotal(gridSummery, 4, Val(returnRowStart), Val(returnRowEnd), True)
        

        .Col = 5
        .text = rowTotal(gridSummery, 5, Val(billRowStart), Val(billRowEnd), True) - rowTotal(gridSummery, 5, Val(cancalRowStart), Val(cancelRowEnd), True) - rowTotal(gridSummery, 5, Val(returnRowStart), Val(returnRowEnd), True)
    
    End With
    
    formatGridString gridSummery, 3, "#,##0.00"
    formatGridString gridSummery, 4, "#,##0.00"
    formatGridString gridSummery, 5, "#,##0.00"
    
    
    gridSummery.Visible = True

End Sub

Private Sub cmbCat_Change()
    Dim temSQL As String
    With rsSC
        If .State = 1 Then .Close
        temSQL = "Select * from tblServiceSubCategory where   Deleted = 0 AND ForR = 1 AND ServiceCategoryID = " & Val(cmbCat.BoundText) & " ORDER BY ServiceSubcategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbSc
        Set .RowSource = rsSC
        .ListField = "ServiceSubcategory"
        .BoundColumn = "ServiceSubcategoryID"
        .text = Empty
    End With
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
        End If
End Sub

Private Sub cmbPm_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbPm.text = Empty
End Sub

Private Sub cmbSc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbSc.text = Empty

End Sub

Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbUser.text = Empty
End Sub

Private Sub fillCombos()
    Dim cat As New clsFillCombos
    cat.FillAnyCombo cmbCat, "ServiceCategory", True
    Dim PayM As New clsFillCombos
    PayM.FillAnyCombo cmbPm, "PaymentMethod", False
    Dim Cashier As New clsFillCombos
    Cashier.FillBoolCombo cmbUser, "Staff", "Name", "IsAUser", False

End Sub

Private Sub Form_Load()
    fillCombos
    GetSettings
End Sub

Private Sub GetSettings()
    'GetCommonSettings Me
    dtpDate.Value = Date
End Sub

Private Sub SaveSettings()
   'saveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub


