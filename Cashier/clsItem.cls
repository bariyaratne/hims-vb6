VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Private varItemID As Long
    Private varVTM As String
    Private varVMP As String
    Private varAMP As String
    Private varAMPP As String
    Private varVMPP As String
    Private varDisplay As String
    Private varCode As String
    Private varvtmId As Long
    Private varvmpId As Long
    Private varvmppId As Long
    Private varisAtm As Boolean
    Private varisVtm As Boolean
    Private varisAmp As Boolean
    Private varisVmp As Boolean
    Private varisAmpp As Boolean
    Private varisVmpp As Boolean
    Private varUnitId As Long
    Private varTradeNameID As Long
    Private varGenericNameID As Long
    Private varItemCategoryID As Long
    Private varStrengthUnitID As Long
    Private varIssueUnitID As Long
    Private varPackUnitID As Long
    Private varStrengthOfIssueUnit As Double
    Private varIssueUnitsPerPack As Double
    Private varROL As Double
    Private varROQ As Double
    Private varMinQty As Double
    Private varManufacturerID As Long
    Private varImporterID As Long
    Private varComments As String
    Private varupsize_ts
    Private varpriceByItem As Boolean
    Private varpriceByBatch As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSql = "SELECT * FROM tblItem Where ItemID = " & varItemID
        If .State = 1 Then .Close
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !VTM = varVTM
        !VMP = varVMP
        !AMP = varAMP
        !AMPP = varAMPP
        !VMPP = varVMPP
        !Display = varDisplay
        !Code = varCode
        !vtmId = varvtmId
        !vmpId = varvmpId
        !vmppId = varvmppId
        !isAtm = varisAtm
        !isVtm = varisVtm
        !isAmp = varisAmp
        !isVmp = varisVmp
        !isAmpp = varisAmpp
        !isVmpp = varisVmpp
        !UnitId = varUnitId
        !TradeNameID = varTradeNameID
        !GenericNameID = varGenericNameID
        !ItemCategoryID = varItemCategoryID
        !StrengthUnitID = varStrengthUnitID
        !IssueUnitID = varIssueUnitID
        !PackUnitID = varPackUnitID
        !StrengthOfIssueUnit = varStrengthOfIssueUnit
        !IssueUnitsPerPack = varIssueUnitsPerPack
        !ROL = varROL
        !ROQ = varROQ
        !MinQty = varMinQty
        !ManufacturerID = varManufacturerID
        !ImporterID = varImporterID
        !Comments = varComments
        !priceByItem = varpriceByItem
        !priceByBatch = varpriceByBatch
        .Update
        If newEntry = True Then
            .Close
            temSql = "SELECT @@IDENTITY AS NewID"
           .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
            varItemID = !NewID
        Else
            varItemID = !ItemID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "SELECT * FROM tblItem WHERE ItemID = " & varItemID
        If .State = 1 Then .Close
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!ItemID) Then
               varItemID = !ItemID
            End If
            If Not IsNull(!VTM) Then
               varVTM = !VTM
            End If
            If Not IsNull(!VMP) Then
               varVMP = !VMP
            End If
            If Not IsNull(!AMP) Then
               varAMP = !AMP
            End If
            If Not IsNull(!AMPP) Then
               varAMPP = !AMPP
            End If
            If Not IsNull(!VMPP) Then
               varVMPP = !VMPP
            End If
            If Not IsNull(!Display) Then
               varDisplay = !Display
            End If
            If Not IsNull(!Code) Then
               varCode = !Code
            End If
            If Not IsNull(!vtmId) Then
               varvtmId = !vtmId
            End If
            If Not IsNull(!vmpId) Then
               varvmpId = !vmpId
            End If
            If Not IsNull(!vmppId) Then
               varvmppId = !vmppId
            End If
            If Not IsNull(!isAtm) Then
               varisAtm = !isAtm
            End If
            If Not IsNull(!isVtm) Then
               varisVtm = !isVtm
            End If
            If Not IsNull(!isAmp) Then
               varisAmp = !isAmp
            End If
            If Not IsNull(!isVmp) Then
               varisVmp = !isVmp
            End If
            If Not IsNull(!isAmpp) Then
               varisAmpp = !isAmpp
            End If
            If Not IsNull(!isVmpp) Then
               varisVmpp = !isVmpp
            End If
            If Not IsNull(!UnitId) Then
               varUnitId = !UnitId
            End If
            If Not IsNull(!TradeNameID) Then
               varTradeNameID = !TradeNameID
            End If
            If Not IsNull(!GenericNameID) Then
               varGenericNameID = !GenericNameID
            End If
            If Not IsNull(!ItemCategoryID) Then
               varItemCategoryID = !ItemCategoryID
            End If
            If Not IsNull(!StrengthUnitID) Then
               varStrengthUnitID = !StrengthUnitID
            End If
            If Not IsNull(!IssueUnitID) Then
               varIssueUnitID = !IssueUnitID
            End If
            If Not IsNull(!PackUnitID) Then
               varPackUnitID = !PackUnitID
            End If
            If Not IsNull(!StrengthOfIssueUnit) Then
               varStrengthOfIssueUnit = !StrengthOfIssueUnit
            End If
            If Not IsNull(!IssueUnitsPerPack) Then
               varIssueUnitsPerPack = !IssueUnitsPerPack
            End If
            If Not IsNull(!ROL) Then
               varROL = !ROL
            End If
            If Not IsNull(!ROQ) Then
               varROQ = !ROQ
            End If
            If Not IsNull(!MinQty) Then
               varMinQty = !MinQty
            End If
            If Not IsNull(!ManufacturerID) Then
               varManufacturerID = !ManufacturerID
            End If
            If Not IsNull(!ImporterID) Then
               varImporterID = !ImporterID
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!priceByItem) Then
               varpriceByItem = !priceByItem
            End If
            If Not IsNull(!priceByBatch) Then
               varpriceByBatch = !priceByBatch
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varItemID = 0
    varVTM = Empty
    varVMP = Empty
    varAMP = Empty
    varAMPP = Empty
    varVMPP = Empty
    varDisplay = Empty
    varCode = Empty
    varvtmId = 0
    varvmpId = 0
    varvmppId = 0
    varisAtm = False
    varisVtm = False
    varisAmp = False
    varisVmp = False
    varisAmpp = False
    varisVmpp = False
    varUnitId = 0
    varTradeNameID = 0
    varGenericNameID = 0
    varItemCategoryID = 0
    varStrengthUnitID = 0
    varIssueUnitID = 0
    varPackUnitID = 0
    varStrengthOfIssueUnit = 0
    varIssueUnitsPerPack = 0
    varROL = 0
    varROQ = 0
    varMinQty = 0
    varManufacturerID = 0
    varImporterID = 0
    varComments = Empty
    varupsize_ts = Empty
    varpriceByItem = False
    varpriceByBatch = False
End Sub

Public Property Let ItemID(ByVal vItemID As Long)
    Call clearData
    varItemID = vItemID
    Call loadData
End Property

Public Property Get ItemID() As Long
    ItemID = varItemID
End Property

Public Property Let VTM(ByVal vVTM As String)
    varVTM = vVTM
End Property

Public Property Get VTM() As String
    VTM = varVTM
End Property

Public Property Let VMP(ByVal vVMP As String)
    varVMP = vVMP
End Property

Public Property Get VMP() As String
    VMP = varVMP
End Property

Public Property Let AMP(ByVal vAMP As String)
    varAMP = vAMP
End Property

Public Property Get AMP() As String
    AMP = varAMP
End Property

Public Property Let AMPP(ByVal vAMPP As String)
    varAMPP = vAMPP
End Property

Public Property Get AMPP() As String
    AMPP = varAMPP
End Property

Public Property Let VMPP(ByVal vVMPP As String)
    varVMPP = vVMPP
End Property

Public Property Get VMPP() As String
    VMPP = varVMPP
End Property

Public Property Let Display(ByVal vDisplay As String)
    varDisplay = vDisplay
End Property

Public Property Get Display() As String
    Display = varDisplay
End Property

Public Property Let Code(ByVal vCode As String)
    varCode = vCode
End Property

Public Property Get Code() As String
    Code = varCode
End Property

Public Property Let vtmId(ByVal vvtmId As Long)
    varvtmId = vvtmId
End Property

Public Property Get vtmId() As Long
    vtmId = varvtmId
End Property

Public Property Let vmpId(ByVal vvmpId As Long)
    varvmpId = vvmpId
End Property

Public Property Get vmpId() As Long
    vmpId = varvmpId
End Property

Public Property Let vmppId(ByVal vvmppId As Long)
    varvmppId = vvmppId
End Property

Public Property Get vmppId() As Long
    vmppId = varvmppId
End Property

Public Property Let isAtm(ByVal visAtm As Boolean)
    varisAtm = visAtm
End Property

Public Property Get isAtm() As Boolean
    isAtm = varisAtm
End Property

Public Property Let isVtm(ByVal visVtm As Boolean)
    varisVtm = visVtm
End Property

Public Property Get isVtm() As Boolean
    isVtm = varisVtm
End Property

Public Property Let isAmp(ByVal visAmp As Boolean)
    varisAmp = visAmp
End Property

Public Property Get isAmp() As Boolean
    isAmp = varisAmp
End Property

Public Property Let isVmp(ByVal visVmp As Boolean)
    varisVmp = visVmp
End Property

Public Property Get isVmp() As Boolean
    isVmp = varisVmp
End Property

Public Property Let isAmpp(ByVal visAmpp As Boolean)
    varisAmpp = visAmpp
End Property

Public Property Get isAmpp() As Boolean
    isAmpp = varisAmpp
End Property

Public Property Let isVmpp(ByVal visVmpp As Boolean)
    varisVmpp = visVmpp
End Property

Public Property Get isVmpp() As Boolean
    isVmpp = varisVmpp
End Property

Public Property Let UnitId(ByVal vUnitId As Long)
    varUnitId = vUnitId
End Property

Public Property Get UnitId() As Long
    UnitId = varUnitId
End Property

Public Property Let TradeNameID(ByVal vTradeNameID As Long)
    varTradeNameID = vTradeNameID
End Property

Public Property Get TradeNameID() As Long
    TradeNameID = varTradeNameID
End Property

Public Property Let GenericNameID(ByVal vGenericNameID As Long)
    varGenericNameID = vGenericNameID
End Property

Public Property Get GenericNameID() As Long
    GenericNameID = varGenericNameID
End Property

Public Property Let ItemCategoryID(ByVal vItemCategoryID As Long)
    varItemCategoryID = vItemCategoryID
End Property

Public Property Get ItemCategoryID() As Long
    ItemCategoryID = varItemCategoryID
End Property

Public Property Let StrengthUnitID(ByVal vStrengthUnitID As Long)
    varStrengthUnitID = vStrengthUnitID
End Property

Public Property Get StrengthUnitID() As Long
    StrengthUnitID = varStrengthUnitID
End Property

Public Property Let IssueUnitID(ByVal vIssueUnitID As Long)
    varIssueUnitID = vIssueUnitID
End Property

Public Property Get IssueUnitID() As Long
    IssueUnitID = varIssueUnitID
End Property

Public Property Let PackUnitID(ByVal vPackUnitID As Long)
    varPackUnitID = vPackUnitID
End Property

Public Property Get PackUnitID() As Long
    PackUnitID = varPackUnitID
End Property

Public Property Let StrengthOfIssueUnit(ByVal vStrengthOfIssueUnit As Double)
    varStrengthOfIssueUnit = vStrengthOfIssueUnit
End Property

Public Property Get StrengthOfIssueUnit() As Double
    StrengthOfIssueUnit = varStrengthOfIssueUnit
End Property

Public Property Let IssueUnitsPerPack(ByVal vIssueUnitsPerPack As Double)
    varIssueUnitsPerPack = vIssueUnitsPerPack
End Property

Public Property Get IssueUnitsPerPack() As Double
    IssueUnitsPerPack = varIssueUnitsPerPack
End Property

Public Property Let ROL(ByVal vROL As Double)
    varROL = vROL
End Property

Public Property Get ROL() As Double
    ROL = varROL
End Property

Public Property Let ROQ(ByVal vROQ As Double)
    varROQ = vROQ
End Property

Public Property Get ROQ() As Double
    ROQ = varROQ
End Property

Public Property Let MinQty(ByVal vMinQty As Double)
    varMinQty = vMinQty
End Property

Public Property Get MinQty() As Double
    MinQty = varMinQty
End Property

Public Property Let ManufacturerID(ByVal vManufacturerID As Long)
    varManufacturerID = vManufacturerID
End Property

Public Property Get ManufacturerID() As Long
    ManufacturerID = varManufacturerID
End Property

Public Property Let ImporterID(ByVal vImporterID As Long)
    varImporterID = vImporterID
End Property

Public Property Get ImporterID() As Long
    ImporterID = varImporterID
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let priceByItem(ByVal vpriceByItem As Boolean)
    varpriceByItem = vpriceByItem
End Property

Public Property Get priceByItem() As Boolean
    priceByItem = varpriceByItem
End Property

Public Property Let priceByBatch(ByVal vpriceByBatch As Boolean)
    varpriceByBatch = vpriceByBatch
End Property

Public Property Get priceByBatch() As Boolean
    priceByBatch = varpriceByBatch
End Property


