VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceCategory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varServiceCategoryID As Long
    Private varServiceCategory As String
    Private varComments As String
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate
    Private varDeletedTime
    Private varFee As Double
    Private varCanChange As Boolean
    Private varToMedicineCharge As Boolean
    Private varInwardSurcharge As Double
    Private varForOPD As Boolean
    Private varForBHT As Boolean
    Private varForGSB As Boolean
    Private varForMT As Boolean
    Private varForHST As Boolean
    Private varForLab As Boolean
    Private varForR As Boolean
    Private varForExpence As Boolean
    Private varForIncome As Boolean
    Private varupsize_ts
    Private varRetail As Boolean
    Private varWholeSale As Boolean
    Private varPrintTotalOnly As Boolean
    Private varSerialBySubCategory As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblServiceCategory Where ServiceCategoryID = " & varServiceCategoryID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !ServiceCategory = varServiceCategory
        !Comments = varComments
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !Fee = varFee
        !CanChange = varCanChange
        !ToMedicineCharge = varToMedicineCharge
        !InwardSurcharge = varInwardSurcharge
        !ForOPD = varForOPD
        !ForBHT = varForBHT
        !ForGSB = varForGSB
        !ForMT = varForMT
        !ForHST = varForHST
        !ForLab = varForLab
        !ForR = varForR
        !ForExpence = varForExpence
        !ForIncome = varForIncome
        !Retail = varRetail
        !WholeSale = varWholeSale
        !PrintTotalOnly = varPrintTotalOnly
        !SerialBySubCategory = varSerialBySubCategory
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varServiceCategoryID = !NewID
        Else
            varServiceCategoryID = !ServiceCategoryID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblServiceCategory WHERE ServiceCategoryID = " & varServiceCategoryID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!ServiceCategoryID) Then
               varServiceCategoryID = !ServiceCategoryID
            End If
            If Not IsNull(!ServiceCategory) Then
               varServiceCategory = !ServiceCategory
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!Fee) Then
               varFee = !Fee
            End If
            If Not IsNull(!CanChange) Then
               varCanChange = !CanChange
            End If
            If Not IsNull(!ToMedicineCharge) Then
               varToMedicineCharge = !ToMedicineCharge
            End If
            If Not IsNull(!InwardSurcharge) Then
               varInwardSurcharge = !InwardSurcharge
            End If
            If Not IsNull(!ForOPD) Then
               varForOPD = !ForOPD
            End If
            If Not IsNull(!ForBHT) Then
               varForBHT = !ForBHT
            End If
            If Not IsNull(!ForGSB) Then
               varForGSB = !ForGSB
            End If
            If Not IsNull(!ForMT) Then
               varForMT = !ForMT
            End If
            If Not IsNull(!ForHST) Then
               varForHST = !ForHST
            End If
            If Not IsNull(!ForLab) Then
               varForLab = !ForLab
            End If
            If Not IsNull(!ForR) Then
               varForR = !ForR
            End If
            If Not IsNull(!ForExpence) Then
               varForExpence = !ForExpence
            End If
            If Not IsNull(!ForIncome) Then
               varForIncome = !ForIncome
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!Retail) Then
               varRetail = !Retail
            End If
            If Not IsNull(!WholeSale) Then
               varWholeSale = !WholeSale
            End If
            If Not IsNull(!PrintTotalOnly) Then
               varPrintTotalOnly = !PrintTotalOnly
            End If
            If Not IsNull(!SerialBySubCategory) Then
               varSerialBySubCategory = !SerialBySubCategory
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varServiceCategoryID = 0
    varServiceCategory = Empty
    varComments = Empty
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varFee = 0
    varCanChange = False
    varToMedicineCharge = False
    varInwardSurcharge = 0
    varForOPD = False
    varForBHT = False
    varForGSB = False
    varForMT = False
    varForHST = False
    varForLab = False
    varForR = False
    varForExpence = False
    varForIncome = False
    varupsize_ts = Empty
    varRetail = False
    varWholeSale = False
    varPrintTotalOnly = False
    varSerialBySubCategory = False
End Sub

Public Property Let ServiceCategoryID(ByVal vServiceCategoryID As Long)
    Call clearData
    varServiceCategoryID = vServiceCategoryID
    Call loadData
End Property

Public Property Get ServiceCategoryID() As Long
    ServiceCategoryID = varServiceCategoryID
End Property

Public Property Let ServiceCategory(ByVal vServiceCategory As String)
    varServiceCategory = vServiceCategory
End Property

Public Property Get ServiceCategory() As String
    ServiceCategory = varServiceCategory
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate()
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime()
    DeletedTime = varDeletedTime
End Property

Public Property Let Fee(ByVal vFee As Double)
    varFee = vFee
End Property

Public Property Get Fee() As Double
    Fee = varFee
End Property

Public Property Let CanChange(ByVal vCanChange As Boolean)
    varCanChange = vCanChange
End Property

Public Property Get CanChange() As Boolean
    CanChange = varCanChange
End Property

Public Property Let ToMedicineCharge(ByVal vToMedicineCharge As Boolean)
    varToMedicineCharge = vToMedicineCharge
End Property

Public Property Get ToMedicineCharge() As Boolean
    ToMedicineCharge = varToMedicineCharge
End Property

Public Property Let InwardSurcharge(ByVal vInwardSurcharge As Double)
    varInwardSurcharge = vInwardSurcharge
End Property

Public Property Get InwardSurcharge() As Double
    InwardSurcharge = varInwardSurcharge
End Property

Public Property Let ForOPD(ByVal vForOPD As Boolean)
    varForOPD = vForOPD
End Property

Public Property Get ForOPD() As Boolean
    ForOPD = varForOPD
End Property

Public Property Let ForBHT(ByVal vForBHT As Boolean)
    varForBHT = vForBHT
End Property

Public Property Get ForBHT() As Boolean
    ForBHT = varForBHT
End Property

Public Property Let ForGSB(ByVal vForGSB As Boolean)
    varForGSB = vForGSB
End Property

Public Property Get ForGSB() As Boolean
    ForGSB = varForGSB
End Property

Public Property Let ForMT(ByVal vForMT As Boolean)
    varForMT = vForMT
End Property

Public Property Get ForMT() As Boolean
    ForMT = varForMT
End Property

Public Property Let ForHST(ByVal vForHST As Boolean)
    varForHST = vForHST
End Property

Public Property Get ForHST() As Boolean
    ForHST = varForHST
End Property

Public Property Let ForLab(ByVal vForLab As Boolean)
    varForLab = vForLab
End Property

Public Property Get ForLab() As Boolean
    ForLab = varForLab
End Property

Public Property Let ForR(ByVal vForR As Boolean)
    varForR = vForR
End Property

Public Property Get ForR() As Boolean
    ForR = varForR
End Property

Public Property Let ForExpence(ByVal vForExpence As Boolean)
    varForExpence = vForExpence
End Property

Public Property Get ForExpence() As Boolean
    ForExpence = varForExpence
End Property

Public Property Let ForIncome(ByVal vForIncome As Boolean)
    varForIncome = vForIncome
End Property

Public Property Get ForIncome() As Boolean
    ForIncome = varForIncome
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let Retail(ByVal vRetail As Boolean)
    varRetail = vRetail
End Property

Public Property Get Retail() As Boolean
    Retail = varRetail
End Property

Public Property Let WholeSale(ByVal vWholeSale As Boolean)
    varWholeSale = vWholeSale
End Property

Public Property Get WholeSale() As Boolean
    WholeSale = varWholeSale
End Property

Public Property Let PrintTotalOnly(ByVal vPrintTotalOnly As Boolean)
    varPrintTotalOnly = vPrintTotalOnly
End Property

Public Property Get PrintTotalOnly() As Boolean
    PrintTotalOnly = varPrintTotalOnly
End Property

Public Property Let SerialBySubCategory(ByVal vSerialBySubCategory As Boolean)
    varSerialBySubCategory = vSerialBySubCategory
End Property

Public Property Get SerialBySubCategory() As Boolean
    SerialBySubCategory = varSerialBySubCategory
End Property


