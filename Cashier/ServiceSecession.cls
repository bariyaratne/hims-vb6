VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceSecession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varServiceSecessionID As Long
    Private varServiceSecession As String
    Private varServiceCategoryID As Long
    Private varServiceSubcategoryID As Long
    Private varMaxNo As Long
    Private varStartTime
    Private varDurationMinutes As Long
    Private varComments As String
    Private varDeleted As Boolean
    Private varDeletedDate
    Private varDeletedTime
    Private varDeletedDateTime
    Private varDeletedUserID As Long
    Private varAddedDate
    Private varAddedTime
    Private varAddedDateTime
    Private varAddedUserID As Long
    Private varupsize_ts

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblServiceSecession Where ServiceSecessionID = " & varServiceSecessionID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !ServiceSecession = varServiceSecession
        !ServiceCategoryID = varServiceCategoryID
        !ServiceSubcategoryID = varServiceSubcategoryID
        !MaxNo = varMaxNo
        !StartTime = varStartTime
        !DurationMinutes = varDurationMinutes
        !Comments = varComments
        !Deleted = varDeleted
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !DeletedDateTime = varDeletedDateTime
        !DeletedUserID = varDeletedUserID
        !AddedDate = varAddedDate
        !AddedTime = varAddedTime
        !AddedDateTime = varAddedDateTime
        !AddedUserID = varAddedUserID
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varServiceSecessionID = !NewID
        Else
            varServiceSecessionID = !ServiceSecessionID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblServiceSecession WHERE ServiceSecessionID = " & varServiceSecessionID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!ServiceSecessionID) Then
               varServiceSecessionID = !ServiceSecessionID
            End If
            If Not IsNull(!ServiceSecession) Then
               varServiceSecession = !ServiceSecession
            End If
            If Not IsNull(!ServiceCategoryID) Then
               varServiceCategoryID = !ServiceCategoryID
            End If
            If Not IsNull(!ServiceSubcategoryID) Then
               varServiceSubcategoryID = !ServiceSubcategoryID
            End If
            If Not IsNull(!MaxNo) Then
               varMaxNo = !MaxNo
            End If
            If Not IsNull(!StartTime) Then
               varStartTime = !StartTime
            End If
            If Not IsNull(!DurationMinutes) Then
               varDurationMinutes = !DurationMinutes
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!DeletedDateTime) Then
               varDeletedDateTime = !DeletedDateTime
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!AddedDate) Then
               varAddedDate = !AddedDate
            End If
            If Not IsNull(!AddedTime) Then
               varAddedTime = !AddedTime
            End If
            If Not IsNull(!AddedDateTime) Then
               varAddedDateTime = !AddedDateTime
            End If
            If Not IsNull(!AddedUserID) Then
               varAddedUserID = !AddedUserID
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varServiceSecessionID = 0
    varServiceSecession = Empty
    varServiceCategoryID = 0
    varServiceSubcategoryID = 0
    varMaxNo = 0
    varStartTime = Empty
    varDurationMinutes = 0
    varComments = Empty
    varDeleted = False
    varDeletedDate = Empty
    varDeletedTime = Empty
    varDeletedDateTime = Empty
    varDeletedUserID = 0
    varAddedDate = Empty
    varAddedTime = Empty
    varAddedDateTime = Empty
    varAddedUserID = 0
    varupsize_ts = Empty
End Sub

Public Property Let ServiceSecessionID(ByVal vServiceSecessionID As Long)
    Call clearData
    varServiceSecessionID = vServiceSecessionID
    Call loadData
End Property

Public Property Get ServiceSecessionID() As Long
    ServiceSecessionID = varServiceSecessionID
End Property

Public Property Let ServiceSecession(ByVal vServiceSecession As String)
    varServiceSecession = vServiceSecession
End Property

Public Property Get ServiceSecession() As String
    ServiceSecession = varServiceSecession
End Property

Public Property Let ServiceCategoryID(ByVal vServiceCategoryID As Long)
    varServiceCategoryID = vServiceCategoryID
End Property

Public Property Get ServiceCategoryID() As Long
    ServiceCategoryID = varServiceCategoryID
End Property

Public Property Let ServiceSubcategoryID(ByVal vServiceSubcategoryID As Long)
    varServiceSubcategoryID = vServiceSubcategoryID
End Property

Public Property Get ServiceSubcategoryID() As Long
    ServiceSubcategoryID = varServiceSubcategoryID
End Property

Public Property Let MaxNo(ByVal vMaxNo As Long)
    varMaxNo = vMaxNo
End Property

Public Property Get MaxNo() As Long
    MaxNo = varMaxNo
End Property

Public Property Let StartTime(ByVal vStartTime)
    varStartTime = vStartTime
End Property

Public Property Get StartTime()
    StartTime = varStartTime
End Property

Public Property Let DurationMinutes(ByVal vDurationMinutes As Long)
    varDurationMinutes = vDurationMinutes
End Property

Public Property Get DurationMinutes() As Long
    DurationMinutes = varDurationMinutes
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedDate(ByVal vDeletedDate)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate()
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime()
    DeletedTime = varDeletedTime
End Property

Public Property Let DeletedDateTime(ByVal vDeletedDateTime)
    varDeletedDateTime = vDeletedDateTime
End Property

Public Property Get DeletedDateTime()
    DeletedDateTime = varDeletedDateTime
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let AddedDate(ByVal vAddedDate)
    varAddedDate = vAddedDate
End Property

Public Property Get AddedDate()
    AddedDate = varAddedDate
End Property

Public Property Let AddedTime(ByVal vAddedTime)
    varAddedTime = vAddedTime
End Property

Public Property Get AddedTime()
    AddedTime = varAddedTime
End Property

Public Property Let AddedDateTime(ByVal vAddedDateTime)
    varAddedDateTime = vAddedDateTime
End Property

Public Property Get AddedDateTime()
    AddedDateTime = varAddedDateTime
End Property

Public Property Let AddedUserID(ByVal vAddedUserID As Long)
    varAddedUserID = vAddedUserID
End Property

Public Property Get AddedUserID() As Long
    AddedUserID = varAddedUserID
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property


