VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Speciality"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varSpecialityID As Long
    Private varSpeciality As String
    Private varSpecialityComments As String
    Private varupsize_ts

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblSpeciality Where SpecialityID = " & varSpecialityID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !Speciality = varSpeciality
        !SpecialityComments = varSpecialityComments
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varSpecialityID = !NewID
        Else
            varSpecialityID = !SpecialityID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSpeciality WHERE SpecialityID = " & varSpecialityID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!SpecialityID) Then
               varSpecialityID = !SpecialityID
            End If
            If Not IsNull(!Speciality) Then
               varSpeciality = !Speciality
            End If
            If Not IsNull(!SpecialityComments) Then
               varSpecialityComments = !SpecialityComments
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varSpecialityID = 0
    varSpeciality = Empty
    varSpecialityComments = Empty
    varupsize_ts = Empty
End Sub

Public Property Let SpecialityID(ByVal vSpecialityID As Long)
    Call clearData
    varSpecialityID = vSpecialityID
    Call loadData
End Property

Public Property Get SpecialityID() As Long
    SpecialityID = varSpecialityID
End Property

Public Property Let Speciality(ByVal vSpeciality As String)
    varSpeciality = vSpeciality
End Property

Public Property Get Speciality() As String
    Speciality = varSpeciality
End Property

Public Property Let SpecialityComments(ByVal vSpecialityComments As String)
    varSpecialityComments = vSpecialityComments
End Property

Public Property Get SpecialityComments() As String
    SpecialityComments = varSpecialityComments
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property


