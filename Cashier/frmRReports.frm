VERSION 5.00
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRReports 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Rediology Reports"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9135
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   9135
   Begin btButtonEx.ButtonEx btnBills 
      Height          =   495
      Left            =   1080
      TabIndex        =   4
      Top             =   2400
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Bills"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   960
      TabIndex        =   2
      Top             =   240
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   113049603
      CurrentDate     =   41125
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   960
      TabIndex        =   3
      Top             =   720
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   113049603
      CurrentDate     =   41125
   End
   Begin VB.Label Label2 
      Caption         =   "To"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "From"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   735
   End
End
Attribute VB_Name = "frmRReports"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
    Dim temSql As String

Private Sub btnBills_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = ""
        
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtrRBills
        Set .DataSource = rsTem
    End With
End Sub

Private Sub SaveSettings()

    SaveCommonSettings Me
End Sub
Private Sub GetSettings()
    GetCommonSettings Me
    dtpFrom.Value = DateSerial(Year(Date), Month(Date), 1)
    dtpTo.Value = Date
End Sub
