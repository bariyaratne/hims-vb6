VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRDailyBillSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Radiology Bill Summery"
   ClientHeight    =   8985
   ClientLeft      =   3930
   ClientTop       =   -180
   ClientWidth     =   11145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   11145
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   1800
      TabIndex        =   12
      Top             =   1560
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtCount 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   3120
      TabIndex        =   7
      Top             =   8040
      Width           =   2415
   End
   Begin VB.TextBox txtValue 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   3120
      TabIndex        =   5
      Top             =   8520
      Width           =   2415
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   9840
      TabIndex        =   1
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   8520
      TabIndex        =   0
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1800
      TabIndex        =   8
      Top             =   1080
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   113639427
      CurrentDate     =   39969
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   7200
      TabIndex        =   10
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridBill 
      Height          =   5895
      Left            =   240
      TabIndex        =   13
      Top             =   2040
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   10398
      _Version        =   393216
   End
   Begin VB.Label Label1 
      Caption         =   "Category"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "From"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Total Value"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   8520
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Total Count"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   8040
      Width           =   2775
   End
   Begin VB.Label lblTopic 
      Alignment       =   2  'Center
      Caption         =   "Topic"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   10815
   End
   Begin VB.Label lblSubtopic 
      Alignment       =   2  'Center
      Caption         =   "Topic"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   10815
   End
End
Attribute VB_Name = "frmRDailyBillSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    Dim MyGrid As grid
    Dim MyGridRow() As GridRow
    Dim MyGridCell() As GridCell
    Dim i As Integer
    Dim n As Integer
    Dim totalValue As Double
    Dim totalCount As Long
    Dim totalPFee As Double

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridBill, "Radiology Bill Summery - " & cmbCat.text & " - " & "On : " & Format(dtpFrom.Value, "dd MMM yyyy")
End Sub

Private Sub btnPrint_Click()
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    
    
    With ThisReportFormat
        .ColSpace = 500
        
        .SubTopicFontSize = 8
    
        .ColFontSize = 7
        .ColSpace = 200
    
    End With
    
    GridPrint gridBill, ThisReportFormat, "Radiology Bill Summery - " & cmbCat.text & " - " & "From : " & Format(dtpFrom.Value, "dd MM yy")
    Printer.EndDoc
    
End Sub

Private Sub cmbPaymentMethod_Change()
    Call formatGrid
    Call FillGrid

End Sub



Private Sub cmbType_Change()
    Call formatGrid
    Call FillGrid
End Sub

Private Sub cmbType_Click()
    Call formatGrid
    Call FillGrid
End Sub

Private Sub dtpDate_Change()
    Call formatGrid
    Call FillGrid
End Sub





Private Sub cmbCat_Change()
    formatGrid
    FillGrid
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
    End If
End Sub

Private Sub dtpFrom_Change()
    Call formatGrid
    Call FillGrid
End Sub


Private Sub dtpTo_Change()
    Call formatGrid
    Call FillGrid
End Sub

Private Sub Form_Load()
    Call fillCombos
    Call formatGrid
    Call GetSettings
    Call FillGrid
End Sub

Private Sub formatGrid()
    With gridBill
        .Clear
    
        .Rows = 4
        .Cols = 4
    
        .FixedCols = 1
        .FixedRows = 1
    
        .row = 0
        
        .col = 1
        .text = "OPD"

        .col = 2
        .text = "BHT"
        
        .col = 3
        .text = "Total"

        .row = 1
        
        .col = 0
        .text = "Previous Day"

        .col = 1
        .text = preOpdIncome

        .col = 2
        .text = preBhtIncome
        
        .col = 3
        .text = preTotIncome
        
        .row = 2

        .col = 0
        .text = "Todays Income"

        .col = 1
        .text = todayOpdIncome
        
        .col = 2
        .text = todayBhtIncome

        .col = 3
        .text = todayTotIncome
        

        .row = 3
        .col = 0
        .text = "Total"
        
        .col = 1
        .text = preOpdIncome + todayOpdIncome
        
        .col = 2
        .text = preBhtIncome + todayBhtIncome
        
        .col = 3
        .text = preTotIncome + todayTotIncome
        

    End With
End Sub


Private Function income(FromDate As Date, ToDate As Date, cat As Long, ForOPD As Boolean, ForBHT As Boolean) As Double
Dim BTotalValue As Double
Dim oTotalValue As Double
Dim temBHT As Long

    Dim TemString As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT IncomeBillID, DisplayBillID, BillSettled, Cancelled, BHTID, PatientID, CompletedDate, NetTotal, NetTotalToPay " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE      (CompletedDate BETWEEN CONVERT(DATETIME, '" & FromDate & "', 102) AND CONVERT(DATETIME, '" & ToDate & "', 102)) AND (IsRBill = 1) AND (Completed = 1) " & _
                    "ORDER BY DisplayBillID"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            If IsNull(!BHTID) Then
                temBHT = 0
            Else
                temBHT = !BHTID
            End If
            If Val(cat) = 0 Or serviceCategoryPresent(!IncomeBillID, cat) Then
                If temBHT = 0 Then
                    oTotalValue = oTotalValue + !NetTotal
                Else
                    BTotalValue = BTotalValue + !NetTotal
                End If
            End If
        
            .MoveNext
        Wend
        .Close
        
    End With
    If ForOPD = True Then income = oTotalValue
    If ForBHT = True Then income = BTotalValue
    If ForBHT = True And ForOPD = True Then income = BTotalValue + oTotalValue
End Function

Private Function preOpdIncome() As Double
    preOpdIncome = income(dtpFrom.Value - 1, dtpFrom.Value - 1, Val(cmbCat.BoundText), True, False)
End Function

Private Function preBhtIncome() As Double
    preBhtIncome = income(dtpFrom.Value - 1, dtpFrom.Value - 1, Val(cmbCat.BoundText), False, True)
End Function

Private Function todayBhtIncome() As Double
        todayBhtIncome = income(dtpFrom.Value, dtpFrom.Value, Val(cmbCat.BoundText), True, False)
End Function

Private Function todayOpdIncome() As Double
        todayOpdIncome = income(dtpFrom.Value, dtpFrom.Value, Val(cmbCat.BoundText), False, True)
End Function

Private Function todayTotIncome() As Double
        todayTotIncome = income(dtpFrom.Value, dtpFrom.Value, Val(cmbCat.BoundText), True, True)
End Function

Private Function preTotIncome() As Double
    preTotIncome = income(dtpFrom.Value - 1, dtpFrom.Value - 1, Val(cmbCat.BoundText), True, True)
End Function

Private Sub FillGrid()
End Sub




Private Sub fillCombos()
    Dim cat As New clsFillCombos
    cat.FillAnyCombo cmbCat, "ServiceCategory", True
End Sub

Private Sub GetSettings(): On Error Resume Next
    dtpFrom.Value = GetSetting(App.EXEName, Me.Name, dtpFrom.Name, "00:00:00")
    
    GetCommonSettings Me
    dtpFrom.Value = Date
    
    lblTopic.Caption = HospitalName
    lblSubtopic.Caption = "Radiology Bills - Date " & Format(dtpFrom.Value, "dd MMMM yyyy")
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
    SaveSetting App.EXEName, Me.Name, dtpFrom.Name, dtpFrom.Value
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

