VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSummeryStaff 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Summery by Staff"
   ClientHeight    =   8940
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13335
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   13335
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   5520
      TabIndex        =   1
      Top             =   1560
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridSummery 
      Height          =   6735
      Left            =   120
      TabIndex        =   0
      Top             =   2040
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   11880
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   11880
      TabIndex        =   4
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1800
      TabIndex        =   5
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   115605507
      CurrentDate     =   39969
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   1800
      TabIndex        =   6
      Top             =   1080
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   1800
      TabIndex        =   7
      Top             =   600
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbPm 
      Height          =   360
      Left            =   1800
      TabIndex        =   8
      Top             =   1560
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      ListField       =   "cmbPm"
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   375
      Left            =   8160
      TabIndex        =   9
      Top             =   1560
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   6840
      TabIndex        =   10
      Top             =   1560
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   600
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   8400
      Width           =   4695
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   6120
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   8400
      Width           =   4695
   End
   Begin VB.Label bbbb 
      Caption         =   "Printer"
      Height          =   255
      Left            =   -120
      TabIndex        =   16
      Top             =   8400
      Width           =   1815
   End
   Begin VB.Label cccc 
      Caption         =   "Paper"
      Height          =   255
      Left            =   5520
      TabIndex        =   15
      Top             =   8400
      Width           =   1815
   End
   Begin VB.Label Label11 
      Caption         =   "Cashier"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label8 
      Caption         =   "Category"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Booking Date"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Payment Method"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1560
      Width           =   1455
   End
End
Attribute VB_Name = "frmSummeryStaff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridSummery, "Summery - " & cmbUser.text & " - " & cmbCat.text & " - " & cmbPm.text, "Date : " & Format(dtpDate.Value, "dd MM yy"), True
End Sub

Private Sub btnPrint_Click()
    Dim myPrinter As Printer
    For Each myPrinter In Printers
        If myPrinter.DeviceName = ReportPrinterName Then
            Set Printer = myPrinter
        End If
    Next
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    With ThisReportFormat
        .ColSpace = 500
        .SubTopicFontSize = 8
    End With
    GridPrint gridSummery, ThisReportFormat, "Detailed Summery - " & cmbUser.text & " - " & cmbCat.text & " - " & cmbPm.text, "Date : " & Format(dtpDate.Value, "dd MM yy")
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    Dim temSQL As String
    
    Dim billRowStart As Long
    Dim billRowEnd As Long
    
    Dim cancalRowStart As Long
    Dim cancelRowEnd As Long
    
    Dim returnRowStart As Long
    Dim returnRowEnd As Long
    
    
    gridSummery.Visible = False
    
    gridSummery.Clear
    gridSummery.row = 0
    gridSummery.col = 0
    With gridSummery
        .text = "Billed"
        .Rows = 2
        .row = 1
        billRowStart = 1
    End With
    
    
temSQL = "SELECT TOP 100 PERCENT dbo.tblStaff.Name AS [User], dbo.tblPaymentMethod.PaymentMethod AS Payment, dbo.tblServiceCategory.ServiceCategory AS Service, SUM(dbo.tblPatientService.HospitalChargePaid) AS [Hospital Fee], SUM( dbo.tblIncomeBill.ProfessionalFeePaid) AS [Professional Fee], SUM(dbo.tblPatientService.ChargePaid) AS [Total Fee], dbo.tblPatientService.ServiceDate AS AppointmentDate, tblDoctor.Name AS Doctor " & _
"FROM dbo.tblPatientService LEFT OUTER JOIN dbo.tblProfessionalCharges LEFT OUTER JOIN dbo.tblStaff tblDoctor ON dbo.tblProfessionalCharges.StaffID = tblDoctor.StaffID ON dbo.tblPatientService.PatientServiceID = dbo.tblProfessionalCharges.PatientServiceID LEFT OUTER JOIN dbo.tblServiceCategory ON dbo.tblPatientService.ServiceCategoryID = dbo.tblServiceCategory.ServiceCategoryID RIGHT OUTER JOIN dbo.tblPaymentMethod RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblPaymentMethod.PaymentMethodID = dbo.tblIncomeBill.PaymentMethodID ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN dbo.tblStaff ON dbo.tblIncomeBill.CompletedUserID = dbo.tblStaff.StaffID " & _
"WHERE (dbo.tblIncomeBill.Completed = 1) AND (dbo.tblPatientService.Deleted = 0) AND (((dbo.tblIncomeBill.CompletedDate = CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) AND (dbo.tblPaymentMethod.FinalPay = 1) ) OR ((dbo.tblIncomeBill.CreditDate = CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) AND (dbo.tblPaymentMethod.FinalPay <> 1) )) " & _
"GROUP BY dbo.tblPaymentMethod.PaymentMethod, dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, dbo.tblPatientService.ServiceDate, tblDoctor.Name  " & _
"ORDER BY dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, dbo.tblPaymentMethod.PaymentMethod "
    FillAnyGrid temSQL, gridSummery, True



temSQL = "SELECT TOP 100 PERCENT dbo.tblStaff.Name AS [User], dbo.tblPaymentMethod.PaymentMethod AS Payment, dbo.tblServiceCategory.ServiceCategory AS Service, SUM(dbo.tblPatientService.HospitalChargeCancelled) AS [Hospital Fee], SUM( dbo.tblIncomeBill.ProfessionalFeeCancelled) AS [Professional Fee], SUM(dbo.tblPatientService.ChargeCancelled) AS [Total Fee], dbo.tblPatientService.ServiceDate AS AppointmentDate, tblDoctor.Name AS Doctor " & _
"FROM dbo.tblPatientService LEFT OUTER JOIN dbo.tblProfessionalCharges LEFT OUTER JOIN dbo.tblStaff tblDoctor ON dbo.tblProfessionalCharges.StaffID = tblDoctor.StaffID ON dbo.tblPatientService.PatientServiceID = dbo.tblProfessionalCharges.PatientServiceID LEFT OUTER JOIN dbo.tblServiceCategory ON dbo.tblPatientService.ServiceCategoryID = dbo.tblServiceCategory.ServiceCategoryID RIGHT OUTER JOIN dbo.tblPaymentMethod RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblPaymentMethod.PaymentMethodID = dbo.tblIncomeBill.CancelledPaymentMethodID ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN dbo.tblStaff ON dbo.tblIncomeBill.CancelledUserID = dbo.tblStaff.StaffID " & _
"WHERE      (dbo.tblPatientService.Deleted = 0) AND (dbo.tblIncomeBill.Cancelled = 1) AND (dbo.tblIncomeBill.CancelledDate BETWEEN CONVERT(DATETIME,  " & _
"                        '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) " & _
"GROUP BY dbo.tblPaymentMethod.PaymentMethod, dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, dbo.tblPatientService.ServiceDate, tblDoctor.Name " & _
"ORDER BY dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, dbo.tblPaymentMethod.PaymentMethod"
    With gridSummery
        billRowEnd = .row
        .Rows = .Rows + 2
        .row = .Rows - 1
        .col = 0
        .text = "Cancellations"
        .Rows = .Rows + 1
        .row = .Rows - 1
        cancalRowStart = .row
    End With
    
    FillAnyGrid temSQL, gridSummery, True
    
temSQL = "SELECT TOP 100 PERCENT dbo.tblStaff.Name AS [User], dbo.tblPaymentMethod.PaymentMethod AS Payment, dbo.tblServiceCategory.ServiceCategory AS Service, SUM(dbo.tblPatientService.HospitaChargelReturned) AS [Hospital Fee], SUM( dbo.tblIncomeBill.ProfessionalFeeReturned) AS [Professional Fee], SUM(dbo.tblPatientService.ChargeReturned) AS [Total Fee], dbo.tblPatientService.ServiceDate AS AppointmentDate, tblDoctor.Name AS Doctor " & _
"FROM dbo.tblPatientService LEFT OUTER JOIN dbo.tblProfessionalCharges LEFT OUTER JOIN dbo.tblStaff tblDoctor ON dbo.tblProfessionalCharges.StaffID = tblDoctor.StaffID ON dbo.tblPatientService.PatientServiceID = dbo.tblProfessionalCharges.PatientServiceID LEFT OUTER JOIN dbo.tblServiceCategory ON dbo.tblPatientService.ServiceCategoryID = dbo.tblServiceCategory.ServiceCategoryID RIGHT OUTER JOIN dbo.tblPaymentMethod RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblPaymentMethod.PaymentMethodID = dbo.tblIncomeBill.ReturnedPaymentMethodID ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN dbo.tblStaff ON dbo.tblIncomeBill.ReturnedUserID = dbo.tblStaff.StaffID " & _
"WHERE      (dbo.tblPatientService.Deleted = 0) AND (dbo.tblIncomeBill.Returned = 1) AND (dbo.tblIncomeBill.ReturnedDate BETWEEN CONVERT(DATETIME,  " & _
"                        '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) " & _
"GROUP BY dbo.tblPaymentMethod.PaymentMethod, dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, dbo.tblPatientService.ServiceDate, tblDoctor.Name  " & _
"ORDER BY dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, dbo.tblPaymentMethod.PaymentMethod"
    With gridSummery
       cancelRowEnd = .row
        .Rows = .Rows + 2
        .row = .Rows - 1
       
        .col = 0
        .text = "Returns"
        .Rows = .Rows + 2
        .row = .Rows - 1
        returnRowStart = .row
    End With
    
    FillAnyGrid temSQL, gridSummery, True
    
    returnRowEnd = gridSummery.row
    
    gridReset gridSummery
    
    If IsNumeric(cmbCat.BoundText) = True Then
        hideRows gridSummery, 2, cmbCat.text, "Service"
    End If
    
    If IsNumeric(cmbPm.BoundText) = True Then
        hideRows gridSummery, 1, cmbPm.text, "Payment"
    End If
    
    If IsNumeric(cmbUser.BoundText) = True Then
        hideRows gridSummery, 0, cmbUser.text, "User"
    End If
    
    With gridSummery
        .Rows = .Rows + 2
        .row = .Rows - 1
        .col = 0
        .text = "Total"
        
        .col = 3
        .text = rowTotal(gridSummery, 3, Val(billRowStart), Val(billRowEnd), True) - rowTotal(gridSummery, 3, Val(cancalRowStart), Val(cancelRowEnd), True) - rowTotal(gridSummery, 3, Val(returnRowStart), Val(returnRowEnd), True)

        .col = 4
        .text = rowTotal(gridSummery, 4, Val(billRowStart), Val(billRowEnd), True) - rowTotal(gridSummery, 4, Val(cancalRowStart), Val(cancelRowEnd), True) - rowTotal(gridSummery, 4, Val(returnRowStart), Val(returnRowEnd), True)
        

        .col = 5
        .text = rowTotal(gridSummery, 5, Val(billRowStart), Val(billRowEnd), True) - rowTotal(gridSummery, 5, Val(cancalRowStart), Val(cancelRowEnd), True) - rowTotal(gridSummery, 5, Val(returnRowStart), Val(returnRowEnd), True)
    
    End With
    
    formatGridString gridSummery, 3, "#,##0.00"
    formatGridString gridSummery, 4, "#,##0.00"
    formatGridString gridSummery, 5, "#,##0.00"
    
    
    gridSummery.Visible = True

End Sub
Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
        End If
End Sub

Private Sub cmbPm_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbPm.text = Empty
End Sub

Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbUser.text = Empty
End Sub

Private Sub fillCombos()
    Dim cat As New clsFillCombos
    cat.FillAnyCombo cmbCat, "ServiceCategory", True
    Dim PayM As New clsFillCombos
    PayM.FillAnyCombo cmbPm, "PaymentMethod", False
    Dim Cashier As New clsFillCombos
    Cashier.FillBoolCombo cmbUser, "Staff", "Name", "IsAUser", False

End Sub

Private Sub Form_Load()
    fillCombos
    GetSettings
End Sub

Private Sub GetSettings()
    'GetCommonSettings Me
    dtpDate.Value = Date
End Sub

Private Sub SaveSettings()
   'saveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub




