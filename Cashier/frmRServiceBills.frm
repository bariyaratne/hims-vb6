VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmRServiceBills 
   BackColor       =   &H008080FF&
   Caption         =   "Radiology Bills"
   ClientHeight    =   8715
   ClientLeft      =   120
   ClientTop       =   510
   ClientWidth     =   15120
   FillColor       =   &H0000FFFF&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8715
   ScaleWidth      =   15120
   WindowState     =   2  'Maximized
   Begin VB.CommandButton btnSet 
      Caption         =   "Set"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   109
      Top             =   3480
      Width           =   375
   End
   Begin VB.CheckBox chkForeigner 
      BackColor       =   &H008080FF&
      Caption         =   "&Foreigner"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4200
      TabIndex        =   106
      Top             =   6840
      Width           =   1215
   End
   Begin TabDlg.SSTab SSTab2 
      Height          =   4575
      Left            =   6720
      TabIndex        =   91
      Top             =   3360
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   8070
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "List"
      TabPicture(0)   =   "frmRServiceBills.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "btnBhtDoc"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "btnReprint"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "btnXray"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "btnNurseView"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "btnDoctorView"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "txtOPDBillID"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "txtDelID"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "txtDuration"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "gridList"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).ControlCount=   9
      TabCaption(1)   =   "Details"
      TabPicture(1)   =   "frmRServiceBills.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "btnFee"
      Tab(1).Control(1)=   "btnAbsent"
      Tab(1).Control(2)=   "btnRefund"
      Tab(1).Control(3)=   "btnCancel"
      Tab(1).Control(4)=   "btnSettle"
      Tab(1).Control(5)=   "btnChange"
      Tab(1).Control(6)=   "rtbDetails"
      Tab(1).Control(7)=   "chkSettlePrint"
      Tab(1).ControlCount=   8
      Begin MSFlexGridLib.MSFlexGrid gridList 
         Height          =   3615
         Left            =   120
         TabIndex        =   92
         Top             =   360
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   6376
         _Version        =   393216
         SelectionMode   =   1
      End
      Begin VB.TextBox txtDuration 
         Height          =   360
         Left            =   840
         TabIndex        =   103
         Top             =   3240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtDelID 
         Height          =   360
         Left            =   1560
         TabIndex        =   102
         Top             =   3000
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtOPDBillID 
         Height          =   375
         Left            =   480
         TabIndex        =   101
         Top             =   3240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CheckBox chkSettlePrint 
         BackColor       =   &H008080FF&
         Caption         =   "&Print"
         Height          =   255
         Left            =   -69600
         TabIndex        =   96
         Top             =   3840
         Value           =   1  'Checked
         Width           =   975
      End
      Begin RichTextLib.RichTextBox rtbDetails 
         Height          =   3375
         Left            =   -74880
         TabIndex        =   93
         Top             =   480
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   5953
         _Version        =   393217
         TextRTF         =   $"frmRServiceBills.frx":0038
      End
      Begin btButtonEx.ButtonEx btnChange 
         Height          =   375
         Left            =   -68280
         TabIndex        =   94
         Top             =   4080
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         Enabled         =   0   'False
         BackColor       =   32896
         Caption         =   "Name Change"
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnSettle 
         Height          =   375
         Left            =   -69600
         TabIndex        =   95
         Top             =   4080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         Appearance      =   3
         Enabled         =   0   'False
         BackColor       =   32896
         Caption         =   "Settle"
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnCancel 
         Height          =   375
         Left            =   -70920
         TabIndex        =   97
         Top             =   4080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         Appearance      =   3
         Enabled         =   0   'False
         BackColor       =   32896
         Caption         =   "Cancel"
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnRefund 
         Height          =   375
         Left            =   -74880
         TabIndex        =   98
         Top             =   4080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "Refund"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnDoctorView 
         Height          =   375
         Left            =   1560
         TabIndex        =   99
         Top             =   4080
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "Doctor View"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnNurseView 
         Height          =   375
         Left            =   120
         TabIndex        =   100
         Top             =   4080
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "Nurse View"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnXray 
         Height          =   375
         Left            =   4920
         TabIndex        =   104
         Top             =   4080
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "X-Ray View"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnReprint 
         Height          =   375
         Left            =   6480
         TabIndex        =   105
         Top             =   4080
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         Appearance      =   3
         Enabled         =   0   'False
         BackColor       =   32896
         Caption         =   "Reprint"
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnBhtDoc 
         Height          =   375
         Left            =   3120
         TabIndex        =   107
         Top             =   4080
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "Doctor Payment"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnAbsent 
         Height          =   375
         Left            =   -73560
         TabIndex        =   108
         Top             =   4080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "Mark Absent"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnFee 
         Height          =   375
         Left            =   -72240
         TabIndex        =   110
         Top             =   4080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         Appearance      =   3
         BackColor       =   32896
         Caption         =   "Fee Change"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComCtl2.MonthView dtpAppDate 
      Height          =   2820
      Left            =   9720
      TabIndex        =   88
      Top             =   480
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   4974
      _Version        =   393216
      ForeColor       =   192
      BackColor       =   8438015
      Appearance      =   1
      MonthBackColor  =   8438015
      StartOfWeek     =   112525313
      TitleBackColor  =   16777215
      TrailingForeColor=   16777215
      CurrentDate     =   41136
   End
   Begin MSDataListLib.DataList lstCategory 
      Height          =   1020
      Left            =   6840
      TabIndex        =   5
      Top             =   120
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   1799
      _Version        =   393216
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H008080FF&
      Height          =   735
      Left            =   6720
      TabIndex        =   72
      Top             =   7920
      Width           =   6495
      Begin VB.ComboBox cmbPaper 
         Height          =   360
         Left            =   4560
         Style           =   2  'Dropdown List
         TabIndex        =   74
         Top             =   240
         Width           =   1815
      End
      Begin VB.ComboBox cmbPrinter 
         Height          =   360
         Left            =   720
         Style           =   2  'Dropdown List
         TabIndex        =   73
         Top             =   240
         Width           =   3615
      End
      Begin VB.Label Label29 
         BackColor       =   &H0080FFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Paper"
         Height          =   255
         Left            =   2400
         TabIndex        =   76
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label30 
         BackColor       =   &H0080FFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Printer"
         Height          =   255
         Left            =   120
         TabIndex        =   75
         Top             =   240
         Width           =   1815
      End
   End
   Begin MSDataListLib.DataList lstSession 
      Height          =   1740
      Left            =   120
      TabIndex        =   7
      Top             =   1560
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   3069
      _Version        =   393216
   End
   Begin VB.TextBox txtSessionId 
      Height          =   360
      Left            =   8520
      TabIndex        =   87
      Top             =   8040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtProId 
      Height          =   360
      Left            =   8880
      TabIndex        =   86
      Top             =   8040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtCatId 
      Height          =   360
      Left            =   7800
      TabIndex        =   85
      Top             =   8040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSubCatId 
      Height          =   360
      Left            =   8160
      TabIndex        =   84
      Top             =   8040
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtTotalProFee 
      Height          =   360
      Left            =   7080
      TabIndex        =   83
      Top             =   7920
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtTotalHosFee 
      Height          =   360
      Left            =   6720
      TabIndex        =   82
      Top             =   7920
      Visible         =   0   'False
      Width           =   375
   End
   Begin btButtonEx.ButtonEx btnUpdate 
      Height          =   375
      Left            =   5400
      TabIndex        =   60
      Top             =   6840
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      BackColor       =   32896
      Caption         =   "&Update"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1215
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   2143
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Outpatient"
      TabPicture(0)   =   "frmRServiceBills.frx":00B3
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label8"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtPatient"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtPhone"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "txtNic"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "BHT"
      TabPicture(1)   =   "frmRServiceBills.frx":00CF
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtBhtPatientName"
      Tab(1).Control(1)=   "cmbBHT"
      Tab(1).Control(2)=   "Label13"
      Tab(1).Control(3)=   "Label9"
      Tab(1).ControlCount=   4
      Begin VB.TextBox txtNic 
         Height          =   375
         Left            =   3000
         TabIndex        =   112
         Top             =   720
         Width           =   1695
      End
      Begin VB.TextBox txtBhtPatientName 
         Height          =   375
         Left            =   -74880
         TabIndex        =   3
         Top             =   720
         Width           =   3615
      End
      Begin VB.TextBox txtPhone 
         Height          =   375
         Left            =   4800
         TabIndex        =   1
         Top             =   720
         Width           =   1695
      End
      Begin VB.TextBox txtPatient 
         Height          =   375
         Left            =   120
         TabIndex        =   0
         Top             =   720
         Width           =   2775
      End
      Begin MSDataListLib.DataCombo cmbBHT 
         Height          =   360
         Left            =   -71160
         TabIndex        =   4
         Top             =   720
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Text            =   "BHT"
      End
      Begin VB.Label Label13 
         Caption         =   "BHT"
         Height          =   255
         Left            =   -71160
         TabIndex        =   116
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "Name"
         Height          =   255
         Left            =   -74880
         TabIndex        =   115
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label8 
         Caption         =   "Phone"
         Height          =   255
         Left            =   4800
         TabIndex        =   114
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "NIC"
         Height          =   255
         Left            =   3000
         TabIndex        =   113
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Name"
         Height          =   255
         Left            =   120
         TabIndex        =   111
         Top             =   480
         Width           =   1215
      End
   End
   Begin btButtonEx.ButtonEx btnAdd 
      Height          =   375
      Left            =   14040
      TabIndex        =   23
      Top             =   2760
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      Appearance      =   3
      BackColor       =   32896
      Caption         =   "&Add"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtDisplayBillID 
      Height          =   375
      Left            =   7080
      TabIndex        =   80
      Top             =   6960
      Visible         =   0   'False
      Width           =   375
   End
   Begin MSDataListLib.DataCombo cmbHSS 
      Height          =   360
      Left            =   2520
      TabIndex        =   78
      Top             =   8280
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtSerialNo 
      Height          =   375
      Left            =   7440
      Locked          =   -1  'True
      TabIndex        =   26
      Top             =   6960
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox txtPaymentMethod 
      Height          =   375
      Left            =   2520
      TabIndex        =   65
      Top             =   7800
      Width           =   4095
   End
   Begin VB.TextBox txtPtID 
      Height          =   375
      Left            =   6720
      TabIndex        =   69
      Top             =   6960
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CheckBox chkPrint 
      BackColor       =   &H008080FF&
      Caption         =   "&Print"
      Height          =   255
      Left            =   4200
      TabIndex        =   22
      Top             =   7080
      Width           =   735
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   6
      Left            =   1800
      TabIndex        =   56
      Top             =   5640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   5
      Left            =   1800
      TabIndex        =   51
      Top             =   5280
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   4
      Left            =   1800
      TabIndex        =   46
      Top             =   4920
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   3
      Left            =   1800
      TabIndex        =   41
      Top             =   4560
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   2
      Left            =   1800
      TabIndex        =   36
      Top             =   4200
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   1
      Left            =   1800
      TabIndex        =   33
      Top             =   3840
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtSpecialityID 
      Height          =   375
      Index           =   0
      Left            =   1800
      TabIndex        =   31
      Top             =   3480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   6
      Left            =   2280
      TabIndex        =   57
      Top             =   5640
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   5
      Left            =   2280
      TabIndex        =   52
      Top             =   5280
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   4
      Left            =   2280
      TabIndex        =   47
      Top             =   4920
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   3
      Left            =   2280
      TabIndex        =   42
      Top             =   4560
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   2
      Left            =   2280
      TabIndex        =   37
      Top             =   4200
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   1
      Left            =   2280
      TabIndex        =   34
      Top             =   3840
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtServiceProfessionalChargesID 
      Height          =   375
      Index           =   0
      Left            =   2280
      TabIndex        =   32
      Top             =   3480
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtCharge 
      Height          =   375
      Left            =   12840
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   2280
      Width           =   2295
   End
   Begin VB.TextBox txtProfessionalCharge 
      Height          =   375
      Left            =   12840
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   1680
      Width           =   2295
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   6
      Left            =   5400
      TabIndex        =   59
      Top             =   5640
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   5
      Left            =   5400
      TabIndex        =   54
      Top             =   5280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   4
      Left            =   5400
      TabIndex        =   49
      Top             =   4920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   3
      Left            =   5400
      TabIndex        =   44
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   2
      Left            =   5400
      TabIndex        =   39
      Top             =   4200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   1
      Left            =   5400
      TabIndex        =   21
      Top             =   3840
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtFee1 
      Alignment       =   1  'Right Justify
      Height          =   375
      Index           =   0
      Left            =   5400
      TabIndex        =   18
      Top             =   3480
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   0
      Left            =   2760
      TabIndex        =   17
      Top             =   3480
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtEditID 
      Height          =   360
      Left            =   7920
      TabIndex        =   67
      Top             =   6960
      Visible         =   0   'False
      Width           =   375
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   8640
      TabIndex        =   66
      Top             =   6960
      Visible         =   0   'False
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMM yyyy"
      Format          =   112525315
      CurrentDate     =   39956
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   375
      Left            =   13920
      TabIndex        =   61
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      BackColor       =   32896
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtHospitalCharge 
      Height          =   375
      Left            =   12840
      TabIndex        =   11
      Top             =   1080
      Width           =   2295
   End
   Begin btButtonEx.ButtonEx btnDelete 
      Height          =   375
      Left            =   120
      TabIndex        =   25
      Top             =   6840
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      Appearance      =   3
      BackColor       =   32896
      Caption         =   "&Delete"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   1
      Left            =   2760
      TabIndex        =   20
      Top             =   3840
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   2
      Left            =   2760
      TabIndex        =   38
      Top             =   4200
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   3
      Left            =   2760
      TabIndex        =   43
      Top             =   4560
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   4
      Left            =   2760
      TabIndex        =   48
      Top             =   4920
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   5
      Left            =   2760
      TabIndex        =   53
      Top             =   5280
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbStaff1 
      Height          =   360
      Index           =   6
      Left            =   2760
      TabIndex        =   58
      Top             =   5640
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpTime 
      Height          =   375
      Left            =   10920
      TabIndex        =   68
      Top             =   6960
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMM yyyy"
      Format          =   112525314
      CurrentDate     =   39956
   End
   Begin MSDataListLib.DataCombo cmbPaymentMethod 
      Height          =   360
      Left            =   2520
      TabIndex        =   63
      Top             =   7320
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtComments 
      Height          =   375
      Left            =   2280
      TabIndex        =   30
      Top             =   1800
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.DTPicker dtpAppTime 
      Height          =   375
      Left            =   12840
      TabIndex        =   29
      Top             =   360
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMM yyyy"
      Format          =   112525314
      CurrentDate     =   39956
   End
   Begin MSComCtl2.DTPicker dtpStart 
      Height          =   375
      Left            =   14160
      TabIndex        =   77
      Top             =   0
      Visible         =   0   'False
      Width           =   375
      _ExtentX        =   661
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMM yyyy"
      Format          =   112525314
      CurrentDate     =   39956
   End
   Begin MSDataListLib.DataCombo cmbAgent 
      Height          =   360
      Left            =   2520
      TabIndex        =   81
      Top             =   8280
      Visible         =   0   'False
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSFlexGridLib.MSFlexGrid gridService 
      Height          =   2655
      Left            =   120
      TabIndex        =   24
      Top             =   3960
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   4683
      _Version        =   393216
   End
   Begin MSDataListLib.DataList lstSc 
      Height          =   1740
      Left            =   4560
      TabIndex        =   9
      Top             =   1560
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   3069
      _Version        =   393216
      BackColor       =   -2147483634
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Label1"
      Height          =   1695
      Left            =   1200
      TabIndex        =   90
      Top             =   6960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblDate 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   10320
      TabIndex        =   89
      Top             =   120
      Width           =   2415
   End
   Begin VB.Label lblHSS 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Credit Company"
      Height          =   255
      Left            =   120
      TabIndex        =   79
      Top             =   8280
      Width           =   1815
   End
   Begin VB.Label lblSC 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Service Su&bcategory"
      Height          =   255
      Left            =   4560
      TabIndex        =   8
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label Label16 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Time"
      Height          =   255
      Left            =   12840
      TabIndex        =   28
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label Label14 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      Height          =   255
      Left            =   9720
      TabIndex        =   27
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label Label12 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Session"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label Label11 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Payment Co&mments"
      Height          =   255
      Left            =   120
      TabIndex        =   64
      Top             =   7800
      Width           =   1815
   End
   Begin VB.Label Label10 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Payment &Method"
      Height          =   255
      Left            =   120
      TabIndex        =   62
      Top             =   7320
      Width           =   1815
   End
   Begin VB.Label Label7 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "&Total"
      Height          =   255
      Left            =   12840
      TabIndex        =   14
      Top             =   2040
      Width           =   1215
   End
   Begin VB.Label Label3 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Professional Charges"
      Height          =   255
      Left            =   12840
      TabIndex        =   12
      Top             =   1440
      Width           =   1815
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   6
      Left            =   600
      TabIndex        =   55
      Top             =   6360
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   5
      Left            =   600
      TabIndex        =   50
      Top             =   5760
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   4
      Left            =   600
      TabIndex        =   45
      Top             =   5400
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   3
      Left            =   600
      TabIndex        =   40
      Top             =   4920
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   2
      Left            =   600
      TabIndex        =   35
      Top             =   4440
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   1
      Left            =   600
      TabIndex        =   19
      Top             =   3960
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblSpeciality1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "lblSpeciality1"
      Height          =   255
      Index           =   0
      Left            =   600
      TabIndex        =   16
      Top             =   3480
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   71
      Top             =   6840
      Width           =   1695
   End
   Begin VB.Label Label6 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   70
      Top             =   6840
      Width           =   735
   End
   Begin VB.Label Label5 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "&Hospital Charge"
      Height          =   255
      Left            =   12840
      TabIndex        =   10
      Top             =   840
      Width           =   1815
   End
End
Attribute VB_Name = "frmRServiceBills"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsSC As New ADODB.Recordset
    Dim temSql As String
    Dim rsSPC As New ADODB.Recordset
    Dim rsStaff() As New ADODB.Recordset
    Dim PSCCount As Long
    Dim FirstActi As Boolean
    Dim rsHSS As New ADODB.Recordset

    Dim Agents As New clsFillCombos

    Dim CsetPrinter As New cSetDfltPrinter
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean

    Dim rsSecession As New ADODB.Recordset
    
    Dim rsBHT As New ADODB.Recordset
    
    Dim currentPatient As New Patient
    Dim currentBht As New BHT
    Dim currentCat As New ServiceCategory
    
    Dim focusedBackColour
    Dim noFocusBackColour
    Dim todayStaffId As Long

Private Sub btnAbsent_Click()
Dim i As Integer
i = MsgBox("Are you sure you want to mark this patient Absent?", vbYesNo, "Absent?")
If i = vbNo Then Exit Sub

    Dim temBillId As Long
    temBillId = Val(gridList.TextMatrix(gridList.row, 0))
    Dim TemString As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT * " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE IncomeBillID = " & temBillId
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        !Absent = True
        !AbsentUserID = UserID
        !AbsentDate = Date
        !AbsentTime = Now
        .Update
        If .RecordCount > 0 Then
            Dim strTem  As String
            Dim rsTem1 As New ADODB.Recordset
            Dim rsTem2 As New ADODB.Recordset
            Dim dblNo1 As Double
            If rsTem1.State = 1 Then rsTem1.Close
            temSql = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & temBillId
            rsTem1.Open temSql, cnnStores, adOpenStatic, adLockOptimistic
            While rsTem1.EOF = False
                rsTem1!ProfessionalCharge = 0
                rsTem1!ProfessionalChargePaid = 0
                rsTem1!Absent = True
                rsTem1!AbsentUserID = UserID
                rsTem1!AbsentDate = Date
                rsTem1!AbsentTime = Now
                rsTem1.Update
                temSql = "SELECT * From tblProfessionalCharges WHERE  PatientServiceID = " & rsTem1!PatientServiceID
                If rsTem2.State = 1 Then rsTem2.Close
                rsTem2.Open temSql, cnnStores, adOpenStatic, adLockOptimistic
                While rsTem2.EOF = False
                    rsTem2!FeePaid = 0
                    rsTem2!Absent = True
                    rsTem2!AbsentUserID = UserID
                    rsTem2!AbsentDate = Date
                    rsTem2!AbsentTime = Now
                    rsTem2.Update
                    rsTem2.MoveNext
                Wend
                rsTem2.Close
                rsTem1.MoveNext
            Wend
            rsTem1.Close
        End If
        .Close
    End With
    MsgBox "Marked as absent"
    
End Sub

Private Sub btnAdd_Click()
     If IsNumeric(txtOPDBillID.text) = False Then txtOPDBillID.text = NewRBillID(dtpDate.Value, dtpTime.Value)
    Dim rsTem As New ADODB.Recordset
       
   If lstSc.Visible = True And IsNumeric(lstSc.BoundText) = False Then
        MsgBox "Please select a subcategoty"
        Exit Sub
   End If
   
    Dim n As Integer
    If IsNumeric(cmbPaymentMethod.BoundText) = False Then
        MsgBox "Payment method?"
        cmbPaymentMethod.SetFocus
        Exit Sub
    End If

    
    If IsNumeric(lstCategory.BoundText) = False Then
        MsgBox "Service?"
        lstCategory.SetFocus
        Exit Sub
    End If
    
    If IsNumeric(lstSession.BoundText) = False Then
        MsgBox "Session?"
        lstSession.SetFocus
        Exit Sub
    End If
    
    If InStr(LCase(lstCategory), "x-ray") > 0 And dtpAppDate.Value <> Date Then
        Dim tr As Integer
        MsgBox "Can not book for other days for X-Rays"
'        dtpDate.SetFocus
        Exit Sub
    End If
    
    
    
'        On Error Resume Next

    If SSTab1.Tab = 0 Then
        If Trim(txtPatient.text) = "" Then
            MsgBox "Please enter a name for the patient"
            txtPatient.SetFocus
            Exit Sub
        End If
    Else
        If Trim(txtBhtPatientName.text) = "" Then
            MsgBox "Please enter a name for the patient"
            txtBhtPatientName.SetFocus
            Exit Sub
        End If
        If Trim(cmbBHT.text) = "" Then
            MsgBox "Please enter a BHT"
            cmbBHT.SetFocus
            Exit Sub
        End If
    
    End If
    
    If SSTab1.Tab = 0 Then
        If currentPatient.PatientID = 0 Then
            currentPatient.FirstName = txtPatient.text
            currentPatient.Phone = txtPhone.text
            currentPatient.NICNo = txtNic.text
            currentPatient.SaveData
            txtPtID.text = currentPatient.PatientID
        End If
    '        With rsTem
    
    '            If .State = 1 Then .Close
    '            temSQL = "Select * from tblPatientMainDetails where PatientID = 0 "
    '            .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
    '            .AddNew
    '            !FirstName = txtPatient.text
    '            !Phone = txtPhone.text
    '            .Update
    '            temSQL = "SELECT @@IDENTITY AS NewID"
    '            .Close
    '            .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    '            txtPtID.text = !NewID
    '            .Close
    '        End With
    ElseIf SSTab1.Tab = 1 Then
        If currentPatient.PatientID = 0 Then
            currentPatient.FirstName = txtBhtPatientName.text
            currentPatient.SaveData
            txtPtID.text = currentPatient.PatientID
        End If
        If currentBht.BHTID = 0 Then
            currentBht.BHT = cmbBHT.text
            currentBht.PatientID = currentPatient.PatientID
            currentBht.DOA = Date
            currentBht.TOA = Time
            currentBht.SaveData
        End If
    End If
    
    
    
'        On Error Resume Next

    
'    With rsTem
'        If .State = 1 Then .Close
'        If IsNumeric(txtEditID.text) = True Then
'            temSQL = "Select * from tblPatientService where PatientServiceID = " & Val(txtEditID.text)
'            .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'            If .RecordCount <= 0 Then
'                .AddNew
'            End If
'        Else
'            temSQL = "Select * from tblPatientService  where PatientServiceID = 0 "
'            .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'            .AddNew
'        End If
'        !RBillID = Val(txtOPDBillID.text)
'        !ServiceCategoryID = Val(lstCategory.BoundText)
'        !ServiceSubcategoryID = Val(lstSc.BoundText)
'        !Comments = txtComments.text
'        !ServiceDate = dtpAppDate.Value
'        !ServiceTime = dtpAppTime.Value
'        !Charge = Val(txtCharge.text)
'        !ProfessionalCharge = Val(txtProfessionalCharge.text)
'        !HospitalCharge = Val(txtHospitalCharge.text)
'        !UserID = UserID
'        !BHTID = Val(cmbBHT.BoundText)
'        getNextSessionNo
'        !PatientID = Val(txtPtID.text)
'        !SerialNo = Val(txtSerialNo.text)
'        !SecessionID = Val(lstSession.BoundText)
'        .Update
'        temSQL = "SELECT @@IDENTITY AS NewID"
'        .Close
'        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
'        txtEditID.text = !NewID
'        .Close
'    End With



    Dim addPs As New PatientService
    Dim PM As New clsPaymentMethod
    PM.PaymentMethodID = Val(cmbPaymentMethod.BoundText)

    With addPs
        If IsNumeric(txtEditID.text) = True Then
            .PatientServiceID = Val(txtEditID.text)
        Else
        
        End If
        .RBillID = Val(txtOPDBillID.text)
        .ServiceCategoryID = Val(lstCategory.BoundText)
        .ServiceSubcategoryID = Val(lstSc.BoundText)
        .Comments = txtComments.text
        .ServiceDate = dtpAppDate.Value
        .ServiceTime = dtpAppTime.Value
        
        .Charge = Val(txtCharge.text)
        .ProfessionalCharge = Val(txtProfessionalCharge.text)
        .HospitalCharge = Val(txtHospitalCharge.text)
        
        If PM.FinalPay Then
            .Charge = Val(txtCharge.text)
            .ChargePaid = Val(txtCharge.text)
            .ChargeCancelled = 0
            .ChargeReturned = 0
            .ChargeToGive = Val(txtCharge.text)
            .ChargeToPay = 0
            .ProfessionalCharge = Val(txtProfessionalCharge.text)
            .ProfessionalChargeCancelled = 0
            .ProfessionalChargePaid = Val(txtProfessionalCharge.text)
            .ProfessionalChargeReturned = 0
            .ProfessionalChargeToGive = Val(txtProfessionalCharge.text)
            .ProfessionalChargeToPay = 0
            .HospitaChargelReturned = 0
            .HospitalCharge = Val(txtHospitalCharge.text)
            .HospitalChargeCancelled = 0
            .HospitalChargeToGive = Val(txtHospitalCharge.text)
            .HospitalChargeToPay = 0
            .HospitalChargePaid = Val(txtHospitalCharge.text)
        Else
            .Charge = Val(txtCharge.text)
            .ChargePaid = 0
            .ChargeCancelled = 0
            .ChargeReturned = 0
            .ChargeToGive = 0
            .ChargeToPay = Val(txtCharge.text)
            .ProfessionalCharge = Val(txtProfessionalCharge.text)
            .ProfessionalChargeCancelled = 0
            .ProfessionalChargePaid = 0
            .ProfessionalChargeReturned = 0
            .ProfessionalChargeToGive = 0
            .ProfessionalChargeToPay = Val(txtProfessionalCharge.text)
            .HospitaChargelReturned = 0
            .HospitalCharge = Val(txtHospitalCharge.text)
            .HospitalChargeCancelled = 0
            .HospitalChargeToGive = 0
            .HospitalChargeToPay = Val(txtHospitalCharge.text)
            .HospitalChargePaid = 0
        End If
        
        
        .UserID = UserID
        .BHTID = Val(cmbBHT.BoundText)
        getNextSessionNo
        .PatientID = Val(txtPtID.text)
        .SerialNo = Val(txtSerialNo.text)
        .SecessionID = Val(lstSession.BoundText)
        .SaveData
        txtEditID.text = .PatientServiceID
    End With











'    For n = 0 To lblSpeciality1.UBound
'        If lblSpeciality1(n).Visible = True Then
'            With rsTem
'                If .State = 1 Then .Close
'                temSQL = "Select * from tblProfessionalCharges where ServiceProfessionalChargesID = " & Val(txtServiceProfessionalChargesID(n).text) & " AND PatientServiceID = " & Val(txtEditID.text)
'                .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'                If .RecordCount > 0 Then
'
'                Else
'                    .AddNew
'                    !UserID = UserID
'                    !ForRBillID = Val(txtOPDBillID.text)
'                    !PatientServiceID = Val(txtEditID.text)
'                    !ServiceProfessionalChargesID = Val(txtServiceProfessionalChargesID(n).text)
'                    !StaffID = Val(cmbStaff1(n).BoundText)
'                End If
'                !Date = dtpDate.Value
'                !Time = dtpTime.Value
'                !Fee = Val(txtFee1(n).text)
'                !IsRBill = True
'                .Update
'            End With
'        End If
'    Next n
    
    Dim pfoChg As New ProfessionalCharge
    
    For n = 0 To lblSpeciality1.UBound
        If lblSpeciality1(n).Visible = True Then
            With rsTem
                If .State = 1 Then .Close
                temSql = "Select * from tblProfessionalCharges where ServiceProfessionalChargesID = " & Val(txtServiceProfessionalChargesID(n).text) & " AND PatientServiceID = " & Val(txtEditID.text)
                .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
                
                If .RecordCount > 0 Then
                    pfoChg.ProfessionalChargesID = !ProfessionalChargesID
                Else
                    pfoChg.ProfessionalChargesID = 0
                    pfoChg.UserID = UserID
                    pfoChg.ForRBillID = Val(txtOPDBillID.text)
                    pfoChg.PatientServiceID = Val(txtEditID.text)
                    pfoChg.ServiceProfessionalChargesID = Val(txtServiceProfessionalChargesID(n).text)
                    pfoChg.StaffID = Val(cmbStaff1(n).BoundText)
                End If
                pfoChg.BilledDate = dtpDate.Value
                pfoChg.BillledTime = dtpTime.Value
                pfoChg.IsRBill = True
                
                If PM.FinalPay = True Or PM.SettlingPay = True Then
                    pfoChg.Fee = Val(txtFee1(n).text)
                    pfoChg.FeeCancelled = 0
                    pfoChg.FeePaid = Val(txtFee1(n).text)
                    pfoChg.FeeReturned = 0
                    pfoChg.FeeToGive = Val(txtFee1(n).text)
                    pfoChg.FeeToPay = 0
                Else
                    pfoChg.Fee = Val(txtFee1(n).text)
                    pfoChg.FeeCancelled = 0
                    pfoChg.FeePaid = 0
                    pfoChg.FeeReturned = 0
                    pfoChg.FeeToGive = 0
                    pfoChg.FeeToPay = Val(txtFee1(n).text)
                End If
                pfoChg.SaveData
            End With
        End If
    Next n
    
    Call FillGrid
    
    saveStaffToReg
    
    Call ClearAddValues
    
    lstCategory.Locked = True
    cmbPaymentMethod.Locked = True
    lstSession.Locked = True
    If lstSc.Visible = True Then
        lstSc.SetFocus
    Else
        lstCategory.SetFocus
    End If
    
    
   ' btnUpdate_Click
    
End Sub

Private Sub saveStaffToReg()
    Dim n As Integer
    For n = 0 To 6
        If cmbStaff1(n).Visible = True Then
            If cmbStaff1(n).text = "" Then
                
            Else
                SaveSetting App.EXEName, Me.Name, "cmbStaff" & n, cmbStaff1(n).text
            End If
        End If
    Next
End Sub

Private Sub ClearAddValues()
    Dim n As Long
    
'    lstCategory.Text = Empty
    
    lstSc.text = Empty
    txtComments.text = Empty
    txtProfessionalCharge.text = Empty
    txtHospitalCharge.text = Empty
    txtCharge.text = Empty
    txtEditID.text = Empty
    txtDelID.text = Empty
    
    For n = 0 To lblSpeciality1.UBound
        lblSpeciality1(n).Visible = False
        lblSpeciality1(n).Caption = Empty
        cmbStaff1(n).Visible = False
        cmbStaff1(n).text = Empty
        txtServiceProfessionalChargesID(n).text = Empty
        txtFee1(n).Visible = False
        txtFee1(n).text = Empty
        txtSpecialityID(n).text = Empty
    Next
    
    dtpAppTime.Value = Date
    
End Sub

Private Sub ClearBillValues()
    lstCategory.text = Empty
    lstSc.text = Empty
    lstSession.text = Empty
    
    cmbPaymentMethod.BoundText = 1
    txtPatient.text = ""
    txtPhone.text = ""
    txtNic.text = ""
    txtBhtPatientName.text = ""
    cmbBHT.text = ""
    txtOPDBillID.text = Empty
    txtPtID.text = Empty
    lblTotal.Caption = "0.00"
    txtPaymentMethod.text = Empty
    txtSerialNo.text = Empty
    chkForeigner.Value = 0
    dtpAppDate.Value = Date
    
    Set currentPatient = New Patient
    Set currentBht = New BHT
    Set currentCat = New ServiceCategory
    
    
End Sub

Private Sub btnBhtDoc_Click()
    Dim rsTem As New ADODB.Recordset
    If Val(lstSession.BoundText) = 0 Then
        MsgBox "Please select a session"
        lstSession.SetFocus
        Exit Sub
    End If
    If Val(lstSc.BoundText) = 0 Then
        MsgBox "Please select a sub category"
        lstSc.SetFocus
        Exit Sub
    End If
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT dbo.tblPatientService.SerialNo, dbo.tblPatientMainDetails.FirstName AS PatientName, dbo.tblPatientService.ProfessionalCharge, dbo.tblIncomeBill.BillSettled as Settled, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.Returned, dbo.tblPatientService.ProfessionalCharge , dbo.tblPatientService.Charge, dbo.tblIncomeBill.BillSettled, dbo.tblIncomeBill.displayBillID, dbo.tblBHT.BHT  " & _
                            "FROM dbo.tblBHT RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblBHT.BHTID = dbo.tblIncomeBill.BHTID RIGHT OUTER JOIN dbo.tblPatientService ON dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN                         dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where dbo.tblIncomeBill.BillSettled = 1 AND dbo.tblPatientService.Deleted = 0 AND dbo.tblIncomeBill.Cancelled = 0 AND dbo.tblIncomeBill.Returned = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServicesubCategoryID = " & Val(lstSc.BoundText) & " AND SECESSIONID  = " & Val(lstSession.BoundText) & " ORDER BY  dbo.tblPatientService.SerialNo"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With DataReportDoctorBHTView
        Set .DataSource = rsTem
           .Sections.Item("header").Controls.Item("lblName").Caption = "Consultant : " & lstSc.text
            .Sections.Item("header").Controls.Item("lblDate").Caption = "Date : " & Format(dtpAppDate.Value, "dd MMMM yyyy") & " - " & lstSession.text
            .Show
    End With

End Sub

Private Sub btnCancel_Click()

    'On Error GoTo eh

    Dim er  As Integer
    er = MsgBox("Are you sure you want to CANCEL?", vbYesNo, "Cancel")
    If er = vbNo Then Exit Sub

    Dim temBillId As Long
    Dim temBill As New IncomeBill
    
    
    temBillId = Val(gridList.TextMatrix(gridList.row, 0))
    temBill.IncomeBillID = temBillId
    
    If IsNumeric(cmbPaymentMethod.BoundText) = False Then
        MsgBox "Please select a Re-Payment Method"
        cmbPaymentMethod.SetFocus
        Exit Sub
    End If
    If ProfessionalFeePaidR(temBillId) = True Then
        MsgBox "You can't cancel this bill as Professional Payments are already done"
        Exit Sub
    End If
    With temBill
        .Cancelled = True
        .CancelledDate = Date
        .CancelledTime = Time
        .CancelledPaymentComments = txtPaymentMethod.text
        .CancelledPaymentMethodID = .PaymentMethodID      ' Val(cmbPaymentMethod.BoundText)
        .CancelledUserID = UserID
        .CancelledValue = .NetTotal
        .NetTotalToPay = 0
        .GrossTotalToPay = 0
        .DiscountToPay = 0
        
        .HospitalFeeCancelled = .HospitalFee
        .HospitalFeeToGive = 0
        .HospitalFeeToPay = 0
        .HospitalFeePaid = 0
        
        .ProfessionalFeeCancelled = .ProfessionalFee
        .ProfessionalFeeToGive = 0
        .ProfessionalFeeToPay = 0
        .ProfessionalFeePaid = 0
        
        .TotalFeeCancelled = .TotalFee
        .TotalFeeToGive = 0
        .TotalFeeToPay = 0
        .TotalFeePaid = 0
        
        
        .BillSettled = False
        .SaveData
    End With
    Dim rsTem As New ADODB.Recordset
    Dim temSql As String
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblProfessionalCharges where ForRBillID = " & temBillId
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                Dim pc As New ProfessionalCharge
                pc.ProfessionalChargesID = !ProfessionalChargesID
                pc.Cancelled = True
                pc.CancelledDate = Date
                pc.CancelledDateTime = Now
                pc.CancelledUserID = UserID
                pc.FeeCancelled = pc.Fee
                pc.FeePaid = 0
                pc.FeeToGive = 0
                pc.FeeToPay = 0
                pc.SaveData
                .MoveNext
            Wend
        End If
        .Close
    End With
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblPatientService where RBIllID = " & temBillId
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                Dim ps As New PatientService
                ps.PatientServiceID = !PatientServiceID
                ps.ChargeCancelled = ps.Charge
                ps.ChargePaid = 0
                ps.ChargeToGive = 0
                ps.ChargeToPay = 0
                
                ps.HospitalChargeCancelled = ps.HospitalCharge
                ps.HospitalChargePaid = 0
                ps.HospitalChargeToGive = 0
                ps.HospitalChargeToPay = 0
                
                ps.ProfessionalChargeCancelled = ps.ProfessionalCharge
                ps.ProfessionalChargePaid = 0
                ps.ProfessionalChargeToGive = 0
                ps.ProfessionalChargeToPay = 0
                ps.SaveData
                
                .MoveNext
            Wend
        End If
        .Close
    End With
    
    
    MsgBox "Cancelled"
    fillSessionGrid
    Exit Sub
eh:
    MsgBox "Error" & vbNewLine & Err.Description
    fillSessionGrid
End Sub

Private Sub btnChange_Click()

    On Error GoTo eh

    Dim temBillId As Long
    Dim temBill As New IncomeBill
    Dim temPt As New Patient
    
    
    temBillId = Val(gridList.TextMatrix(gridList.row, 0))
    temBill.IncomeBillID = temBillId
    If temBill.IncomeBillID = 0 Then Exit Sub
    temPt.PatientID = temBill.PatientID
    temPt.FirstName = InputBox("Enter patient Name", "Name", temPt.FirstName)
    temPt.Phone = InputBox("Enter phone", "Phone", temPt.Phone)
    temPt.SaveData
    MsgBox "Changed"
    
Exit Sub

eh:
    MsgBox "Error" & vbNewLine & Err.Description
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnDelete_Click()


    If Val(txtDelID.text) = 0 Then
        MsgBox "Please select one to delete"
        Exit Sub
    End If


    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        If IsNumeric(txtDelID.text) = True Then
            temSql = "Select * from tblPatientService where PatientServiceID = " & Val(txtDelID.text)
            .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
            If .RecordCount > 0 Then
                !Deleted = True
                !DeletedUserID = UserID
                !DeletedDate = Date
                !DeletedTime = Now
                .Update
            End If
            .Close
        End If
    End With
    
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblProfessionalCharges where PatientServiceID = " & Val(txtDelID.text)
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Cancelled = True
            !CancelledDate = Date
            !CancelledTime = Now
            !CancelledDateTime = Now
            !CancelledUserID = UserID
            .Update
            .MoveNext
        Wend
        .Close
    End With
    
    
    Call FillGrid
    Call ClearAddValues
    If gridService.Rows < 2 Then
        txtSerialNo.text = Empty
        cmbPaymentMethod.Locked = False
        lstCategory.Locked = False
        lstSession.Locked = False
        lstCategory.SetFocus
    Else
        If lstSc.Visible = True Then
            lstSc.SetFocus
        Else
            lstCategory.SetFocus
        End If
    End If
    
End Sub

Private Sub btnDoctorView_Click()
    Dim rsTem As New ADODB.Recordset
    If Val(lstSession.BoundText) = 0 Then
        MsgBox "Please select a session"
        lstSession.SetFocus
        Exit Sub
    End If
    If Val(lstSc.BoundText) = 0 Then
        MsgBox "Please select a sub category"
        lstSc.SetFocus
        Exit Sub
    End If
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT dbo.tblPatientService.SerialNo, dbo.tblPatientService.Absent, dbo.tblPatientMainDetails.FirstName AS PatientName, dbo.tblPatientService.ProfessionalChargePaid, dbo.tblIncomeBill.BillSettled as Settled, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.Returned, dbo.tblPatientService.ProfessionalCharge , dbo.tblPatientService.Charge, dbo.tblIncomeBill.BillSettled, dbo.tblIncomeBill.displayBillID, dbo.tblBHT.BHT  " & _
                            "FROM dbo.tblBHT RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblBHT.BHTID = dbo.tblIncomeBill.BHTID RIGHT OUTER JOIN dbo.tblPatientService ON dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN                         dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServicesubCategoryID = " & Val(lstSc.BoundText) & " AND SECESSIONID  = " & Val(lstSession.BoundText) & " ORDER BY  dbo.tblPatientService.SerialNo"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With DataReportDoctorView
        Set .DataSource = rsTem
           .Sections.Item("header").Controls.Item("lblName").Caption = "Consultant : " & lstSc.text
            .Sections.Item("header").Controls.Item("lblDate").Caption = "Date : " & Format(dtpAppDate.Value, "dd MMMM yyyy") & " - " & lstSession.text
            .Show
    End With
End Sub

Private Sub btnFee_Click()
 



    

    Dim temBillId As Long
    Dim temBill As New IncomeBill
    Dim temReBill As New IncomeReturnBill
    
    
    temBillId = Val(gridList.TextMatrix(gridList.row, 0))
    temBill.IncomeBillID = temBillId
    

    
    If temBill.BillSettled = True Then
        MsgBox "Settled bills can not be changed"
        Exit Sub
    End If
    
    frmRBillChange.txtBillId.text = temBillId
    frmRBillChange.Show 1

End Sub

Private Sub btnNurseView_Click()
    Dim rsTem As New ADODB.Recordset
    If Val(lstSession.BoundText) = 0 Then
        MsgBox "Please select a session"
        lstSession.SetFocus
        Exit Sub
    End If
    If Val(lstSc.BoundText) = 0 Then
        MsgBox "Please select a sub category"
        lstSc.SetFocus
        Exit Sub
    End If
    
    'If lstSession.Locked = True Then Exit Sub
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT  dbo.tblPatientService.Absent, dbo.tblPatientService.SerialNo, dbo.tblPatientMainDetails.FirstName AS PatientName, dbo.tblIncomeBill.displayBillID, dbo.tblIncomeBill.Cancelled,  dbo.tblIncomeBill.BillSettled as Settled, dbo.tblIncomeBill.Returned,  dbo.tblBHT.BHT " & _
                            "FROM          dbo.tblPatientService LEFT OUTER JOIN dbo.tblBHT ON dbo.tblPatientService.BHTID = dbo.tblBHT.BHTID LEFT OUTER JOIN dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServiceSubCategoryID = " & Val(lstSc.BoundText) & " AND SECESSIONID  = " & Val(lstSession.BoundText) & " ORDER BY  dbo.tblPatientService.SerialNo"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With DataReportNurseView
        Set .DataSource = rsTem
            .Sections.Item("header").Controls.Item("lblName").Caption = "Consultant : " & lstSc.text
            .Sections.Item("header").Controls.Item("lblDate").Caption = "Date : " & Format(dtpAppDate.Value, "dd MMMM yyyy") & " - " & lstSession.text
            .Show
    End With

End Sub


Private Sub btnRefund_Click()




    On Error GoTo eh

    Dim temBillId As Long
    Dim temBill As New IncomeBill
    Dim temReBill As New IncomeReturnBill
    
    
    temBillId = Val(gridList.TextMatrix(gridList.row, 0))
    temBill.IncomeBillID = temBillId
    

    
    If IsNumeric(cmbPaymentMethod.BoundText) = False Then
        MsgBox "Please select a Re-Payment Method"
        cmbPaymentMethod.SetFocus
        Exit Sub
    End If
    If ProfessionalFeePaidR(temBillId) = True Then
        MsgBox "You can't cancel this bill as Professional Payments are already done"
        Exit Sub
    End If
    
    
    frmRBillRefund.txtBillId.text = temBillId
    frmRBillRefund.Show 1
    
    
   
    fillSessionGrid
    Exit Sub
eh:
    MsgBox "Error" & vbNewLine & Err.Description
    fillSessionGrid

End Sub

Private Sub btnReprint_Click()
    Dim temBillId As Long
    temBillId = Val(gridList.TextMatrix(gridList.row, 0))
    printAnyBill temBillId, cmbPrinter.text, cmbPaper.text, Me
End Sub

Private Sub btnSet_Click()
    todayStaffId = Val(cmbStaff1(0).BoundText)
End Sub

Private Sub btnSettle_Click()
    Dim TemPay As New clsPaymentMethod
    TemPay.PaymentMethodID = Val(cmbPaymentMethod.BoundText)
    Dim rsTem As New ADODB.Recordset
    Dim DisplayBillID As Long
    DisplayBillID = Val(gridList.TextMatrix(gridList.row, 0))
    
'    With rsTem
'        If .State = 1 Then .Close
'        temSQL = "Select * from tblIncomeBill where IncomeBillID = " & DisplayBillID
'        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'        If .RecordCount > 0 Then
'            If TemPay.FinalPay = True Then
'                !CompletedDate = Date
'                !CompletedTime = Now
'                !CompletedUserID = UserID
'                !BillSettled = True
'                !GrossTotal = !GrossTotalToPay
'                !NetTotal = !NetTotalToPay
'                !GrossTotalToPay = 0
'                !NetTotalToPay = 0
'                !PaymentMethodID = Val(cmbPaymentMethod.BoundText)
'                !PaymentComments = txtPaymentMethod.text
'            Else
'                MsgBox "Please select appropriate payment method"
'                cmbPaymentMethod.SetFocus
'                Exit Sub
'            End If
'            .Update
'        Else
'            MsgBox "Error. Bill NOT added. Please reenter"
'            Exit Sub
'        End If
'
'
'        .Close
'    End With
    
    Dim settlingBill As New IncomeBill
    With settlingBill
        settlingBill.IncomeBillID = DisplayBillID
            If TemPay.FinalPay = True Then
                .CompletedDate = Date
                .CompletedTime = Now
                .CompletedUserID = UserID
                .BillSettled = True
                .GrossTotal = .GrossTotalToPay
                .NetTotal = .NetTotalToPay
                .GrossTotalToPay = 0
                .NetTotalToPay = 0
                .PaymentMethodID = Val(cmbPaymentMethod.BoundText)
                .PaymentComments = txtPaymentMethod.text
                
                .HospitalFeeToGive = .HospitalFee
                .HospitalFeeToPay = 0
                
                .ProfessionalFeeToGive = .ProfessionalFee
                .ProfessionalFeeToPay = 0
                
                .TotalFeeToGive = .TotalFee
                .TotalFeeToPay = 0
                
                
            Else
                MsgBox "Please select appropriate payment method"
                cmbPaymentMethod.SetFocus
                Exit Sub
            End If
            .SaveData
    End With
    
    Dim rsTem1 As New ADODB.Recordset
    Dim rsTem2 As New ADODB.Recordset
    
    
    temSql = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & DisplayBillID
    rsTem1.Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    While rsTem1.EOF = False
        Dim temPtSv As New PatientService
        temPtSv.PatientServiceID = rsTem1!PatientServiceID
                
        temPtSv.ChargeToGive = temPtSv.Charge
        temPtSv.ChargeToPay = 0
        temPtSv.ChargePaid = temPtSv.Charge
        
        temPtSv.HospitalChargeToGive = temPtSv.HospitalCharge
        temPtSv.HospitalChargePaid = temPtSv.Charge
        temPtSv.HospitalChargeToPay = 0
        
        temPtSv.ProfessionalChargePaid = temPtSv.ProfessionalCharge
        temPtSv.ProfessionalChargeToPay = 0
        temPtSv.ProfessionalChargeToGive = temPtSv.ProfessionalCharge
        
        temPtSv.SaveData
        
        temSql = "SELECT * From tblProfessionalCharges WHERE  PatientServiceID = " & rsTem1!PatientServiceID
                If rsTem2.State = 1 Then rsTem2.Close
                rsTem2.Open temSql, cnnStores, adOpenStatic, adLockReadOnly
                While rsTem2.EOF = False
                    Dim temProChg As New ProfessionalCharge
                    temProChg.ProfessionalChargesID = rsTem2!ProfessionalChargesID
                    
                    temProChg.FeeToPay = 0
                    temProChg.FeePaid = temProChg.Fee
                    temProChg.FeeToGive = temProChg.Fee
                    
                    temProChg.SaveData
                    
                    
                    rsTem2.MoveNext
                Wend
                rsTem2.Close
                rsTem1.MoveNext
            Wend
            rsTem1.Close


    
    
    
    
    
    
    If chkSettlePrint.Value = 1 Then printAnyBill DisplayBillID, cmbPrinter.text, cmbPaper.text, Me
    
    Call ClearAddValues
    Call ClearBillValues
    Call FillGrid
    txtPatient.SetFocus

End Sub

Private Sub btnUpdate_Click()
    Dim TemPay As New clsPaymentMethod
        
        
    If gridService.Rows < 2 Then
        MsgBox "Nothing to add"
        lstCategory.SetFocus
        Exit Sub
    End If

    If SSTab1.Tab = 0 Then
        If Trim(txtPatient.text) = "" Then
            MsgBox "Please enter a name for the patient"
            txtPatient.SetFocus
            Exit Sub
        End If
    Else
        If Trim(txtBhtPatientName.text) = "" Then
            MsgBox "Please enter a name for the patient"
            txtBhtPatientName.SetFocus
            Exit Sub
        End If
        If Trim(cmbBHT.text) = "" Then
            MsgBox "Please enter a BHT"
            cmbBHT.SetFocus
            Exit Sub
        End If
    
    End If
    
        
    TemPay.PaymentMethodID = Val(cmbPaymentMethod.BoundText)
    
    
    
'    If cmbPaymentMethod.BoundText = 4 Then
'        If Val(cmbHSS.BoundText) = 0 Then
'            MsgBox "Please select a Health Scheme Supplier"
'            cmbHSS.Visible = True
'            cmbHSS.SetFocus
'            Exit Sub
'        End If
'    End If
        
        
    lstCategory.Locked = False
    cmbPaymentMethod.Locked = False
    lstSession.Locked = False
    
    Dim rsTem As New ADODB.Recordset
    Dim DisplayBillID As Long
'
'    With rsTem
'        If .State = 1 Then .Close
'        temSQL = "Select Count(IncomeBillID) as BillCount from tblIncomeBill where Completed = 1 AND IsRBill = 1"
'        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
'        If .RecordCount > 0 Then
'            If IsNull(!BillCount) = False Then
'                DisplayBillID = !BillCount + 1
'            Else
'                DisplayBillID = 1
'            End If
'        Else
'            DisplayBillID = 1
'        End If
'    End With
'
    DisplayBillID = NewRDisplayBillID(Val(txtOPDBillID.text))
    
    txtDisplayBillID.text = DisplayBillID
    
'    With rsTem
'        If .State = 1 Then .Close
'        temSQL = "Select * from tblIncomeBill where IncomeBillID = " & Val(txtOPDBillID.text)
'        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'        If .RecordCount > 0 Then
'            !DisplayBillID = DisplayBillID
'            !Completed = True
'
'            If TemPay.FinalPay = True Then
'                !CompletedDate = Date
'                !CompletedTime = Now
'                !CompletedUserID = UserID
'                !BillSettled = True
'                !GrossTotal = Val(lblTotal.Caption)
'                !NetTotal = Val(lblTotal.Caption)
'
'                !GrossTotalToPay = 0
'                !NetTotalToPay = 0
'                !PaymentMethodID = Val(cmbPaymentMethod.BoundText)
'                !PaymentComments = txtPaymentMethod.text
'                !CreditPaymentMethodID = 0
'                !CreditPaymentComments = ""
'            Else
'                !BillSettled = False
'                !CreditDate = Date
'                !CreditTime = Time
'                !CreditUserID = UserID
'                !GrossTotalToPay = Val(lblTotal.Caption)
'                !NetTotalToPay = Val(lblTotal.Caption)
'                !GrossTotal = 0
'                !NetTotal = 0
'                !PaymentMethodID = Val(cmbPaymentMethod.BoundText)
'                !PaymentComments = txtPaymentMethod.text
'                !CreditPaymentMethodID = Val(cmbPaymentMethod.BoundText)
'                !CreditPaymentComments = txtPaymentMethod.text
'            End If
'
'            !HospitalFee = Val(txtTotalHosFee.text)
'            !ProfessionalFee = Val(txtTotalProFee.text)
'
'
'            !CategoryID = Val(txtCatId.text)
'            !SubCategoryId = Val(txtSubCatId.text)
'            !SessionId = Val(txtSessionId.text)
'            !ProfessionalId = Val(txtProId.text)
'
'            !PatientID = currentPatient.PatientID
'            !IsRBill = True
'            !StoreID = UserStoreID
'            !HSSID = Val(cmbHSS.BoundText)
'            !BHTID = currentBht.BHTID
'            !AgentID = Val(cmbAgent.BoundText)
'            .Update
'        Else
'            MsgBox "Error. Bill NOT added. Please reenter"
'            Exit Sub
'        End If
            
    Dim savingBill As New IncomeBill
    
            
    With savingBill
        
        .IncomeBillID = Val(txtOPDBillID.text)
            
            .DisplayBillID = DisplayBillID
            .Completed = True
            
            
            If TemPay.FinalPay = True Then
            
                .HospitalFee = Val(txtTotalHosFee.text)
                .HospitalFeeCancelled = 0
                .HospitalFeeReturned = 0
                .HospitalFeeToGive = Val(txtTotalHosFee.text)
                .HospitalFeeToPay = 0
                .HospitalFeePaid = .HospitalFee
                
                .ProfessionalFee = Val(txtTotalProFee.text)
                .ProfessionalFeeCancelled = 0
                .ProfessionalFeeReturned = 0
                .ProfessionalFeeToGive = Val(txtTotalProFee.text)
                .ProfessionalFeeToPay = 0
                .ProfessionalFeePaid = .ProfessionalFee
                
                .TotalFee = Val(lblTotal.Caption)
                .TotalFeeCancelled = 0
                .TotalFeeToGive = Val(lblTotal.Caption)
                .TotalFeeToPay = 0
                .TotalFeeToGive = 0
                .TotalFeePaid = .TotalFee
            
            
                .CompletedDate = Date
                .CompletedTime = Now
                .CompletedUserID = UserID
                
                .GrossTotal = Val(lblTotal.Caption)
                .NetTotal = Val(lblTotal.Caption)

                .GrossTotalToPay = 0
                .NetTotalToPay = 0
                
                .PaymentMethodID = Val(cmbPaymentMethod.BoundText)
                .PaymentComments = txtPaymentMethod.text
                
                .CreditPaymentMethodID = 0
                .CreditPaymentComments = ""
            Else
                .CreditDate = Date
                .CreditTime = Time
                .CreditUserID = UserID
                .GrossTotalToPay = Val(lblTotal.Caption)
                .NetTotalToPay = Val(lblTotal.Caption)
                .GrossTotal = 0
                .NetTotal = 0
                .PaymentMethodID = Val(cmbPaymentMethod.BoundText)
                .PaymentComments = txtPaymentMethod.text
                .CreditPaymentMethodID = Val(cmbPaymentMethod.BoundText)
                .CreditPaymentComments = txtPaymentMethod.text
                
                
                .HospitalFee = Val(txtTotalHosFee.text)
                .HospitalFeeCancelled = 0
                .HospitalFeeReturned = 0
                .HospitalFeeToGive = 0
                .HospitalFeeToPay = Val(txtTotalHosFee.text)
                .HospitalFeePaid = 0
                
                .ProfessionalFee = Val(txtTotalProFee.text)
                .ProfessionalFeeCancelled = 0
                .ProfessionalFeeReturned = 0
                .ProfessionalFeeToGive = 0
                .ProfessionalFeeToPay = Val(txtTotalProFee.text)
                .ProfessionalFeePaid = 0
                
                .TotalFee = Val(lblTotal.Caption)
                .TotalFeeCancelled = 0
                .TotalFeeToReturned = 0
                .TotalFeeToGive = 0
                .TotalFeeToPay = Val(lblTotal.Caption)
                .TotalFeePaid = 0
            
                
                
            End If
            
            If TemPay.SettlingPay = True Then
                .BillSettled = True
                .CompletedUserID = UserID
            Else
                .BillSettled = False
            End If
            
            
            .CategoryID = Val(txtCatId.text)
            .SubCategoryId = Val(txtSubCatId.text)
            .SessionId = Val(txtSessionId.text)
            .ProfessionalId = Val(txtProId.text)
            
            .PatientID = currentPatient.PatientID
            .IsRBill = True
            .StoreID = UserStoreID
            .HSSID = Val(cmbHSS.BoundText)
            .BHTID = currentBht.BHTID
            .AgentID = Val(cmbAgent.BoundText)
            .SaveData
        

    End With
    
    If cmbPaymentMethod.BoundText = 4 Then
        UpdateCompanyBalance Val(cmbHSS.BoundText), Val(lblTotal.Caption), False, True, True, Val(cmbPaymentMethod.BoundText), txtPaymentMethod.text
    End If
    
    If chkPrint.Value = 1 Then printAnyBill Val(txtOPDBillID.text), cmbPrinter.text, cmbPaper.text, Me
    
    Call fillSessionGrid
    MsgBox "Bill Settled Successfully"
    
    
    Call ClearAddValues
    Call ClearBillValues
    Call FillGrid
    txtPatient.SetFocus
End Sub

Private Sub printBill()
    Dim temBillPoints As MyBillPoints
    
    CsetPrinter.SetPrinterAsDefault (cmbPrinter.text): DoEvents
    Dim MyFOnt As ReportFont

    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle): DoEvents
    End If
    CsetPrinter.SetPrinterAsDefault (cmbPrinter.text): DoEvents
    
    Dim CenterX As Long
    Dim FieldX As Long
    Dim NoX As Long
    Dim ValueX As Long
    Dim AllLines() As String
    Dim i As Integer
    Dim temY As Long
    Dim n As Long
    
    With MyFOnt
        .Name = DefaultFont.Name
        .Bold = False
        .Italic = False
        .Size = 9
        .Italic = False
        .Underline = False
    End With
    
    If SelectForm(cmbPaper.text, Me.hwnd) = 1 Then
        temBillPoints = PrintThisBill(txtDisplayBillID.text, cmbPaymentMethod.text, txtPatient.text, Format(Date, "dd MM yyyy"), Format(Time, "hh:mm AMPM"), "Roentgents Bills", RadiologyName)
        
        
        CenterX = temBillPoints.CenterX
'        NoX = temBillPoints.VX
        FieldX = temBillPoints.DX
        ValueX = temBillPoints.VX
        
        temY = temBillPoints.DY
        
        Printer.CurrentY = temY
               
        Printer.Print
        
        If Trim(txtSerialNo.text) <> "" Then
            temY = Printer.CurrentY
            PrintingText FieldX, temY, ValueX, 0, "Secession : " & lstSession.text, leftAlign, MyFOnt
            temY = Printer.CurrentY
            PrintingText FieldX, temY, ValueX, 0, "Serial : " & txtSerialNo.text, leftAlign, MyFOnt
        End If
        
            temY = Printer.CurrentY
            PrintingText FieldX, temY, ValueX, 0, "Service : " & lstCategory.text, leftAlign, MyFOnt

'        For i = 1 To gridService.Rows - 1
'            temY = Printer.CurrentY
'            n = i
'            temY = Printer.CurrentY
'            PrintingText FieldX, temY, ValueX, 0, "Service : " & gridService.TextMatrix(i, 2), LeftAlign, MyFOnt
'            temY = Printer.CurrentY
'            PrintingText FieldX, temY, ValueX, 0, "Charge : " & gridService.TextMatrix(i, 4), RightAlign, MyFOnt
'
'        Next
        
        For i = 1 To gridService.Rows - 1
            temY = Printer.CurrentY
            n = i
            'PrintingText FieldX, temY, NoX, 0, CStr(n), rightAlign, MyFOnt
            PrintingText FieldX, temY, ValueX, 0, gridService.TextMatrix(i, 2), leftAlign, MyFOnt
            PrintingText FieldX, temY, ValueX, 0, gridService.TextMatrix(i, 4), rightAlign, MyFOnt
        Next
        
        
        Printer.Print
        temY = Printer.CurrentY
        PrintingText FieldX, temY, ValueX, 0, "---------", rightAlign, MyFOnt
        temY = Printer.CurrentY
        MyFOnt.Bold = False
        MyFOnt.Bold = False
        MyFOnt.Size = 11
        PrintingText FieldX, temY, ValueX, 0, "Total", leftAlign, MyFOnt
        PrintingText FieldX, temY, ValueX, 0, lblTotal.Caption, rightAlign, MyFOnt
        MyFOnt.Bold = False
        MyFOnt.Bold = False
        MyFOnt.Size = 9
        
        temY = Printer.CurrentY
        PrintingText FieldX, temY, ValueX, 0, "---------", rightAlign, MyFOnt
        Printer.Print
        Printer.Print
        
        temY = temBillPoints.CY
        PrintingText FieldX, temY - 720, ValueX, 0, "Cashier :  " & UserFullName, rightAlign, MyFOnt
        
        Printer.EndDoc
        
    End If
End Sub

Private Sub PrintingText(X1 As Long, Y1 As Long, X2 As Long, Y2 As Long, PrintText As String, PrintAlignment As TextAlignment, PrintFont As ReportFont)
    
    If PrintAlignment = leftAlign Then
        Printer.CurrentX = X1
    ElseIf PrintAlignment = rightAlign Then
        Printer.CurrentX = X2 - Printer.TextWidth(PrintText)
    ElseIf PrintAlignment = CentreAlign Then
        Printer.CurrentX = (X1 + X2 / 2) - (Printer.TextWidth(PrintText) / 2)
    Else
        Printer.CurrentX = X1
    End If
    If Y1 <> 0 Then Printer.CurrentY = Y1
    Printer.Font.Name = PrintFont.Name
    Printer.Font.Size = PrintFont.Size
    Printer.Font.Italic = PrintFont.Italic
    Printer.Font.Bold = PrintFont.Bold
    Printer.Font.Underline = PrintFont.Underline
    
    Printer.Print PrintText
End Sub

Private Sub btnXray_Click()
    Dim rsTem As New ADODB.Recordset
    If Val(lstSession.BoundText) = 0 Then
        MsgBox "Please select a session"
        lstSession.SetFocus
        Exit Sub
    End If
'    If Val(lstSc.BoundText) = 0 Then
'        MsgBox "Please select a sub category"
'        lstSc.SetFocus
'        Exit Sub
'    End If
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillId, dbo.tblPatientMainDetails.FirstName AS PatientName, min( dbo.tblPatientService.SerialNo) as SerialNo , dbo.tblPatientMainDetails.Phone AS Phone, dbo.tblIncomeBill.HospitalFeePaid ,  dbo.tblIncomeBill.ProfessionalFeePaid , (dbo.tblIncomeBill.HospitalFeePaid + dbo.tblIncomeBill.ProfessionalFeePaid) NetTotalPaid, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled as Settled, dbo.tblIncomeBill.Returned, dbo.tblBHT.BHT  " & _
                            "FROM dbo.tblBHT RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblBHT.BHTID = dbo.tblIncomeBill.BHTID RIGHT OUTER JOIN dbo.tblPatientService ON dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND SECESSIONID  = " & Val(lstSession.BoundText) & _
                            "GROUP BY dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.NetTotal, dbo.tblIncomeBill.DisplayBillID, dbo.tblPatientMainDetails.FirstName, dbo.tblPatientMainDetails.Phone, dbo.tblIncomeBill.HospitalFeePaid, dbo.tblIncomeBill.ProfessionalFeePaid, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled , dbo.tblIncomeBill.Returned, dbo.tblBHT.BHT, ((dbo.tblIncomeBill.HospitalFeePaid + dbo.tblIncomeBill.ProfessionalFeePaid)) " & _
                            "ORDER BY  min(dbo.tblPatientService.SerialNo)"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtrXRayView
        Set .DataSource = rsTem
           .Sections.Item("header").Controls.Item("lblName").Caption = "Consultant : " & lstSc.text
            .Sections.Item("header").Controls.Item("lblDate").Caption = "Date : " & Format(dtpAppDate.Value, "dd MMMM yyyy") & " - " & lstSession.text
            .Show
    End With

End Sub

Private Sub chkForeigner_Click()
    If Val(txtOPDBillID.text) = 0 Then Exit Sub

    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT * " & _
                    "FROM tblPatientService " & _
                    "WHERE Deleted = 0 AND RBillID = " & Val(txtOPDBillID.text)
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 10 Then Exit Sub
        While .EOF = False
            If chkForeigner.Value = 1 Then
                !Charge = !Charge * 2
                !ProfessionalCharge = !ProfessionalCharge * 2
                !HospitalCharge = !HospitalCharge * 2
            Else
                !Charge = !Charge / 2
                !ProfessionalCharge = !ProfessionalCharge / 2
                !HospitalCharge = !HospitalCharge / 2
            End If
            .Update
            .MoveNext
        Wend
        If .State = 1 Then .Close
        temSql = "Select * from tblProfessionalCharges where ForRBillID = " & Val(txtOPDBillID.text)
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 10 Then Exit Sub
        While .EOF = False
            If chkForeigner.Value = 1 Then
                !Fee = !Fee * 2
            Else
                !Fee = !Fee / 2
            End If
            .Update
            .MoveNext
        Wend
        .Close
    End With
    Call ClearAddValues
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub cmbBHT_Change()
'    Dim BHT As New clsBHT
'    Dim pt As New clsPatient
'    BHT.BHTID = Val(cmbBHT.BoundText)
'    pt.ID = BHT.PatientID
'    txtPatient.text = pt.FirstName
'    txtPtID.text = BHT.PatientID
End Sub

Private Sub cmbBHT_GotFocus()
    SendKeys "{home}+{end}"
End Sub

Private Sub cmbBHT_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        lstCategory.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        cmbBHT.text = Empty
    End If
End Sub

Private Sub Command1_Click()
    btnCancel_Click
End Sub

Private Sub dtpAppDate_Click()
    fillSessionGrid
    lblDate.Caption = Format(dtpAppDate.Value, LongDateFormat)

End Sub

Private Sub dtpAppDate_DateClick(ByVal DateClicked As Date)
    fillSessionGrid
    lblDate.Caption = Format(dtpAppDate.Value, LongDateFormat)
End Sub

Private Sub gridList_DblClick()
    SSTab2.Tab = 1
    
    Dim temBill As New IncomeBill
    Dim TemStaff As New Staff
    Dim temTitle As New Title
    Dim temPM As New clsPaymentMethod
    Dim temPt As New Patient
    Dim temId As Long
    Dim temStr As String
    temId = Val(gridList.TextMatrix(gridList.row, 0))
    
    If temId = 0 Then
        rtbDetails.text = ""
        Exit Sub
    End If
    temStr = ""
    temBill.IncomeBillID = temId
    temPt.PatientID = temBill.PatientID
    temStr = temStr & "Patient : " & temPt.FirstName
    temStr = temStr & vbNewLine & "Bill No : " & temBill.DisplayBillID
    temStr = temStr & vbNewLine & "Phone : " & temPt.Phone
    
    If temBill.CreditUserID <> 0 Then
        TemStaff.StaffID = temBill.CreditUserID
        temStr = temStr & vbNewLine & "Booked By : " & TemStaff.Name
        temStr = temStr & vbNewLine & "Booked Date : " & Format(temBill.CreditDate, "dd MMM yyyy")
        temStr = temStr & vbNewLine & "Booked Time : " & Format(temBill.CreditTime, "hh:mm AMPM")
        temPM.PaymentMethodID = temBill.CreditPaymentMethodID
        temStr = temStr & vbNewLine & "Booked Method : " & temPM.PaymentMethod
        
        If temBill.BillSettled = True Then
            TemStaff.StaffID = temBill.CompletedUserID
            temStr = temStr & vbNewLine & "Bill Settled By: " & TemStaff.Name
            temStr = temStr & vbNewLine & "Bill Settled Date : " & Format(temBill.CompletedDate, "dd MMM yyyy")
            temStr = temStr & vbNewLine & "Bill Settled Time : " & Format(temBill.CompletedTime, "hh:mm AMPM")
            temPM.PaymentMethodID = temBill.PaymentMethodID
            temStr = temStr & vbNewLine & "Bill Settled Method : " & temPM.PaymentMethod
        Else
            temStr = temStr & vbNewLine & "Yet to settle the bill"
        End If
    Else
        TemStaff.StaffID = temBill.CompletedUserID
        temStr = temStr & vbNewLine & "Booked By : " & TemStaff.Name
        temStr = temStr & vbNewLine & "Booked Date : " & Format(temBill.CompletedDate, "dd MMM yyyy")
        temStr = temStr & vbNewLine & "Booked Time : " & Format(temBill.CompletedTime, "hh:mm AMPM")
    End If
    If temBill.Cancelled = True Then
        TemStaff.StaffID = temBill.CancelledUserID
        temStr = "Cancelled By : " & TemStaff.Name
        temStr = temStr & vbNewLine & "Cancelled Date : " & Format(temBill.CancelledDate, "dd MMM yyyy")
        temStr = temStr & vbNewLine & "Cancelled Time : " & Format(temBill.CancelledTime, "hh:mm AMPM")
    End If
    If temBill.Returned = True Then
        TemStaff.StaffID = temBill.CancelledUserID
        temStr = "Refunded By : " & TemStaff.Name
        temStr = temStr & vbNewLine & "Refund Date : " & Format(temBill.ReturnedDate, "dd MMM yyyy")
        temStr = temStr & vbNewLine & "Refund Time : " & Format(temBill.ReturnedTime, "hh:mm AMPM")
        temStr = temStr & vbNewLine & "Refund Time : " & Format(temBill.ReturnedValue, "#,##0.00")
    End If
    
    rtbDetails.text = temStr
    
End Sub

Private Sub lstCategory_Click()
    If lstCategory.Locked = True Then
        MsgBox "You can't change the category once Services are added. Only one service category can be added for one Roentgents service bill. If you want to add a new service category, finish this bill and start a new bill or delete the already entered services."
        gridService.SetFocus
    End If
    Set currentCat = New ServiceCategory
    currentCat.ServiceCategoryID = Val(lstCategory.BoundText)
    lstCategory_Change
End Sub

Private Sub lstCategory_GotFocus()
    lstCategory.BackColor = focusedBackColour
    If IsNumeric(lstCategory.BoundText) = False Then
        SendKeys "{home}"
    End If
End Sub

Private Sub lstCategory_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty

        lstSession.SetFocus
        
'        If lstSc.Visible = True Then
'            lstSc.SetFocus
'        Else
'            If txtComments.Visible = True Then
'                txtComments.SetFocus
'            Else
'                txtHospitalCharge.SetFocus
'            End If
'        End If
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        lstCategory.text = Empty
    End If
End Sub

Private Sub cmbPatient_Click(Area As Integer)
'    If IsNumeric(cmbPatient.BoundText) = False Then
'        txtPtID.Text = Empty
'        Exit Sub
'    Else
'        txtPtID.Text = Val(cmbPatient.BoundText)
'    End If
'    Call FillGrid
    
End Sub

Private Sub lstCategory_Change()
    txtCatId.text = Val(lstCategory.BoundText)
    If IsNumeric(lstCategory.BoundText) = False Then Exit Sub
    
    Dim rsTem As New ADODB.Recordset
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblServiceCategory where ServiceCategoryID = " & Val(lstCategory.BoundText)
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If chkForeigner.Value = 1 Then
                txtHospitalCharge.text = Format(!Fee * 2, "0.00")
            Else
                txtHospitalCharge.text = Format(!Fee, "0.00")
            End If
            If !CanChange = True Then
                txtHospitalCharge.Locked = False
            Else
                txtHospitalCharge.Locked = True
            End If
        End If
        .Close
    End With
    
    With rsSC
        If .State = 1 Then .Close
        temSql = "Select * from tblServiceSubCategory where   Deleted = 0 AND ForR = 1 AND ServiceCategoryID = " & Val(lstCategory.BoundText) & " ORDER BY ServiceSubcategory"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            lstSc.Visible = True
            lblSC.Visible = True
        Else
            lstSc.Visible = False
            lblSC.Visible = False
        End If
    End With
    With lstSc
        Set .RowSource = rsSC
        .ListField = "ServiceSubcategory"
        .BoundColumn = "ServiceSubcategoryID"
        .text = Empty
    End With
    
    With rsSecession
        If .State = 1 Then .Close
        temSql = "Select * from tblServiceSecession where  ServiceCategoryID = " & Val(lstCategory.BoundText) & " AND ServiceSubcategoryID = 0 Order by ServiceSecession"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With lstSession
        Set .RowSource = rsSecession
        .ListField = "ServiceSecession"
        .BoundColumn = "ServiceSecessionID"
        .text = Empty
    End With
    
    txtFee1_Change (0)
    txtHospitalCharge_Change
    txtProfessionalCharge_Change
    
    sessionChanged
    
End Sub

Private Sub ClearServiceValues()
    Dim n As Integer
    txtComments.text = Empty
    txtProfessionalCharge.text = Empty
    txtHospitalCharge.text = Empty
    txtCharge.text = Empty
    txtEditID.text = Empty
    txtDelID.text = Empty
    For n = 0 To lblSpeciality1.UBound
        lblSpeciality1(n).Visible = False
        lblSpeciality1(n).Caption = Empty
        cmbStaff1(n).Visible = False
        cmbStaff1(n).text = Empty
        txtServiceProfessionalChargesID(n).text = Empty
        txtFee1(n).Visible = False
        txtFee1(n).text = Empty
        txtSpecialityID(n).text = Empty
    Next

End Sub

Private Sub cmbPatient_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        lstCategory.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtPatient.text = Empty
    End If
End Sub

'Private Sub cmbPatient_LostFocus()
'    cmbPatient.text = UCase(cmbPatient.text)
'    Dim rsTem As New ADODB.Recordset
'    If IsNumeric(cmbPatient.BoundText) = True Then
'        txtPtID.text = cmbPatient.BoundText
'    Else
'        With rsTem
'            If .State = 1 Then .Close
'            temSQL = "Select * from tblPatientMainDetails where PatientID = 0 "
'            .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'            .AddNew
'            !FirstName = cmbPatient.text
'
'            .Update
'            temSQL = "SELECT @@IDENTITY AS NewID"
'            .Close
'            .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
'            txtPtID.text = !NewID
'            .Close
'        End With
'    End If
'End Sub

Private Sub cmbPaymentMethod_Change()
    If cmbPaymentMethod.BoundText = 1 Then
        txtComments.Visible = False
        txtComments.text = Empty
    Else
        txtComments.Visible = True
    End If
    If cmbPaymentMethod.BoundText = 4 Then
        cmbHSS.Visible = True
        lblHSS.Visible = True
    Else
        cmbHSS.Visible = False
        lblHSS.Visible = False
    End If
    cmbAgent.Visible = False
    If cmbPaymentMethod.BoundText = 9 Then
        cmbAgent.Visible = True
        Agents.FillSQLCombo cmbAgent, "Agent", "Agent", "SELECT Agent, AgentID From dbo.tblAgent Where (Deleted = 0) And (CashAgent = 0) And (CreditAgent = 1) ORDER BY Agent"
    ElseIf cmbPaymentMethod.BoundText = 2 Then
        cmbAgent.Visible = True
        Agents.FillSQLCombo cmbAgent, "Agent", "Agent", "SELECT Agent, AgentID From dbo.tblAgent Where (Deleted = 0) And (CashAgent = 1) And (CreditAgent = 0) ORDER BY Agent"
    Else
        cmbAgent.Visible = False
    End If
        
End Sub

Private Sub cmbPaymentMethod_Click(Area As Integer)
    If cmbPaymentMethod.Locked = True Then
        MsgBox "You can't change the Payment Method once Services are added. If you want to add a new service category, finish this bill and start a new bill or delete the already entered services."
        gridService.SetFocus
    End If

End Sub

Private Sub scChanged()
    txtSubCatId.text = Val(lstSc.BoundText)
    Call ClearServiceValues
    
    gridList.Clear
    
    If IsNumeric(lstCategory.BoundText) = False Then Exit Sub
    If IsNumeric(lstSc.BoundText) = False Then Exit Sub
    If lstSc.Visible = False Then Exit Sub
    
    Dim rsTem As New ADODB.Recordset
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblServiceSubCategory where ServiceSubCategoryID = " & Val(lstSc.BoundText)
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If chkForeigner.Value = 1 Then
                txtHospitalCharge.text = Format(!Fee * 2, "0.00")
            Else
                txtHospitalCharge.text = Format(!Fee, "0.00")
            End If
            If !CanChange = True Then
                txtHospitalCharge.Locked = False
            Else
                txtHospitalCharge.Locked = True
            End If
        End If
        .Close
    End With
    
    Dim n As Integer
    
    With rsSPC
        If .State = 1 Then .Close
        temSql = "SELECT Top 7 tblSpeciality.Speciality, tblSpeciality.SpecialityID, tblServiceProfessionalCharges.Fee,  tblServiceProfessionalCharges.StaffID, tblServiceProfessionalCharges.ServiceProfessionalChargesID " & _
                    "FROM tblSpeciality RIGHT JOIN tblServiceProfessionalCharges ON tblSpeciality.SpecialityID = tblServiceProfessionalCharges.SpecialityID " & _
                    "Where (((tblServiceProfessionalCharges.ServiceSubcategoryID) = " & Val(lstSc.BoundText) & ") AND ((tblServiceProfessionalCharges.Deleted)=0 ))" & _
                    "ORDER BY tblServiceProfessionalCharges.ServiceProfessionalChargesID DESC"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        PSCCount = .RecordCount
        ReDim rsStaff(.RecordCount)
        Select Case PSCCount
            Case 1:
                gridService.Height = 2675
                gridService.Top = 3960
            Case 2:
                gridService.Height = 2415
                gridService.Top = 4320
            Case 3:
                gridService.Height = 2255
                gridService.Top = 4680
            Case 4:
                gridService.Height = 2095
                gridService.Top = 5040
            Case Else:
                gridService.Height = 1835
                gridService.ToolTipText = 5400
        End Select
            
        For n = 0 To PSCCount - 1
            lblSpeciality1(n).Visible = True
            lblSpeciality1(n).Caption = !Speciality
            
            txtServiceProfessionalChargesID(n).text = !ServiceProfessionalChargesID
            txtSpecialityID(n).text = !SpecialityID
            
            cmbStaff1(n).Visible = True
            If rsStaff(n).State = 1 Then rsStaff(n).Close
            temSql = "SELECT tblStaff.Name as TitleStaff, tblStaff.StaffID FROM tblStaff LEFT JOIN tblTitle ON tblStaff.TitleID = tblTitle.TitleID Where SpecialityID = " & !SpecialityID & " ORDER BY tblTitle.Title, tblStaff.Name"
            rsStaff(n).Open temSql, cnnStores, adOpenStatic, adLockReadOnly
            Set cmbStaff1(n).RowSource = rsStaff(n)
            cmbStaff1(n).ListField = "TitleStaff"
            cmbStaff1(n).BoundColumn = "StaffID"
            If !StaffID <> 0 Then
                cmbStaff1(n).BoundText = !StaffID
            Else
                cmbStaff1(n).BoundText = todayStaffId
            End If
            
'            On Error Resume Next
            If cmbStaff1(n).text = "" Then
                cmbStaff1(n).text = GetSetting(App.EXEName, Me.Name, "cmbStaff" & n, "")
            End If
            On Error GoTo 0
        
            txtFee1(n).Visible = True
            If chkForeigner.Value = 1 Then
                txtFee1(n).text = Format(!Fee * 2, "0.00")
            Else
                txtFee1(n).text = Format(!Fee, "0.00")
            End If
            .MoveNext
            
        Next
        If PSCCount = 0 Then
            For n = 0 To lblSpeciality1.UBound
                lblSpeciality1(n).Visible = False
                lblSpeciality1(n).Caption = Empty
                cmbStaff1(n).Visible = False
                cmbStaff1(n).text = Empty
                txtServiceProfessionalChargesID(n).text = Empty
                txtFee1(n).Visible = False
                txtFee1(n).text = Empty
                txtSpecialityID(n).text = Empty
            Next
        Else
            For n = PSCCount To lblSpeciality1.UBound
                lblSpeciality1(n).Visible = False
                lblSpeciality1(n).Caption = Empty
                cmbStaff1(n).Visible = False
                cmbStaff1(n).text = Empty
                txtServiceProfessionalChargesID(n).text = Empty
                txtFee1(n).Visible = False
                txtFee1(n).text = Empty
                txtSpecialityID(n).text = Empty
            Next
        End If
    End With
    

    
    txtFee1_Change (0)
    txtHospitalCharge_Change
    txtProfessionalCharge_Change
        
    
    fillSessionGrid
    
End Sub



Private Sub lstCategory_LostFocus()
    lstCategory.BackColor = noFocusBackColour
End Sub

Private Sub lstSc_Click()
    If lstSc.Locked = True Then
        MsgBox "You can't change the Secession once Services are added. If you want to change the secession, finish this bill and start a new bill or delete the already entered services."
        gridService.SetFocus
    End If
    scChanged
    sessionChanged
    SSTab2.Tab = 0
End Sub

Private Sub lstSc_GotFocus()
    lstSc.BackColor = focusedBackColour
    If IsNumeric(lstSc.BoundText) = False Then
        SendKeys "{up}"
    End If
End Sub

Private Sub lstSc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtCharge.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        lstSc.text = Empty
        fillSessionGrid
    End If
End Sub

Private Sub sessionChanged()
    txtSessionId.text = Val(lstSession.BoundText)
    Dim rsTem As New ADODB.Recordset

    If Val(lstSession.BoundText) = 0 Then Exit Sub
    If lstSession.Locked = True Then Exit Sub
    
    fillSessionGrid
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblServiceSecession Where ServiceSecessionID = " & Val(lstSession.BoundText)
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            txtDuration.text = !DurationMinutes
            dtpStart.Value = !StartTime
        Else
            txtDuration.text = 0
        End If
        .Close
    End With
    If Val(txtDuration.text) = 0 Then
        dtpAppTime.Value = dtpStart.Value
    Else
        dtpAppTime.Value = DateAdd("n", Val(txtDuration.text) + Val(txtSerialNo.text) - 1, dtpStart.Value)
    End If
End Sub

Private Sub getNextSessionNo()
    Dim rsTem As New ADODB.Recordset
    
    If Val(lstSession.BoundText) = 0 Then Exit Sub
    'If lstSession.Locked = True Then Exit Sub
    
    With rsTem
        If .State = 1 Then .Close
        If currentCat.SerialBySubCategory = True Then
            temSql = "Select Count(tblPatientService.SerialNo) as MaxSerialNo FROM          dbo.tblPatientService LEFT OUTER JOIN dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID Where Completed=1 and Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServiceSubCategoryID = " & Val(lstSc.BoundText) & " and SECESSIONID  = " & Val(lstSession.BoundText)
        Else
            temSql = "Select Count(tblPatientService.SerialNo) as MaxSerialNo FROM          dbo.tblPatientService LEFT OUTER JOIN dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID Where  Completed=1 and Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServiceCategoryID = " & Val(lstCategory.BoundText) & " and SECESSIONID  = " & Val(lstSession.BoundText)
        End If
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!MaxSerialNo) = False Then
            txtSerialNo.text = !MaxSerialNo + 1
        Else
            txtSerialNo.text = 1
        End If
        If Val(txtSerialNo.text) <= 0 Then
            txtSerialNo.text = 1
        End If
        
        If InStr(LCase(lstCategory), "x-ray") > 0 Then
            txtSerialNo.text = Val(txtSerialNo.text) + gridService.Rows - 1
        End If
        
        .Close
    End With

End Sub

Private Sub fillSessionGrid()
    Dim D(0) As Integer
    Dim P(0) As Integer
    
    'On Error Resume Next
    
'    temSql = "SELECT dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillId, dbo.tblPatientMainDetails.FirstName AS PatientName, dbo.tblIncomeBill.HospitalFee,  dbo.tblIncomeBill.ProfessionalFee, dbo.tblIncomeBill.BillSettled, dbo.tblIncomeBill.Cancelled " & _
'                        "FROM  dbo.tblIncomeBill LEFT OUTER JOIN dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
'                        "Where ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND CategoryID = " & Val(lstCategory.BoundText) & " AND SubCategoryID = " & Val(lstSc.BoundText) & " and tblIncomeBill.SessionId  = " & Val(lstSession.BoundText) & " ORDER BY  dbo.tblIncomeBill.DisplayBillId"
'
    temSql = ""
    If IsNumeric(lstSession.BoundText) = True And IsNumeric(lstSc.BoundText) = True Then
        temSql = "SELECT dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillId as [Bill No], dbo.tblPatientMainDetails.FirstName AS [Patient], min(dbo.tblPatientService.SerialNo) as Serial, dbo.tblPatientMainDetails.Phone AS Phone, dbo.tblIncomeBill.HospitalFee as [Hos. Fee],  dbo.tblIncomeBill.ProfessionalFee as [Pro. Fee], dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled , dbo.tblIncomeBill.Returned, dbo.tblIncomeBill.Absent, dbo.tblIncomeBill.BHTID  " & _
                            "FROM          dbo.tblPatientService LEFT OUTER JOIN " & _
                            "dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN " & _
                            "dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServiceSubcategoryID = " & Val(lstSc.BoundText) & " AND SECESSIONID  = " & Val(lstSession.BoundText) & _
                            " GROUP BY dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillID, dbo.tblPatientMainDetails.FirstName,  dbo.tblPatientMainDetails.Phone, dbo.tblIncomeBill.HospitalFee, dbo.tblIncomeBill.ProfessionalFee, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled , dbo.tblIncomeBill.Returned, dbo.tblIncomeBill.Absent, dbo.tblIncomeBill.BHTID " & _
                            " Order by min(dbo.tblPatientService.SerialNo) "
                            
    ElseIf IsNumeric(lstSession.BoundText) = False And IsNumeric(lstSc.BoundText) = True Then
        temSql = "SELECT dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillId as [Bill No], dbo.tblPatientMainDetails.FirstName AS Patient, min(dbo.tblPatientService.SerialNo) as Serial, dbo.tblPatientMainDetails.Phone AS Phone, dbo.tblIncomeBill.HospitalFee as [Hos. Fee],  dbo.tblIncomeBill.ProfessionalFee as [Pro. Fee], dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled , dbo.tblIncomeBill.Returned, dbo.tblIncomeBill.Absent, dbo.tblIncomeBill.BHTID   " & _
                            "FROM          dbo.tblPatientService LEFT OUTER JOIN " & _
                            "dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN " & _
                            "dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND ServiceSubcategoryID = " & Val(lstSc.BoundText) & _
                            " GROUP BY dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillID, dbo.tblPatientMainDetails.FirstName,  dbo.tblPatientMainDetails.Phone, dbo.tblIncomeBill.HospitalFee, dbo.tblIncomeBill.ProfessionalFee, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled , dbo.tblIncomeBill.Returned, dbo.tblIncomeBill.Absent, dbo.tblIncomeBill.BHTID " & _
                            "ORDER BY  min(dbo.tblPatientService.SerialNo) "
    ElseIf IsNumeric(lstSession.BoundText) = True And IsNumeric(lstSc.BoundText) = False Then
        temSql = "SELECT dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillId  as [Bill No], dbo.tblPatientMainDetails.FirstName AS Patient, min(dbo.tblPatientService.SerialNo) as Serial, dbo.tblPatientMainDetails.Phone AS Phone, dbo.tblIncomeBill.HospitalFee as [Hos. Fee],  dbo.tblIncomeBill.ProfessionalFee as [Pro. Fee], dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled,  dbo.tblIncomeBill.Returned, dbo.tblIncomeBill.Absent, dbo.tblIncomeBill.BHTID    " & _
                            "FROM          dbo.tblPatientService LEFT OUTER JOIN " & _
                            "dbo.tblIncomeBill ON dbo.tblPatientService.RBillID = dbo.tblIncomeBill.IncomeBillID LEFT OUTER JOIN " & _
                            "dbo.tblPatientMainDetails ON dbo.tblIncomeBill.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                            "Where Deleted = 0 AND ServiceDate = '" & Format(dtpAppDate.Value, "dd MMMM yyyy") & "' AND SECESSIONID  = " & Val(lstSession.BoundText) & _
                            " GROUP BY dbo.tblIncomeBill.IncomeBillID, dbo.tblIncomeBill.DisplayBillID, dbo.tblPatientMainDetails.FirstName,  dbo.tblPatientMainDetails.Phone, dbo.tblIncomeBill.HospitalFee, dbo.tblIncomeBill.ProfessionalFee, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.BillSettled , dbo.tblIncomeBill.Returned, dbo.tblIncomeBill.Absent, dbo.tblIncomeBill.BHTID " & _
                            "ORDER BY  min(dbo.tblPatientService.SerialNo) "
    Else
        temSql = ""
    End If
    
    gridList.Visible = False
    
    If temSql <> "" Then
        FillDetailGrid temSql, gridList, 0, D, P
    Else
        gridList.Clear
    End If
    
    
    replaceGridString gridList, 7, "True", "Cancelled"
    replaceGridString gridList, 7, "False", ""
    replaceGridString gridList, 8, "True", "Settled"
    replaceGridString gridList, 8, "False", "Yet to pay"
    replaceGridString gridList, 9, "True", "Returned"
    replaceGridString gridList, 9, "False", ""
    replaceGridString gridList, 10, "True", "Absent"
    replaceGridString gridList, 10, "False", ""
    
    replaceBhtId gridList, 11
    
    formatGridString gridList, 5, "#,##0.00"
    formatGridString gridList, 6, "#,##0.00"
    
    colourGridByRow gridList, &HC0&, &HFF80FF, &HC0C0FF
    
    gridList.ColWidth(0) = 0
    
    gridList.Visible = True
    
    
End Sub

Private Sub lstSc_LostFocus()
    lstSc.BackColor = noFocusBackColour
End Sub

Private Sub lstSession_Click()
    If lstSession.Locked = True Then
        MsgBox "You can't change the Secession once Services are added. If you want to change the secession, finish this bill and start a new bill or delete the already entered services."
        gridService.SetFocus
    End If
    sessionChanged
    SSTab2.Tab = 0
    
    
    If InStr(lstSession.text, "CTB") > 0 Then
        cmbPaymentMethod.text = "Agent"
    Else
        If SSTab1.Tab = 0 Then
           cmbBHT.BoundText = Empty
           cmbPaymentMethod.BoundText = 1
           cmbPaymentMethod.Locked = False
       Else
           cmbPaymentMethod.BoundText = 10
           cmbPaymentMethod.Locked = True
       End If
    End If
    
    
End Sub

Private Sub lstSession_GotFocus()
    lstSession.BackColor = focusedBackColour
    If IsNumeric(lstSession.BoundText) = False Then
        SendKeys "{up}"
    End If
End Sub

Private Sub lstSession_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        If lstSc.Visible = True Then
            lstSc.SetFocus
        Else
            If txtComments.Visible = True Then
                txtComments.SetFocus
            Else
                txtHospitalCharge.SetFocus
            End If
        End If
        
'        If txtComments.Visible = True Then
'            txtComments.SetFocus
'        Else
'            txtHospitalCharge.SetFocus
'        End If
    ElseIf KeyCode = vbKeyEscape Then
        lstSession.text = Empty
    End If
End Sub

Private Sub cmbStaff1_Change(Index As Integer)
    If Index = 0 Then
        txtProId.text = Val(cmbStaff1(0).BoundText)
    End If
End Sub

Private Sub cmbStaff1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtFee1(Index).SetFocus
    ElseIf KeyCode = vbKeyEscape Then
'        cmbStaff1(Index).Text = Empty
    End If
End Sub


Private Sub Form_Activate()
    If FirstActi = True Then
        Call GetSettings
        Me.Caption = Me.Caption & RadiologyName
        FirstActi = False
    End If
    On Error Resume Next
    Me.WindowState = 2
End Sub

Private Sub Form_Load()
    FirstActi = True
    Call PopulatePrinters
    Call PopulatePapers
    Call GetSettings
    Call FillCombos
    Call ClearServiceValues
    Call ClearAddValues
    Call ClearBillValues
    If IsNumeric(txtOPDBillID.text) = False Then txtOPDBillID.text = NewRBillID(dtpDate.Value, dtpTime.Value)
    Call FillGrid
    noFocusBackColour = &H80000005
    focusedBackColour = &H80FFFF
    
    
End Sub

Private Sub GetSettings()
    On Error Resume Next
    GetCommonSettings Me
    dtpDate.Value = Date
    dtpTime.Value = Time
    dtpAppDate.Value = Date
    cmbPaymentMethod.BoundText = 1 ' Val(GetSetting(App.EXEName, Me.Name, cmbPaymentMethod.Name, 1))
    chkPrint.Value = GetSetting(App.EXEName, Me.Name, chkPrint.Name, 1)
    cmbPrinter.text = GetSetting(App.EXEName, Me.Name, cmbPrinter.Name, "")
    cmbPaper.text = GetSetting(App.EXEName, Me.Name, cmbPaper.Name, "A4")
    todayStaffId = Val(GetSetting(App.EXEName, Me.Name, "todayStaffId", ""))
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
    SaveSetting App.EXEName, Me.Name, "todayStaffId", todayStaffId
    SaveSetting App.EXEName, Me.Name, cmbPaymentMethod.Name, cmbPaymentMethod.BoundText
    SaveSetting App.EXEName, Me.Name, chkPrint.Name, chkPrint.Value
    SaveSetting App.EXEName, Me.Name, cmbPrinter.Name, cmbPrinter.text
    SaveSetting App.EXEName, Me.Name, cmbPaper.Name, cmbPaper.text
End Sub

Private Sub FillCombos()
    Dim cat As New clsFillCombos
    cat.FillBoolList lstCategory, "ServiceCategory", "ServiceCategory", "ForR", True
    Dim PayM As New clsFillCombos
    PayM.FillBoolCombo cmbPaymentMethod, "PaymentMethod", "PaymentMethod", "CanReceive", False
    With rsHSS
        If .State = 1 Then .Close
        temSql = "SELECT tblHealthSchemeSuppliers.* FROM tblHealthSchemeSuppliers ORDER BY tblHealthSchemeSuppliers.HealthSchemeSupplierName"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbHSS
        Set .RowSource = rsHSS
        .ListField = "HealthSchemeSupplierName"
        .BoundColumn = "HealthSchemeSupplierID"
    End With
    
    With rsBHT
        If .State = 1 Then .Close
        temSql = "Select * from tblBHT where IsBHT = 1 And Discharge = 0 order by BHT"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbBHT
        Set .RowSource = rsBHT
        .ListField = "BHT"
        .BoundColumn = "BHTID"
    End With
    
End Sub

Private Sub FillGrid()
    Call FormatGrid
    Dim rsTem As New ADODB.Recordset
    
    Dim TotalCharge As Double
    Dim HosCharge As Double
    Dim ProCharge As Double
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT tblPatientService.PatientServiceID, tblPatientService.ServiceDate, tblServiceCategory.ServiceCategory, tblServiceSubcategory.ServiceSubcategory, tblPatientService.Comments, tblPatientService.Charge, tblServiceSecession.ServiceSecession, tblPatientService.SerialNo " & _
                    "FROM ((tblPatientService LEFT JOIN tblServiceCategory ON tblPatientService.ServiceCategoryID = tblServiceCategory.ServiceCategoryID) LEFT JOIN tblServiceSubcategory ON tblPatientService.ServiceSubcategoryID = tblServiceSubcategory.ServiceSubcategoryID) LEFT JOIN tblServiceSecession ON tblPatientService.SecessionID = tblServiceSecession.ServiceSecessionID " & _
                    "WHERE (((tblPatientService.Deleted)=0) AND ((tblPatientService.RBillID)<> 0)  AND ((tblPatientService.RBillID)=" & Val(txtOPDBillID.text) & ")) " & _
                    "ORDER BY tblPatientService.PatientServiceID"
                    
        temSql = "SELECT tblPatientService.PatientServiceID, tblPatientService.ServiceDate, tblServiceCategory.ServiceCategory, tblServiceSubcategory.ServiceSubcategory, tblPatientService.Comments, tblPatientService.Charge, tblPatientService.ProfessionalCharge, tblPatientService.HospitalCharge, tblServiceSecession.ServiceSecession, tblPatientService.SerialNo " & _
                    "FROM ((tblPatientService LEFT JOIN tblServiceCategory ON tblPatientService.ServiceCategoryID = tblServiceCategory.ServiceCategoryID) LEFT JOIN tblServiceSubcategory ON tblPatientService.ServiceSubcategoryID = tblServiceSubcategory.ServiceSubcategoryID) LEFT JOIN tblServiceSecession ON tblPatientService.SecessionID = tblServiceSecession.ServiceSecessionID " & _
                    "WHERE (((tblPatientService.Deleted)=0 OR (tblPatientService.Deleted IS NULL)) AND ((tblPatientService.RBillID)<> 0)  AND ((tblPatientService.RBillID)=" & Val(txtOPDBillID.text) & ")) " & _
                    "ORDER BY tblPatientService.PatientServiceID"
                    
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            gridService.Rows = gridService.Rows + 1
            gridService.row = gridService.Rows - 1
            gridService.col = 0
            gridService.text = !PatientServiceID
            gridService.col = 1
            gridService.text = !ServiceDate
            gridService.col = 2
            If IsNull(!ServiceSubcategory) = True Then
                gridService.text = !ServiceCategory
            Else
                gridService.text = !ServiceSubcategory   '  !ServiceCategory & " - " & !ServiceSubCategory
            End If
            gridService.col = 3
            gridService.text = !Comments
            gridService.col = 4
            gridService.text = Format(!Charge, "0.00")
            TotalCharge = TotalCharge + !Charge
            HosCharge = HosCharge + !HospitalCharge
            ProCharge = ProCharge + !ProfessionalCharge
            gridService.col = 5
            gridService.text = Format(!ServiceSecession, "")
            gridService.col = 6
            gridService.text = Format(!SerialNo, "")
            gridService.col = 7
            gridService.text = HosCharge
            gridService.col = 8
            gridService.text = ProCharge
            
            .MoveNext
        Wend
    End With
    lblTotal.Caption = Format(TotalCharge, "0.00")
    txtTotalHosFee.text = HosCharge
    txtTotalProFee.text = ProCharge
End Sub

Private Sub FormatGrid()
    '   0   ID
    '   1   Date
    '   2   Service
    '   3   Comments
    '   4   Charges
    '   5   Secession
    '   6   Serial
    
    With gridService
        .Cols = 9
        .Rows = 1
        .row = 0
        
        .col = 0
        .text = "ID"
        .ColWidth(0) = 0
        
        
        .col = 1
        .ColWidth(1) = 0
        .text = "Date"
        
        .col = 2
        .ColWidth(2) = 2500
        .text = "Service"
        
        .col = 3
        .ColWidth(3) = 2500
        .text = "Comments "
        
        .col = 4
        .ColWidth(4) = 1200
        .text = "Charge"
        
        .col = 5
        .ColWidth(5) = 1200
        .text = "Secession"
        
        .col = 6
        .ColWidth(6) = 1200
        .text = "Serial"
        
        .col = 7
        .ColWidth(7) = 0
        .text = "HosFee"
        
        .col = 8
        .ColWidth(8) = 0
        .text = "ProFee"
        
        
    End With
    lblTotal.Caption = "0.00"
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call ClearGrid
    Call SaveSettings
End Sub

Private Sub ClearGrid()
    With gridService
        While .Rows >= 2
            .row = .Rows - 1
            txtDelID.text = Val(.TextMatrix(.row, 0))
            If Val(txtDelID.text) > 0 Then
                btnDelete_Click
            Else
                Exit Sub
            End If
        Wend
    End With
End Sub

Private Sub gridList_Click()
    Dim temId As Long
    Dim temBill As New IncomeBill
    temId = Val(gridList.TextMatrix(gridList.row, 0))
    
    If temId = 0 Then Exit Sub
    
    temBill.IncomeBillID = temId
    If temBill.Cancelled = False And temBill.Returned = False Then
        btnCancel.Enabled = True
        btnReprint.Enabled = True
        btnRefund.Enabled = True
        btnChange.Enabled = True
    Else
        btnCancel.Enabled = False
        btnReprint.Enabled = False
        btnRefund.Enabled = False
        btnCancel.Enabled = False
    End If
    If temBill.BillSettled = False And temBill.Cancelled = False And temBill.Returned = False Then
        btnSettle.Enabled = True
    Else
        btnSettle.Enabled = False
    End If
       
     If temBill.BillSettled = True Then
        btnChange.Enabled = False
     End If
    
    
End Sub

Private Sub gridService_Click()
    With gridService
        txtDelID.text = Val(.TextMatrix(.row, 0))
        .col = .Cols - 1
        .ColSel = 0
    End With
End Sub

Private Sub gridService_DblClick()
'    Dim rsTem As New ADODB.Recordset
'    With gridService
'        txtEditID.Text = Val(.TextMatrix(.Row, 0))
'        .Col = .Cols - 1
'        .ColSel = 0
'    End With
'    With rsTem
'        If .State = 1 Then .Close
'        If IsNumeric(txtEditID.Text) = True Then
'            temSql = "Select * from tblPatientService where PatientServiceID = " & Val(txtEditID.Text)
'            .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
'            If .RecordCount > 0 Then
'                lstCategory.BoundText = !ServiceCategoryID
'                lstSc.BoundText = !ServiceSubcategoryID
'                txtComments.Text = !Comments
'                dtpDate.Value = !ServiceDate
'                dtpTime.Value = !ServiceTime
'                txtCharge.Text = Format(!Charge, "0.00")
'                txtHospitalCharge.Text = Format(!HospitalCharge, "0.00")
'                txtProfessionalCharge.Text = Format(!ProfessionalCharge, "0.00")
'            End If
'            .Close
'        End If
'    End With
'    Dim n As Integer
'    For n = 0 To lblSpeciality1.UBound
'        If lblSpeciality1(n).Visible = True Then
'            With rsTem
'                If .State = 1 Then .Close
'                temSql = "Select * from tblProfessionalCharges where ServiceProfessionalChargesID = " & Val(txtServiceProfessionalChargesID(n).Text) & " AND PatientServiceID = " & Val(txtEditID.Text)
'                .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
'                If .RecordCount > 0 Then
'                    cmbStaff1(n).BoundText = !StaffID
'                    If chkForeigner.Value = 1 Then
'                        txtFee1(n).Text = Format(!Fee * 2, "0.00")
'                    Else
'                        txtFee1(n).Text = Format(!Fee, "0.00")
'                    End If
'                End If
'                .Close
'            End With
'        Else
'            txtFee1(n).Text = 0
'        End If
'    Next n
End Sub


Private Sub lstSession_LostFocus()
    lstSession.BackColor = noFocusBackColour
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 0 Then
        cmbBHT.BoundText = Empty
        cmbPaymentMethod.BoundText = 1
        cmbPaymentMethod.Locked = False
    Else
        cmbPaymentMethod.BoundText = 10
        cmbPaymentMethod.Locked = True
    End If
End Sub



Private Sub txtBhtPatientName_GotFocus()
    SendKeys "{home}+{end}"
End Sub

Private Sub txtBhtPatientName_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbBHT.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtBhtPatientName.text = Empty
    End If
End Sub

Private Sub txtCharge_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        btnAdd_Click
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtCharge.text = Empty
    End If
End Sub

Private Sub txtComments_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtHospitalCharge.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtComments.text = Empty
    End If
End Sub

Private Sub txtFee1_Change(Index As Integer)
    Dim n As Long
    Dim temTotal As Double
    For n = 0 To txtFee1.UBound
        temTotal = temTotal + Val(txtFee1(n).text)
    Next
    txtProfessionalCharge.text = Format(temTotal, "0.00")
End Sub

Private Sub txtFee1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        If Index >= txtFee1.UBound Then
            If cmbStaff1(Index + 1).Visible = True Then
                cmbStaff1(Index + 1).SetFocus
            Else
                btnAdd_Click
            End If
        Else
            btnAdd_Click
        End If
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        
    End If
End Sub

Private Sub txtHospitalCharge_Change()
    txtCharge.text = Format(Val(txtHospitalCharge.text) + Val(txtProfessionalCharge.text), "0.00")
End Sub

Private Sub txtHospitalCharge_GotFocus()
    SendKeys "{home}+{end}"
End Sub

Private Sub txtHospitalCharge_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtProfessionalCharge.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtHospitalCharge.text = Empty
    End If
End Sub

Private Sub txtNic_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtPhone.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtNic.text = Empty
    End If
End Sub

Private Sub txtPatient_GotFocus()
    On Error Resume Next
    SendKeys "{home}+{end}"
End Sub

Private Sub txtPatient_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtNic.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        txtPatient.text = Empty
    End If
End Sub

Private Sub txtPhone_GotFocus()
    SendKeys "{home}+{end}"
End Sub

Private Sub txtPhone_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        lstCategory.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtPhone.text = Empty
    End If

End Sub

Private Sub txtProfessionalCharge_Change()
    txtCharge.text = Format(Val(txtHospitalCharge.text) + Val(txtProfessionalCharge.text), "0.00")
End Sub

Private Sub txtProfessionalCharge_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtCharge.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        KeyCode = Empty
        txtProfessionalCharge.text = Empty
    End If
End Sub


Private Sub PopulatePrinters()
    Dim myPrinter As Printer
    For Each myPrinter In Printers
        cmbPrinter.AddItem myPrinter.DeviceName
    Next
End Sub

Private Sub PopulatePapers(): On Error Resume Next
    cmbPaper.Clear
    SetPrinter = False
    CsetPrinter.SetPrinterAsDefault (cmbPrinter.text): DoEvents
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
'        With FormSize
'            .cx = BillPaperHeight
'            .cy = BillPaperWidth
'        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                'FormItem = PtrCtoVbString(.pName) & " - " & .Size.cx / 1000 & " mm X " & .Size.cy / 1000 & " mm   (" & i + 1 & ")"
                'ComboBillPrinterPapers.AddItem FormItem
                cmbPaper.AddItem PtrCtoVbString(.pName)
'                ListBillPrinterPapers.AddItem PtrCtoVbString(.pName) & vbTab & .Size.cx / 1000 & " mm X " & .Size.cy / 1000 & " mm"
            End With
        Next i
        ClosePrinter (PrinterHandle): DoEvents
    End If
End Sub

Private Sub cmbPrinter_Change()
    Call PopulatePapers
End Sub

Private Sub cmbPrinter_Click()
    Call PopulatePapers
End Sub
