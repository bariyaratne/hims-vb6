VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDIMain 
   BackColor       =   &H8000000C&
   Caption         =   "Cashier - Galle Co-Operative Hospital Ltd"
   ClientHeight    =   8820
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   15120
   Icon            =   "MDIMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   7080
      Top             =   4200
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   6600
      Top             =   4320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":29C12
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":302C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":38C75
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":409BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":48BC2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":4E3AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":55B28
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5160
      Top             =   4080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":5C800
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":62EB0
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":6B863
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":735AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":7B7B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":80F9A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIMain.frx":88716
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "Edit"
      Begin VB.Menu mnuCategories 
         Caption         =   "Catogeries"
         Begin VB.Menu mnuEditPatientCategory 
            Caption         =   "Patient Catogery"
         End
         Begin VB.Menu mnuEditServiceCategory 
            Caption         =   "Service Catogeries"
         End
         Begin VB.Menu mnuEditServiceSubcategory 
            Caption         =   "Service Subcatogery"
         End
         Begin VB.Menu mnuEditCategoriesServiceSecessions 
            Caption         =   "Service Secessions"
         End
         Begin VB.Menu mnuEditProfessionalChargesForServiceSubcategories 
            Caption         =   "Professional Charges for Service Subcategories"
         End
         Begin VB.Menu mnuEditSurgeries 
            Caption         =   "Surgeries"
         End
      End
      Begin VB.Menu mmnuEditItems 
         Caption         =   "Items"
         Begin VB.Menu mnuEditItemsItemMaster 
            Caption         =   "Item Master"
         End
         Begin VB.Menu mnuEditItemCategory 
            Caption         =   "Categories"
         End
      End
      Begin VB.Menu mnuHospital 
         Caption         =   "Hospital"
         Begin VB.Menu mnuEditSpeciality 
            Caption         =   "Speciality"
         End
         Begin VB.Menu mnuStaff 
            Caption         =   "Staff"
         End
         Begin VB.Menu mnuDepartments 
            Caption         =   "Departments"
         End
         Begin VB.Menu mnuEditRoomCategory 
            Caption         =   "Room Categories"
         End
         Begin VB.Menu mnuRooms 
            Caption         =   "Rooms"
         End
         Begin VB.Menu mnuEditPrevilages 
            Caption         =   "Previlages"
            Begin VB.Menu mnuEditPrevilagesMenuEnable 
               Caption         =   "Enable Menus"
            End
            Begin VB.Menu mnuPrevilagesMenuVisible 
               Caption         =   "Visible Menu"
            End
            Begin VB.Menu mnuPrevilagesEnableControls 
               Caption         =   "Enable Controls"
            End
            Begin VB.Menu mnuPrevilagesVisibleControls 
               Caption         =   "Visible Controls"
            End
         End
      End
      Begin VB.Menu mnuHealthSchemeSuppliers 
         Caption         =   "Health Scheme Suppliers"
      End
      Begin VB.Menu mnuEditAgents 
         Caption         =   "Agents"
      End
      Begin VB.Menu mnuEditHospitalCharges 
         Caption         =   "Charges"
      End
   End
   Begin VB.Menu mnuR 
      Caption         =   "Billing"
      Begin VB.Menu mnuRBills 
         Caption         =   "Radiology Bills"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuRBillsAddFilms 
         Caption         =   "Add Films"
      End
   End
   Begin VB.Menu mnuRReports 
      Caption         =   "Reports"
      Begin VB.Menu mnuReportsSummery 
         Caption         =   "Summery"
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuReportsSummeryApp 
         Caption         =   "Summery with Appointment"
      End
      Begin VB.Menu mnuReportStaffSummery 
         Caption         =   "Staff Summery"
      End
      Begin VB.Menu mnuReportsSummerySub 
         Caption         =   "Subcategory Summery"
      End
      Begin VB.Menu mnuReportsProfessionalSummeryA 
         Caption         =   "Professional Summery By Appointment Date"
      End
      Begin VB.Menu mnuReportsProfessionalSummery 
         Caption         =   "Professional Summery By Booking Date"
      End
      Begin VB.Menu mnuReportsCategoryProfSummery 
         Caption         =   "Category Professional Summery"
      End
      Begin VB.Menu mnuRReportsBills 
         Caption         =   "Bills"
      End
      Begin VB.Menu mnuRReportsCancelledBills 
         Caption         =   "Cancelled Bills"
      End
      Begin VB.Menu mnuRReportsIncomeSummery 
         Caption         =   "Income Summery"
      End
      Begin VB.Menu mnuRBillsFilmSummery 
         Caption         =   "Films Summery"
      End
      Begin VB.Menu mnuRBillValues 
         Caption         =   "Radiology Bill Values"
      End
      Begin VB.Menu mnuRBillsByCategories 
         Caption         =   "Radiology Bills By Categories"
      End
      Begin VB.Menu mnuRSarchBills 
         Caption         =   "Search Radiology Bills"
      End
   End
   Begin VB.Menu mnuPreferences 
      Caption         =   "Preferences"
      Begin VB.Menu mnuProgramPreferances 
         Caption         =   "Program Preferances"
      End
      Begin VB.Menu mnuPrintingPreferances 
         Caption         =   "Printing Preferances"
      End
      Begin VB.Menu mnuPreferanceHospitalCharges 
         Caption         =   "Inward Charges"
      End
      Begin VB.Menu mnuHospitalDetails 
         Caption         =   "Hospital Details"
      End
   End
   Begin VB.Menu mnuWindow 
      Caption         =   "Window"
      WindowList      =   -1  'True
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuTipOfTheDay 
         Caption         =   "Tip of the day"
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "About"
      End
      Begin VB.Menu mnuTableOfContants 
         Caption         =   "Table of Contents"
      End
      Begin VB.Menu mnuHelpAdministrator 
         Caption         =   "Administrator"
      End
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    
Private Sub frmBackOfficePeriodPaymentSummery_Click()
    With frmPeriodPaymentSummery
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
    
End Sub

Private Sub MDIForm_Load()
'    lblDateTime.Caption = Format("Date : " & Format(Date, "dd MM yy") & "   Time : " & Format(Time, "H:M AMPM"))
    Dim bhtCount As Long
    Dim rsTem As New ADODB.Recordset
    With rsTem
        .Open "SELECT * FROM tblBHT", cnnStores, adOpenStatic, adLockReadOnly
        bhtCount = .RecordCount
    End With
    'MsgBox bhtCount
End Sub

Private Sub createTables()
    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "CREATE TABLE tblOPDBill ([OPDBillID] AUTOINCREMENT, [MinSale] DOUBLE, [MaxSale] DOUBLE,  [MinIncrement] DOUBLE, [SaleIncrement] DOUBLE, [IncentiveIncrement] DOUBLE, [Deleted] YesNo, CONSTRAINT [PK_tblRepIncentive] PRIMARY KEY ([RepIncentiveID]))"
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
        temSql = ""
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
        temSql = ""
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
        temSql = ""
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
        temSql = ""
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
        temSql = ""
        .Open temSql, cnnStores, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
    
    End With

End Sub



Private Sub DelUncompletedBills()
'    Dim rsTem As New ADODB.Recordset
'    Dim temSql As String
'    With rsTem
'        If .State = 1 Then .Close
'        temSql = "Delete from tblIncomeBill where Completed = 0 AND UserID = " & UserID
'        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
'        If .State = 1 Then .Close
'    End With
End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim i As Integer
    i = MsgBox("Are you sure you want to Exit?", vbQuestion + vbYesNo, "Exit?")
    If i = vbNo Then
        Cancel = True
    End If
End Sub



Private Sub mmnuBHTRoomOccupancy_Click()
    frmRoomPatients.Show
    frmRoomPatients.ZOrder 0
End Sub

Private Sub mnnuBackOfficeGSBSummery_Click()
    frmAllGSBSummeryF.Show
    frmAllGSBSummeryF.ZOrder 0
End Sub

Private Sub mnuAbout_Click()
    frmTem.Show
End Sub

Private Sub mnuAdmit_Click()
    frmAdmit.Show
    frmAdmit.ZOrder 0
    frmAdmit.Top = 0
    frmAdmit.Left = 0
End Sub

Private Sub mnuAgentCancellations_Click()
    With frmAgentBillCancellationSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuAgentPayments_Click()
    With frmAgentPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuAgentReprints_Click()
    With frmAgentBillReprint
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficeAgentPayments_Click()
    frmAgentBills.Show
    frmAgentBills.ZOrder 0
End Sub

Private Sub mnuBackOfficeAllShiftEndSummeries_Click()
    frmAllShiftEndSummeries.Show
    frmAllShiftEndSummeries.ZOrder 0
    
End Sub

Private Sub mnuBackOfficeBHTBookKeepingSummery_Click()
    frmBHTBookeepingSummery.Show
    frmBHTBookeepingSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeBHTBookKeepingSummeryExtended_Click()
    frmBHTBookeepingSummeryExtended.Show
    frmBHTBookeepingSummeryExtended.ZOrder 0
End Sub

Private Sub mnuBackOfficeBHTPaymentBookKeepingSummery_Click()
    frmBHTPaymentBookeepingSummery.Show
    frmBHTPaymentBookeepingSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeBHTPayments_Click()
    On Error Resume Next
    With frmHealthSchemeSupplierBillSettling
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuBackOfficeBHTServicesActualSummery_Click()
    frmBHTServicesActualSummery.Show
    frmBHTServicesActualSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeBHTServicesActualSummeryD_Click()
    frmBHTServicesActualSummeryD.Show
    frmBHTServicesActualSummeryD.ZOrder 0

End Sub

Private Sub mnuBackOfficeBHTServicesOPDValue_Click()
    frmBHTServicesOPDValue.Show
    frmBHTServicesOPDValue.ZOrder 0
End Sub

Private Sub mnuBackOfficeBHTServicesOPDValueD_Click()
    frmBHTServicesOPDValueD.Show
    frmBHTServicesOPDValueD.ZOrder 0
End Sub

Private Sub mnuBackOfficeBHTSummery_Click()
    frmAllBHTSummeryF.Show
    frmAllBHTSummeryF.ZOrder 0
End Sub

Private Sub mnuBackOfficeBookKeepingSummery_Click()
    frmGSBBookeepingSummery.Show
    frmGSBBookeepingSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeBookKeepingSummeryE_Click()
    frmGSBBookeepingSummeryExtended.Show
    frmGSBBookeepingSummeryExtended.ZOrder 0
End Sub

Private Sub mnuBackOfficeDailySummeryReport_Click()
    frmDailySummeryReport2.Show
    frmDailySummeryReport2.ZOrder 0
End Sub

Private Sub mnuBackOfficeDailySummeryReport2_Click()
    frmDailySummeryReport.Show
    frmDailySummeryReport.ZOrder 0
End Sub

Private Sub mnuBackOfficeDailySummeryReportHospital_Click()
    frmDailySummeryReportHospital.Show
    frmDailySummeryReportHospital.ZOrder 0
End Sub

Private Sub mnuBackOfficeDayEndCounts_Click()
    With frmDayEndSummeryCounts
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficeDetailedSummery_Click()
    With frmSummeryDetails
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficeExpenceDetails_Click()
    frmExpenceDetails.Show
    frmExpenceDetails.ZOrder 0
End Sub

Private Sub mnuBackOfficeGSBAgeAnalysis_Click()
    frmAgeAnalysisPatientGSB.Show
    frmAgeAnalysisPatientGSB.ZOrder 0
End Sub

Private Sub mnuBackOfficeGSBPaymentBookKeepingSummery_Click()
    frmGSBPaymentBookeepingSummery.Show
    frmGSBPaymentBookeepingSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeHealthSchemeSuppliersAgeAnalysis_Click()
    With frmAgeAnalysisBHT
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficeHSSCurrentBalance_Click()
    With frmCurrentCompanyBalance
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficeHSSMTAgeAnalysis_Click()
    frmAgeAnalysisMT.Show
    frmAgeAnalysisMT.ZOrder 0
End Sub

Private Sub mnuBackOfficeHSSMTPayments_Click()
    frmMTHealthSchemeSupplierBillSettling.Show
    frmMTHealthSchemeSupplierBillSettling.ZOrder 0
End Sub

Private Sub mnuBackOfficeMedicalTestBillsWithLabServices_Click()
    frmMTBillsWithLabServices.Show
    frmMTBillsWithLabServices.ZOrder 0
End Sub

Private Sub mnuBackOfficeMTCreditLetters_Click()
    frmMTCreditLetters.Show
    frmMTCreditLetters.ZOrder 0
End Sub

Private Sub mnuBackOfficePatientsAgeAnalysis_Click()
    frmAgeAnalysisPatientBHT.Show
    frmAgeAnalysisPatientBHT.ZOrder 0
End Sub

Private Sub mnuBackOfficePatientsMTAgeAnalysis_Click()
    frmAgeAnalysisMT.Show
    frmAgeAnalysisMT.ZOrder 0
End Sub

'Private Sub mnuBackOfficePayments_Click()
'    On Error Resume Next
'    With frmHealthSchemeSupplierBillSettling
'        .Show
'        .ZOrder 0
'        .Top = 0
'        .Left = 0
'    End With
'End Sub

Private Sub mnuBackOfficePaymentsAgeAnalysis_Click()
    frmBHTProfessionalPaymentsAgeAnalysis.Show
    frmBHTProfessionalPaymentsAgeAnalysis.ZOrder 0
End Sub

Private Sub mnuBackOfficePaymentSummery_Click()
    With frmPaymentSummery
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficePeriodSummeryReport_Click()
    frmPeriodSummeryDetails.Show
    frmPeriodSummeryDetails.ZOrder 0
End Sub

Private Sub mnuBackOfficePeriodSummeryReportByDate_Click()
    frmPeriodSummeryDetailsByDate.Show
    frmPeriodSummeryDetailsByDate.ZOrder 0
End Sub

Private Sub mnuBackOfficeShiftEndCounts_Click()
    With frmShiftEndSummeryCounts
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackOfficeViewServices_Click()
    With frmServiceDetails
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBackup_Click()
    With frmBackUp
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTAddDetailsOfSx_Click()
    frmAddDetailsOfSx.Show
    frmAddDetailsOfSx.ZOrder 0
End Sub

Private Sub mnuBHTAdditionalCharges_Click()
    With frmBHTCharge
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTAdmissionBook_Click()
    frmAdmissionBook.Show
    frmAdmissionBook.ZOrder 0
End Sub

Private Sub mnuBHTAdvanceBulkDischarge_Click()
    frmBHTBulkDischarge.Show
    frmBHTBulkDischarge.ZOrder 0
End Sub

Private Sub mnuBHTBHTSummery_Click()
    With frmBHTSummeryF
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTCashierDischarge_Click()
    frmBHTSummeryFNoDischarge.Show
    frmBHTSummeryFNoDischarge.ZOrder 0
End Sub

Private Sub mnuBHTCashierSummery_Click()
    frmBHTSummeryCashier.Show
    frmBHTSummeryCashier.ZOrder 0
End Sub

Private Sub mnuBHTEditRoomDetails_Click()
    frmEditRoomDetails.Show
    frmEditRoomDetails.ZOrder 0
End Sub

Private Sub mnuBHTGetDetailsOfSx_Click()
    frmGetDetailsOfSx.Show
    frmGetDetailsOfSx.ZOrder 0
End Sub

Private Sub mnuBHTPaymentBillSearch_Click()
    frmSearchBHTBill.Show
    frmSearchBHTBill.ZOrder 0
End Sub

Private Sub mnuBHTPaymentCancellations_Click()
    With frmBHTBillCancellationSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTPaymentsPay_Click()
    With frmBHTPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTPaymentsReprints_Click()
    With frmBHTBillReprintSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTProfessionalCHarges_Click()
    With frmBHTProfessionalCharges
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTProfessionalCHargesD_Click()
    With frmBHTProfessionalChargesD
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuBHTRefunds_Click()
    With frmBHTRefund
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTReverseDischarge_Click()
    frmReverseDischarge1.Show
    frmReverseDischarge1.ZOrder 0
End Sub

Private Sub mnuBHTServices_Click()
    With frmBHTServiceBills
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTServicesD_Click()
    With frmBHTServiceBillsD
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuBHTServiceValues_Click()
    frmBHTServiceValues.Show
    frmBHTServiceValues.ZOrder 0
End Sub

Private Sub mnuBHTServiceValuesByDOD_Click()
    frmBHTServiceValuesByDOD.Show
    frmBHTServiceValuesByDOD.ZOrder 0
End Sub

Private Sub mnuBHTSummeryA_Click()
    With frmBHTSummery
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTSummeryAllF_Click()
    With frmBHTSummeryFD
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuChangeRoom_Click()
    With frmChangeRoom
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuBHTAdmit_Click()
    frmAdmit.Show
    frmAdmit.ZOrder 0
End Sub

Private Sub mnuBHTDischarge_Click()
    frmDischarge.Show
    frmDischarge.ZOrder 0
End Sub

Private Sub mnuDayEndSummery_Click()
    With frmDayEndSummery
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuDepartments_Click()
    With frmDepartments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuDischarge_Click()
    With frmDischarge
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditAgents_Click()
    With frmAgents
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuEditBHT_Click()
    With frmBHTEdit
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditCategoriesServiceSecessions_Click()
    With frmServiceSecession
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditHospitalCharges_Click()
    frmHospitalCharges.Show
    frmHospitalCharges.ZOrder 0
End Sub

Private Sub mnuEditItemCategory_Click()
    frmItemCatogeries.Show
    frmItemCatogeries.ZOrder 0
End Sub

Private Sub mnuEditItemsItemMaster_Click()
    frmItemMaster.Show
    frmItemMaster.ZOrder 0
End Sub

Private Sub mnuEditPatientCategory_Click()
    With frmPatientCategory
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditProfessionalChargesForServiceCategories_Click()
    With frmPreofessionalChargesForServiceCategories
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditPrevilagesMenuEnable_Click()
    With frmAuthorityPrevilagesMenuEnable
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditProfessionalChargesForServiceSubcategories_Click()
    With frmProfessionalChargesForServiceSubcategories
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditRoomCategory_Click()
    With frmRoomCategory
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditServiceCategory_Click()
    With frmServiceCategory
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditServiceSubcategory_Click()
    With frmServiceSubCategory
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuEditSpeciality_Click()
    frmSpeciality.Show
    frmSpeciality.ZOrder 0
End Sub

Private Sub mnuEditSurgeries_Click()
    frmSx.Show
    frmSx.ZOrder 0
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuGSBAdmit_Click()
    With frmAdmitGSBNew
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBEDIT_Click()
    frmGSBEdit.Show
    frmGSBEdit.ZOrder 0
End Sub

Private Sub mnuGSBPay_Click()
    With frmGSBPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBPaymentCancellations_Click()
    With frmGSBBillCancellationSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBPaymentReprint_Click()
    With frmGSBBillReprintSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBPaymentSearch_Click()
    frmSearchGSBill.Show
    frmSearchGSBill.ZOrder 0
End Sub

Private Sub mnuGSBProfessionalCharges_Click()
    With frmGSBProfessionalCharges
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBRefunds_Click()
    With frmGSBRefund
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBReprint_Click()
    frmEditGSB.Show
    frmEditGSB.ZOrder 0
End Sub

Private Sub mnuGSBReverseDischarge_Click()
    frmReverseDischargeGSB.Show
    frmReverseDischargeGSB.ZOrder 0
End Sub

Private Sub mnuGSBServices_Click()
    With frmGSBServiceBills
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuGSBServiceValuesByDOD_Click()
    frmGSBServiceValuesByDOD.Show
    frmGSBServiceValuesByDOD.ZOrder 0
End Sub

Private Sub mnuGSBSummery_Click()
    With frmGSBSummery
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuHealthSchemeSuppliers_Click()
    With frmHealthSchemeSuppliers
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuHelpAdministrator_Click()
    With frmAdministrator
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuLabBillCancellation_Click()
    With frmLabBillCancellationSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuLabBillReprint_Click()
    With frmLabBillReprintSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuLabBills_Click()
    With frmLabServiceBills
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub



Private Sub mnuPatientDetails_Click()
    With frmPatientsDetails
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuLabBillsList_Click()
    frmLabBillList.Show
    frmLabBillList.ZOrder 0
End Sub

Private Sub mnuLabLabServiceValues_Click()
    frmLabServiceValues.Show
    frmLabServiceValues.ZOrder 0
End Sub

Private Sub mnuLabPatientList_Click()
    frmLabServicePatientList.Show
    frmLabServicePatientList.ZOrder 0
End Sub

Private Sub mnuLabSearchBills_Click()
    frmSearchLabBill.Show
    frmSearchLabBill.ZOrder 0
End Sub

Private Sub mnuLabServiceBills_Click()
    frmLabServiceCategoryList.Show
    frmLabServiceCategoryList.ZOrder 0
End Sub

Private Sub mnuMedicalTestBillCancellation_Click()
    With frmMTBillCancellationSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuMedicalTestBillRefunds_Click()
    frmMTRefund.Show
    frmMTRefund.ZOrder 0
End Sub

Private Sub mnuMedicalTestBillReprints_Click()
    With frmMTBillReprintSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuMedicalTestBills_Click()
    With frmMTBills
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuMTBillSearch_Click()
    frmSearchMTBill.Show
    frmSearchMTBill.ZOrder 0
End Sub

Private Sub mnuMTServiceValues_Click()
    frmMTServiceValues.Show
    frmMTServiceValues.ZOrder 0
End Sub

Private Sub mnuOPDBillCancellation_Click()
    With frmOPDBillCancellationSearch
        .Show
        .ZOrder 0
    End With
End Sub

Private Sub mnuOPDBillReprints_Click()
    With frmOPDBillReprintSearch
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuOPDBills_Click()
    With frmOPDServiceBills
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub


Private Sub mnuOPDRefunds_Click()
    With frmOPDRefund
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuOPDSearchBills_Click()
    frmSearchOPDBill.Show
    frmSearchOPDBill.ZOrder 0
End Sub

Private Sub mnuOPDServiceBills_Click()
    With frmOPDServiceCategoryList
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuOPDSErviceCounts_Click()
    With frmOPDServiceCounts
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuOPDServiceValues_Click()
    With frmOPDServiceValues
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPaymentsAddExpences_Click()
    frmExpenceBills.Show
    frmExpenceBills.ZOrder 0
End Sub

Private Sub mnuPaymentsCancelExpences_Click()
    frmExpenceBillCancellationSearch.Show
    frmExpenceBillCancellationSearch.ZOrder 0
End Sub

Private Sub mnuPaymentsHSSBHT_Click()
    With frmHSSBillSettlingCashier
        .Show
        .ZOrder 0
'        .Top = 0
'        .Left = 0
    End With
End Sub

Private Sub mnuPaymentsHSSMT_Click()
    frmMTHealthSchemeSupplierBillSettlingCashier.Show
    frmMTHealthSchemeSupplierBillSettlingCashier.ZOrder 0
End Sub

Private Sub mnuPaymentsProfessionalFeePaymentsForGSB_Click()
    With frmGSBProfessionalPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPaymentsProfessionalFeePaymentsForInwardPatients_Click()
    With frmBHTProfessionalPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPaymentsProfessionalFeePaymentsForLabPatients_Click()
    With frmLabProfessionalPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPaymentsProfessionalFeePaymentsForMedicalTests_Click()
    With frmMTProfessionalPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With

End Sub

Private Sub mnuPaymentsProfessionalFeePaymentsForOPDPatients_Click()
    With frmOPDProfessionalPayments
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPaymentsProfessionalFeePaymentsForRPatients_Click()
    frmRProfessionalPayments.Show
    frmRProfessionalPayments.ZOrder 0
End Sub

Private Sub mnuPharmacyBillCancellation_Click()
    With frmPharmacyBillCancellation
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPharmacyBillReturn_Click()
    With frmPharmacyBillReturns
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPharmacyBills_Click()
    With frmPharmacyBills
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPreferanceHospitalCharges_Click()
    With frmInwardPatientCharges
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPrevilagesMenuVisible_Click()
    With frmAuthorityPrevilagesMenuVisible
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuPrintingPreferances_Click()
    With frmPrintingPreferances
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuProgramPreferances_Click()
    With frmProgramPreferance
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub


Private Sub mnuRBillCancellation_Click()
    frmRBillCancellationSearch.Show
    frmRBillCancellationSearch.ZOrder 0
End Sub

Private Sub mnuRBillRefunds_Click()
    frmRRefund.Show
    frmRRefund.ZOrder 0
End Sub

Private Sub mnuRBillReprints_Click()
    frmRBillReprintSearch.Show
    frmRBillReprintSearch.ZOrder 0
End Sub

Private Sub mnuRBills_Click()
    frmRServiceBills.Show
    frmRServiceBills.ZOrder 0
End Sub


Private Sub mnuRBillsAddFilms_Click()
    frmRFilms.Show
    frmRFilms.ZOrder 0
End Sub

Private Sub mnuRBillsByCategories_Click()
    frmRServiceCategoryList.Show
    frmRServiceCategoryList.ZOrder 0
End Sub

Private Sub mnuRBillsFilmSummery_Click()
    frmRFilmsSummery.Show
    frmRFilmsSummery.ZOrder 0
End Sub

Private Sub mnuRBillValues_Click()
    frmRServiceValues.Show
    frmRServiceValues.ZOrder 0
End Sub

Private Sub mnuReportsCategoryProfSummery_Click()
    frmServiceProfessionalSummery.Show
    frmServiceProfessionalSummery.ZOrder 0
End Sub

Private Sub mnuReportsProfessionalSummery_Click()
    frmProfessionalSummery.Show
    frmProfessionalSummery.ZOrder 0
End Sub

Private Sub mnuReportsProfessionalSummeryA_Click()
    frmProfessionalSummeryA.Show
    frmProfessionalSummeryA.ZOrder 0
End Sub

Private Sub mnuReportsSummery_Click()
    frmSummery.Show
    frmSummery.ZOrder 0
End Sub

Private Sub mnuReportsSummeryApp_Click()
    frmSummeryApp.Show
    frmSummeryApp.ZOrder 0
End Sub

Private Sub mnuReportsSummerySub_Click()
    frmSummerySub.Show
    frmSummerySub.ZOrder 0
End Sub

Private Sub mnuReportStaffSummery_Click()
    frmSummeryStaff.Show
    frmSummeryStaff.ZOrder 0
End Sub

Private Sub mnuRooms_Click()
    With frmRoom
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuRReportsBills_Click()
    frmRPeriodBills.Show
    frmRPeriodBills.ZOrder 0
End Sub

Private Sub mnuRReportsCancelledBills_Click()
    frmRPeriodBillsCalcelled.Show 0
    frmRPeriodBillsCalcelled.ZOrder 0
End Sub

Private Sub mnuRReportsIncomeSummery_Click()
    frmRDailyBillSummery.Show
    frmRDailyBillSummery.ZOrder 0
End Sub

Private Sub mnuRSarchBills_Click()
    frmSearchRBill.Show
    frmSearchRBill.ZOrder 0
End Sub

Private Sub mnuShiftEndSummery_Click()
    With frmShiftEndSummery
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuStaff_Click()
    With frmStaff
        .Show
        .ZOrder 0
        .Top = 0
        .Left = 0
    End With
End Sub

Private Sub mnuTipOfTheDay_Click()
    frmTem.Show
End Sub

Private Sub Timer1_Timer()
'    lblDateTime.Caption = Format("Date : " & Format(Date, "dd MMMM yyyy") & "   Time : " & Format(Time, "H:M AMPM"))
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Index
        
        Case 5:
        Case 6:
        Case 7: mnuExit_Click
    End Select
End Sub

