VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IncomeBill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varIncomeBillID
    Private varDisplayBillID
    Private varDate
    Private varTime
    Private varUserID As Long
    Private varStoreID As Long
    Private varGrossTotal As Double
    Private varDiscount As Double
    Private varNetTotal As Double
    Private varGrossTotalToPay As Double
    Private varDiscountToPay As Double
    Private varNetTotalToPay As Double
    Private varHospitalFee As Double
    Private varProfessionalFee As Double
    Private varTotalFee As Double
    Private varHospitalFeePaid As Double
    Private varProfessionalFeePaid As Double
    Private varTotalFeePaid As Double
    Private varHospitalFeeToPay As Double
    Private varProfessionalFeeToPay As Double
    Private varTotalFeeToPay As Double
    Private varHospitalFeeCancelled As Double
    Private varProfessionalFeeCancelled As Double
    Private varTotalFeeCancelled As Double
    Private varHospitalFeeToGive As Double
    Private varProfessionalFeeToGive As Double
    Private varTotalFeeToGive As Double
    Private varHospitalFeeReturned As Double
    Private varProfessionalFeeReturned As Double
    Private varTotalFeeToReturned As Double
    Private varIsOPDBill As Boolean
    Private varIsLabBill As Boolean
    Private varIsPharmacyBill As Boolean
    Private varIsInwardPaymentBill As Boolean
    Private varIsMedicalTestBill As Boolean
    Private varIsGSBill As Boolean
    Private varIsAgentBill As Boolean
    Private varIsOtherBill As Boolean
    Private varIsRBill As Boolean
    Private varIsIncomeBill As Boolean
    Private varIsExpenceBill As Boolean
    Private varOPDBillID As Long
    Private varLabBillID As Long
    Private varPharmacyBillID As Long
    Private varPatientID As Long
    Private varBHTID As Long
    Private varAgentID As Long
    Private varOtherBillID As Long
    Private varPaymentMethodID As Long
    Private varPaymentComments As String
    Private varCreditPaymentMethodID As Long
    Private varCreditPaymentComments As String
    Private varCompleted As Boolean
    Private varCompletedUserID As Long
    Private varCompletedDate
    Private varCompletedTime
    Private varCreditUserID As Long
    Private varCreditDate
    Private varCreditTime
    Private varCancelled As Boolean
    Private varCancelledUserID As Long
    Private varCancelledDate
    Private varCancelledTime
    Private varCancelledPaymentMethodID As Long
    Private varCancelledPaymentComments As String
    Private varCancelledValue As Double
    Private varReturnedValue As Double
    Private varHSSID As Long
    Private varIsHSSPaymentBill As Boolean
    Private varIsHSTBill As Boolean
    Private varPaidHSSID As Long
    Private varBalance As Double
    Private varBillSettled As Boolean
    Private varupsize_ts
    Private varCategoryId
    Private varSubCategoryId
    Private varProfessionalId
    Private varSessionId
    Private varReturned As Boolean
    Private varReturnedUserID As Long
    Private varReturnedDate
    Private varReturnedTime
    Private varReturnedPaymentMethodID As Long
    Private varReturnedPaymentComments As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblIncomeBill Where IncomeBillID = " & varIncomeBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !DisplayBillID = varDisplayBillID
        !Date = varDate
        !Time = varTime
        !UserID = varUserID
        !StoreID = varStoreID
        !GrossTotal = varGrossTotal
        !Discount = varDiscount
        !NetTotal = varNetTotal
        !GrossTotalToPay = varGrossTotalToPay
        !DiscountToPay = varDiscountToPay
        !NetTotalToPay = varNetTotalToPay
        !HospitalFee = varHospitalFee
        !ProfessionalFee = varProfessionalFee
        !TotalFee = varTotalFee
        !HospitalFeePaid = varHospitalFeePaid
        !ProfessionalFeePaid = varProfessionalFeePaid
        !TotalFeePaid = varTotalFeePaid
        !HospitalFeeToPay = varHospitalFeeToPay
        !ProfessionalFeeToPay = varProfessionalFeeToPay
        !TotalFeeToPay = varTotalFeeToPay
        !HospitalFeeCancelled = varHospitalFeeCancelled
        !ProfessionalFeeCancelled = varProfessionalFeeCancelled
        !TotalFeeCancelled = varTotalFeeCancelled
        !HospitalFeeToGive = varHospitalFeeToGive
        !ProfessionalFeeToGive = varProfessionalFeeToGive
        !TotalFeeToGive = varTotalFeeToGive
        !HospitalFeeReturned = varHospitalFeeReturned
        !ProfessionalFeeReturned = varProfessionalFeeReturned
        !TotalFeeToReturned = varTotalFeeToReturned
        !IsOPDBill = varIsOPDBill
        !IsLabBill = varIsLabBill
        !IsPharmacyBill = varIsPharmacyBill
        !IsInwardPaymentBill = varIsInwardPaymentBill
        !IsMedicalTestBill = varIsMedicalTestBill
        !IsGSBill = varIsGSBill
        !IsAgentBill = varIsAgentBill
        !IsOtherBill = varIsOtherBill
        !IsRBill = varIsRBill
        !IsIncomeBill = varIsIncomeBill
        !IsExpenceBill = varIsExpenceBill
        !OPDBillID = varOPDBillID
        !LabBillID = varLabBillID
        !PharmacyBillID = varPharmacyBillID
        !PatientID = varPatientID
        !BHTID = varBHTID
        !AgentID = varAgentID
        !OtherBillID = varOtherBillID
        !PaymentMethodID = varPaymentMethodID
        !PaymentComments = varPaymentComments
        !CreditPaymentMethodID = varCreditPaymentMethodID
        !CreditPaymentComments = varCreditPaymentComments
        !Completed = varCompleted
        !CompletedUserID = varCompletedUserID
        !CompletedDate = varCompletedDate
        !CompletedTime = varCompletedTime
        !CreditUserID = varCreditUserID
        !CreditDate = varCreditDate
        !CreditTime = varCreditTime
        !Cancelled = varCancelled
        !CancelledUserID = varCancelledUserID
        !CancelledDate = varCancelledDate
        !CancelledTime = varCancelledTime
        !CancelledPaymentMethodID = varCancelledPaymentMethodID
        !CancelledPaymentComments = varCancelledPaymentComments
        !CancelledValue = varCancelledValue
        !ReturnedValue = varReturnedValue
        !HSSID = varHSSID
        !IsHSSPaymentBill = varIsHSSPaymentBill
        !IsHSTBill = varIsHSTBill
        !PaidHSSID = varPaidHSSID
        !Balance = varBalance
        !BillSettled = varBillSettled
        !CategoryId = varCategoryId
        !SubCategoryId = varSubCategoryId
        !ProfessionalId = varProfessionalId
        !SessionId = varSessionId
        !Returned = varReturned
        !ReturnedUserID = varReturnedUserID
        !ReturnedDate = varReturnedDate
        !ReturnedTime = varReturnedTime
        !ReturnedPaymentMethodID = varReturnedPaymentMethodID
        !ReturnedPaymentComments = varReturnedPaymentComments
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varIncomeBillID = !NewID
        Else
            varIncomeBillID = !IncomeBillID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblIncomeBill WHERE IncomeBillID = " & varIncomeBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!IncomeBillID) Then
               varIncomeBillID = !IncomeBillID
            End If
            If Not IsNull(!DisplayBillID) Then
               varDisplayBillID = !DisplayBillID
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!UserID) Then
               varUserID = !UserID
            End If
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!GrossTotal) Then
               varGrossTotal = !GrossTotal
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!NetTotal) Then
               varNetTotal = !NetTotal
            End If
            If Not IsNull(!GrossTotalToPay) Then
               varGrossTotalToPay = !GrossTotalToPay
            End If
            If Not IsNull(!DiscountToPay) Then
               varDiscountToPay = !DiscountToPay
            End If
            If Not IsNull(!NetTotalToPay) Then
               varNetTotalToPay = !NetTotalToPay
            End If
            If Not IsNull(!HospitalFee) Then
               varHospitalFee = !HospitalFee
            End If
            If Not IsNull(!ProfessionalFee) Then
               varProfessionalFee = !ProfessionalFee
            End If
            If Not IsNull(!TotalFee) Then
               varTotalFee = !TotalFee
            End If
            If Not IsNull(!HospitalFeePaid) Then
               varHospitalFeePaid = !HospitalFeePaid
            End If
            If Not IsNull(!ProfessionalFeePaid) Then
               varProfessionalFeePaid = !ProfessionalFeePaid
            End If
            If Not IsNull(!TotalFeePaid) Then
               varTotalFeePaid = !TotalFeePaid
            End If
            If Not IsNull(!HospitalFeeToPay) Then
               varHospitalFeeToPay = !HospitalFeeToPay
            End If
            If Not IsNull(!ProfessionalFeeToPay) Then
               varProfessionalFeeToPay = !ProfessionalFeeToPay
            End If
            If Not IsNull(!TotalFeeToPay) Then
               varTotalFeeToPay = !TotalFeeToPay
            End If
            If Not IsNull(!HospitalFeeCancelled) Then
               varHospitalFeeCancelled = !HospitalFeeCancelled
            End If
            If Not IsNull(!ProfessionalFeeCancelled) Then
               varProfessionalFeeCancelled = !ProfessionalFeeCancelled
            End If
            If Not IsNull(!TotalFeeCancelled) Then
               varTotalFeeCancelled = !TotalFeeCancelled
            End If
            If Not IsNull(!HospitalFeeToGive) Then
               varHospitalFeeToGive = !HospitalFeeToGive
            End If
            If Not IsNull(!ProfessionalFeeToGive) Then
               varProfessionalFeeToGive = !ProfessionalFeeToGive
            End If
            If Not IsNull(!TotalFeeToGive) Then
               varTotalFeeToGive = !TotalFeeToGive
            End If
            If Not IsNull(!HospitalFeeReturned) Then
               varHospitalFeeReturned = !HospitalFeeReturned
            End If
            If Not IsNull(!ProfessionalFeeReturned) Then
               varProfessionalFeeReturned = !ProfessionalFeeReturned
            End If
            If Not IsNull(!TotalFeeToReturned) Then
               varTotalFeeToReturned = !TotalFeeToReturned
            End If
            If Not IsNull(!IsOPDBill) Then
               varIsOPDBill = !IsOPDBill
            End If
            If Not IsNull(!IsLabBill) Then
               varIsLabBill = !IsLabBill
            End If
            If Not IsNull(!IsPharmacyBill) Then
               varIsPharmacyBill = !IsPharmacyBill
            End If
            If Not IsNull(!IsInwardPaymentBill) Then
               varIsInwardPaymentBill = !IsInwardPaymentBill
            End If
            If Not IsNull(!IsMedicalTestBill) Then
               varIsMedicalTestBill = !IsMedicalTestBill
            End If
            If Not IsNull(!IsGSBill) Then
               varIsGSBill = !IsGSBill
            End If
            If Not IsNull(!IsAgentBill) Then
               varIsAgentBill = !IsAgentBill
            End If
            If Not IsNull(!IsOtherBill) Then
               varIsOtherBill = !IsOtherBill
            End If
            If Not IsNull(!IsRBill) Then
               varIsRBill = !IsRBill
            End If
            If Not IsNull(!IsIncomeBill) Then
               varIsIncomeBill = !IsIncomeBill
            End If
            If Not IsNull(!IsExpenceBill) Then
               varIsExpenceBill = !IsExpenceBill
            End If
            If Not IsNull(!OPDBillID) Then
               varOPDBillID = !OPDBillID
            End If
            If Not IsNull(!LabBillID) Then
               varLabBillID = !LabBillID
            End If
            If Not IsNull(!PharmacyBillID) Then
               varPharmacyBillID = !PharmacyBillID
            End If
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!BHTID) Then
               varBHTID = !BHTID
            End If
            If Not IsNull(!AgentID) Then
               varAgentID = !AgentID
            End If
            If Not IsNull(!OtherBillID) Then
               varOtherBillID = !OtherBillID
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!PaymentComments) Then
               varPaymentComments = !PaymentComments
            End If
            If Not IsNull(!CreditPaymentMethodID) Then
               varCreditPaymentMethodID = !CreditPaymentMethodID
            End If
            If Not IsNull(!CreditPaymentComments) Then
               varCreditPaymentComments = !CreditPaymentComments
            End If
            If Not IsNull(!Completed) Then
               varCompleted = !Completed
            End If
            If Not IsNull(!CompletedUserID) Then
               varCompletedUserID = !CompletedUserID
            End If
            If Not IsNull(!CompletedDate) Then
               varCompletedDate = !CompletedDate
            End If
            If Not IsNull(!CompletedTime) Then
               varCompletedTime = !CompletedTime
            End If
            If Not IsNull(!CreditUserID) Then
               varCreditUserID = !CreditUserID
            End If
            If Not IsNull(!CreditDate) Then
               varCreditDate = !CreditDate
            End If
            If Not IsNull(!CreditTime) Then
               varCreditTime = !CreditTime
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!CancelledUserID) Then
               varCancelledUserID = !CancelledUserID
            End If
            If Not IsNull(!CancelledDate) Then
               varCancelledDate = !CancelledDate
            End If
            If Not IsNull(!CancelledTime) Then
               varCancelledTime = !CancelledTime
            End If
            If Not IsNull(!CancelledPaymentMethodID) Then
               varCancelledPaymentMethodID = !CancelledPaymentMethodID
            End If
            If Not IsNull(!CancelledPaymentComments) Then
               varCancelledPaymentComments = !CancelledPaymentComments
            End If
            If Not IsNull(!CancelledValue) Then
               varCancelledValue = !CancelledValue
            End If
            If Not IsNull(!ReturnedValue) Then
               varReturnedValue = !ReturnedValue
            End If
            If Not IsNull(!HSSID) Then
               varHSSID = !HSSID
            End If
            If Not IsNull(!IsHSSPaymentBill) Then
               varIsHSSPaymentBill = !IsHSSPaymentBill
            End If
            If Not IsNull(!IsHSTBill) Then
               varIsHSTBill = !IsHSTBill
            End If
            If Not IsNull(!PaidHSSID) Then
               varPaidHSSID = !PaidHSSID
            End If
            If Not IsNull(!Balance) Then
               varBalance = !Balance
            End If
            If Not IsNull(!BillSettled) Then
               varBillSettled = !BillSettled
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!CategoryId) Then
               varCategoryId = !CategoryId
            End If
            If Not IsNull(!SubCategoryId) Then
               varSubCategoryId = !SubCategoryId
            End If
            If Not IsNull(!ProfessionalId) Then
               varProfessionalId = !ProfessionalId
            End If
            If Not IsNull(!SessionId) Then
               varSessionId = !SessionId
            End If
            If Not IsNull(!Returned) Then
               varReturned = !Returned
            End If
            If Not IsNull(!ReturnedUserID) Then
               varReturnedUserID = !ReturnedUserID
            End If
            If Not IsNull(!ReturnedDate) Then
               varReturnedDate = !ReturnedDate
            End If
            If Not IsNull(!ReturnedTime) Then
               varReturnedTime = !ReturnedTime
            End If
            If Not IsNull(!ReturnedPaymentMethodID) Then
               varReturnedPaymentMethodID = !ReturnedPaymentMethodID
            End If
            If Not IsNull(!ReturnedPaymentComments) Then
               varReturnedPaymentComments = !ReturnedPaymentComments
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varIncomeBillID = Empty
    varDisplayBillID = Empty
    varDate = Empty
    varTime = Empty
    varUserID = 0
    varStoreID = 0
    varGrossTotal = 0
    varDiscount = 0
    varNetTotal = 0
    varGrossTotalToPay = 0
    varDiscountToPay = 0
    varNetTotalToPay = 0
    varHospitalFee = 0
    varProfessionalFee = 0
    varTotalFee = 0
    varHospitalFeePaid = 0
    varProfessionalFeePaid = 0
    varTotalFeePaid = 0
    varHospitalFeeToPay = 0
    varProfessionalFeeToPay = 0
    varTotalFeeToPay = 0
    varHospitalFeeCancelled = 0
    varProfessionalFeeCancelled = 0
    varTotalFeeCancelled = 0
    varHospitalFeeToGive = 0
    varProfessionalFeeToGive = 0
    varTotalFeeToGive = 0
    varHospitalFeeReturned = 0
    varProfessionalFeeReturned = 0
    varTotalFeeToReturned = 0
    varIsOPDBill = False
    varIsLabBill = False
    varIsPharmacyBill = False
    varIsInwardPaymentBill = False
    varIsMedicalTestBill = False
    varIsGSBill = False
    varIsAgentBill = False
    varIsOtherBill = False
    varIsRBill = False
    varIsIncomeBill = False
    varIsExpenceBill = False
    varOPDBillID = 0
    varLabBillID = 0
    varPharmacyBillID = 0
    varPatientID = 0
    varBHTID = 0
    varAgentID = 0
    varOtherBillID = 0
    varPaymentMethodID = 0
    varPaymentComments = Empty
    varCreditPaymentMethodID = 0
    varCreditPaymentComments = Empty
    varCompleted = False
    varCompletedUserID = 0
    varCompletedDate = Empty
    varCompletedTime = Empty
    varCreditUserID = 0
    varCreditDate = Empty
    varCreditTime = Empty
    varCancelled = False
    varCancelledUserID = 0
    varCancelledDate = Empty
    varCancelledTime = Empty
    varCancelledPaymentMethodID = 0
    varCancelledPaymentComments = Empty
    varCancelledValue = 0
    varReturnedValue = 0
    varHSSID = 0
    varIsHSSPaymentBill = False
    varIsHSTBill = False
    varPaidHSSID = 0
    varBalance = 0
    varBillSettled = False
    varupsize_ts = Empty
    varCategoryId = Empty
    varSubCategoryId = Empty
    varProfessionalId = Empty
    varSessionId = Empty
    varReturned = False
    varReturnedUserID = 0
    varReturnedDate = Empty
    varReturnedTime = Empty
    varReturnedPaymentMethodID = 0
    varReturnedPaymentComments = Empty
End Sub

Public Property Let IncomeBillID(ByVal vIncomeBillID)
    Call clearData
    varIncomeBillID = vIncomeBillID
    Call loadData
End Property

Public Property Get IncomeBillID()
    IncomeBillID = varIncomeBillID
End Property

Public Property Let DisplayBillID(ByVal vDisplayBillID)
    varDisplayBillID = vDisplayBillID
End Property

Public Property Get DisplayBillID()
    DisplayBillID = varDisplayBillID
End Property

Public Property Let Time(ByVal vTime)
    varTime = vTime
End Property

Public Property Get Time()
    Time = varTime
End Property

Public Property Let UserID(ByVal vUserID As Long)
    varUserID = vUserID
End Property

Public Property Get UserID() As Long
    UserID = varUserID
End Property

Public Property Let StoreID(ByVal vStoreID As Long)
    varStoreID = vStoreID
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let GrossTotal(ByVal vGrossTotal As Double)
    varGrossTotal = vGrossTotal
End Property

Public Property Get GrossTotal() As Double
    GrossTotal = varGrossTotal
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let NetTotal(ByVal vNetTotal As Double)
    varNetTotal = vNetTotal
End Property

Public Property Get NetTotal() As Double
    NetTotal = varNetTotal
End Property

Public Property Let GrossTotalToPay(ByVal vGrossTotalToPay As Double)
    varGrossTotalToPay = vGrossTotalToPay
End Property

Public Property Get GrossTotalToPay() As Double
    GrossTotalToPay = varGrossTotalToPay
End Property

Public Property Let DiscountToPay(ByVal vDiscountToPay As Double)
    varDiscountToPay = vDiscountToPay
End Property

Public Property Get DiscountToPay() As Double
    DiscountToPay = varDiscountToPay
End Property

Public Property Let NetTotalToPay(ByVal vNetTotalToPay As Double)
    varNetTotalToPay = vNetTotalToPay
End Property

Public Property Get NetTotalToPay() As Double
    NetTotalToPay = varNetTotalToPay
End Property

Public Property Let HospitalFee(ByVal vHospitalFee As Double)
    varHospitalFee = vHospitalFee
End Property

Public Property Get HospitalFee() As Double
    HospitalFee = varHospitalFee
End Property

Public Property Let ProfessionalFee(ByVal vProfessionalFee As Double)
    varProfessionalFee = vProfessionalFee
End Property

Public Property Get ProfessionalFee() As Double
    ProfessionalFee = varProfessionalFee
End Property

Public Property Let TotalFee(ByVal vTotalFee As Double)
    varTotalFee = vTotalFee
End Property

Public Property Get TotalFee() As Double
    TotalFee = varTotalFee
End Property

Public Property Let HospitalFeePaid(ByVal vHospitalFeePaid As Double)
    varHospitalFeePaid = vHospitalFeePaid
End Property

Public Property Get HospitalFeePaid() As Double
    HospitalFeePaid = varHospitalFeePaid
End Property

Public Property Let ProfessionalFeePaid(ByVal vProfessionalFeePaid As Double)
    varProfessionalFeePaid = vProfessionalFeePaid
End Property

Public Property Get ProfessionalFeePaid() As Double
    ProfessionalFeePaid = varProfessionalFeePaid
End Property

Public Property Let TotalFeePaid(ByVal vTotalFeePaid As Double)
    varTotalFeePaid = vTotalFeePaid
End Property

Public Property Get TotalFeePaid() As Double
    TotalFeePaid = varTotalFeePaid
End Property

Public Property Let HospitalFeeToPay(ByVal vHospitalFeeToPay As Double)
    varHospitalFeeToPay = vHospitalFeeToPay
End Property

Public Property Get HospitalFeeToPay() As Double
    HospitalFeeToPay = varHospitalFeeToPay
End Property

Public Property Let ProfessionalFeeToPay(ByVal vProfessionalFeeToPay As Double)
    varProfessionalFeeToPay = vProfessionalFeeToPay
End Property

Public Property Get ProfessionalFeeToPay() As Double
    ProfessionalFeeToPay = varProfessionalFeeToPay
End Property

Public Property Let TotalFeeToPay(ByVal vTotalFeeToPay As Double)
    varTotalFeeToPay = vTotalFeeToPay
End Property

Public Property Get TotalFeeToPay() As Double
    TotalFeeToPay = varTotalFeeToPay
End Property

Public Property Let HospitalFeeCancelled(ByVal vHospitalFeeCancelled As Double)
    varHospitalFeeCancelled = vHospitalFeeCancelled
End Property

Public Property Get HospitalFeeCancelled() As Double
    HospitalFeeCancelled = varHospitalFeeCancelled
End Property

Public Property Let ProfessionalFeeCancelled(ByVal vProfessionalFeeCancelled As Double)
    varProfessionalFeeCancelled = vProfessionalFeeCancelled
End Property

Public Property Get ProfessionalFeeCancelled() As Double
    ProfessionalFeeCancelled = varProfessionalFeeCancelled
End Property

Public Property Let TotalFeeCancelled(ByVal vTotalFeeCancelled As Double)
    varTotalFeeCancelled = vTotalFeeCancelled
End Property

Public Property Get TotalFeeCancelled() As Double
    TotalFeeCancelled = varTotalFeeCancelled
End Property

Public Property Let HospitalFeeToGive(ByVal vHospitalFeeToGive As Double)
    varHospitalFeeToGive = vHospitalFeeToGive
End Property

Public Property Get HospitalFeeToGive() As Double
    HospitalFeeToGive = varHospitalFeeToGive
End Property

Public Property Let ProfessionalFeeToGive(ByVal vProfessionalFeeToGive As Double)
    varProfessionalFeeToGive = vProfessionalFeeToGive
End Property

Public Property Get ProfessionalFeeToGive() As Double
    ProfessionalFeeToGive = varProfessionalFeeToGive
End Property

Public Property Let TotalFeeToGive(ByVal vTotalFeeToGive As Double)
    varTotalFeeToGive = vTotalFeeToGive
End Property

Public Property Get TotalFeeToGive() As Double
    TotalFeeToGive = varTotalFeeToGive
End Property

Public Property Let HospitalFeeReturned(ByVal vHospitalFeeReturned As Double)
    varHospitalFeeReturned = vHospitalFeeReturned
End Property

Public Property Get HospitalFeeReturned() As Double
    HospitalFeeReturned = varHospitalFeeReturned
End Property

Public Property Let ProfessionalFeeReturned(ByVal vProfessionalFeeReturned As Double)
    varProfessionalFeeReturned = vProfessionalFeeReturned
End Property

Public Property Get ProfessionalFeeReturned() As Double
    ProfessionalFeeReturned = varProfessionalFeeReturned
End Property

Public Property Let TotalFeeToReturned(ByVal vTotalFeeToReturned As Double)
    varTotalFeeToReturned = vTotalFeeToReturned
End Property

Public Property Get TotalFeeToReturned() As Double
    TotalFeeToReturned = varTotalFeeToReturned
End Property

Public Property Let IsOPDBill(ByVal vIsOPDBill As Boolean)
    varIsOPDBill = vIsOPDBill
End Property

Public Property Get IsOPDBill() As Boolean
    IsOPDBill = varIsOPDBill
End Property

Public Property Let IsLabBill(ByVal vIsLabBill As Boolean)
    varIsLabBill = vIsLabBill
End Property

Public Property Get IsLabBill() As Boolean
    IsLabBill = varIsLabBill
End Property

Public Property Let IsPharmacyBill(ByVal vIsPharmacyBill As Boolean)
    varIsPharmacyBill = vIsPharmacyBill
End Property

Public Property Get IsPharmacyBill() As Boolean
    IsPharmacyBill = varIsPharmacyBill
End Property

Public Property Let IsInwardPaymentBill(ByVal vIsInwardPaymentBill As Boolean)
    varIsInwardPaymentBill = vIsInwardPaymentBill
End Property

Public Property Get IsInwardPaymentBill() As Boolean
    IsInwardPaymentBill = varIsInwardPaymentBill
End Property

Public Property Let IsMedicalTestBill(ByVal vIsMedicalTestBill As Boolean)
    varIsMedicalTestBill = vIsMedicalTestBill
End Property

Public Property Get IsMedicalTestBill() As Boolean
    IsMedicalTestBill = varIsMedicalTestBill
End Property

Public Property Let IsGSBill(ByVal vIsGSBill As Boolean)
    varIsGSBill = vIsGSBill
End Property

Public Property Get IsGSBill() As Boolean
    IsGSBill = varIsGSBill
End Property

Public Property Let IsAgentBill(ByVal vIsAgentBill As Boolean)
    varIsAgentBill = vIsAgentBill
End Property

Public Property Get IsAgentBill() As Boolean
    IsAgentBill = varIsAgentBill
End Property

Public Property Let IsOtherBill(ByVal vIsOtherBill As Boolean)
    varIsOtherBill = vIsOtherBill
End Property

Public Property Get IsOtherBill() As Boolean
    IsOtherBill = varIsOtherBill
End Property

Public Property Let IsRBill(ByVal vIsRBill As Boolean)
    varIsRBill = vIsRBill
End Property

Public Property Get IsRBill() As Boolean
    IsRBill = varIsRBill
End Property

Public Property Let IsIncomeBill(ByVal vIsIncomeBill As Boolean)
    varIsIncomeBill = vIsIncomeBill
End Property

Public Property Get IsIncomeBill() As Boolean
    IsIncomeBill = varIsIncomeBill
End Property

Public Property Let IsExpenceBill(ByVal vIsExpenceBill As Boolean)
    varIsExpenceBill = vIsExpenceBill
End Property

Public Property Get IsExpenceBill() As Boolean
    IsExpenceBill = varIsExpenceBill
End Property

Public Property Let OPDBillID(ByVal vOPDBillID As Long)
    varOPDBillID = vOPDBillID
End Property

Public Property Get OPDBillID() As Long
    OPDBillID = varOPDBillID
End Property

Public Property Let LabBillID(ByVal vLabBillID As Long)
    varLabBillID = vLabBillID
End Property

Public Property Get LabBillID() As Long
    LabBillID = varLabBillID
End Property

Public Property Let PharmacyBillID(ByVal vPharmacyBillID As Long)
    varPharmacyBillID = vPharmacyBillID
End Property

Public Property Get PharmacyBillID() As Long
    PharmacyBillID = varPharmacyBillID
End Property

Public Property Let PatientID(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let BHTID(ByVal vBHTID As Long)
    varBHTID = vBHTID
End Property

Public Property Get BHTID() As Long
    BHTID = varBHTID
End Property

Public Property Let AgentID(ByVal vAgentID As Long)
    varAgentID = vAgentID
End Property

Public Property Get AgentID() As Long
    AgentID = varAgentID
End Property

Public Property Let OtherBillID(ByVal vOtherBillID As Long)
    varOtherBillID = vOtherBillID
End Property

Public Property Get OtherBillID() As Long
    OtherBillID = varOtherBillID
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let PaymentComments(ByVal vPaymentComments As String)
    varPaymentComments = vPaymentComments
End Property

Public Property Get PaymentComments() As String
    PaymentComments = varPaymentComments
End Property

Public Property Let CreditPaymentMethodID(ByVal vCreditPaymentMethodID As Long)
    varCreditPaymentMethodID = vCreditPaymentMethodID
End Property

Public Property Get CreditPaymentMethodID() As Long
    CreditPaymentMethodID = varCreditPaymentMethodID
End Property

Public Property Let CreditPaymentComments(ByVal vCreditPaymentComments As String)
    varCreditPaymentComments = vCreditPaymentComments
End Property

Public Property Get CreditPaymentComments() As String
    CreditPaymentComments = varCreditPaymentComments
End Property

Public Property Let Completed(ByVal vCompleted As Boolean)
    varCompleted = vCompleted
End Property

Public Property Get Completed() As Boolean
    Completed = varCompleted
End Property

Public Property Let CompletedUserID(ByVal vCompletedUserID As Long)
    varCompletedUserID = vCompletedUserID
End Property

Public Property Get CompletedUserID() As Long
    CompletedUserID = varCompletedUserID
End Property

Public Property Let CompletedDate(ByVal vCompletedDate)
    varCompletedDate = vCompletedDate
End Property

Public Property Get CompletedDate()
    CompletedDate = varCompletedDate
End Property

Public Property Let CompletedTime(ByVal vCompletedTime)
    varCompletedTime = vCompletedTime
End Property

Public Property Get CompletedTime()
    CompletedTime = varCompletedTime
End Property

Public Property Let CreditUserID(ByVal vCreditUserID As Long)
    varCreditUserID = vCreditUserID
End Property

Public Property Get CreditUserID() As Long
    CreditUserID = varCreditUserID
End Property

Public Property Let CreditDate(ByVal vCreditDate)
    varCreditDate = vCreditDate
End Property

Public Property Get CreditDate()
    CreditDate = varCreditDate
End Property

Public Property Let CreditTime(ByVal vCreditTime)
    varCreditTime = vCreditTime
End Property

Public Property Get CreditTime()
    CreditTime = varCreditTime
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledUserID(ByVal vCancelledUserID As Long)
    varCancelledUserID = vCancelledUserID
End Property

Public Property Get CancelledUserID() As Long
    CancelledUserID = varCancelledUserID
End Property

Public Property Let CancelledDate(ByVal vCancelledDate)
    varCancelledDate = vCancelledDate
End Property

Public Property Get CancelledDate()
    CancelledDate = varCancelledDate
End Property

Public Property Let CancelledTime(ByVal vCancelledTime)
    varCancelledTime = vCancelledTime
End Property

Public Property Get CancelledTime()
    CancelledTime = varCancelledTime
End Property

Public Property Let CancelledPaymentMethodID(ByVal vCancelledPaymentMethodID As Long)
    varCancelledPaymentMethodID = vCancelledPaymentMethodID
End Property

Public Property Get CancelledPaymentMethodID() As Long
    CancelledPaymentMethodID = varCancelledPaymentMethodID
End Property

Public Property Let CancelledPaymentComments(ByVal vCancelledPaymentComments As String)
    varCancelledPaymentComments = vCancelledPaymentComments
End Property

Public Property Get CancelledPaymentComments() As String
    CancelledPaymentComments = varCancelledPaymentComments
End Property

Public Property Let CancelledValue(ByVal vCancelledValue As Double)
    varCancelledValue = vCancelledValue
End Property

Public Property Get CancelledValue() As Double
    CancelledValue = varCancelledValue
End Property

Public Property Let ReturnedValue(ByVal vReturnedValue As Double)
    varReturnedValue = vReturnedValue
End Property

Public Property Get ReturnedValue() As Double
    ReturnedValue = varReturnedValue
End Property

Public Property Let HSSID(ByVal vHSSID As Long)
    varHSSID = vHSSID
End Property

Public Property Get HSSID() As Long
    HSSID = varHSSID
End Property

Public Property Let IsHSSPaymentBill(ByVal vIsHSSPaymentBill As Boolean)
    varIsHSSPaymentBill = vIsHSSPaymentBill
End Property

Public Property Get IsHSSPaymentBill() As Boolean
    IsHSSPaymentBill = varIsHSSPaymentBill
End Property

Public Property Let IsHSTBill(ByVal vIsHSTBill As Boolean)
    varIsHSTBill = vIsHSTBill
End Property

Public Property Get IsHSTBill() As Boolean
    IsHSTBill = varIsHSTBill
End Property

Public Property Let PaidHSSID(ByVal vPaidHSSID As Long)
    varPaidHSSID = vPaidHSSID
End Property

Public Property Get PaidHSSID() As Long
    PaidHSSID = varPaidHSSID
End Property

Public Property Let Balance(ByVal vBalance As Double)
    varBalance = vBalance
End Property

Public Property Get Balance() As Double
    Balance = varBalance
End Property

Public Property Let BillSettled(ByVal vBillSettled As Boolean)
    varBillSettled = vBillSettled
End Property

Public Property Get BillSettled() As Boolean
    BillSettled = varBillSettled
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let CategoryId(ByVal vCategoryId)
    varCategoryId = vCategoryId
End Property

Public Property Get CategoryId()
    CategoryId = varCategoryId
End Property

Public Property Let SubCategoryId(ByVal vSubCategoryId)
    varSubCategoryId = vSubCategoryId
End Property

Public Property Get SubCategoryId()
    SubCategoryId = varSubCategoryId
End Property

Public Property Let ProfessionalId(ByVal vProfessionalId)
    varProfessionalId = vProfessionalId
End Property

Public Property Get ProfessionalId()
    ProfessionalId = varProfessionalId
End Property

Public Property Let SessionId(ByVal vSessionId)
    varSessionId = vSessionId
End Property

Public Property Get SessionId()
    SessionId = varSessionId
End Property

Public Property Let Returned(ByVal vReturned As Boolean)
    varReturned = vReturned
End Property

Public Property Get Returned() As Boolean
    Returned = varReturned
End Property

Public Property Let ReturnedUserID(ByVal vReturnedUserID As Long)
    varReturnedUserID = vReturnedUserID
End Property

Public Property Get ReturnedUserID() As Long
    ReturnedUserID = varReturnedUserID
End Property

Public Property Let ReturnedDate(ByVal vReturnedDate)
    varReturnedDate = vReturnedDate
End Property

Public Property Get ReturnedDate()
    ReturnedDate = varReturnedDate
End Property

Public Property Let ReturnedTime(ByVal vReturnedTime)
    varReturnedTime = vReturnedTime
End Property

Public Property Get ReturnedTime()
    ReturnedTime = varReturnedTime
End Property

Public Property Let ReturnedPaymentMethodID(ByVal vReturnedPaymentMethodID As Long)
    varReturnedPaymentMethodID = vReturnedPaymentMethodID
End Property

Public Property Get ReturnedPaymentMethodID() As Long
    ReturnedPaymentMethodID = varReturnedPaymentMethodID
End Property

Public Property Let ReturnedPaymentComments(ByVal vReturnedPaymentComments As String)
    varReturnedPaymentComments = vReturnedPaymentComments
End Property

Public Property Get ReturnedPaymentComments() As String
    ReturnedPaymentComments = varReturnedPaymentComments
End Property


