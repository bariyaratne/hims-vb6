VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varPatientServiceID As Long
    Private varPatientID As Long
    Private varBHTID As Long
    Private varOPDBillID As Long
    Private varLabBillID As Long
    Private varMedicalTestBillID As Long
    Private varHSTBillID As Long
    Private varRBillID As Long
    Private varExpenceBillID As Long
    Private varIncomeBillID As Long
    Private varServiceDate
    Private varServiceTime
    Private varServiceCategoryID As Long
    Private varServiceSubcategoryID As Long
    Private varComments As String
    Private varHospitalCharge As Double
    Private varProfessionalCharge As Double
    Private varCharge As Double
    Private varUserID As Long
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate
    Private varDeletedTime
    Private varAddToMedicineCharge As Boolean
    Private varSecessionID As Long
    Private varSerialNo As Long
    Private varupsize_ts
    Private varHospitalChargeCancelled As Double
    Private varProfessionalChargeCancelled As Double
    Private varChargeCancelled As Double
    Private varHospitaChargelReturned As Double
    Private varProfessionalChargeReturned As Double
    Private varChargeReturned As Double
    Private varHospitalChargePaid As Double
    Private varProfessionalChargePaid As Double
    Private varChargePaid As Double
    Private varHospitalChargeToPay As Double
    Private varProfessionalChargeToPay As Double
    Private varChargeToPay As Double
    Private varHospitalChargeToGive As Double
    Private varProfessionalChargeToGive As Double
    Private varChargeToGive As Double

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblPatientService Where PatientServiceID = " & varPatientServiceID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !PatientID = varPatientID
        !BHTID = varBHTID
        !OPDBillID = varOPDBillID
        !LabBillID = varLabBillID
        !MedicalTestBillID = varMedicalTestBillID
        !HSTBillID = varHSTBillID
        !RBillID = varRBillID
        !ExpenceBillID = varExpenceBillID
        !IncomeBillID = varIncomeBillID
        !ServiceDate = varServiceDate
        !ServiceTime = varServiceTime
        !ServiceCategoryID = varServiceCategoryID
        !ServiceSubcategoryID = varServiceSubcategoryID
        !Comments = varComments
        !HospitalCharge = varHospitalCharge
        !ProfessionalCharge = varProfessionalCharge
        !Charge = varCharge
        !UserID = varUserID
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !AddToMedicineCharge = varAddToMedicineCharge
        !SecessionID = varSecessionID
        !SerialNo = varSerialNo
        !HospitalChargeCancelled = varHospitalChargeCancelled
        !ProfessionalChargeCancelled = varProfessionalChargeCancelled
        !ChargeCancelled = varChargeCancelled
        !HospitaChargelReturned = varHospitaChargelReturned
        !ProfessionalChargeReturned = varProfessionalChargeReturned
        !ChargeReturned = varChargeReturned
        !HospitalChargePaid = varHospitalChargePaid
        !ProfessionalChargePaid = varProfessionalChargePaid
        !ChargePaid = varChargePaid
        !HospitalChargeToPay = varHospitalChargeToPay
        !ProfessionalChargeToPay = varProfessionalChargeToPay
        !ChargeToPay = varChargeToPay
        !HospitalChargeToGive = varHospitalChargeToGive
        !ProfessionalChargeToGive = varProfessionalChargeToGive
        !ChargeToGive = varChargeToGive
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varPatientServiceID = !NewID
        Else
            varPatientServiceID = !PatientServiceID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientService WHERE PatientServiceID = " & varPatientServiceID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!PatientServiceID) Then
               varPatientServiceID = !PatientServiceID
            End If
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!BHTID) Then
               varBHTID = !BHTID
            End If
            If Not IsNull(!OPDBillID) Then
               varOPDBillID = !OPDBillID
            End If
            If Not IsNull(!LabBillID) Then
               varLabBillID = !LabBillID
            End If
            If Not IsNull(!MedicalTestBillID) Then
               varMedicalTestBillID = !MedicalTestBillID
            End If
            If Not IsNull(!HSTBillID) Then
               varHSTBillID = !HSTBillID
            End If
            If Not IsNull(!RBillID) Then
               varRBillID = !RBillID
            End If
            If Not IsNull(!ExpenceBillID) Then
               varExpenceBillID = !ExpenceBillID
            End If
            If Not IsNull(!IncomeBillID) Then
               varIncomeBillID = !IncomeBillID
            End If
            If Not IsNull(!ServiceDate) Then
               varServiceDate = !ServiceDate
            End If
            If Not IsNull(!ServiceTime) Then
               varServiceTime = !ServiceTime
            End If
            If Not IsNull(!ServiceCategoryID) Then
               varServiceCategoryID = !ServiceCategoryID
            End If
            If Not IsNull(!ServiceSubcategoryID) Then
               varServiceSubcategoryID = !ServiceSubcategoryID
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!HospitalCharge) Then
               varHospitalCharge = !HospitalCharge
            End If
            If Not IsNull(!ProfessionalCharge) Then
               varProfessionalCharge = !ProfessionalCharge
            End If
            If Not IsNull(!Charge) Then
               varCharge = !Charge
            End If
            If Not IsNull(!UserID) Then
               varUserID = !UserID
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!AddToMedicineCharge) Then
               varAddToMedicineCharge = !AddToMedicineCharge
            End If
            If Not IsNull(!SecessionID) Then
               varSecessionID = !SecessionID
            End If
            If Not IsNull(!SerialNo) Then
               varSerialNo = !SerialNo
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!HospitalChargeCancelled) Then
               varHospitalChargeCancelled = !HospitalChargeCancelled
            End If
            If Not IsNull(!ProfessionalChargeCancelled) Then
               varProfessionalChargeCancelled = !ProfessionalChargeCancelled
            End If
            If Not IsNull(!ChargeCancelled) Then
               varChargeCancelled = !ChargeCancelled
            End If
            If Not IsNull(!HospitaChargelReturned) Then
               varHospitaChargelReturned = !HospitaChargelReturned
            End If
            If Not IsNull(!ProfessionalChargeReturned) Then
               varProfessionalChargeReturned = !ProfessionalChargeReturned
            End If
            If Not IsNull(!ChargeReturned) Then
               varChargeReturned = !ChargeReturned
            End If
            If Not IsNull(!HospitalChargePaid) Then
               varHospitalChargePaid = !HospitalChargePaid
            End If
            If Not IsNull(!ProfessionalChargePaid) Then
               varProfessionalChargePaid = !ProfessionalChargePaid
            End If
            If Not IsNull(!ChargePaid) Then
               varChargePaid = !ChargePaid
            End If
            If Not IsNull(!HospitalChargeToPay) Then
               varHospitalChargeToPay = !HospitalChargeToPay
            End If
            If Not IsNull(!ProfessionalChargeToPay) Then
               varProfessionalChargeToPay = !ProfessionalChargeToPay
            End If
            If Not IsNull(!ChargeToPay) Then
               varChargeToPay = !ChargeToPay
            End If
            If Not IsNull(!HospitalChargeToGive) Then
               varHospitalChargeToGive = !HospitalChargeToGive
            End If
            If Not IsNull(!ProfessionalChargeToGive) Then
               varProfessionalChargeToGive = !ProfessionalChargeToGive
            End If
            If Not IsNull(!ChargeToGive) Then
               varChargeToGive = !ChargeToGive
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPatientServiceID = 0
    varPatientID = 0
    varBHTID = 0
    varOPDBillID = 0
    varLabBillID = 0
    varMedicalTestBillID = 0
    varHSTBillID = 0
    varRBillID = 0
    varExpenceBillID = 0
    varIncomeBillID = 0
    varServiceDate = Empty
    varServiceTime = Empty
    varServiceCategoryID = 0
    varServiceSubcategoryID = 0
    varComments = Empty
    varHospitalCharge = 0
    varProfessionalCharge = 0
    varCharge = 0
    varUserID = 0
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varAddToMedicineCharge = False
    varSecessionID = 0
    varSerialNo = 0
    varupsize_ts = Empty
    varHospitalChargeCancelled = 0
    varProfessionalChargeCancelled = 0
    varChargeCancelled = 0
    varHospitaChargelReturned = 0
    varProfessionalChargeReturned = 0
    varChargeReturned = 0
    varHospitalChargePaid = 0
    varProfessionalChargePaid = 0
    varChargePaid = 0
    varHospitalChargeToPay = 0
    varProfessionalChargeToPay = 0
    varChargeToPay = 0
    varHospitalChargeToGive = 0
    varProfessionalChargeToGive = 0
    varChargeToGive = 0
End Sub

Public Property Let PatientServiceID(ByVal vPatientServiceID As Long)
    Call clearData
    varPatientServiceID = vPatientServiceID
    Call loadData
End Property

Public Property Get PatientServiceID() As Long
    PatientServiceID = varPatientServiceID
End Property

Public Property Let PatientID(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let BHTID(ByVal vBHTID As Long)
    varBHTID = vBHTID
End Property

Public Property Get BHTID() As Long
    BHTID = varBHTID
End Property

Public Property Let OPDBillID(ByVal vOPDBillID As Long)
    varOPDBillID = vOPDBillID
End Property

Public Property Get OPDBillID() As Long
    OPDBillID = varOPDBillID
End Property

Public Property Let LabBillID(ByVal vLabBillID As Long)
    varLabBillID = vLabBillID
End Property

Public Property Get LabBillID() As Long
    LabBillID = varLabBillID
End Property

Public Property Let MedicalTestBillID(ByVal vMedicalTestBillID As Long)
    varMedicalTestBillID = vMedicalTestBillID
End Property

Public Property Get MedicalTestBillID() As Long
    MedicalTestBillID = varMedicalTestBillID
End Property

Public Property Let HSTBillID(ByVal vHSTBillID As Long)
    varHSTBillID = vHSTBillID
End Property

Public Property Get HSTBillID() As Long
    HSTBillID = varHSTBillID
End Property

Public Property Let RBillID(ByVal vRBillID As Long)
    varRBillID = vRBillID
End Property

Public Property Get RBillID() As Long
    RBillID = varRBillID
End Property

Public Property Let ExpenceBillID(ByVal vExpenceBillID As Long)
    varExpenceBillID = vExpenceBillID
End Property

Public Property Get ExpenceBillID() As Long
    ExpenceBillID = varExpenceBillID
End Property

Public Property Let IncomeBillID(ByVal vIncomeBillID As Long)
    varIncomeBillID = vIncomeBillID
End Property

Public Property Get IncomeBillID() As Long
    IncomeBillID = varIncomeBillID
End Property

Public Property Let ServiceDate(ByVal vServiceDate)
    varServiceDate = vServiceDate
End Property

Public Property Get ServiceDate()
    ServiceDate = varServiceDate
End Property

Public Property Let ServiceTime(ByVal vServiceTime)
    varServiceTime = vServiceTime
End Property

Public Property Get ServiceTime()
    ServiceTime = varServiceTime
End Property

Public Property Let ServiceCategoryID(ByVal vServiceCategoryID As Long)
    varServiceCategoryID = vServiceCategoryID
End Property

Public Property Get ServiceCategoryID() As Long
    ServiceCategoryID = varServiceCategoryID
End Property

Public Property Let ServiceSubcategoryID(ByVal vServiceSubcategoryID As Long)
    varServiceSubcategoryID = vServiceSubcategoryID
End Property

Public Property Get ServiceSubcategoryID() As Long
    ServiceSubcategoryID = varServiceSubcategoryID
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let HospitalCharge(ByVal vHospitalCharge As Double)
    varHospitalCharge = vHospitalCharge
End Property

Public Property Get HospitalCharge() As Double
    HospitalCharge = varHospitalCharge
End Property

Public Property Let ProfessionalCharge(ByVal vProfessionalCharge As Double)
    varProfessionalCharge = vProfessionalCharge
End Property

Public Property Get ProfessionalCharge() As Double
    ProfessionalCharge = varProfessionalCharge
End Property

Public Property Let Charge(ByVal vCharge As Double)
    varCharge = vCharge
End Property

Public Property Get Charge() As Double
    Charge = varCharge
End Property

Public Property Let UserID(ByVal vUserID As Long)
    varUserID = vUserID
End Property

Public Property Get UserID() As Long
    UserID = varUserID
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate()
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime()
    DeletedTime = varDeletedTime
End Property

Public Property Let AddToMedicineCharge(ByVal vAddToMedicineCharge As Boolean)
    varAddToMedicineCharge = vAddToMedicineCharge
End Property

Public Property Get AddToMedicineCharge() As Boolean
    AddToMedicineCharge = varAddToMedicineCharge
End Property

Public Property Let SecessionID(ByVal vSecessionID As Long)
    varSecessionID = vSecessionID
End Property

Public Property Get SecessionID() As Long
    SecessionID = varSecessionID
End Property

Public Property Let SerialNo(ByVal vSerialNo As Long)
    varSerialNo = vSerialNo
End Property

Public Property Get SerialNo() As Long
    SerialNo = varSerialNo
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let HospitalChargeCancelled(ByVal vHospitalChargeCancelled As Double)
    varHospitalChargeCancelled = vHospitalChargeCancelled
End Property

Public Property Get HospitalChargeCancelled() As Double
    HospitalChargeCancelled = varHospitalChargeCancelled
End Property

Public Property Let ProfessionalChargeCancelled(ByVal vProfessionalChargeCancelled As Double)
    varProfessionalChargeCancelled = vProfessionalChargeCancelled
End Property

Public Property Get ProfessionalChargeCancelled() As Double
    ProfessionalChargeCancelled = varProfessionalChargeCancelled
End Property

Public Property Let ChargeCancelled(ByVal vChargeCancelled As Double)
    varChargeCancelled = vChargeCancelled
End Property

Public Property Get ChargeCancelled() As Double
    ChargeCancelled = varChargeCancelled
End Property

Public Property Let HospitaChargelReturned(ByVal vHospitaChargelReturned As Double)
    varHospitaChargelReturned = vHospitaChargelReturned
End Property

Public Property Get HospitaChargelReturned() As Double
    HospitaChargelReturned = varHospitaChargelReturned
End Property

Public Property Let ProfessionalChargeReturned(ByVal vProfessionalChargeReturned As Double)
    varProfessionalChargeReturned = vProfessionalChargeReturned
End Property

Public Property Get ProfessionalChargeReturned() As Double
    ProfessionalChargeReturned = varProfessionalChargeReturned
End Property

Public Property Let ChargeReturned(ByVal vChargeReturned As Double)
    varChargeReturned = vChargeReturned
End Property

Public Property Get ChargeReturned() As Double
    ChargeReturned = varChargeReturned
End Property

Public Property Let HospitalChargePaid(ByVal vHospitalChargePaid As Double)
    varHospitalChargePaid = vHospitalChargePaid
End Property

Public Property Get HospitalChargePaid() As Double
    HospitalChargePaid = varHospitalChargePaid
End Property

Public Property Let ProfessionalChargePaid(ByVal vProfessionalChargePaid As Double)
    varProfessionalChargePaid = vProfessionalChargePaid
End Property

Public Property Get ProfessionalChargePaid() As Double
    ProfessionalChargePaid = varProfessionalChargePaid
End Property

Public Property Let ChargePaid(ByVal vChargePaid As Double)
    varChargePaid = vChargePaid
End Property

Public Property Get ChargePaid() As Double
    ChargePaid = varChargePaid
End Property

Public Property Let HospitalChargeToPay(ByVal vHospitalChargeToPay As Double)
    varHospitalChargeToPay = vHospitalChargeToPay
End Property

Public Property Get HospitalChargeToPay() As Double
    HospitalChargeToPay = varHospitalChargeToPay
End Property

Public Property Let ProfessionalChargeToPay(ByVal vProfessionalChargeToPay As Double)
    varProfessionalChargeToPay = vProfessionalChargeToPay
End Property

Public Property Get ProfessionalChargeToPay() As Double
    ProfessionalChargeToPay = varProfessionalChargeToPay
End Property

Public Property Let ChargeToPay(ByVal vChargeToPay As Double)
    varChargeToPay = vChargeToPay
End Property

Public Property Get ChargeToPay() As Double
    ChargeToPay = varChargeToPay
End Property

Public Property Let HospitalChargeToGive(ByVal vHospitalChargeToGive As Double)
    varHospitalChargeToGive = vHospitalChargeToGive
End Property

Public Property Get HospitalChargeToGive() As Double
    HospitalChargeToGive = varHospitalChargeToGive
End Property

Public Property Let ProfessionalChargeToGive(ByVal vProfessionalChargeToGive As Double)
    varProfessionalChargeToGive = vProfessionalChargeToGive
End Property

Public Property Get ProfessionalChargeToGive() As Double
    ProfessionalChargeToGive = varProfessionalChargeToGive
End Property

Public Property Let ChargeToGive(ByVal vChargeToGive As Double)
    varChargeToGive = vChargeToGive
End Property

Public Property Get ChargeToGive() As Double
    ChargeToGive = varChargeToGive
End Property


