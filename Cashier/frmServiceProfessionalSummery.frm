VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmServiceProfessionalSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Service Professional Summery By Booking Date"
   ClientHeight    =   8820
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13320
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8820
   ScaleWidth      =   13320
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   6000
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   6120
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   8400
      Width           =   4695
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   600
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   8400
      Width           =   4695
   End
   Begin MSFlexGridLib.MSFlexGrid gridSummery 
      Height          =   7095
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   12515
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   11880
      TabIndex        =   4
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   2280
      TabIndex        =   5
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   115802115
      CurrentDate     =   39969
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   2280
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   375
      Left            =   8640
      TabIndex        =   7
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   7320
      TabIndex        =   8
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   2280
      TabIndex        =   13
      Top             =   600
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label2 
      Caption         =   "User"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1080
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "Booking Date"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label11 
      Caption         =   "Service"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   600
      Width           =   2175
   End
   Begin VB.Label cccc 
      Caption         =   "Paper"
      Height          =   255
      Left            =   5520
      TabIndex        =   10
      Top             =   8400
      Width           =   1815
   End
   Begin VB.Label bbbb 
      Caption         =   "Printer"
      Height          =   255
      Left            =   -120
      TabIndex        =   9
      Top             =   8400
      Width           =   1815
   End
End
Attribute VB_Name = "frmServiceProfessionalSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim fillinfGrid As Boolean
    Dim fillingGrid As Boolean

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridSummery, "Professional Summery", "Date : " & Format(dtpDate.Value, "dd MM yy"), True
End Sub

Private Sub btnPrint_Click()
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    With ThisReportFormat
        .ColSpace = 500
        .SubTopicFontSize = 8
    End With
    GridPrint gridSummery, ThisReportFormat, "Professional Summery", "Date : " & Format(dtpDate.Value, "dd MM yy")
    Printer.EndDoc
End Sub


Private Sub searchAndAdd(hosFee As Double, proFee As Double, staffName As String)
    Dim staffExists As Boolean
    staffExists = False
    Dim MyRow As Integer
    Dim rowHf As Double
    Dim rowPf As Double
    With gridSummery
        Dim i As Integer
        For i = 1 To .Rows - 1
            If (.TextMatrix(i, 0)) = staffName Then
                staffExists = True
                MyRow = i
            End If
        Next
        If staffExists = False Then
            .Rows = .Rows + 1
            MyRow = .Rows - 1
            .TextMatrix(MyRow, 0) = staffName
        End If
        
        .Row = MyRow
        
        .Col = 1
        .text = Val(.text) + proFee
        .text = Format(Val(.text), "0.00")
        rowHf = Val(.text)
        
        .Col = 2
        .text = Val(.text) + hosFee
        .text = Format(Val(.text), "0.00")
        rowPf = Val(.text)
        
        .Col = 3
        .text = Format(rowHf + rowPf, "0.00")
        
        .Col = 4
        .text = Val(.text) + 1
        
    End With

End Sub

Private Sub FormatGrid()
    With gridSummery
        .Rows = 1
        .Cols = 5
        .Row = 0
        .Col = 0
        .text = "Doctor/Tehcnician"
        .Col = 1
        .text = "Professional Charges"
        .Col = 2
        .text = "Hospital Charges"
        .Col = 3
        .text = "Total Charges"
        .Col = 4
        .text = "Count"
    End With
End Sub

Private Sub FillGrid()
    fillingGrid = True
    FormatGrid
    Dim TotalCount As Double
    Dim TotalValue As Double
    Dim TotalPFee As Double
    
    TotalCount = 0
    TotalValue = 0
    TotalPFee = 0
    Dim TemString As String
    Dim rsTem As New ADODB.Recordset
    
    Dim ib As New IncomeBill
    Dim ps As New PatientService
    Dim pfo As New ProfessionalCharge
    Dim stf As New Staff
    Dim tt As New Title
    
    Dim temSql As String
    
    With rsTem
        If .State = 1 Then .Close
        temSql = "SELECT IncomeBillID, DisplayBillID, BillSettled, Cancelled, BHTID, PatientID, CompletedDate, NetTotal, NetTotalToPay " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE      (CompletedDate BETWEEN CONVERT(DATETIME, '" & dtpDate.Value & "', 102) AND CONVERT(DATETIME, '" & dtpDate.Value & "', 102)) AND (IsRBill = 1) AND (Completed = 1)  AND (Cancelled = 0) " & _
                    "ORDER BY DisplayBillID"
        .Open temSql, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            
            If Val(cmbCat.BoundText) = 0 Or serviceCategoryPresent(!IncomeBillID, Val(cmbCat.BoundText)) Then
                    
                    
                    Dim strTem  As String
                    Dim rsTem1 As New ADODB.Recordset
                    Dim rsTem2 As New ADODB.Recordset
                    Dim dblNo1 As Double
                    strTem = ""
                    dblNo1 = 0
                    If rsTem1.State = 1 Then rsTem1.Close
                    temSql = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & !IncomeBillID
                    rsTem1.Open temSql, cnnStores, adOpenStatic, adLockReadOnly
                    Dim hosFee As Double
                    Dim profFee As Double
                    While rsTem1.EOF = False
                        temSql = "SELECT * FROM tblProfessionalCharges Where PatientServiceID = " & rsTem1!PatientServiceID
                        
                        
                        
                        
                        If rsTem2.State = 1 Then rsTem2.Close
                        rsTem2.Open temSql, cnnStores, adOpenStatic, adLockReadOnly
                        Dim myStaff As New clsStaff
                        Dim i As Integer
                        i = 0
                        
                        While rsTem2.EOF = False
                            stf.StaffID = rsTem2!StaffID
                            tt.TitleID = stf.TitleID
                            If i = 0 Then
                                 searchAndAdd rsTem1!HospitalCharge, rsTem2!Fee, tt.Title & " " & stf.Name
                            Else
                                 searchAndAdd 0, rsTem2!Fee, tt.Title & " " & stf.Name
                            End If
                            i = i + 1
                            rsTem2.MoveNext
                        Wend
                        rsTem2.Close
                        rsTem1.MoveNext
                    Wend
                    rsTem1.Close
                    
            End If
        
            .MoveNext
        Wend
        .Close
  
        
        
    End With
    fillingGrid = False
    
End Sub



Private Sub btnProcess_Click()
    FillGrid
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbCat.text = Empty
    End If
End Sub

Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbUser.text = Empty
End Sub

Private Sub fillCombos()
    Dim cat As New clsFillCombos
    cat.FillAnyCombo cmbCat, "ServiceCategory", True
'    Dim PayM As New clsFillCombos
'    PayM.FillBoolCombo cmbPm, "PaymentMethod", "PaymentMethod", "FinalPay", False
    Dim Cashier As New clsFillCombos
    Cashier.FillSpecificField cmbUser, "Staff", "Name", False

End Sub

Private Sub Form_Load()
fillingGrid = False
    fillCombos
    GetSettings
End Sub

Private Sub GetSettings()
    'GetCommonSettings Me
    dtpDate.Value = Date
End Sub

Private Sub SaveSettings()
   'saveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub






