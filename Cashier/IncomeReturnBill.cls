VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IncomeReturnBill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varIncomeReturnBillID As Long
    Private varReturnDate
    Private varReturnTime
    Private varReturnUserID As Long
    Private varCancelled As Boolean
    Private varCancelledDate
    Private varCancelledTime
    Private varCancelledUserID As Long
    Private varReturnValue As Double
    Private varIncomeBillID As Long
    Private varBHTID As Long
    Private varPaymentMethodID As Long
    Private varupsize_ts
    Private varPaymentComments As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblIncomeReturnBill Where IncomeReturnBillID = " & varIncomeReturnBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !ReturnDate = varReturnDate
        !ReturnTime = varReturnTime
        !ReturnUserID = varReturnUserID
        !Cancelled = varCancelled
        !CancelledDate = varCancelledDate
        !CancelledTime = varCancelledTime
        !CancelledUserID = varCancelledUserID
        !ReturnValue = varReturnValue
        !IncomeBillID = varIncomeBillID
        !BHTID = varBHTID
        !PaymentMethodID = varPaymentMethodID
        !PaymentComments = varPaymentComments
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varIncomeReturnBillID = !NewID
        Else
            varIncomeReturnBillID = !IncomeReturnBillID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblIncomeReturnBill WHERE IncomeReturnBillID = " & varIncomeReturnBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!IncomeReturnBillID) Then
               varIncomeReturnBillID = !IncomeReturnBillID
            End If
            If Not IsNull(!ReturnDate) Then
               varReturnDate = !ReturnDate
            End If
            If Not IsNull(!ReturnTime) Then
               varReturnTime = !ReturnTime
            End If
            If Not IsNull(!ReturnUserID) Then
               varReturnUserID = !ReturnUserID
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!CancelledDate) Then
               varCancelledDate = !CancelledDate
            End If
            If Not IsNull(!CancelledTime) Then
               varCancelledTime = !CancelledTime
            End If
            If Not IsNull(!CancelledUserID) Then
               varCancelledUserID = !CancelledUserID
            End If
            If Not IsNull(!ReturnValue) Then
               varReturnValue = !ReturnValue
            End If
            If Not IsNull(!IncomeBillID) Then
               varIncomeBillID = !IncomeBillID
            End If
            If Not IsNull(!BHTID) Then
               varBHTID = !BHTID
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!PaymentComments) Then
               varPaymentComments = !PaymentComments
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varIncomeReturnBillID = 0
    varReturnDate = Empty
    varReturnTime = Empty
    varReturnUserID = 0
    varCancelled = False
    varCancelledDate = Empty
    varCancelledTime = Empty
    varCancelledUserID = 0
    varReturnValue = 0
    varIncomeBillID = 0
    varBHTID = 0
    varPaymentMethodID = 0
    varupsize_ts = Empty
    varPaymentComments = Empty
End Sub

Public Property Let IncomeReturnBillID(ByVal vIncomeReturnBillID As Long)
    Call clearData
    varIncomeReturnBillID = vIncomeReturnBillID
    Call loadData
End Property

Public Property Get IncomeReturnBillID() As Long
    IncomeReturnBillID = varIncomeReturnBillID
End Property

Public Property Let ReturnDate(ByVal vReturnDate)
    varReturnDate = vReturnDate
End Property

Public Property Get ReturnDate()
    ReturnDate = varReturnDate
End Property

Public Property Let ReturnTime(ByVal vReturnTime)
    varReturnTime = vReturnTime
End Property

Public Property Get ReturnTime()
    ReturnTime = varReturnTime
End Property

Public Property Let ReturnUserID(ByVal vReturnUserID As Long)
    varReturnUserID = vReturnUserID
End Property

Public Property Get ReturnUserID() As Long
    ReturnUserID = varReturnUserID
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledDate(ByVal vCancelledDate)
    varCancelledDate = vCancelledDate
End Property

Public Property Get CancelledDate()
    CancelledDate = varCancelledDate
End Property

Public Property Let CancelledTime(ByVal vCancelledTime)
    varCancelledTime = vCancelledTime
End Property

Public Property Get CancelledTime()
    CancelledTime = varCancelledTime
End Property

Public Property Let CancelledUserID(ByVal vCancelledUserID As Long)
    varCancelledUserID = vCancelledUserID
End Property

Public Property Get CancelledUserID() As Long
    CancelledUserID = varCancelledUserID
End Property

Public Property Let ReturnValue(ByVal vReturnValue As Double)
    varReturnValue = vReturnValue
End Property

Public Property Get ReturnValue() As Double
    ReturnValue = varReturnValue
End Property

Public Property Let IncomeBillID(ByVal vIncomeBillID As Long)
    varIncomeBillID = vIncomeBillID
End Property

Public Property Get IncomeBillID() As Long
    IncomeBillID = varIncomeBillID
End Property

Public Property Let BHTID(ByVal vBHTID As Long)
    varBHTID = vBHTID
End Property

Public Property Get BHTID() As Long
    BHTID = varBHTID
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let PaymentComments(ByVal vPaymentComments As String)
    varPaymentComments = vPaymentComments
End Property

Public Property Get PaymentComments() As String
    PaymentComments = varPaymentComments
End Property


