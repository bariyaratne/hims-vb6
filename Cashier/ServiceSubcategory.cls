VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ServiceSubcategory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varServiceSubcategoryID As Long
    Private varServiceSubcategoryCode As String
    Private varServiceSubcategory As String
    Private varServiceCategoryID As Long
    Private varcomments As String
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate
    Private varDeletedTime
    Private varFee As Double
    Private varCanChange As Boolean
    Private varToMedicineCharge As Boolean
    Private varForOPD As Boolean
    Private varForBHT As Boolean
    Private varForGSB As Boolean
    Private varForMT As Boolean
    Private varForHST As Boolean
    Private varForLab As Boolean
    Private varForR As Boolean
    Private varForIncome As Boolean
    Private varForExpence As Boolean
    Private varupsize_ts
    Private varHourlyRate As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblServiceSubcategory Where ServiceSubcategoryID = " & varServiceSubcategoryID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !ServiceSubcategoryCode = varServiceSubcategoryCode
        !ServiceSubcategory = varServiceSubcategory
        !ServiceCategoryID = varServiceCategoryID
        !comments = varcomments
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !Fee = varFee
        !CanChange = varCanChange
        !ToMedicineCharge = varToMedicineCharge
        !ForOPD = varForOPD
        !ForBHT = varForBHT
        !ForGSB = varForGSB
        !ForMT = varForMT
        !ForHST = varForHST
        !ForLab = varForLab
        !ForR = varForR
        !ForIncome = varForIncome
        !ForExpence = varForExpence
        !HourlyRate = varHourlyRate
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varServiceSubcategoryID = !NewID
        Else
            varServiceSubcategoryID = !ServiceSubcategoryID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblServiceSubcategory WHERE ServiceSubcategoryID = " & varServiceSubcategoryID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!ServiceSubcategoryID) Then
               varServiceSubcategoryID = !ServiceSubcategoryID
            End If
            If Not IsNull(!ServiceSubcategoryCode) Then
               varServiceSubcategoryCode = !ServiceSubcategoryCode
            End If
            If Not IsNull(!ServiceSubcategory) Then
               varServiceSubcategory = !ServiceSubcategory
            End If
            If Not IsNull(!ServiceCategoryID) Then
               varServiceCategoryID = !ServiceCategoryID
            End If
            If Not IsNull(!comments) Then
               varcomments = !comments
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!Fee) Then
               varFee = !Fee
            End If
            If Not IsNull(!CanChange) Then
               varCanChange = !CanChange
            End If
            If Not IsNull(!ToMedicineCharge) Then
               varToMedicineCharge = !ToMedicineCharge
            End If
            If Not IsNull(!ForOPD) Then
               varForOPD = !ForOPD
            End If
            If Not IsNull(!ForBHT) Then
               varForBHT = !ForBHT
            End If
            If Not IsNull(!ForGSB) Then
               varForGSB = !ForGSB
            End If
            If Not IsNull(!ForMT) Then
               varForMT = !ForMT
            End If
            If Not IsNull(!ForHST) Then
               varForHST = !ForHST
            End If
            If Not IsNull(!ForLab) Then
               varForLab = !ForLab
            End If
            If Not IsNull(!ForR) Then
               varForR = !ForR
            End If
            If Not IsNull(!ForIncome) Then
               varForIncome = !ForIncome
            End If
            If Not IsNull(!ForExpence) Then
               varForExpence = !ForExpence
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!HourlyRate) Then
               varHourlyRate = !HourlyRate
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varServiceSubcategoryID = 0
    varServiceSubcategoryCode = Empty
    varServiceSubcategory = Empty
    varServiceCategoryID = 0
    varcomments = Empty
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varFee = 0
    varCanChange = False
    varToMedicineCharge = False
    varForOPD = False
    varForBHT = False
    varForGSB = False
    varForMT = False
    varForHST = False
    varForLab = False
    varForR = False
    varForIncome = False
    varForExpence = False
    varupsize_ts = Empty
    varHourlyRate = False
End Sub

Public Property Let ServiceSubcategoryID(ByVal vServiceSubcategoryID As Long)
    Call clearData
    varServiceSubcategoryID = vServiceSubcategoryID
    Call loadData
End Property

Public Property Get ServiceSubcategoryID() As Long
    ServiceSubcategoryID = varServiceSubcategoryID
End Property

Public Property Let ServiceSubcategoryCode(ByVal vServiceSubcategoryCode As String)
    varServiceSubcategoryCode = vServiceSubcategoryCode
End Property

Public Property Get ServiceSubcategoryCode() As String
    ServiceSubcategoryCode = varServiceSubcategoryCode
End Property

Public Property Let ServiceSubcategory(ByVal vServiceSubcategory As String)
    varServiceSubcategory = vServiceSubcategory
End Property

Public Property Get ServiceSubcategory() As String
    ServiceSubcategory = varServiceSubcategory
End Property

Public Property Let ServiceCategoryID(ByVal vServiceCategoryID As Long)
    varServiceCategoryID = vServiceCategoryID
End Property

Public Property Get ServiceCategoryID() As Long
    ServiceCategoryID = varServiceCategoryID
End Property

Public Property Let comments(ByVal vcomments As String)
    varcomments = vcomments
End Property

Public Property Get comments() As String
    comments = varcomments
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate()
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime()
    DeletedTime = varDeletedTime
End Property

Public Property Let Fee(ByVal vFee As Double)
    varFee = vFee
End Property

Public Property Get Fee() As Double
    Fee = varFee
End Property

Public Property Let CanChange(ByVal vCanChange As Boolean)
    varCanChange = vCanChange
End Property

Public Property Get CanChange() As Boolean
    CanChange = varCanChange
End Property

Public Property Let ToMedicineCharge(ByVal vToMedicineCharge As Boolean)
    varToMedicineCharge = vToMedicineCharge
End Property

Public Property Get ToMedicineCharge() As Boolean
    ToMedicineCharge = varToMedicineCharge
End Property

Public Property Let ForOPD(ByVal vForOPD As Boolean)
    varForOPD = vForOPD
End Property

Public Property Get ForOPD() As Boolean
    ForOPD = varForOPD
End Property

Public Property Let ForBHT(ByVal vForBHT As Boolean)
    varForBHT = vForBHT
End Property

Public Property Get ForBHT() As Boolean
    ForBHT = varForBHT
End Property

Public Property Let ForGSB(ByVal vForGSB As Boolean)
    varForGSB = vForGSB
End Property

Public Property Get ForGSB() As Boolean
    ForGSB = varForGSB
End Property

Public Property Let ForMT(ByVal vForMT As Boolean)
    varForMT = vForMT
End Property

Public Property Get ForMT() As Boolean
    ForMT = varForMT
End Property

Public Property Let ForHST(ByVal vForHST As Boolean)
    varForHST = vForHST
End Property

Public Property Get ForHST() As Boolean
    ForHST = varForHST
End Property

Public Property Let ForLab(ByVal vForLab As Boolean)
    varForLab = vForLab
End Property

Public Property Get ForLab() As Boolean
    ForLab = varForLab
End Property

Public Property Let ForR(ByVal vForR As Boolean)
    varForR = vForR
End Property

Public Property Get ForR() As Boolean
    ForR = varForR
End Property

Public Property Let ForIncome(ByVal vForIncome As Boolean)
    varForIncome = vForIncome
End Property

Public Property Get ForIncome() As Boolean
    ForIncome = varForIncome
End Property

Public Property Let ForExpence(ByVal vForExpence As Boolean)
    varForExpence = vForExpence
End Property

Public Property Get ForExpence() As Boolean
    ForExpence = varForExpence
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let HourlyRate(ByVal vHourlyRate As Boolean)
    varHourlyRate = vHourlyRate
End Property

Public Property Get HourlyRate() As Boolean
    HourlyRate = varHourlyRate
End Property


