Attribute VB_Name = "modPrintBill"
Option Explicit
    Dim CsetPrinter As New cSetDfltPrinter
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    
    Dim temSQL As String

    Dim LeftMargin As Double
    Dim TopMargin As Double
    Dim myY As Double
    Dim myLabelX As Double
    Dim myValX As Double
    Dim myNuX As Double
    Dim myColan As Double
    Dim centre As Double
    
    Dim CenterX As Long
    Dim FieldX As Long
    Dim NoX As Long
    Dim ValueX As Long
    Dim AllLines() As String
    
    Dim printLower As String
    Dim printUpper As String
    
    
    Dim footerStart As Double
    Dim paperCut As Double

    Dim temY As Long
    Dim n As Long
    
    Dim printingBill As New IncomeBill
    Dim printingCat As New ServiceCategory
    Dim printingPt As New Patient
    Dim printingBht As New BHT
    
    


Public Sub printAnyBill(IncomeBillID As Long, billingPrinter As String, billingPaper As String, billingForm As Form)
    
    printingBill.IncomeBillID = IncomeBillID
    printingCat.ServiceCategoryID = printingBill.CategoryId
    printingPt.PatientID = printingBill.PatientID
    printingBht.BHTID = printingBill.BHTID
    

    
    Dim temBillPoints As MyBillPoints
    
    CsetPrinter.SetPrinterAsDefault (billingPrinter): DoEvents
    Dim MyFOnt As ReportFont

    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle): DoEvents
    End If
    CsetPrinter.SetPrinterAsDefault (billingPrinter): DoEvents
    
    
    If SelectForm(billingPaper, billingForm.hwnd) <> 1 Then
        MsgBox "Printer Error. Please double check printer and paper size"
        Printer.KillDoc
        
    End If
    
    Call setMargins
    Call printHeading
    
    
    Printer.Print
    Printer.Print
    
        
    If printingCat.PrintTotalOnly = True Then
        Call printWithoutProfessionals
    Else
        Call printWithProfessionals
    End If

    myY = paperCut - 1440 * 0.75

    Printer.FontSize = "11"
    Printer.FontBold = False

    Printer.CurrentX = myNuX - Printer.TextWidth(printUpper)
    Printer.CurrentY = myY
    Printer.Print printUpper
    
    Printer.FontSize = "11"
    Printer.FontBold = False

    myY = Printer.CurrentY
    Printer.CurrentX = myLabelX
    Printer.CurrentY = myY
    Printer.Print "Total Fee"
    
    Printer.FontSize = "14"
    Printer.FontBold = True
    
    Printer.CurrentX = myNuX - Printer.TextWidth(Format(printingBill.GrossTotal, "#,##0.00"))
    Printer.CurrentY = myY
    Printer.Print Format(printingBill.GrossTotal, "#,##0.00")

    myY = Printer.CurrentY
    Printer.FontSize = "11"
    Printer.FontBold = False

    Printer.CurrentX = myNuX - Printer.TextWidth(printUpper)
    Printer.CurrentY = myY
    Printer.Print printLower

    printFooter
                        
                        
    Printer.EndDoc

End Sub


Private Sub printWithProfessionals()
        Dim strTem  As String
        Dim rsTem1 As New ADODB.Recordset
        Dim rsTem2 As New ADODB.Recordset
        Dim dblNo1 As Double
        strTem = ""
        dblNo1 = 0
        If rsTem1.State = 1 Then rsTem1.Close
        temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & printingBill.IncomeBillID
        rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        
        Dim ptSv As New PatientService
        ptSv.PatientServiceID = rsTem1!PatientServiceID
        
        While rsTem1.EOF = False
            
                
                
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Appointment Date"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print Format(ptSv.ServiceDate, "dd MMM yyyy")
            
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Appointment Time"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print Format(ptSv.ServiceTime, "hh:mm AMPM")
            
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Appointment No"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            
            Printer.FontSize = "16"
            Printer.FontBold = True
            
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print ptSv.serialNo
            
            
            
            Printer.FontSize = "11"
            Printer.Print
            Printer.FontBold = False
                    
            
            
            temSQL = "SELECT * From tblProfessionalCharges WHERE  PatientServiceID = " & rsTem1!PatientServiceID
            If rsTem2.State = 1 Then rsTem2.Close
            rsTem2.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            Dim myStaff As New clsStaff
            While rsTem2.EOF = False
                        
                Dim TemStaff As New Staff
                Dim temTitle As New Title
                Dim temSpe As New Speciality
                
                TemStaff.StaffID = rsTem2!StaffID
                temSpe.SpecialityID = TemStaff.SpecialityID
                temTitle.TitleID = TemStaff.TitleID
                
                myY = Printer.CurrentY
                Printer.CurrentX = myLabelX
                Printer.CurrentY = myY
                Printer.Print temSpe.Speciality
                Printer.CurrentX = myColan
                Printer.CurrentY = myY
                Printer.Print ":"
                Printer.CurrentX = myValX
                Printer.CurrentY = myY
                Printer.Print temTitle.Title & " " & TemStaff.Name
            
            
                myY = Printer.CurrentY
                Printer.CurrentX = myLabelX
                Printer.CurrentY = myY
                Printer.Print "Charge"
                Printer.CurrentX = myColan
                Printer.CurrentY = myY
                Printer.Print ":"
                
                Printer.FontSize = "11"
                Printer.FontBold = True
                
                Printer.CurrentX = myValX
                Printer.CurrentY = myY
                Printer.Print Format(rsTem2!Fee, "#,##0.00")
        
            
                            rsTem2.MoveNext
                        Wend
                        rsTem2.Close
                        
                        rsTem1.MoveNext
                                
                        Printer.FontSize = "11"
                        Printer.FontBold = False
                    
                        myY = Printer.CurrentY
                        Printer.CurrentX = myLabelX
                        Printer.CurrentY = myY
                        Printer.Print "Hospital Fee"
                        Printer.CurrentX = myColan
                        Printer.CurrentY = myY
                        Printer.Print ":"
                        
                        Printer.FontSize = "11"
                        Printer.FontBold = True
                        
                        Printer.CurrentX = myValX
                        Printer.CurrentY = myY
                        Printer.Print Format(ptSv.HospitalCharge, "#,##0.00")
                    
                    
                    Wend
                    rsTem1.Close
                    

End Sub

Private Sub printWithoutProfessionals()
    Dim strTem  As String
    Dim rsTem1 As New ADODB.Recordset
    Dim rsTem2 As New ADODB.Recordset
    Dim dblNo1 As Double
    strTem = ""
    dblNo1 = 0
    If rsTem1.State = 1 Then rsTem1.Close
    temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & printingBill.IncomeBillID
    rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    While rsTem1.EOF = False
        Dim temPtSv As New PatientService
        Dim temSub As New ServiceSubcategory
        
        temPtSv.PatientServiceID = rsTem1!PatientServiceID
        temSub.ServiceSubcategoryID = temPtSv.ServiceSubcategoryID
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Number"
        Printer.CurrentX = myNuX - Printer.TextWidth(temPtSv.serialNo)
        Printer.CurrentY = myY
        Printer.Print Format((temPtSv.serialNo))
        
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temSub.ServiceSubcategory
        Printer.CurrentX = myNuX - Printer.TextWidth(Format(temPtSv.Charge, "#,##0.00"))
        Printer.CurrentY = myY
        Printer.Print Format(temPtSv.Charge, "#,##0.00")
        
        rsTem1.MoveNext
    Wend
    rsTem1.Close
        
    Printer.Print
    Printer.Print
    

End Sub

Private Sub setMargins()
        LeftMargin = 0
        TopMargin = 1440 * 0.8
        centre = Printer.Width / 2
        myLabelX = 1440 * 0.8
        myValX = 1440 * 2.5
        myNuX = Printer.Width - 1440 * 0.7
        myColan = 1440 * 1.9
        footerStart = Printer.Height - 1440 * 1.5
        paperCut = Printer.Height - 1440 * 1
    
        printUpper = "_____________"
        printLower = "============="
End Sub



Private Sub printHeading()

        

        
        Printer.FontBold = True
        Printer.Font = "Arial"
        Printer.FontSize = "14"
        Printer.Print
        
        
        Printer.CurrentY = TopMargin
        Printer.CurrentX = centre - (Printer.TextWidth(printingCat.ServiceCategory) / 2)
        Printer.Print printingCat.ServiceCategory
        
        Printer.Print
        
        Printer.Font = "Arial"
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Ref. No."
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print printingBill.DisplayBillID
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Name"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print printingPt.FirstName

        If printingPt.NICNo <> "" Then
        
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "NIC"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print printingPt.NICNo
        
        
        End If


        If printingBill.BHTID <> 0 Then
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "BHT"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print printingBht.BHT
        End If


End Sub

Private Sub printFooter()
    Dim less As Long
    less = 1440 * 0.1
    Dim temUser As New Staff
    temUser.StaffID = printingBill.CompletedUserID
    Dim temDisplace As Double
    Dim temDisLow As Double
    temDisLow = 1440 * 0.25
    temDisplace = 1400 * 2.5
    Printer.Font.Size = 9
        myY = footerStart + 1440 * 0.2
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "User"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX + temDisplace
        Printer.CurrentY = myY + temDisLow
        Printer.Print DecreptedWord(temUser.UserName);
        
        myY = Printer.CurrentY - less
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "Billed Time"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX + temDisplace
        Printer.CurrentY = myY + temDisLow
        Printer.Print Format(printingBill.CompletedTime, "hh:mm AMPM");
        
        myY = Printer.CurrentY - less
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "Billed Date"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX + temDisplace
        Printer.CurrentY = myY + temDisLow
        Printer.Print Format(printingBill.CompletedDate, "dd MMM yyyy")
        
End Sub

Public Sub printAnyBillOld(IncomeBillID As Long, billingPrinter As String, billingPaper As String, billingForm As Form)
    
    Dim temBillPoints As MyBillPoints
    
    CsetPrinter.SetPrinterAsDefault (billingPrinter): DoEvents
    Dim MyFOnt As ReportFont

    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle): DoEvents
    End If
    CsetPrinter.SetPrinterAsDefault (billingPrinter): DoEvents
    
    Dim CenterX As Long
    Dim FieldX As Long
    Dim NoX As Long
    Dim ValueX As Long
    Dim AllLines() As String
    Dim i As Integer
    Dim temY As Long
    Dim n As Long
    
    With MyFOnt
        .Name = DefaultFont.Name
        .Bold = False
        .Italic = False
        .Size = 9
        .Italic = False
        .Underline = False
    End With
    
    Dim temGrossTotal As String
    Dim temDiscount As Double
    Dim temNetTotal As Double
    Dim TemAgentId As Long
    Dim temCancelled As Boolean
    Dim temSettled As Boolean
    Dim temPatientName As String
    
    Dim temPrintTotalOnly As Boolean
    Dim temDocName As String
    Dim temDocFee As String
    Dim temHosFee As String
    Dim temProFee As Double
    
    Dim temBillNo As String
    Dim temAppNo As String
    Dim temCat As String
    Dim temSubCat As String
    Dim temBillTime As String
    Dim temBillDate As String
    Dim temBilledUser As String
    Dim temStaffName1 As String
    Dim temAppDate As String
    Dim temAppTime As String
    
    Dim rsTem As New ADODB.Recordset
    temSQL = "SELECT dbo.tblIncomeBill.DisplayBillID, dbo.tblIncomeBill.GrossTotal, dbo.tblIncomeBill.Discount, dbo.tblIncomeBill.NetTotal, dbo.tblIncomeBill.AgentID, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.Completed, dbo.tblIncomeBill.BillSettled, dbo.tblIncomeBill.CompletedDate, dbo.tblIncomeBill.CompletedTime, dbo.tblPatientService.RBillID, dbo.tblPatientService.PatientID, dbo.tblPatientMainDetails.FirstName, dbo.tblPatientService.PatientServiceID, dbo.tblPatientService.ServiceDate, dbo.tblPatientService.ServiceTime, dbo.tblPatientService.SerialNo, dbo.tblServiceCategory.ServiceCategory, dbo.tblServiceCategory.PrintTotalOnly, dbo.tblServiceSubcategory.ServiceSubcategory, dbo.tblIncomeBill.CompletedUserID, dbo.tblStaff.UserName, dbo.tblIncomeBill.IncomeBillID " & _
                "FROM dbo.tblStaff RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblStaff.StaffID = dbo.tblIncomeBill.CompletedUserID LEFT OUTER JOIN dbo.tblServiceSubcategory RIGHT OUTER JOIN dbo.tblPatientService ON dbo.tblServiceSubcategory.ServiceSubcategoryID = dbo.tblPatientService.ServiceSubcategoryID LEFT OUTER JOIN dbo.tblServiceCategory ON dbo.tblPatientService.ServiceCategoryID = dbo.tblServiceCategory.ServiceCategoryID ON dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN dbo.tblPatientMainDetails ON dbo.tblPatientService.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                "Where (dbo.tblIncomeBill.Completed = 1) And (dbo.tblPatientService.Deleted = 0) And (dbo.tblIncomeBill.incomeBillId = " & IncomeBillID & ") " & _
                "ORDER BY dbo.tblIncomeBill.IncomeBillID"
    With rsTem
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!PrintTotalOnly) = False Then temPrintTotalOnly = !PrintTotalOnly
            
            temGrossTotal = Format(![GrossTotal], "#,##0.00")
            If IsNull(!Discount) = False Then temDiscount = ![Discount]
            
            
            
            
            
            temNetTotal = ![NetTotal]
            TemAgentId = ![AgentID]
            If IsNull(!Cancelled) = False Then temCancelled = ![Cancelled]
            temSettled = ![BillSettled]
            temPatientName = ![FirstName]
            
            temBillNo = ![DisplayBillID]
            temAppNo = ![serialNo]
            temCat = ![ServiceCategory]
            temSubCat = ![ServiceSubcategory]
            temBillTime = Format(![CompletedTime], "hh:mm AMPM")
            temBillDate = Format(![CompletedDate], "dd MMMM yyyy")
            temBilledUser = DecreptedWord(![UserName])
            temAppDate = Format(![ServiceDate], "dd MMMM yyyy")
            temAppTime = Format(![ServiceTime], "hh:mm AMPM")
            
            
            
    

            Dim strTem  As String
            Dim strFee As String
            Dim rsTem1 As New ADODB.Recordset
            Dim rsTem2 As New ADODB.Recordset
            Dim dblNo1 As Double
            strTem = ""
            dblNo1 = 0
            If rsTem1.State = 1 Then rsTem1.Close
            temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & !IncomeBillID
            rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While rsTem1.EOF = False
                temSQL = "SELECT * From tblProfessionalCharges WHERE  PatientServiceID = " & rsTem1!PatientServiceID
                If rsTem2.State = 1 Then rsTem2.Close
                rsTem2.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
                Dim myStaff As New clsStaff
                While rsTem2.EOF = False
                    myStaff.StaffID = rsTem2!StaffID
                    If strTem = "" Then
                        strTem = myStaff.Name
                    Else
                        strTem = strTem & vbNewLine & myStaff.Name
                    End If
                    If strFee = "" Then
                        strFee = Format(rsTem2!Fee, "#,#0.00")
                    Else
                        strFee = strFee & vbNewLine & Format(rsTem2!Fee, "#,#0.00")
                    End If
                    dblNo1 = dblNo1 + rsTem2!Fee
                    rsTem2.MoveNext
                Wend
                rsTem2.Close
                rsTem1.MoveNext
            Wend
            rsTem1.Close
            
            temDocFee = strFee
            temHosFee = Format(temNetTotal - dblNo1, "#,#0.00")
        Else
            Printer.KillDoc
            Exit Sub
        End If
    End With
    
    Dim LeftMargin As Double
    Dim TopMargin As Double
    Dim myY As Double
    Dim myLabelX As Double
    Dim myValX As Double
    Dim myColan As Double
    Dim centre As Double
    
    

    
    
    If SelectForm(billingPaper, billingForm.hwnd) = 1 Then
            
        LeftMargin = 0
        TopMargin = 1440 * 0.8
        centre = Printer.Width / 2
        myLabelX = 1440 * 0.5
        myValX = 1440 * 2#
        myColan = 1440 * 1.9
        
        Printer.FontBold = True
        Printer.Font = "Arial"
        Printer.FontSize = "14"
        Printer.Print
        
        
        Printer.CurrentY = TopMargin
        Printer.CurrentX = centre - (Printer.TextWidth(temCat) / 2)
        Printer.Print temCat
        
        Printer.Print
        
        Printer.Font = "Arial"
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Ref. No."
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temBillNo
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Name"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temPatientName
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Service"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temSubCat
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Appointment Date"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temAppDate
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Appointment Time"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temAppTime
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Appointment No"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        
        Printer.FontSize = "16"
        Printer.FontBold = True
        
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temAppNo
        
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        If temPrintTotalOnly = True Then
        
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Charge"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            
            Printer.FontSize = "14"
            Printer.FontBold = True
            
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print temGrossTotal
        
        
        Else
        
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Doctor Fee"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            
            Printer.FontSize = "14"
            Printer.FontBold = True
            
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print temDocFee
            
        
            
            Printer.FontSize = "11"
            Printer.FontBold = False
        
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Hospital Fee"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            
            Printer.FontSize = "14"
            Printer.FontBold = True
            
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print temHosFee
            
             
            Printer.FontSize = "11"
            Printer.FontBold = False
        
            myY = Printer.CurrentY
            Printer.CurrentX = myLabelX
            Printer.CurrentY = myY
            Printer.Print "Total Fee"
            Printer.CurrentX = myColan
            Printer.CurrentY = myY
            Printer.Print ":"
            
            Printer.FontSize = "14"
            Printer.FontBold = True
            
            Printer.CurrentX = myValX
            Printer.CurrentY = myY
            Printer.Print temGrossTotal
        
        
        End If
        
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        
        myY = Printer.CurrentY
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "User"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temBilledUser
        
        myY = Printer.CurrentY
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "Billed Time"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temBillTime
        
        myY = Printer.CurrentY
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "Billed Date"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temBillDate
        
        
    
        Printer.EndDoc
        
    End If
End Sub

Public Sub printBHTBill(IncomeBillID As Long, billingPrinter As String, billingPaper As String, billingForm As Form)
    
    Dim temBillPoints As MyBillPoints
    
    CsetPrinter.SetPrinterAsDefault (billingPrinter): DoEvents
    Dim MyFOnt As ReportFont

    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle): DoEvents
    End If
    CsetPrinter.SetPrinterAsDefault (billingPrinter): DoEvents
    
    Dim CenterX As Long
    Dim FieldX As Long
    Dim NoX As Long
    Dim ValueX As Long
    Dim AllLines() As String
    Dim i As Integer
    Dim temY As Long
    Dim n As Long
    
    With MyFOnt
        .Name = DefaultFont.Name
        .Bold = False
        .Italic = False
        .Size = 9
        .Italic = False
        .Underline = False
    End With
    
    Dim temGrossTotal As String
    Dim temDiscount As Double
    Dim temNetTotal As Double
    Dim TemAgentId As Long
    Dim temCancelled As Boolean
    Dim temSettled As Boolean
    Dim temPatientName As String
    
    
    
    Dim temBillNo As String
    Dim temAppNo As String
    Dim temCat As String
    Dim temSubCat As String
    Dim temBillTime As String
    Dim temBillDate As String
    Dim temBilledUser As String
    Dim temStaffName1 As String
    Dim temAppDate As String
    Dim temAppTime As String
    
    Dim rsTem As New ADODB.Recordset
    temSQL = "SELECT dbo.tblIncomeBill.DisplayBillID, dbo.tblIncomeBill.GrossTotal, dbo.tblIncomeBill.Discount, dbo.tblIncomeBill.NetTotal, dbo.tblIncomeBill.AgentID, dbo.tblIncomeBill.Cancelled, dbo.tblIncomeBill.Completed, dbo.tblIncomeBill.BillSettled, dbo.tblIncomeBill.CompletedDate, dbo.tblIncomeBill.CompletedTime, dbo.tblPatientService.RBillID, dbo.tblPatientService.PatientID, dbo.tblPatientMainDetails.FirstName, dbo.tblPatientService.PatientServiceID, dbo.tblPatientService.ServiceDate, dbo.tblPatientService.ServiceTime, dbo.tblPatientService.SerialNo, dbo.tblServiceCategory.ServiceCategory, dbo.tblServiceSubcategory.ServiceSubcategory, dbo.tblIncomeBill.CompletedUserID, dbo.tblStaff.UserName, dbo.tblIncomeBill.IncomeBillID " & _
                "FROM dbo.tblStaff RIGHT OUTER JOIN dbo.tblIncomeBill ON dbo.tblStaff.StaffID = dbo.tblIncomeBill.CompletedUserID LEFT OUTER JOIN dbo.tblServiceSubcategory RIGHT OUTER JOIN dbo.tblPatientService ON dbo.tblServiceSubcategory.ServiceSubcategoryID = dbo.tblPatientService.ServiceSubcategoryID LEFT OUTER JOIN dbo.tblServiceCategory ON dbo.tblPatientService.ServiceCategoryID = dbo.tblServiceCategory.ServiceCategoryID ON dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN dbo.tblPatientMainDetails ON dbo.tblPatientService.PatientID = dbo.tblPatientMainDetails.PatientID " & _
                "Where (dbo.tblIncomeBill.Completed = 1) And (dbo.tblPatientService.Deleted = 0) And (dbo.tblIncomeBill.incomeBillId = " & IncomeBillID & ") " & _
                "ORDER BY dbo.tblIncomeBill.IncomeBillID"
    With rsTem
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then

            temGrossTotal = Format(![GrossTotal], "#,##0.00")
            If IsNull(!Discount) = False Then temDiscount = ![Discount]
            temNetTotal = ![NetTotal]
            TemAgentId = ![AgentID]
            If IsNull(!Cancelled) = False Then temCancelled = ![Cancelled]
            temSettled = ![BillSettled]
            temPatientName = ![FirstName]
            
            temBillNo = ![DisplayBillID]
            temAppNo = ![serialNo]
            temCat = ![ServiceCategory]
            temSubCat = ![ServiceSubcategory]
            temBillTime = Format(![CompletedTime], "hh:mm AMPM")
            temBillDate = Format(![CompletedDate], "dd MMMM yyyy")
            temBilledUser = DecreptedWord(![UserName])
            temAppDate = Format(![ServiceDate], "dd MMMM yyyy")
            temAppTime = Format(![ServiceTime], "hh:mm AMPM")
            
        Else
            Printer.KillDoc
            Exit Sub
        End If
    End With
    
    Dim LeftMargin As Double
    Dim TopMargin As Double
    Dim myY As Double
    Dim myLabelX As Double
    Dim myValX As Double
    Dim myColan As Double
    Dim centre As Double
    
    

    
    
    If SelectForm(billingPaper, billingForm.hwnd) = 1 Then
            
        LeftMargin = 0
        TopMargin = 1440 * 0.8
        centre = Printer.Width / 2
        myLabelX = 1440 * 0.5
        myValX = 1440 * 2#
        myColan = 1440 * 1.9
        
        Printer.FontBold = True
        Printer.Font = "Arial"
        Printer.FontSize = "14"
        Printer.Print
        
        
        Printer.CurrentY = TopMargin
        Printer.CurrentX = centre - (Printer.TextWidth(temCat) / 2)
        Printer.Print temCat
        
        Printer.Print
        
        Printer.Font = "Arial"
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Ref. No."
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temBillNo
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Name"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temPatientName
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Service"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temSubCat
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Appointment Date"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temAppDate
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Appointment Time"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temAppTime
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Appointment No"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        
        Printer.FontSize = "16"
        Printer.FontBold = True
        
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temAppNo
        
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Charge"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        
        Printer.FontSize = "14"
        Printer.FontBold = True
        
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print temGrossTotal
        
        Printer.FontSize = "11"
        Printer.FontBold = False
        
        
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        
        myY = Printer.CurrentY
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "User"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temBilledUser
        
        myY = Printer.CurrentY
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "Billed Time"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temBillTime
        
        myY = Printer.CurrentY
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print "Billed Date"
'        Printer.CurrentX = myLabelX
'        Printer.CurrentY = myY
'        Printer.Print ":"
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print temBillDate
        
        
    
        Printer.EndDoc
        
    End If
End Sub



