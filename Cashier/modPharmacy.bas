Attribute VB_Name = "modPharmacy"
Option Explicit
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset

Public Function findSalePrice(ByVal ItemID As Long, Optional batchId As Long) As Double
    With rsTem
        findSalePrice = 0
        If .State = 1 Then .Close
        If batchId = 0 Then
            temSQL = "SELECT tblCurrentSalePrice.SPrice FROM tblCurrentSalePrice WHERE tblCurrentSalePrice.ItemID=" & ItemID & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentSalePrice.SPrice FROM tblCurrentSalePrice WHERE tblCurrentSalePrice.ItemID=" & ItemID & " And BatchId = " & batchId & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findSalePrice = !SPrice
        End If
        If findSalePrice = 0 And batchId <> 0 Then
            findSalePrice = findSalePrice(ItemID)
        End If
        End With
End Function

Public Function findPurchasePrice(ByVal ItemID As Long, Optional batchId As Long) As Double
    With rsTem
        findPurchasePrice = 0
        If .State = 1 Then .Close
        If batchId = 0 Then
            temSQL = "SELECT tblCurrentPurchasePrice.PPrice FROM tblCurrentPurchasePrice WHERE tblCurrentPurchasePrice.ItemID=" & ItemID & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentPurchasePrice.PPrice FROM tblCurrentPurchasePrice WHERE tblCurrentPurchasePrice.ItemID=" & ItemID & " And BatchId = " & batchId & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findPurchasePrice = !PPrice
        End If
        If findPurchasePrice = 0 And batchId <> 0 Then
            findPurchasePrice = findPurchasePrice(ItemID)
        End If
    End With
End Function

Public Function findWholeSalePrice(ByVal ItemID As Long, Optional batchId As Long) As Double
    With rsTem
        findWholeSalePrice = 0
        If .State = 1 Then .Close
        If batchId = 0 Then
            temSQL = "SELECT tblCurrentWholeSalePrice.WPrice FROM tblCurrentWholeSalePrice WHERE tblCurrentWholeSalePrice.ItemID=" & ItemID & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentWholeSalePrice.WPrice FROM tblCurrentWholeSalePrice WHERE tblCurrentWholeSalePrice.ItemID=" & ItemID & " And BatchId = " & batchId & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findWholeSalePrice = !WPrice
        End If
        If findWholeSalePrice = 0 And batchId <> 0 Then
            findWholeSalePrice = findWholeSalePrice(ItemID)
        End If
        End With
End Function

Public Function findAllSaleCategories() As Collection
    Dim cats As New Collection
    Dim temCat As SaleCategory
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblSaleCategory order by SaleCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            Set temCat = New SaleCategory
            temCat.SaleCategoryID = !SaleCategoryID
            cats.Add temCat
            .MoveNext
        Wend
        .Close
    End With
    Set findAllSaleCategories = cats
End Function


Public Function findNetSale(FromDate As Date, toDate As Date, CatId As Long, DeptId As Long) As Double
    findNetSale = findGrossSale(FromDate, toDate, CatId, DeptId) - findCancellAndReturn(FromDate, toDate, CatId, DeptId)
End Function

Public Function findGrossSale(FromDate As Date, toDate As Date, CatId As Long, DeptId As Long) As Double
    
    
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT sum(tblSaleBill.NetPrice) as sumNetTotal " & _
                    "FROM tblSaleBill " & _
                    "WHERE  "
        If CatId <> 0 Then
            temSQL = temSQL & " tblSaleBill.salecategoryid = " & CatId & " AND "
        End If
        If DeptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & DeptId & " AND "
        End If
        temSQL = temSQL & "  tblSaleBill.Date between '" & Format(FromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumNetTotal) = False Then
            findGrossSale = !sumNetTotal
        Else
            findGrossSale = 0
        End If
    End With
End Function

Public Function findCancellAndReturn(FromDate As Date, toDate As Date, CatId As Long, DeptId As Long) As Double
    With rsTem
        If .State = 1 Then .Close
         temSQL = "SELECT sum(tblReturnBill.NetPrice) AS sumReturn " & _
                    "FROM dbo.tblReturnBill LEFT OUTER JOIN dbo.tblSaleBill ON dbo.tblReturnBill.SaleBillID = dbo.tblSaleBill.SaleBillID "
        If CatId = 0 Then
            temSQL = temSQL & "WHERE tblsalebill.salecategoryid = " & CatId & " AND "
        Else
            temSQL = temSQL & "WHERE "
        End If
        If CatId <> 0 Then
            temSQL = temSQL & " tblSaleBill.salecategoryid = " & CatId & " AND "
        End If
        If DeptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & DeptId & " AND "
        End If
        
        temSQL = temSQL & " ( tblReturnBill.Date between '" & Format(FromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' ) "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumReturn) = False Then
            findCancellAndReturn = !sumReturn
        Else
            findCancellAndReturn = 0
        End If
    End With
End Function


