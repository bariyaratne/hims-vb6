VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRFilms 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Radiology Films"
   ClientHeight    =   8655
   ClientLeft      =   3930
   ClientTop       =   -180
   ClientWidth     =   11145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   11145
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   1800
      TabIndex        =   9
      Top             =   600
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSFlexGridLib.MSFlexGrid gridBill 
      Height          =   6855
      Left            =   240
      TabIndex        =   2
      Top             =   1080
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   12091
      _Version        =   393216
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   9840
      TabIndex        =   1
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   8520
      TabIndex        =   0
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1800
      TabIndex        =   3
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   114884611
      CurrentDate     =   39969
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   7080
      TabIndex        =   5
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   114884611
      CurrentDate     =   39969
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   7200
      TabIndex        =   7
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbItemCategory 
      Height          =   360
      Left            =   1800
      TabIndex        =   10
      Top             =   8040
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbItem 
      Height          =   360
      Left            =   5640
      TabIndex        =   12
      Top             =   8040
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label2 
      Caption         =   "Item Category"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   8040
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Category"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "To"
      Height          =   255
      Left            =   5760
      TabIndex        =   6
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "From"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "frmRFilms"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim MyGrid As Grid
    Dim MyGridRow() As GridRow
    Dim MyGridCell() As GridCell
    Dim i As Integer
    Dim n As Integer
    Dim TotalValue As Double
    Dim TotalCount As Long
    Dim TotalPFee As Double
    Dim rsItem As New ADODB.Recordset
    Dim rsCatogery As New ADODB.Recordset
    Dim fillingGrid As Boolean

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridBill, "X-ray Film Usage Bills - " & cmbCat.text & " - " & "From : " & Format(dtpFrom.Value, "dd MM yy") & " To : " & Format(dtpTo.Value, "dd MM yy")
End Sub

Private Sub btnPrint_Click()
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    
    
    With ThisReportFormat
        .ColSpace = 500
        
        .SubTopicFontSize = 8
    
        .ColFontSize = 7
        .ColSpace = 200
    
    End With
    
    GridPrint gridBill, ThisReportFormat, "X-Ray Film Usage - " & cmbCat.text & " - " & "From : " & Format(dtpFrom.Value, "dd MM yy") & " To : " & Format(dtpTo.Value, "dd MM yy")
    Printer.EndDoc
    
End Sub

Private Sub cmbPaymentMethod_Change()
    Call FormatGrid
    Call FillGrid

End Sub



Private Sub cmbType_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub cmbType_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub dtpDate_Change()
    Call FormatGrid
    Call FillGrid
End Sub





Private Sub cmbCat_Change()
    FormatGrid
    FillGrid
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
    End If
End Sub

Private Sub cmbItem_Change()
    Dim rsTem As New ADODB.Recordset
    Dim temId As Long
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * From tblPatientServiceItem where PatientServiceItemId = " & Val(gridBill.TextMatrix(gridBill.Row, 7))
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !PatientServiceId = Val(gridBill.TextMatrix(gridBill.Row, 8))
            !ItemID = Val(cmbItem.BoundText)
            .Update
            temId = !PatientServiceItemId
            .Close
        Else
            .AddNew
            !PatientServiceId = Val(gridBill.TextMatrix(gridBill.Row, 8))
            !ItemID = Val(cmbItem.BoundText)
            .Update
            .Close
            temSQL = "SELECT @@IDENTITY;"
            .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            temId = .Fields(0).Value
            .Close
        End If

        gridBill.TextMatrix(gridBill.Row, 5) = cmbItem.text
        gridBill.TextMatrix(gridBill.Row, 6) = cmbItem.BoundText
        gridBill.TextMatrix(gridBill.Row, 7) = temId


    End With
End Sub

Private Sub cmbItemCategory_Change()
    fillItems
End Sub


Private Sub dtpFrom_Change()
    Call FormatGrid
    Call FillGrid
End Sub


Private Sub dtpTo_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub Form_Load()
    Call FillCombos
    Call FormatGrid
    Call GetSettings
    Call FillGrid
End Sub

Private Sub FormatGrid()
    With gridBill
        .FixedCols = 0
    
        .Cols = 9

        .Rows = 1

        .Row = 0

        .Col = 0
        .text = "ID"

        .Col = 1
        .text = "Bill ID"

        .Col = 2
        .text = "BHT"

        .Col = 3
        .text = "Patient"

        .Col = 4
        .text = "Film"


        .ColWidth(0) = 0

        
        ReDim MyGridCell(6)

    End With
End Sub

Private Sub FillGrid()
fillingGrid = True
TotalCount = 0
TotalValue = 0
TotalPFee = 0
    Dim TemString As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT IncomeBillID, DisplayBillID, BillSettled, Cancelled, BHTID, PatientID, CompletedDate, NetTotal, NetTotalToPay " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE      (CompletedDate BETWEEN CONVERT(DATETIME, '" & dtpFrom.Value & "', 102) AND CONVERT(DATETIME, '" & dtpTo.Value & "', 102)) AND (IsRBill = 1) AND (Completed = 1) " & _
                    "ORDER BY DisplayBillID"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            
            If Val(cmbCat.BoundText) = 0 Or serviceCategoryPresent(!IncomeBillID, Val(cmbCat.BoundText)) Then
                
                
        
                    gridBill.Rows = gridBill.Rows + 1
                    gridBill.Row = gridBill.Rows - 1
                    
                    gridBill.Col = 0
                    gridBill.ColWidth(0) = 0
                    gridBill.text = !IncomeBillID
                    
                    gridBill.Col = 1
                    gridBill.ColWidth(1) = 600
                    gridBill.text = !DisplayBillID
                    
                    Dim myBHT As New clsBHT
                    If IsNull(!BHTID) = False Then
                        myBHT.BHTID = !BHTID
                    Else
                        myBHT.BHTID = 0
                    End If
                    
                    gridBill.Col = 2
                    gridBill.ColWidth(2) = 1200
                    gridBill.text = myBHT.BHT
                    
                    Dim myPt As New clsPatient
                    If myBHT.BHTID <> 0 Then
                        myPt.ID = myBHT.PatientID
                    Else
                        myPt.ID = !PatientID
                    End If
                    
                    gridBill.Col = 3
                    gridBill.ColWidth(3) = 1200
                    gridBill.text = myPt.FirstName
                    
                    gridBill.Col = 4
                    gridBill.ColWidth(4) = 1000
                    gridBill.text = Format(!NetTotal, "0.00")
                    
                    Dim strTem  As String
                    Dim rsTem1 As New ADODB.Recordset
                    Dim rsTem2 As New ADODB.Recordset
                    Dim dblNo1 As Double
                    strTem = ""
                    dblNo1 = 0
                    If rsTem1.State = 1 Then rsTem1.Close
                    temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & !IncomeBillID
                    rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
                    While rsTem1.EOF = False
                        temSQL = "SELECT dbo.tblItem.ItemID, dbo.tblItem.Display, dbo.tblPatientServiceItem.PatientServiceItemId FROM dbo.tblPatientServiceItem LEFT OUTER JOIN dbo.tblItem ON dbo.tblPatientServiceItem.ItemId = dbo.tblItem.ItemID Where PatientServiceID = " & rsTem1!PatientServiceId
                    
                        gridBill.Col = 8
                        gridBill.ColWidth(6) = 1000
                        gridBill.text = rsTem1!PatientServiceId
                        
                        If rsTem2.State = 1 Then rsTem2.Close
                        rsTem2.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
                        Dim myStaff As New clsStaff
                        While rsTem2.EOF = False
                            If IsNull(rsTem2!Display) = False Then
                                strTem = rsTem2!Display
                            Else
                                strTem = ""
                            End If
                            gridBill.Col = 5
                            gridBill.ColWidth(5) = 3800
                            gridBill.text = strTem
                            gridBill.Col = 6
                            gridBill.ColWidth(6) = 0
                            If IsNull(rsTem2!ItemID) = False Then
                                gridBill.text = rsTem2!ItemID
                            End If
                            gridBill.Col = 7
                            gridBill.ColWidth(6) = 0
                            gridBill.text = rsTem2!PatientServiceItemId
                            rsTem2.MoveNext
                            
                        Wend
                        rsTem2.Close
                        rsTem1.MoveNext
                    Wend
                    rsTem1.Close
                    
                    
        

                    
        
                    TotalCount = TotalCount + 1
                    TotalValue = TotalValue + !NetTotal
                    TotalPFee = TotalPFee + dblNo1
                
                
            End If
        
            .MoveNext
        Wend
        .Close
  
        
        
    End With
    fillingGrid = False
    
End Sub


Private Function serviceCategoryPresent(IncomeBillID As Long, ServiceCatId As Long) As Boolean
            serviceCategoryPresent = False
            Dim rsTem1 As New ADODB.Recordset
            If rsTem1.State = 1 Then rsTem1.Close
            temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & IncomeBillID
            rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While rsTem1.EOF = False
                If ServiceCatId = rsTem1!ServiceCategoryId Then serviceCategoryPresent = True
                rsTem1.MoveNext
            Wend
            rsTem1.Close
End Function

Private Sub FillCombos()
    Dim cat As New clsFillCombos
    cat.FillAnyCombo cmbCat, "ServiceCategory", True
    With rsCatogery
        If .State = 1 Then .Close
        temSQL = "SELECT tblItemCategory.ItemCategoryID, tblItemCategory.ItemCategory FROM tblItemCategory ORDER BY tblItemCategory.ItemCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbItemCategory
        Set .RowSource = rsCatogery
        .ListField = "ItemCategory"
        .BoundColumn = "ItemCategoryID"
        .BoundText = Val(GetSetting(App.EXEName, Me.Name, cmbItemCategory.Name, "46"))
    End With
    fillItems
End Sub

Private Sub fillItems()
    With rsItem
        If .State = 1 Then .Close
        If IsNumeric(cmbItemCategory.BoundText) = True Then
            temSQL = "SELECT * from tblitem where ItemCategoryID = " & Val(cmbItemCategory.BoundText) & " order by display"
        Else
            temSQL = "SELECT * from tblitem order by display"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbItem
        Set .RowSource = rsItem
        .ListField = "Display"
        .BoundColumn = "ItemID"
    End With
End Sub

Private Sub GetSettings(): On Error Resume Next
    dtpFrom.Value = GetSetting(App.EXEName, Me.Name, dtpFrom.Name, "00:00:00")
    dtpTo.Value = GetSetting(App.EXEName, Me.Name, dtpTo.Name, "00:00:00")
    GetCommonSettings Me
    dtpFrom.Value = Date
    dtpTo.Value = Date

End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
    SaveSetting App.EXEName, Me.Name, dtpFrom.Name, dtpFrom.Value
    SaveSetting App.EXEName, Me.Name, dtpTo.Name, dtpTo.Value
    SaveSetting App.EXEName, Me.Name, cmbItemCategory.Name, cmbItemCategory.BoundText
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

Private Sub gridBill_EnterCell()
    Dim temId As Long
    If fillingGrid = False Then
        If gridBill.Col = 5 Then
            temId = Val(cmbItem.BoundText)
            cmbItem.text = Empty
'            cmbItem.BoundText = temId
            cmbItem.Visible = True
            cmbItem.ZOrder 0
            cmbItem.Top = gridBill.Top + gridBill.CellTop
            cmbItem.Left = gridBill.Left + gridBill.CellLeft
            cmbItem.Width = gridBill.CellWidth
            cmbItem.BoundText = Val(gridBill.TextMatrix(gridBill.Row, 6))
        Else
            cmbItem.Visible = False
        End If
    End If
End Sub
