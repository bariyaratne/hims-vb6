VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmProfessionalSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Professional Summery by Booking Date"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11490
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11490
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   720
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   8400
      Width           =   4695
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   6240
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   8400
      Width           =   4695
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   6840
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridSummery 
      Height          =   7095
      Left            =   240
      TabIndex        =   0
      Top             =   1080
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   12515
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   12000
      TabIndex        =   4
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   2400
      TabIndex        =   5
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   115605507
      CurrentDate     =   39969
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   2400
      TabIndex        =   6
      Top             =   600
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   375
      Left            =   9480
      TabIndex        =   7
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   8160
      TabIndex        =   8
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label bbbb 
      Caption         =   "Printer"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   8400
      Width           =   1815
   End
   Begin VB.Label cccc 
      Caption         =   "Paper"
      Height          =   255
      Left            =   5640
      TabIndex        =   11
      Top             =   8400
      Width           =   1815
   End
   Begin VB.Label Label11 
      Caption         =   "Consultant/Technician"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   600
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "Booking Date"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "frmProfessionalSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridSummery, "Professional Summery - " & cmbUser.text, "Date : " & Format(dtpDate.Value, "dd MM yy"), True
End Sub

Private Sub btnPrint_Click()
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    With ThisReportFormat
        .ColSpace = 500
        .SubTopicFontSize = 8
    End With
    GridPrint gridSummery, ThisReportFormat, "Professional Summery - " & cmbUser.text, "Date : " & Format(dtpDate.Value, "dd MM yy")
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    Dim temSQL As String
    
    Dim billRowStart As Long
    Dim billRowEnd As Long
    
    Dim cancalRowStart As Long
    Dim cancelRowEnd As Long
    
    Dim returnRowStart As Long
    Dim returnRowEnd As Long
    
    
    gridSummery.Visible = False
    
    temSQL = "SELECT      dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory, SUM(dbo.tblProfessionalCharges.FeePaid) AS [Sum of Fee], Count(dbo.tblProfessionalCharges.FeePaid) AS [Counts] " & _
"FROM          dbo.tblPatientService LEFT OUTER JOIN " & _
"                        dbo.tblServiceCategory ON dbo.tblPatientService.ServiceCategoryID = dbo.tblServiceCategory.ServiceCategoryID RIGHT OUTER JOIN " & _
"                        dbo.tblProfessionalCharges ON dbo.tblPatientService.PatientServiceID = dbo.tblProfessionalCharges.PatientServiceID LEFT OUTER JOIN " & _
"                        dbo.tblIncomeBill ON dbo.tblProfessionalCharges.ForRBillID = dbo.tblIncomeBill.IncomeBillID RIGHT OUTER JOIN " & _
"                        dbo.tblStaff ON dbo.tblProfessionalCharges.StaffID = dbo.tblStaff.StaffID " & _
"WHERE      (dbo.tblPatientService.Deleted = 0) AND  (dbo.tblIncomeBill.Cancelled = 0)  AND  (dbo.tblIncomeBill.Returned = 0) AND (dbo.tblProfessionalCharges.Fee <> 0) AND (dbo.tblIncomeBill.CompletedDate BETWEEN CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102) AND  " & _
"                        CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMM yyyy") & "', 102)) " & _
"GROUP BY dbo.tblStaff.Name, dbo.tblServiceCategory.ServiceCategory"
    
    FillAnyGrid temSQL, gridSummery, False
    
    
    gridReset gridSummery
    
    If IsNumeric(cmbUser.BoundText) = True Then
        hideRows gridSummery, 0, cmbUser.text, "Name"
    End If
    

    
    With gridSummery
        .Rows = .Rows + 2
        .row = .Rows - 1
        .col = 0
        .text = "Total"
        
        .col = 2
        .text = rowTotal(gridSummery, 2, 1, .Rows - 2, True)
        .col = 3
        .text = rowTotal(gridSummery, 3, 1, .Rows - 2, True)
    
    End With
    
    formatGridString gridSummery, 2, "#,##0.00"
    
    
    gridSummery.Visible = True

End Sub


Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then cmbUser.text = Empty
End Sub

Private Sub fillCombos()
'    Dim cat As New clsFillCombos
'    cat.FillAnyCombo cmbCat, "ServiceCategory", True
'    Dim PayM As New clsFillCombos
'    PayM.FillBoolCombo cmbPm, "PaymentMethod", "PaymentMethod", "FinalPay", False
    Dim Cashier As New clsFillCombos
    Cashier.FillSpecificField cmbUser, "Staff", "Name", False

End Sub

Private Sub Form_Load()
    fillCombos
    GetSettings
End Sub

Private Sub GetSettings()
    'GetCommonSettings Me
    dtpDate.Value = Date
End Sub

Private Sub SaveSettings()
   'saveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub




