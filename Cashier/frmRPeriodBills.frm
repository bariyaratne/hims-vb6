VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRPeriodBills 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Radiology Bills"
   ClientHeight    =   8985
   ClientLeft      =   3930
   ClientTop       =   -180
   ClientWidth     =   11145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   11145
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   5640
      TabIndex        =   16
      Top             =   1560
      Width           =   1215
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   1800
      TabIndex        =   15
      Top             =   1560
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtCount 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   3120
      TabIndex        =   8
      Top             =   8040
      Width           =   2415
   End
   Begin VB.TextBox txtValue 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   3120
      TabIndex        =   6
      Top             =   8520
      Width           =   2415
   End
   Begin MSFlexGridLib.MSFlexGrid gridBill 
      Height          =   5415
      Left            =   240
      TabIndex        =   4
      Top             =   2520
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   9551
      _Version        =   393216
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   9840
      TabIndex        =   1
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   8520
      TabIndex        =   0
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1800
      TabIndex        =   9
      Top             =   1080
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   115212291
      CurrentDate     =   39969
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   7080
      TabIndex        =   11
      Top             =   1080
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   115212291
      CurrentDate     =   39969
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   7200
      TabIndex        =   13
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   1800
      TabIndex        =   17
      Top             =   2040
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label6 
      Caption         =   "Booked By"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Category"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "To"
      Height          =   255
      Left            =   5760
      TabIndex        =   12
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "From"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Total Value"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   8520
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Total Count"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   8040
      Width           =   2775
   End
   Begin VB.Label lblTopic 
      Alignment       =   2  'Center
      Caption         =   "Topic"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   10815
   End
   Begin VB.Label lblSubtopic 
      Alignment       =   2  'Center
      Caption         =   "Topic"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   10815
   End
End
Attribute VB_Name = "frmRPeriodBills"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim MyGrid As Grid
    Dim MyGridRow() As GridRow
    Dim MyGridCell() As GridCell
    Dim i As Integer
    Dim n As Integer
    Dim TotalValue As Double
    Dim TotalCount As Long
    Dim TotalPFee As Double
    Dim TotalCollect As Double

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridBill, "Radiology Bills - " & cmbCat.text & " - " & "From : " & Format(dtpFrom.Value, "dd MM yy") & " To : " & Format(dtpTo.Value, "dd MM yy")
End Sub

Private Sub btnPrint_Click()
    Dim myPrinter As Printer
    For Each myPrinter In Printers
        If myPrinter.DeviceName = ReportPrinterName Then
            Set Printer = myPrinter
        End If
    Next
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    
    
    With ThisReportFormat
        .ColSpace = 500
        
        .SubTopicFontSize = 8
    
        .ColFontSize = 7
        .ColSpace = 200
    
    End With
    
    GridPrint gridBill, ThisReportFormat, "Radiology Bills - " & cmbCat.text & " - " & "From : " & Format(dtpFrom.Value, "dd MM yy") & " To : " & Format(dtpTo.Value, "dd MM yy")
    Printer.EndDoc
    
End Sub

Private Sub cmbPaymentMethod_Change()
    Call FormatGrid
    Call FillGrid

End Sub



Private Sub cmbType_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub cmbType_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub dtpDate_Change()
    Call FormatGrid
    Call FillGrid
End Sub





Private Sub btnProcess_Click()
    Screen.MousePointer = vbHourglass
    
     FormatGrid
    FillGrid
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
    End If
End Sub

Private Sub Form_Load()
    Call fillCombos
    Call FormatGrid
    Call GetSettings
    
End Sub

Private Sub FormatGrid()
    With gridBill
        .FixedCols = 0
    
        .Cols = 17

        .Rows = 1

        .Row = 0

      

        .Rows = 1

        .Row = 0

        .Col = 0
        .text = "ID"


        .Col = 1
        .text = "No"

        .Col = 2
        .text = "Bill No"

        .Col = 3
        .text = "Patient"

        .Col = 4
        .text = "BHT"

        .Col = 5
        .text = "User"
    
        .Col = 6
        .text = "Total"

    .Col = 7
    .text = "Cancelled/Returned"

    .Col = 8
    .text = "Cancelled/Returned Fee"

        .Col = 9
        .text = "Cancelled/Returned User"

        .Col = 10
        .text = "COllection"

        .Col = 8
        .text = "User"

        .Col = 9
        .text = "Cancelled/Returned"
        
        .Col = 10
        .text = "Collection"


        .Col = 11
        .text = "Category"
        
        .Col = 12
        .text = "Subcategory"
        
        .Col = 13
        .text = "Session"
        
        .Col = 14
        .text = "number"
        
        .Col = 15
        .text = "Docto Technician"

        .Col = 16
        .text = "Professional Fee"

        .ColWidth(0) = 0

        
        ReDim MyGridCell(6)

    End With
End Sub

Private Sub FillGrid()
TotalCount = 0
TotalValue = 0
TotalPFee = 0
TotalCollect = 0
Dim billSerial As Long

    billSerial = 0
    Dim TemString As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        
        If IsNumeric(cmbUser.BoundText) = True Then
            temSQL = "SELECT IncomeBillID, DisplayBillID, BillSettled, Cancelled, BHTID, PatientID, CompletedDate, NetTotal, NetTotalToPay " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE      (CompletedDate BETWEEN CONVERT(DATETIME, '" & dtpFrom.Value & "', 102) AND CONVERT(DATETIME, '" & dtpTo.Value & "', 102)) AND (IsRBill = 1) AND (Completed = 1) AND (CompletedUserId = " & Val(cmbUser.BoundText) & ")" & _
                    "ORDER BY DisplayBillID"
          Else
            temSQL = "SELECT IncomeBillID, DisplayBillID, BillSettled, Cancelled, BHTID, PatientID, CompletedDate, NetTotal, NetTotalToPay " & _
                    "From dbo.tblIncomeBill " & _
                    "WHERE      (CompletedDate BETWEEN CONVERT(DATETIME, '" & dtpFrom.Value & "', 102) AND CONVERT(DATETIME, '" & dtpTo.Value & "', 102)) AND (IsRBill = 1) AND (Completed = 1) " & _
                    "ORDER BY DisplayBillID"
          
          End If
                    
                    
                    
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            
            If Val(cmbCat.BoundText) = 0 Or serviceCategoryPresent(!IncomeBillID, Val(cmbCat.BoundText)) Then
                    
                    billSerial = billSerial + 1
                
                    Dim thisBill As New IncomeBill
                    thisBill.IncomeBillID = !IncomeBillID
        
                    gridBill.Rows = gridBill.Rows + 1
                    gridBill.Row = gridBill.Rows - 1
                    
                    gridBill.Col = 0
                    gridBill.ColWidth(0) = 0
                    gridBill.text = thisBill.IncomeBillID
                    
                    gridBill.Col = 1
                    gridBill.ColWidth(1) = 600
                    gridBill.text = billSerial
                    
                    gridBill.Col = 2
                    gridBill.ColWidth(2) = 600
                    gridBill.text = thisBill.DisplayBillID
                    
                    Dim myBHT As New clsBHT
                    If IsNull(thisBill.BHTID) = False Or thisBill.BHTID = 0 Then
                        myBHT.BHTID = 0
                        
                    Else
                        myBHT.BHTID = thisBill.BHTID
                    End If

                    Dim myPt As New clsPatient
                    If myBHT.BHTID <> 0 Then
                        myPt.ID = myBHT.PatientID
                    Else
                        myPt.ID = thisBill.PatientID
                    End If
                    
                    gridBill.Col = 3
                    gridBill.ColWidth(3) = 1200
                    gridBill.text = myPt.FirstName
                    
                    
                    gridBill.Col = 4
                    gridBill.ColWidth(4) = 1200
                    gridBill.text = myBHT.BHT
                                        
                    
                    Dim BilledUser As New Staff
                    BilledUser.StaffID = thisBill.CompletedUserID
                    
                    gridBill.Col = 5
                    gridBill.ColWidth(5) = 1000
                    gridBill.text = BilledUser.Name
                    
                    gridBill.Col = 6
                    gridBill.ColWidth(6) = 1000
                    gridBill.text = Format(thisBill.NetTotal, "0.00")
                    
  
                    gridBill.Col = 7
                    gridBill.ColWidth(7) = 1000
                    If thisBill.Cancelled = True Then
                        gridBill.text = gridBill.text + "Cancelled"
                    End If
                    If thisBill.Returned = True Then
                        gridBill.text = gridBill.text + "Returned"
                    End If
                    
                    
                    gridBill.Col = 8
                    gridBill.ColWidth(8) = 1000
                    If thisBill.Cancelled = True Then
                        gridBill.text = Val(gridBill.text) + thisBill.CancelledValue
                    End If
                    If thisBill.Returned = True Then
                        gridBill.text = Val(gridBill.text) + thisBill.ReturnedValue
                    End If
                    
                    Dim canuser As New Staff
                    canuser.StaffID = thisBill.CancelledUserID
                    Dim retUser As New Staff
                    retUser.StaffID = thisBill.ReturnedUserID
                    gridBill.Col = 9
                    gridBill.ColWidth(9) = 1000
                    If thisBill.Cancelled = True Then
                        gridBill.text = Val(gridBill.text) & canuser.Name
                    End If
                    If thisBill.Returned = True Then
                        gridBill.text = Val(gridBill.text) & retUser.Name
                    End If
                    
                    gridBill.Col = 10
                    gridBill.ColWidth(10) = 1000
                    gridBill.text = Val(gridBill.TextMatrix(gridBill.Row, 6)) - thisBill.CancelledValue - thisBill.ReturnedValue
                    TotalCollect = TotalCollect + Val(gridBill.TextMatrix(gridBill.Row, 10))
                    
                    
                    Dim strTem  As String
                    Dim rsTem1 As New ADODB.Recordset
                    Dim rsTem2 As New ADODB.Recordset
                    Dim dblNo1 As Double
                    strTem = ""
                    dblNo1 = 0
                    If rsTem1.State = 1 Then rsTem1.Close
                    temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & !IncomeBillID
                    rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
                    While rsTem1.EOF = False
                        Dim thisPs As New PatientService
                        Dim thisSs As New ServiceSecession
                        Dim thisCat As New ServiceCategory
                        Dim thisSc As New ServiceSubcategory
                        
                        thisPs.PatientServiceID = rsTem1!PatientServiceID
                        thisSs.ServiceSecessionID = thisPs.SecessionID
                        thisCat.ServiceCategoryID = thisPs.ServiceCategoryID
                        thisSc.ServiceSubcategoryID = thisPs.ServiceSubcategoryID
                        
                        gridBill.Col = 11
                        gridBill.ColWidth(11) = 1000
                        gridBill.text = gridBill.text + thisCat.ServiceCategory
                        
                        gridBill.Col = 12
                        gridBill.ColWidth(12) = 1000
                        gridBill.text = gridBill.text + thisSc.ServiceSubcategory
                        
                        gridBill.Col = 13
                        gridBill.ColWidth(13) = 1000
                        gridBill.text = gridBill.text + thisSs.ServiceSecession
                        
                        gridBill.Col = 14
                        gridBill.ColWidth(14) = 1000
                        gridBill.text = thisPs.serialNo
                        
                        
                        temSQL = "SELECT * From tblProfessionalCharges WHERE  PatientServiceID = " & rsTem1!PatientServiceID
                        If rsTem2.State = 1 Then rsTem2.Close
                        rsTem2.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
                        Dim myStaff As New clsStaff
                        While rsTem2.EOF = False
                            myStaff.StaffID = rsTem2!StaffID
                            If strTem = "" Then
                                strTem = myStaff.Name
                            Else
                                strTem = strTem & ", " & myStaff.Name
                            End If
                            dblNo1 = dblNo1 + rsTem2!Fee
                                                        
                            gridBill.Col = 15
                            gridBill.ColWidth(15) = 1000
                            gridBill.text = myStaff.Name
                            
                            gridBill.Col = 16
                            gridBill.ColWidth(16) = 1000
                            gridBill.text = Format(rsTem2!Fee, "0.00")
                            
                            
                        
                            
                            rsTem2.MoveNext
                            
                            If rsTem2.RecordCount > 1 And rsTem2.EOF = False Then
                                gridBill.Rows = gridBill.Rows + 1
                                gridBill.Row = gridBill.Rows - 1
                            End If
                            
                        Wend
                        rsTem2.Close
                        
                        
                        
                        
                        
                      
                        
                        
                        rsTem1.MoveNext
                        
                        If rsTem1.RecordCount > 1 And rsTem1.EOF = False Then
                            gridBill.Rows = gridBill.Rows + 1
                            gridBill.Row = gridBill.Rows - 1
                        End If
                        
                    Wend
                    rsTem1.Close
                    
                    
        
'                    gridBill.Col = 5
'                    gridBill.ColWidth(5) = 1800
'                    gridBill.text = strTem
'
'                    gridBill.Col = 6
'                    gridBill.ColWidth(6) = 1000
'                    gridBill.text = Format(dblNo1, "0.00")
        
                    TotalCount = TotalCount + 1
                    TotalValue = TotalValue + !NetTotal
                    TotalPFee = TotalPFee + dblNo1
                
                
            End If
        
            .MoveNext
        Wend
        .Close
        
        gridBill.Rows = gridBill.Rows + 2
        gridBill.Row = gridBill.Rows - 1
            
        gridBill.Col = 1
        gridBill.text = "Total"
        
        gridBill.Col = 2
        gridBill.text = TotalCount
        
        gridBill.Col = 6
        gridBill.text = Format(TotalValue, "#,##0.00")
        
        gridBill.Col = 10
        gridBill.text = Format(TotalCollect, "#,##0.00")
        
        gridBill.Col = 16
        gridBill.text = Format(TotalPFee, "#,##0.00")
        
        
        
    End With
    
    txtValue.text = Format(TotalValue, "0.00")
    txtCount.text = TotalCount
End Sub


Private Function serviceCategoryPresent(IncomeBillID As Long, ServiceCatId As Long) As Boolean
            serviceCategoryPresent = False
            Dim rsTem1 As New ADODB.Recordset
            If rsTem1.State = 1 Then rsTem1.Close
            temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & IncomeBillID
            rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While rsTem1.EOF = False
                If ServiceCatId = rsTem1!ServiceCategoryID Then serviceCategoryPresent = True
                rsTem1.MoveNext
            Wend
            rsTem1.Close
End Function

Private Sub fillCombos()
    Dim cat As New clsFillCombos
    cat.FillAnyCombo cmbCat, "ServiceCategory", True
        Dim Cashier As New clsFillCombos
    Cashier.FillSpecificField cmbUser, "Staff", "Name", False
End Sub

Private Sub GetSettings(): On Error Resume Next
    dtpFrom.Value = GetSetting(App.EXEName, Me.Name, dtpFrom.Name, "00:00:00")
    dtpTo.Value = GetSetting(App.EXEName, Me.Name, dtpTo.Name, "00:00:00")
    GetCommonSettings Me
    dtpFrom.Value = Date
    dtpTo.Value = Date
    lblTopic.Caption = HospitalName
    lblSubtopic.Caption = "Radiology Bills - From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpFrom.Value, "dd MMMM yyyy")
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
    SaveSetting App.EXEName, Me.Name, dtpFrom.Name, dtpFrom.Value
    SaveSetting App.EXEName, Me.Name, dtpTo.Name, dtpTo.Value
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

