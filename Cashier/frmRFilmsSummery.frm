VERSION 5.00
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRFilmsSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Radiology Bills"
   ClientHeight    =   8985
   ClientLeft      =   3930
   ClientTop       =   -180
   ClientWidth     =   11145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   11145
   Begin VB.TextBox txtCount 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   3120
      TabIndex        =   8
      Top             =   8040
      Width           =   2415
   End
   Begin VB.TextBox txtValue 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   3120
      TabIndex        =   6
      Top             =   8520
      Width           =   2415
   End
   Begin MSFlexGridLib.MSFlexGrid gridBill 
      Height          =   6375
      Left            =   240
      TabIndex        =   4
      Top             =   1560
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   11245
      _Version        =   393216
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   9840
      TabIndex        =   1
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   8520
      TabIndex        =   0
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1800
      TabIndex        =   9
      Top             =   1080
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   53018627
      CurrentDate     =   39969
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   7200
      TabIndex        =   11
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label4 
      Caption         =   "Date"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Total Value"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   8520
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Total Count"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   8040
      Width           =   2775
   End
   Begin VB.Label lblTopic 
      Alignment       =   2  'Center
      Caption         =   "Topic"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   10815
   End
   Begin VB.Label lblSubtopic 
      Alignment       =   2  'Center
      Caption         =   "Topic"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   10815
   End
End
Attribute VB_Name = "frmRFilmsSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim MyGrid As Grid
    Dim MyGridRow() As GridRow
    Dim MyGridCell() As GridCell
    Dim i As Integer
    Dim n As Integer
    Dim TotalValue As Double
    Dim TotalCount As Long
    Dim TotalPFee As Double

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridBill, "Radiology Bills - " & "From : " & Format(dtpFrom.Value, "dd MM yy")
End Sub

Private Sub btnPrint_Click()
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    
    
    With ThisReportFormat
        .ColSpace = 500
        
        .SubTopicFontSize = 8
    
        .ColFontSize = 7
        .ColSpace = 200
    
    End With
    
    GridPrint gridBill, ThisReportFormat, "Radiology Bills - " & "From : " & Format(dtpFrom.Value, "dd MM yy")
    Printer.EndDoc
    
End Sub

Private Sub cmbPaymentMethod_Change()
    Call FormatGrid
    Call FillGrid

End Sub



Private Sub cmbType_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub cmbType_Click()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub dtpDate_Change()
    Call FormatGrid
    Call FillGrid
End Sub





Private Sub cmbCat_Change()
    FormatGrid
    FillGrid
End Sub


Private Sub dtpFrom_Change()
    Call FormatGrid
    Call FillGrid
End Sub


Private Sub dtpTo_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub Form_Load()
    Call FillCombos
    Call FormatGrid
    Call GetSettings
    Call FillGrid
End Sub

Private Sub FormatGrid()
    With gridBill
        .FixedCols = 0
    
        .Cols = 4

        .Rows = 1

        .Row = 0



        .Col = 0
        .text = "Film"

        .Col = 1
        .text = "Previous Day"

        .Col = 2
        .text = "Today"

        .Col = 3
        .text = "Total"

        


        .ColWidth(0) = 0

        
        ReDim MyGridCell(6)

    End With
End Sub

Private Sub FillGrid()
TotalCount = 0
TotalValue = 0
TotalPFee = 0
Dim previousFilm As String
    Dim TemString As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT DISTINCT TOP 100 PERCENT dbo.tblItem.Display, dbo.tblIncomeBill.CompletedDate, COUNT(*) AS CountOfFilms FROM dbo.tblIncomeBill LEFT OUTER JOIN dbo.tblPatientService ON dbo.tblIncomeBill.IncomeBillID = dbo.tblPatientService.RBillID LEFT OUTER JOIN dbo.tblPatientServiceItem LEFT OUTER JOIN dbo.tblItem ON dbo.tblPatientServiceItem.ItemId = dbo.tblItem.ItemID ON dbo.tblPatientService.PatientServiceID = dbo.tblPatientServiceItem.PatientServiceId " & _
                    "WHERE      (CompletedDate BETWEEN CONVERT(DATETIME, '" & dtpFrom.Value & "', 102) AND CONVERT(DATETIME, '" & dtpFrom.Value - 1 & "', 102)) AND (IsRBill = 1) AND (Completed = 1) " & _
                    "GROUP BY dbo.tblItem.Display, dbo.tblIncomeBill.CompletedDate HAVING (dbo.tblItem.Display IS NOT NULL) ORDER BY dbo.tblItem.Display, dbo.tblIncomeBill.CompletedDate "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
        
                    If previousFilm <> !Display Then
                        gridBill.Rows = gridBill.Rows + 1
                        gridBill.Row = gridBill.Rows - 1
                        previousFilm = !Display
                    Else
                    
                    
                    End If
        
                    gridBill.Col = 0
                    gridBill.ColWidth(0) = 4000
                    gridBill.text = !Display
                    
                
            .MoveNext
        Wend
        .Close
        
    End With
End Sub


Private Function serviceCategoryPresent(IncomeBillID As Long, ServiceCatId As Long) As Boolean
            serviceCategoryPresent = False
            Dim rsTem1 As New ADODB.Recordset
            If rsTem1.State = 1 Then rsTem1.Close
            temSQL = "SELECT * FROM tblPatientService where Deleted = 0 AND RBillID = " & IncomeBillID
            rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While rsTem1.EOF = False
                If ServiceCatId = rsTem1!ServiceCategoryId Then serviceCategoryPresent = True
                rsTem1.MoveNext
            Wend
            rsTem1.Close
End Function

Private Sub FillCombos()

End Sub

Private Sub GetSettings(): On Error Resume Next
    dtpFrom.Value = GetSetting(App.EXEName, Me.Name, dtpFrom.Name, "00:00:00")
    GetCommonSettings Me
    dtpFrom.Value = Date
    lblTopic.Caption = HospitalName
    lblSubtopic.Caption = "Radiology Bills - From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpFrom.Value, "dd MMMM yyyy")
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
    SaveSetting App.EXEName, Me.Name, dtpFrom.Name, dtpFrom.Value
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

