VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BHT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varBHTID As Long
    Private varBHT As String
    Private varPatientID As Long
    Private varDOA
    Private varTOA
    Private varDOD
    Private varTOD
    Private varAdStaffID As Long
    Private varDisStaffID As Long
    Private varDischarge As Boolean
    Private varPaymentMethodID As Long
    Private varFullyPaid As Boolean
    Private varPrice As Double
    Private varDiscount As Double
    Private varDiscountPercent
    Private varNetPrice As Double
    Private varCheckedStaffID As Long
    Private varCurrentPrice As Double
    Private varBalance As Double
    Private varRoomID As Long
    Private varHealthSchemeSupplierID As Long
    Private varGuardianName As String
    Private varGuardianAddress As String
    Private varGuardianNIC As String
    Private varGuardianPhone As String
    Private varPatientCategoryID As Long
    Private varAdmissionRate As Double
    Private varInitialLinanRate As Double
    Private varLaterLinanRate As Double
    Private varMaintananceRate As Double
    Private varMaintainaceCashDiscountRate As Double
    Private varNursingRate As Double
    Private varICUNursingRate As Double
    Private varPtSurcharge As Double
    Private varComSurcharge As Double
    Private varAdmissionFee As Double
    Private varAdmissionCharge As Double
    Private varLinanCharge As Double
    Private varRoomCharge As Double
    Private varServicesCharge As Double
    Private varMaintananceCharge As Double
    Private varNursingCharge As Double
    Private varProfessionalCharge As Double
    Private varAdditionalCharge As Double
    Private varMedicineCharge As Double
    Private varTotalCharge As Double
    Private varPayments As Double
    Private varFAdmissionRate As Double
    Private varFInitialLinanRate As Double
    Private varFLaterLinanRate As Double
    Private varFMaintananceRate As Double
    Private varFMaintainaceCashDiscountRate As Long
    Private varFNursingRate As Double
    Private varFICUNursingRate As Long
    Private varFAdmissionFee As Double
    Private varFAdmissionCharge As Double
    Private varFLinanCharge As Double
    Private varFRoomCharge As Double
    Private varFServicesCharge As Double
    Private varFMaintananceCharge As Double
    Private varFNursingCharge As Double
    Private varFProfessionalCharge As Double
    Private varFMedicineCharge As Double
    Private varFAdditionalCharge As Long
    Private varFTotalCharge As Double
    Private varFPayments As Long
    Private varInwardAddition As Double
    Private varReferringDoctorID As Long
    Private varComments As String
    Private varBillSettled As Boolean
    Private varBillSettledDate
    Private varBillSettledTime
    Private varBillSettledDateTime
    Private varBillSettledUserID As Long
    Private varBillSettledPaymentMethodID As Long
    Private varBillSettledComments As String
    Private varIsBHT As Boolean
    Private varIsGSB As Boolean
    Private varHospitalComments As String
    Private varForeigner As Boolean
    Private varupsize_ts
    Private varTemAge As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblBHT Where BHTID = " & varBHTID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !BHT = varBHT
        !PatientID = varPatientID
        !DOA = varDOA
        !TOA = varTOA
        !DOD = varDOD
        !TOD = varTOD
        !AdStaffID = varAdStaffID
        !DisStaffID = varDisStaffID
        !Discharge = varDischarge
        !PaymentMethodID = varPaymentMethodID
        !FullyPaid = varFullyPaid
        !Price = varPrice
        !Discount = varDiscount
        !DiscountPercent = varDiscountPercent
        !NetPrice = varNetPrice
        !CheckedStaffID = varCheckedStaffID
        !CurrentPrice = varCurrentPrice
        !Balance = varBalance
        !RoomID = varRoomID
        !HealthSchemeSupplierID = varHealthSchemeSupplierID
        !GuardianName = varGuardianName
        !GuardianAddress = varGuardianAddress
        !GuardianNIC = varGuardianNIC
        !GuardianPhone = varGuardianPhone
        !PatientCategoryID = varPatientCategoryID
        !AdmissionRate = varAdmissionRate
        !InitialLinanRate = varInitialLinanRate
        !LaterLinanRate = varLaterLinanRate
        !MaintananceRate = varMaintananceRate
        !MaintainaceCashDiscountRate = varMaintainaceCashDiscountRate
        !NursingRate = varNursingRate
        !ICUNursingRate = varICUNursingRate
        !PtSurcharge = varPtSurcharge
        !ComSurcharge = varComSurcharge
        !AdmissionFee = varAdmissionFee
        !AdmissionCharge = varAdmissionCharge
        !LinanCharge = varLinanCharge
        !RoomCharge = varRoomCharge
        !ServicesCharge = varServicesCharge
        !MaintananceCharge = varMaintananceCharge
        !NursingCharge = varNursingCharge
        !ProfessionalCharge = varProfessionalCharge
        !AdditionalCharge = varAdditionalCharge
        !MedicineCharge = varMedicineCharge
        !TotalCharge = varTotalCharge
        !Payments = varPayments
        !FAdmissionRate = varFAdmissionRate
        !FInitialLinanRate = varFInitialLinanRate
        !FLaterLinanRate = varFLaterLinanRate
        !FMaintananceRate = varFMaintananceRate
        !FMaintainaceCashDiscountRate = varFMaintainaceCashDiscountRate
        !FNursingRate = varFNursingRate
        !FICUNursingRate = varFICUNursingRate
        !FAdmissionFee = varFAdmissionFee
        !FAdmissionCharge = varFAdmissionCharge
        !FLinanCharge = varFLinanCharge
        !FRoomCharge = varFRoomCharge
        !FServicesCharge = varFServicesCharge
        !FMaintananceCharge = varFMaintananceCharge
        !FNursingCharge = varFNursingCharge
        !FProfessionalCharge = varFProfessionalCharge
        !FMedicineCharge = varFMedicineCharge
        !FAdditionalCharge = varFAdditionalCharge
        !FTotalCharge = varFTotalCharge
        !FPayments = varFPayments
        !InwardAddition = varInwardAddition
        !ReferringDoctorID = varReferringDoctorID
        !Comments = varComments
        !BillSettled = varBillSettled
        !BillSettledDate = varBillSettledDate
        !BillSettledTime = varBillSettledTime
        !BillSettledDateTime = varBillSettledDateTime
        !BillSettledUserID = varBillSettledUserID
        !BillSettledPaymentMethodID = varBillSettledPaymentMethodID
        !BillSettledComments = varBillSettledComments
        !IsBHT = varIsBHT
        !IsGSB = varIsGSB
        !HospitalComments = varHospitalComments
        !Foreigner = varForeigner
        !TemAge = varTemAge
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varBHTID = !NewID
        Else
            varBHTID = !BHTID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBHT WHERE BHTID = " & varBHTID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!BHTID) Then
               varBHTID = !BHTID
            End If
            If Not IsNull(!BHT) Then
               varBHT = !BHT
            End If
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!DOA) Then
               varDOA = !DOA
            End If
            If Not IsNull(!TOA) Then
               varTOA = !TOA
            End If
            If Not IsNull(!DOD) Then
               varDOD = !DOD
            End If
            If Not IsNull(!TOD) Then
               varTOD = !TOD
            End If
            If Not IsNull(!AdStaffID) Then
               varAdStaffID = !AdStaffID
            End If
            If Not IsNull(!DisStaffID) Then
               varDisStaffID = !DisStaffID
            End If
            If Not IsNull(!Discharge) Then
               varDischarge = !Discharge
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!FullyPaid) Then
               varFullyPaid = !FullyPaid
            End If
            If Not IsNull(!Price) Then
               varPrice = !Price
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!DiscountPercent) Then
               varDiscountPercent = !DiscountPercent
            End If
            If Not IsNull(!NetPrice) Then
               varNetPrice = !NetPrice
            End If
            If Not IsNull(!CheckedStaffID) Then
               varCheckedStaffID = !CheckedStaffID
            End If
            If Not IsNull(!CurrentPrice) Then
               varCurrentPrice = !CurrentPrice
            End If
            If Not IsNull(!Balance) Then
               varBalance = !Balance
            End If
            If Not IsNull(!RoomID) Then
               varRoomID = !RoomID
            End If
            If Not IsNull(!HealthSchemeSupplierID) Then
               varHealthSchemeSupplierID = !HealthSchemeSupplierID
            End If
            If Not IsNull(!GuardianName) Then
               varGuardianName = !GuardianName
            End If
            If Not IsNull(!GuardianAddress) Then
               varGuardianAddress = !GuardianAddress
            End If
            If Not IsNull(!GuardianNIC) Then
               varGuardianNIC = !GuardianNIC
            End If
            If Not IsNull(!GuardianPhone) Then
               varGuardianPhone = !GuardianPhone
            End If
            If Not IsNull(!PatientCategoryID) Then
               varPatientCategoryID = !PatientCategoryID
            End If
            If Not IsNull(!AdmissionRate) Then
               varAdmissionRate = !AdmissionRate
            End If
            If Not IsNull(!InitialLinanRate) Then
               varInitialLinanRate = !InitialLinanRate
            End If
            If Not IsNull(!LaterLinanRate) Then
               varLaterLinanRate = !LaterLinanRate
            End If
            If Not IsNull(!MaintananceRate) Then
               varMaintananceRate = !MaintananceRate
            End If
            If Not IsNull(!MaintainaceCashDiscountRate) Then
               varMaintainaceCashDiscountRate = !MaintainaceCashDiscountRate
            End If
            If Not IsNull(!NursingRate) Then
               varNursingRate = !NursingRate
            End If
            If Not IsNull(!ICUNursingRate) Then
               varICUNursingRate = !ICUNursingRate
            End If
            If Not IsNull(!PtSurcharge) Then
               varPtSurcharge = !PtSurcharge
            End If
            If Not IsNull(!ComSurcharge) Then
               varComSurcharge = !ComSurcharge
            End If
            If Not IsNull(!AdmissionFee) Then
               varAdmissionFee = !AdmissionFee
            End If
            If Not IsNull(!AdmissionCharge) Then
               varAdmissionCharge = !AdmissionCharge
            End If
            If Not IsNull(!LinanCharge) Then
               varLinanCharge = !LinanCharge
            End If
            If Not IsNull(!RoomCharge) Then
               varRoomCharge = !RoomCharge
            End If
            If Not IsNull(!ServicesCharge) Then
               varServicesCharge = !ServicesCharge
            End If
            If Not IsNull(!MaintananceCharge) Then
               varMaintananceCharge = !MaintananceCharge
            End If
            If Not IsNull(!NursingCharge) Then
               varNursingCharge = !NursingCharge
            End If
            If Not IsNull(!ProfessionalCharge) Then
               varProfessionalCharge = !ProfessionalCharge
            End If
            If Not IsNull(!AdditionalCharge) Then
               varAdditionalCharge = !AdditionalCharge
            End If
            If Not IsNull(!MedicineCharge) Then
               varMedicineCharge = !MedicineCharge
            End If
            If Not IsNull(!TotalCharge) Then
               varTotalCharge = !TotalCharge
            End If
            If Not IsNull(!Payments) Then
               varPayments = !Payments
            End If
            If Not IsNull(!FAdmissionRate) Then
               varFAdmissionRate = !FAdmissionRate
            End If
            If Not IsNull(!FInitialLinanRate) Then
               varFInitialLinanRate = !FInitialLinanRate
            End If
            If Not IsNull(!FLaterLinanRate) Then
               varFLaterLinanRate = !FLaterLinanRate
            End If
            If Not IsNull(!FMaintananceRate) Then
               varFMaintananceRate = !FMaintananceRate
            End If
            If Not IsNull(!FMaintainaceCashDiscountRate) Then
               varFMaintainaceCashDiscountRate = !FMaintainaceCashDiscountRate
            End If
            If Not IsNull(!FNursingRate) Then
               varFNursingRate = !FNursingRate
            End If
            If Not IsNull(!FICUNursingRate) Then
               varFICUNursingRate = !FICUNursingRate
            End If
            If Not IsNull(!FAdmissionFee) Then
               varFAdmissionFee = !FAdmissionFee
            End If
            If Not IsNull(!FAdmissionCharge) Then
               varFAdmissionCharge = !FAdmissionCharge
            End If
            If Not IsNull(!FLinanCharge) Then
               varFLinanCharge = !FLinanCharge
            End If
            If Not IsNull(!FRoomCharge) Then
               varFRoomCharge = !FRoomCharge
            End If
            If Not IsNull(!FServicesCharge) Then
               varFServicesCharge = !FServicesCharge
            End If
            If Not IsNull(!FMaintananceCharge) Then
               varFMaintananceCharge = !FMaintananceCharge
            End If
            If Not IsNull(!FNursingCharge) Then
               varFNursingCharge = !FNursingCharge
            End If
            If Not IsNull(!FProfessionalCharge) Then
               varFProfessionalCharge = !FProfessionalCharge
            End If
            If Not IsNull(!FMedicineCharge) Then
               varFMedicineCharge = !FMedicineCharge
            End If
            If Not IsNull(!FAdditionalCharge) Then
               varFAdditionalCharge = !FAdditionalCharge
            End If
            If Not IsNull(!FTotalCharge) Then
               varFTotalCharge = !FTotalCharge
            End If
            If Not IsNull(!FPayments) Then
               varFPayments = !FPayments
            End If
            If Not IsNull(!InwardAddition) Then
               varInwardAddition = !InwardAddition
            End If
            If Not IsNull(!ReferringDoctorID) Then
               varReferringDoctorID = !ReferringDoctorID
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!BillSettled) Then
               varBillSettled = !BillSettled
            End If
            If Not IsNull(!BillSettledDate) Then
               varBillSettledDate = !BillSettledDate
            End If
            If Not IsNull(!BillSettledTime) Then
               varBillSettledTime = !BillSettledTime
            End If
            If Not IsNull(!BillSettledDateTime) Then
               varBillSettledDateTime = !BillSettledDateTime
            End If
            If Not IsNull(!BillSettledUserID) Then
               varBillSettledUserID = !BillSettledUserID
            End If
            If Not IsNull(!BillSettledPaymentMethodID) Then
               varBillSettledPaymentMethodID = !BillSettledPaymentMethodID
            End If
            If Not IsNull(!BillSettledComments) Then
               varBillSettledComments = !BillSettledComments
            End If
            If Not IsNull(!IsBHT) Then
               varIsBHT = !IsBHT
            End If
            If Not IsNull(!IsGSB) Then
               varIsGSB = !IsGSB
            End If
            If Not IsNull(!HospitalComments) Then
               varHospitalComments = !HospitalComments
            End If
            If Not IsNull(!Foreigner) Then
               varForeigner = !Foreigner
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!TemAge) Then
               varTemAge = !TemAge
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varBHTID = 0
    varBHT = Empty
    varPatientID = 0
    varDOA = Empty
    varTOA = Empty
    varDOD = Empty
    varTOD = Empty
    varAdStaffID = 0
    varDisStaffID = 0
    varDischarge = False
    varPaymentMethodID = 0
    varFullyPaid = False
    varPrice = 0
    varDiscount = 0
    varDiscountPercent = Empty
    varNetPrice = 0
    varCheckedStaffID = 0
    varCurrentPrice = 0
    varBalance = 0
    varRoomID = 0
    varHealthSchemeSupplierID = 0
    varGuardianName = Empty
    varGuardianAddress = Empty
    varGuardianNIC = Empty
    varGuardianPhone = Empty
    varPatientCategoryID = 0
    varAdmissionRate = 0
    varInitialLinanRate = 0
    varLaterLinanRate = 0
    varMaintananceRate = 0
    varMaintainaceCashDiscountRate = 0
    varNursingRate = 0
    varICUNursingRate = 0
    varPtSurcharge = 0
    varComSurcharge = 0
    varAdmissionFee = 0
    varAdmissionCharge = 0
    varLinanCharge = 0
    varRoomCharge = 0
    varServicesCharge = 0
    varMaintananceCharge = 0
    varNursingCharge = 0
    varProfessionalCharge = 0
    varAdditionalCharge = 0
    varMedicineCharge = 0
    varTotalCharge = 0
    varPayments = 0
    varFAdmissionRate = 0
    varFInitialLinanRate = 0
    varFLaterLinanRate = 0
    varFMaintananceRate = 0
    varFMaintainaceCashDiscountRate = 0
    varFNursingRate = 0
    varFICUNursingRate = 0
    varFAdmissionFee = 0
    varFAdmissionCharge = 0
    varFLinanCharge = 0
    varFRoomCharge = 0
    varFServicesCharge = 0
    varFMaintananceCharge = 0
    varFNursingCharge = 0
    varFProfessionalCharge = 0
    varFMedicineCharge = 0
    varFAdditionalCharge = 0
    varFTotalCharge = 0
    varFPayments = 0
    varInwardAddition = 0
    varReferringDoctorID = 0
    varComments = Empty
    varBillSettled = False
    varBillSettledDate = Empty
    varBillSettledTime = Empty
    varBillSettledDateTime = Empty
    varBillSettledUserID = 0
    varBillSettledPaymentMethodID = 0
    varBillSettledComments = Empty
    varIsBHT = False
    varIsGSB = False
    varHospitalComments = Empty
    varForeigner = False
    varupsize_ts = Empty
    varTemAge = Empty
End Sub

Public Property Let BHTID(ByVal vBHTID As Long)
    Call clearData
    varBHTID = vBHTID
    Call loadData
End Property

Public Property Get BHTID() As Long
    BHTID = varBHTID
End Property

Public Property Let BHT(ByVal vBHT As String)
    varBHT = vBHT
End Property

Public Property Get BHT() As String
    BHT = varBHT
End Property

Public Property Let PatientID(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let DOA(ByVal vDOA)
    varDOA = vDOA
End Property

Public Property Get DOA()
    DOA = varDOA
End Property

Public Property Let TOA(ByVal vTOA)
    varTOA = vTOA
End Property

Public Property Get TOA()
    TOA = varTOA
End Property

Public Property Let DOD(ByVal vDOD)
    varDOD = vDOD
End Property

Public Property Get DOD()
    DOD = varDOD
End Property

Public Property Let TOD(ByVal vTOD)
    varTOD = vTOD
End Property

Public Property Get TOD()
    TOD = varTOD
End Property

Public Property Let AdStaffID(ByVal vAdStaffID As Long)
    varAdStaffID = vAdStaffID
End Property

Public Property Get AdStaffID() As Long
    AdStaffID = varAdStaffID
End Property

Public Property Let DisStaffID(ByVal vDisStaffID As Long)
    varDisStaffID = vDisStaffID
End Property

Public Property Get DisStaffID() As Long
    DisStaffID = varDisStaffID
End Property

Public Property Let Discharge(ByVal vDischarge As Boolean)
    varDischarge = vDischarge
End Property

Public Property Get Discharge() As Boolean
    Discharge = varDischarge
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let FullyPaid(ByVal vFullyPaid As Boolean)
    varFullyPaid = vFullyPaid
End Property

Public Property Get FullyPaid() As Boolean
    FullyPaid = varFullyPaid
End Property

Public Property Let Price(ByVal vPrice As Double)
    varPrice = vPrice
End Property

Public Property Get Price() As Double
    Price = varPrice
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let DiscountPercent(ByVal vDiscountPercent)
    varDiscountPercent = vDiscountPercent
End Property

Public Property Get DiscountPercent()
    DiscountPercent = varDiscountPercent
End Property

Public Property Let NetPrice(ByVal vNetPrice As Double)
    varNetPrice = vNetPrice
End Property

Public Property Get NetPrice() As Double
    NetPrice = varNetPrice
End Property

Public Property Let CheckedStaffID(ByVal vCheckedStaffID As Long)
    varCheckedStaffID = vCheckedStaffID
End Property

Public Property Get CheckedStaffID() As Long
    CheckedStaffID = varCheckedStaffID
End Property

Public Property Let CurrentPrice(ByVal vCurrentPrice As Double)
    varCurrentPrice = vCurrentPrice
End Property

Public Property Get CurrentPrice() As Double
    CurrentPrice = varCurrentPrice
End Property

Public Property Let Balance(ByVal vBalance As Double)
    varBalance = vBalance
End Property

Public Property Get Balance() As Double
    Balance = varBalance
End Property

Public Property Let RoomID(ByVal vRoomID As Long)
    varRoomID = vRoomID
End Property

Public Property Get RoomID() As Long
    RoomID = varRoomID
End Property

Public Property Let HealthSchemeSupplierID(ByVal vHealthSchemeSupplierID As Long)
    varHealthSchemeSupplierID = vHealthSchemeSupplierID
End Property

Public Property Get HealthSchemeSupplierID() As Long
    HealthSchemeSupplierID = varHealthSchemeSupplierID
End Property

Public Property Let GuardianName(ByVal vGuardianName As String)
    varGuardianName = vGuardianName
End Property

Public Property Get GuardianName() As String
    GuardianName = varGuardianName
End Property

Public Property Let GuardianAddress(ByVal vGuardianAddress As String)
    varGuardianAddress = vGuardianAddress
End Property

Public Property Get GuardianAddress() As String
    GuardianAddress = varGuardianAddress
End Property

Public Property Let GuardianNIC(ByVal vGuardianNIC As String)
    varGuardianNIC = vGuardianNIC
End Property

Public Property Get GuardianNIC() As String
    GuardianNIC = varGuardianNIC
End Property

Public Property Let GuardianPhone(ByVal vGuardianPhone As String)
    varGuardianPhone = vGuardianPhone
End Property

Public Property Get GuardianPhone() As String
    GuardianPhone = varGuardianPhone
End Property

Public Property Let PatientCategoryID(ByVal vPatientCategoryID As Long)
    varPatientCategoryID = vPatientCategoryID
End Property

Public Property Get PatientCategoryID() As Long
    PatientCategoryID = varPatientCategoryID
End Property

Public Property Let AdmissionRate(ByVal vAdmissionRate As Double)
    varAdmissionRate = vAdmissionRate
End Property

Public Property Get AdmissionRate() As Double
    AdmissionRate = varAdmissionRate
End Property

Public Property Let InitialLinanRate(ByVal vInitialLinanRate As Double)
    varInitialLinanRate = vInitialLinanRate
End Property

Public Property Get InitialLinanRate() As Double
    InitialLinanRate = varInitialLinanRate
End Property

Public Property Let LaterLinanRate(ByVal vLaterLinanRate As Double)
    varLaterLinanRate = vLaterLinanRate
End Property

Public Property Get LaterLinanRate() As Double
    LaterLinanRate = varLaterLinanRate
End Property

Public Property Let MaintananceRate(ByVal vMaintananceRate As Double)
    varMaintananceRate = vMaintananceRate
End Property

Public Property Get MaintananceRate() As Double
    MaintananceRate = varMaintananceRate
End Property

Public Property Let MaintainaceCashDiscountRate(ByVal vMaintainaceCashDiscountRate As Double)
    varMaintainaceCashDiscountRate = vMaintainaceCashDiscountRate
End Property

Public Property Get MaintainaceCashDiscountRate() As Double
    MaintainaceCashDiscountRate = varMaintainaceCashDiscountRate
End Property

Public Property Let NursingRate(ByVal vNursingRate As Double)
    varNursingRate = vNursingRate
End Property

Public Property Get NursingRate() As Double
    NursingRate = varNursingRate
End Property

Public Property Let ICUNursingRate(ByVal vICUNursingRate As Double)
    varICUNursingRate = vICUNursingRate
End Property

Public Property Get ICUNursingRate() As Double
    ICUNursingRate = varICUNursingRate
End Property

Public Property Let PtSurcharge(ByVal vPtSurcharge As Double)
    varPtSurcharge = vPtSurcharge
End Property

Public Property Get PtSurcharge() As Double
    PtSurcharge = varPtSurcharge
End Property

Public Property Let ComSurcharge(ByVal vComSurcharge As Double)
    varComSurcharge = vComSurcharge
End Property

Public Property Get ComSurcharge() As Double
    ComSurcharge = varComSurcharge
End Property

Public Property Let AdmissionFee(ByVal vAdmissionFee As Double)
    varAdmissionFee = vAdmissionFee
End Property

Public Property Get AdmissionFee() As Double
    AdmissionFee = varAdmissionFee
End Property

Public Property Let AdmissionCharge(ByVal vAdmissionCharge As Double)
    varAdmissionCharge = vAdmissionCharge
End Property

Public Property Get AdmissionCharge() As Double
    AdmissionCharge = varAdmissionCharge
End Property

Public Property Let LinanCharge(ByVal vLinanCharge As Double)
    varLinanCharge = vLinanCharge
End Property

Public Property Get LinanCharge() As Double
    LinanCharge = varLinanCharge
End Property

Public Property Let RoomCharge(ByVal vRoomCharge As Double)
    varRoomCharge = vRoomCharge
End Property

Public Property Get RoomCharge() As Double
    RoomCharge = varRoomCharge
End Property

Public Property Let ServicesCharge(ByVal vServicesCharge As Double)
    varServicesCharge = vServicesCharge
End Property

Public Property Get ServicesCharge() As Double
    ServicesCharge = varServicesCharge
End Property

Public Property Let MaintananceCharge(ByVal vMaintananceCharge As Double)
    varMaintananceCharge = vMaintananceCharge
End Property

Public Property Get MaintananceCharge() As Double
    MaintananceCharge = varMaintananceCharge
End Property

Public Property Let NursingCharge(ByVal vNursingCharge As Double)
    varNursingCharge = vNursingCharge
End Property

Public Property Get NursingCharge() As Double
    NursingCharge = varNursingCharge
End Property

Public Property Let ProfessionalCharge(ByVal vProfessionalCharge As Double)
    varProfessionalCharge = vProfessionalCharge
End Property

Public Property Get ProfessionalCharge() As Double
    ProfessionalCharge = varProfessionalCharge
End Property

Public Property Let AdditionalCharge(ByVal vAdditionalCharge As Double)
    varAdditionalCharge = vAdditionalCharge
End Property

Public Property Get AdditionalCharge() As Double
    AdditionalCharge = varAdditionalCharge
End Property

Public Property Let MedicineCharge(ByVal vMedicineCharge As Double)
    varMedicineCharge = vMedicineCharge
End Property

Public Property Get MedicineCharge() As Double
    MedicineCharge = varMedicineCharge
End Property

Public Property Let TotalCharge(ByVal vTotalCharge As Double)
    varTotalCharge = vTotalCharge
End Property

Public Property Get TotalCharge() As Double
    TotalCharge = varTotalCharge
End Property

Public Property Let Payments(ByVal vPayments As Double)
    varPayments = vPayments
End Property

Public Property Get Payments() As Double
    Payments = varPayments
End Property

Public Property Let FAdmissionRate(ByVal vFAdmissionRate As Double)
    varFAdmissionRate = vFAdmissionRate
End Property

Public Property Get FAdmissionRate() As Double
    FAdmissionRate = varFAdmissionRate
End Property

Public Property Let FInitialLinanRate(ByVal vFInitialLinanRate As Double)
    varFInitialLinanRate = vFInitialLinanRate
End Property

Public Property Get FInitialLinanRate() As Double
    FInitialLinanRate = varFInitialLinanRate
End Property

Public Property Let FLaterLinanRate(ByVal vFLaterLinanRate As Double)
    varFLaterLinanRate = vFLaterLinanRate
End Property

Public Property Get FLaterLinanRate() As Double
    FLaterLinanRate = varFLaterLinanRate
End Property

Public Property Let FMaintananceRate(ByVal vFMaintananceRate As Double)
    varFMaintananceRate = vFMaintananceRate
End Property

Public Property Get FMaintananceRate() As Double
    FMaintananceRate = varFMaintananceRate
End Property

Public Property Let FMaintainaceCashDiscountRate(ByVal vFMaintainaceCashDiscountRate As Long)
    varFMaintainaceCashDiscountRate = vFMaintainaceCashDiscountRate
End Property

Public Property Get FMaintainaceCashDiscountRate() As Long
    FMaintainaceCashDiscountRate = varFMaintainaceCashDiscountRate
End Property

Public Property Let FNursingRate(ByVal vFNursingRate As Double)
    varFNursingRate = vFNursingRate
End Property

Public Property Get FNursingRate() As Double
    FNursingRate = varFNursingRate
End Property

Public Property Let FICUNursingRate(ByVal vFICUNursingRate As Long)
    varFICUNursingRate = vFICUNursingRate
End Property

Public Property Get FICUNursingRate() As Long
    FICUNursingRate = varFICUNursingRate
End Property

Public Property Let FAdmissionFee(ByVal vFAdmissionFee As Double)
    varFAdmissionFee = vFAdmissionFee
End Property

Public Property Get FAdmissionFee() As Double
    FAdmissionFee = varFAdmissionFee
End Property

Public Property Let FAdmissionCharge(ByVal vFAdmissionCharge As Double)
    varFAdmissionCharge = vFAdmissionCharge
End Property

Public Property Get FAdmissionCharge() As Double
    FAdmissionCharge = varFAdmissionCharge
End Property

Public Property Let FLinanCharge(ByVal vFLinanCharge As Double)
    varFLinanCharge = vFLinanCharge
End Property

Public Property Get FLinanCharge() As Double
    FLinanCharge = varFLinanCharge
End Property

Public Property Let FRoomCharge(ByVal vFRoomCharge As Double)
    varFRoomCharge = vFRoomCharge
End Property

Public Property Get FRoomCharge() As Double
    FRoomCharge = varFRoomCharge
End Property

Public Property Let FServicesCharge(ByVal vFServicesCharge As Double)
    varFServicesCharge = vFServicesCharge
End Property

Public Property Get FServicesCharge() As Double
    FServicesCharge = varFServicesCharge
End Property

Public Property Let FMaintananceCharge(ByVal vFMaintananceCharge As Double)
    varFMaintananceCharge = vFMaintananceCharge
End Property

Public Property Get FMaintananceCharge() As Double
    FMaintananceCharge = varFMaintananceCharge
End Property

Public Property Let FNursingCharge(ByVal vFNursingCharge As Double)
    varFNursingCharge = vFNursingCharge
End Property

Public Property Get FNursingCharge() As Double
    FNursingCharge = varFNursingCharge
End Property

Public Property Let FProfessionalCharge(ByVal vFProfessionalCharge As Double)
    varFProfessionalCharge = vFProfessionalCharge
End Property

Public Property Get FProfessionalCharge() As Double
    FProfessionalCharge = varFProfessionalCharge
End Property

Public Property Let FMedicineCharge(ByVal vFMedicineCharge As Double)
    varFMedicineCharge = vFMedicineCharge
End Property

Public Property Get FMedicineCharge() As Double
    FMedicineCharge = varFMedicineCharge
End Property

Public Property Let FAdditionalCharge(ByVal vFAdditionalCharge As Long)
    varFAdditionalCharge = vFAdditionalCharge
End Property

Public Property Get FAdditionalCharge() As Long
    FAdditionalCharge = varFAdditionalCharge
End Property

Public Property Let FTotalCharge(ByVal vFTotalCharge As Double)
    varFTotalCharge = vFTotalCharge
End Property

Public Property Get FTotalCharge() As Double
    FTotalCharge = varFTotalCharge
End Property

Public Property Let FPayments(ByVal vFPayments As Long)
    varFPayments = vFPayments
End Property

Public Property Get FPayments() As Long
    FPayments = varFPayments
End Property

Public Property Let InwardAddition(ByVal vInwardAddition As Double)
    varInwardAddition = vInwardAddition
End Property

Public Property Get InwardAddition() As Double
    InwardAddition = varInwardAddition
End Property

Public Property Let ReferringDoctorID(ByVal vReferringDoctorID As Long)
    varReferringDoctorID = vReferringDoctorID
End Property

Public Property Get ReferringDoctorID() As Long
    ReferringDoctorID = varReferringDoctorID
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let BillSettled(ByVal vBillSettled As Boolean)
    varBillSettled = vBillSettled
End Property

Public Property Get BillSettled() As Boolean
    BillSettled = varBillSettled
End Property

Public Property Let BillSettledDate(ByVal vBillSettledDate)
    varBillSettledDate = vBillSettledDate
End Property

Public Property Get BillSettledDate()
    BillSettledDate = varBillSettledDate
End Property

Public Property Let BillSettledTime(ByVal vBillSettledTime)
    varBillSettledTime = vBillSettledTime
End Property

Public Property Get BillSettledTime()
    BillSettledTime = varBillSettledTime
End Property

Public Property Let BillSettledDateTime(ByVal vBillSettledDateTime)
    varBillSettledDateTime = vBillSettledDateTime
End Property

Public Property Get BillSettledDateTime()
    BillSettledDateTime = varBillSettledDateTime
End Property

Public Property Let BillSettledUserID(ByVal vBillSettledUserID As Long)
    varBillSettledUserID = vBillSettledUserID
End Property

Public Property Get BillSettledUserID() As Long
    BillSettledUserID = varBillSettledUserID
End Property

Public Property Let BillSettledPaymentMethodID(ByVal vBillSettledPaymentMethodID As Long)
    varBillSettledPaymentMethodID = vBillSettledPaymentMethodID
End Property

Public Property Get BillSettledPaymentMethodID() As Long
    BillSettledPaymentMethodID = varBillSettledPaymentMethodID
End Property

Public Property Let BillSettledComments(ByVal vBillSettledComments As String)
    varBillSettledComments = vBillSettledComments
End Property

Public Property Get BillSettledComments() As String
    BillSettledComments = varBillSettledComments
End Property

Public Property Let IsBHT(ByVal vIsBHT As Boolean)
    varIsBHT = vIsBHT
End Property

Public Property Get IsBHT() As Boolean
    IsBHT = varIsBHT
End Property

Public Property Let IsGSB(ByVal vIsGSB As Boolean)
    varIsGSB = vIsGSB
End Property

Public Property Get IsGSB() As Boolean
    IsGSB = varIsGSB
End Property

Public Property Let HospitalComments(ByVal vHospitalComments As String)
    varHospitalComments = vHospitalComments
End Property

Public Property Get HospitalComments() As String
    HospitalComments = varHospitalComments
End Property

Public Property Let Foreigner(ByVal vForeigner As Boolean)
    varForeigner = vForeigner
End Property

Public Property Get Foreigner() As Boolean
    Foreigner = varForeigner
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let TemAge(ByVal vTemAge As String)
    varTemAge = vTemAge
End Property

Public Property Get TemAge() As String
    TemAge = varTemAge
End Property


