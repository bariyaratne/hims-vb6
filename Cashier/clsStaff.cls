VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsStaff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varStaffID As Long
    Private varTitleID As Long
    Private varSexID As Long
    Private varName As String
    Private varListedName As String
    Private varQualifications As String
    Private varRegistation As String
    Private varDesignation As String
    Private varSpecialityID As Long
    Private varAuthorityID As Long
    Private varPrivateAddress As String
    Private varPrivatePhone As String
    Private varPrivateFax As String
    Private varPrivateEmail As String
    Private varMobilePhone As String
    Private varOfficialAddress As String
    Private varOfficialPhone As String
    Private varOfficialFax As String
    Private varOfficialEmail As String
    Private varWebsite As String
    Private varComments As String
    Private varPhoto As String
    Private varSignature As String
    Private varNextOfKin As String
    Private varNextOfKinDetails As String
    Private varUserName As String
    Private varPassword As String
    Private varUser As Boolean
    Private varLogged As Boolean
    Private varDeleted As Boolean
    Private varCredit As Double
    Private varupsize_ts
    Private varIsAUser As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStaff Where StaffID = " & varStaffID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !StaffID = varStaffID
        !TitleID = varTitleID
        !SexID = varSexID
        !Name = varName
        !ListedName = varListedName
        !Qualifications = varQualifications
        !Registation = varRegistation
        !Designation = varDesignation
        !SpecialityID = varSpecialityID
        !AuthorityID = varAuthorityID
        !PrivateAddress = varPrivateAddress
        !PrivatePhone = varPrivatePhone
        !PrivateFax = varPrivateFax
        !PrivateEmail = varPrivateEmail
        !MobilePhone = varMobilePhone
        !OfficialAddress = varOfficialAddress
        !OfficialPhone = varOfficialPhone
        !OfficialFax = varOfficialFax
        !OfficialEmail = varOfficialEmail
        !Website = varWebsite
        !Comments = varComments
        !Photo = varPhoto
        !Signature = varSignature
        !NextOfKin = varNextOfKin
        !NextOfKinDetails = varNextOfKinDetails
        !UserName = varUserName
        !Password = varPassword
        !User = varUser
        !Logged = varLogged
        !Deleted = varDeleted
        !Credit = varCredit
        !IsAUser = varIsAUser
        .Update
        varStaffID = !StaffID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStaff WHERE StaffID = " & varStaffID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!TitleID) Then
               varTitleID = !TitleID
            End If
            If Not IsNull(!SexID) Then
               varSexID = !SexID
            End If
            If Not IsNull(!Name) Then
               varName = !Name
            End If
            If Not IsNull(!ListedName) Then
               varListedName = !ListedName
            End If
            If Not IsNull(!Qualifications) Then
               varQualifications = !Qualifications
            End If
            If Not IsNull(!Registation) Then
               varRegistation = !Registation
            End If
            If Not IsNull(!Designation) Then
               varDesignation = !Designation
            End If
            If Not IsNull(!SpecialityID) Then
               varSpecialityID = !SpecialityID
            End If
            If Not IsNull(!AuthorityID) Then
               varAuthorityID = !AuthorityID
            End If
            If Not IsNull(!PrivateAddress) Then
               varPrivateAddress = !PrivateAddress
            End If
            If Not IsNull(!PrivatePhone) Then
               varPrivatePhone = !PrivatePhone
            End If
            If Not IsNull(!PrivateFax) Then
               varPrivateFax = !PrivateFax
            End If
            If Not IsNull(!PrivateEmail) Then
               varPrivateEmail = !PrivateEmail
            End If
            If Not IsNull(!MobilePhone) Then
               varMobilePhone = !MobilePhone
            End If
            If Not IsNull(!OfficialAddress) Then
               varOfficialAddress = !OfficialAddress
            End If
            If Not IsNull(!OfficialPhone) Then
               varOfficialPhone = !OfficialPhone
            End If
            If Not IsNull(!OfficialFax) Then
               varOfficialFax = !OfficialFax
            End If
            If Not IsNull(!OfficialEmail) Then
               varOfficialEmail = !OfficialEmail
            End If
            If Not IsNull(!Website) Then
               varWebsite = !Website
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!Photo) Then
               varPhoto = !Photo
            End If
            If Not IsNull(!Signature) Then
               varSignature = !Signature
            End If
            If Not IsNull(!NextOfKin) Then
               varNextOfKin = !NextOfKin
            End If
            If Not IsNull(!NextOfKinDetails) Then
               varNextOfKinDetails = !NextOfKinDetails
            End If
            If Not IsNull(!UserName) Then
               varUserName = !UserName
            End If
            If Not IsNull(!Password) Then
               varPassword = !Password
            End If
            If Not IsNull(!User) Then
               varUser = !User
            End If
            If Not IsNull(!Logged) Then
               varLogged = !Logged
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!Credit) Then
               varCredit = !Credit
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!IsAUser) Then
               varIsAUser = !IsAUser
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varStaffID = 0
    varTitleID = 0
    varSexID = 0
    varName = Empty
    varListedName = Empty
    varQualifications = Empty
    varRegistation = Empty
    varDesignation = Empty
    varSpecialityID = 0
    varAuthorityID = 0
    varPrivateAddress = Empty
    varPrivatePhone = Empty
    varPrivateFax = Empty
    varPrivateEmail = Empty
    varMobilePhone = Empty
    varOfficialAddress = Empty
    varOfficialPhone = Empty
    varOfficialFax = Empty
    varOfficialEmail = Empty
    varWebsite = Empty
    varComments = Empty
    varPhoto = Empty
    varSignature = Empty
    varNextOfKin = Empty
    varNextOfKinDetails = Empty
    varUserName = Empty
    varPassword = Empty
    varUser = False
    varLogged = False
    varDeleted = False
    varCredit = 0
    varupsize_ts = Empty
    varIsAUser = False
End Sub

Public Property Let StaffID(ByVal vStaffID As Long)
    Call clearData
    varStaffID = vStaffID
    Call loadData
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let TitleID(ByVal vTitleID As Long)
    varTitleID = vTitleID
End Property

Public Property Get TitleID() As Long
    TitleID = varTitleID
End Property

Public Property Let SexID(ByVal vSexID As Long)
    varSexID = vSexID
End Property

Public Property Get SexID() As Long
    SexID = varSexID
End Property

Public Property Let Name(ByVal vName As String)
    varName = vName
End Property

Public Property Get Name() As String
    Name = varName
End Property

Public Property Let ListedName(ByVal vListedName As String)
    varListedName = vListedName
End Property

Public Property Get ListedName() As String
    ListedName = varListedName
End Property

Public Property Let Qualifications(ByVal vQualifications As String)
    varQualifications = vQualifications
End Property

Public Property Get Qualifications() As String
    Qualifications = varQualifications
End Property

Public Property Let Registation(ByVal vRegistation As String)
    varRegistation = vRegistation
End Property

Public Property Get Registation() As String
    Registation = varRegistation
End Property

Public Property Let Designation(ByVal vDesignation As String)
    varDesignation = vDesignation
End Property

Public Property Get Designation() As String
    Designation = varDesignation
End Property

Public Property Let SpecialityID(ByVal vSpecialityID As Long)
    varSpecialityID = vSpecialityID
End Property

Public Property Get SpecialityID() As Long
    SpecialityID = varSpecialityID
End Property

Public Property Let AuthorityID(ByVal vAuthorityID As Long)
    varAuthorityID = vAuthorityID
End Property

Public Property Get AuthorityID() As Long
    AuthorityID = varAuthorityID
End Property

Public Property Let PrivateAddress(ByVal vPrivateAddress As String)
    varPrivateAddress = vPrivateAddress
End Property

Public Property Get PrivateAddress() As String
    PrivateAddress = varPrivateAddress
End Property

Public Property Let PrivatePhone(ByVal vPrivatePhone As String)
    varPrivatePhone = vPrivatePhone
End Property

Public Property Get PrivatePhone() As String
    PrivatePhone = varPrivatePhone
End Property

Public Property Let PrivateFax(ByVal vPrivateFax As String)
    varPrivateFax = vPrivateFax
End Property

Public Property Get PrivateFax() As String
    PrivateFax = varPrivateFax
End Property

Public Property Let PrivateEmail(ByVal vPrivateEmail As String)
    varPrivateEmail = vPrivateEmail
End Property

Public Property Get PrivateEmail() As String
    PrivateEmail = varPrivateEmail
End Property

Public Property Let MobilePhone(ByVal vMobilePhone As String)
    varMobilePhone = vMobilePhone
End Property

Public Property Get MobilePhone() As String
    MobilePhone = varMobilePhone
End Property

Public Property Let OfficialAddress(ByVal vOfficialAddress As String)
    varOfficialAddress = vOfficialAddress
End Property

Public Property Get OfficialAddress() As String
    OfficialAddress = varOfficialAddress
End Property

Public Property Let OfficialPhone(ByVal vOfficialPhone As String)
    varOfficialPhone = vOfficialPhone
End Property

Public Property Get OfficialPhone() As String
    OfficialPhone = varOfficialPhone
End Property

Public Property Let OfficialFax(ByVal vOfficialFax As String)
    varOfficialFax = vOfficialFax
End Property

Public Property Get OfficialFax() As String
    OfficialFax = varOfficialFax
End Property

Public Property Let OfficialEmail(ByVal vOfficialEmail As String)
    varOfficialEmail = vOfficialEmail
End Property

Public Property Get OfficialEmail() As String
    OfficialEmail = varOfficialEmail
End Property

Public Property Let Website(ByVal vWebsite As String)
    varWebsite = vWebsite
End Property

Public Property Get Website() As String
    Website = varWebsite
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let Photo(ByVal vPhoto As String)
    varPhoto = vPhoto
End Property

Public Property Get Photo() As String
    Photo = varPhoto
End Property

Public Property Let Signature(ByVal vSignature As String)
    varSignature = vSignature
End Property

Public Property Get Signature() As String
    Signature = varSignature
End Property

Public Property Let NextOfKin(ByVal vNextOfKin As String)
    varNextOfKin = vNextOfKin
End Property

Public Property Get NextOfKin() As String
    NextOfKin = varNextOfKin
End Property

Public Property Let NextOfKinDetails(ByVal vNextOfKinDetails As String)
    varNextOfKinDetails = vNextOfKinDetails
End Property

Public Property Get NextOfKinDetails() As String
    NextOfKinDetails = varNextOfKinDetails
End Property

Public Property Let UserName(ByVal vUserName As String)
    varUserName = vUserName
End Property

Public Property Get UserName() As String
    UserName = varUserName
End Property

Public Property Let Password(ByVal vPassword As String)
    varPassword = vPassword
End Property

Public Property Get Password() As String
    Password = varPassword
End Property

Public Property Let User(ByVal vUser As Boolean)
    varUser = vUser
End Property

Public Property Get User() As Boolean
    User = varUser
End Property

Public Property Let Logged(ByVal vLogged As Boolean)
    varLogged = vLogged
End Property

Public Property Get Logged() As Boolean
    Logged = varLogged
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let Credit(ByVal vCredit As Double)
    varCredit = vCredit
End Property

Public Property Get Credit() As Double
    Credit = varCredit
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let IsAUser(ByVal vIsAUser As Boolean)
    varIsAUser = vIsAUser
End Property

Public Property Get IsAUser() As Boolean
    IsAUser = varIsAUser
End Property


