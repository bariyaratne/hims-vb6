VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ProfessionalCharge"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varProfessionalChargesID As Long
    Private varPatientServiceCharge As Boolean
    Private varProfessionalCharge As Boolean
    Private varPatientServiceID As Long
    Private varStaffID As Long
    Private varUserID As Long
    Private varForBHTID As Long
    Private varForOPDBillID As Long
    Private varForLabBillID As Long
    Private varForMedicalTestBillID As Long
    Private varForHSTBillID As Long
    Private varForRBillID As Long
    Private varForExpenceBillID As Long
    Private varForIncomeBillID As Long
    Private varServiceProfessionalChargesID As Long
    Private varFee As Double
    Private varComments As String
    Private varCancelled As Boolean
    Private varCancelledUserID As Long
    Private varCancelledDate
    Private varCancelledTime
    Private varCancelledDateTime
    Private varDate
    Private varTime
    Private varPaidFee As Double
    Private varPaid As Boolean
    Private varPaidUserID As Long
    Private varPaidDate
    Private varPaidTime
    Private varPaidComments As String
    Private varPaidCancelled As Boolean
    Private varPaidCancelledDate
    Private varPaidCancelledTime
    Private varPaidCancelledUserID As Long
    Private varPaidCancelledDateTime
    Private varProfessionalPaymentBillID As Long
    Private varCancelledProfessionalPaymentBillID As Long
    Private varIsOPDBill As Boolean
    Private varIsLabBill As Boolean
    Private varIsPharmacyBill As Boolean
    Private varIsInwardPaymentBill As Boolean
    Private varIsMedicalTestBill As Boolean
    Private varIsGSBill As Boolean
    Private varIsHSTBill As Boolean
    Private varIsRBill As Boolean
    Private varIsIncomeBill As Boolean
    Private varIsExpenceBill As Boolean
    Private varupsize_ts
    Private varFeeCancelled As Double
    Private varFeeReturned As Double
    Private varFeePaid As Double
    Private varFeeToPay As Double
    Private varFeeToGive As Double
    Private varBilledDate
    Private varBillledTime

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblProfessionalCharges Where ProfessionalChargesID = " & varProfessionalChargesID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !PatientServiceCharge = varPatientServiceCharge
        !ProfessionalCharge = varProfessionalCharge
        !PatientServiceID = varPatientServiceID
        !StaffID = varStaffID
        !UserID = varUserID
        !ForBHTID = varForBHTID
        !ForOPDBillID = varForOPDBillID
        !ForLabBillID = varForLabBillID
        !ForMedicalTestBillID = varForMedicalTestBillID
        !ForHSTBillID = varForHSTBillID
        !ForRBillID = varForRBillID
        !ForExpenceBillID = varForExpenceBillID
        !ForIncomeBillID = varForIncomeBillID
        !ServiceProfessionalChargesID = varServiceProfessionalChargesID
        !Fee = varFee
        !Comments = varComments
        !Cancelled = varCancelled
        !CancelledUserID = varCancelledUserID
        !CancelledDate = varCancelledDate
        !CancelledTime = varCancelledTime
        !CancelledDateTime = varCancelledDateTime
        !Date = varDate
        !Time = varTime
        !PaidFee = varPaidFee
        !Paid = varPaid
        !PaidUserID = varPaidUserID
        !PaidDate = varPaidDate
        !PaidTime = varPaidTime
        !PaidComments = varPaidComments
        !PaidCancelled = varPaidCancelled
        !PaidCancelledDate = varPaidCancelledDate
        !PaidCancelledTime = varPaidCancelledTime
        !PaidCancelledUserID = varPaidCancelledUserID
        !PaidCancelledDateTime = varPaidCancelledDateTime
        !ProfessionalPaymentBillID = varProfessionalPaymentBillID
        !CancelledProfessionalPaymentBillID = varCancelledProfessionalPaymentBillID
        !IsOPDBill = varIsOPDBill
        !IsLabBill = varIsLabBill
        !IsPharmacyBill = varIsPharmacyBill
        !IsInwardPaymentBill = varIsInwardPaymentBill
        !IsMedicalTestBill = varIsMedicalTestBill
        !IsGSBill = varIsGSBill
        !IsHSTBill = varIsHSTBill
        !IsRBill = varIsRBill
        !IsIncomeBill = varIsIncomeBill
        !IsExpenceBill = varIsExpenceBill
        !FeeCancelled = varFeeCancelled
        !FeeReturned = varFeeReturned
        !FeePaid = varFeePaid
        !FeeToPay = varFeeToPay
        !FeeToGive = varFeeToGive
        !BilledDate = varBilledDate
        !BillledTime = varBillledTime
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varProfessionalChargesID = !NewID
        Else
            varProfessionalChargesID = !ProfessionalChargesID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblProfessionalCharges WHERE ProfessionalChargesID = " & varProfessionalChargesID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!ProfessionalChargesID) Then
               varProfessionalChargesID = !ProfessionalChargesID
            End If
            If Not IsNull(!PatientServiceCharge) Then
               varPatientServiceCharge = !PatientServiceCharge
            End If
            If Not IsNull(!ProfessionalCharge) Then
               varProfessionalCharge = !ProfessionalCharge
            End If
            If Not IsNull(!PatientServiceID) Then
               varPatientServiceID = !PatientServiceID
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!UserID) Then
               varUserID = !UserID
            End If
            If Not IsNull(!ForBHTID) Then
               varForBHTID = !ForBHTID
            End If
            If Not IsNull(!ForOPDBillID) Then
               varForOPDBillID = !ForOPDBillID
            End If
            If Not IsNull(!ForLabBillID) Then
               varForLabBillID = !ForLabBillID
            End If
            If Not IsNull(!ForMedicalTestBillID) Then
               varForMedicalTestBillID = !ForMedicalTestBillID
            End If
            If Not IsNull(!ForHSTBillID) Then
               varForHSTBillID = !ForHSTBillID
            End If
            If Not IsNull(!ForRBillID) Then
               varForRBillID = !ForRBillID
            End If
            If Not IsNull(!ForExpenceBillID) Then
               varForExpenceBillID = !ForExpenceBillID
            End If
            If Not IsNull(!ForIncomeBillID) Then
               varForIncomeBillID = !ForIncomeBillID
            End If
            If Not IsNull(!ServiceProfessionalChargesID) Then
               varServiceProfessionalChargesID = !ServiceProfessionalChargesID
            End If
            If Not IsNull(!Fee) Then
               varFee = !Fee
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!CancelledUserID) Then
               varCancelledUserID = !CancelledUserID
            End If
            If Not IsNull(!CancelledDate) Then
               varCancelledDate = !CancelledDate
            End If
            If Not IsNull(!CancelledTime) Then
               varCancelledTime = !CancelledTime
            End If
            If Not IsNull(!CancelledDateTime) Then
               varCancelledDateTime = !CancelledDateTime
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!PaidFee) Then
               varPaidFee = !PaidFee
            End If
            If Not IsNull(!Paid) Then
               varPaid = !Paid
            End If
            If Not IsNull(!PaidUserID) Then
               varPaidUserID = !PaidUserID
            End If
            If Not IsNull(!PaidDate) Then
               varPaidDate = !PaidDate
            End If
            If Not IsNull(!PaidTime) Then
               varPaidTime = !PaidTime
            End If
            If Not IsNull(!PaidComments) Then
               varPaidComments = !PaidComments
            End If
            If Not IsNull(!PaidCancelled) Then
               varPaidCancelled = !PaidCancelled
            End If
            If Not IsNull(!PaidCancelledDate) Then
               varPaidCancelledDate = !PaidCancelledDate
            End If
            If Not IsNull(!PaidCancelledTime) Then
               varPaidCancelledTime = !PaidCancelledTime
            End If
            If Not IsNull(!PaidCancelledUserID) Then
               varPaidCancelledUserID = !PaidCancelledUserID
            End If
            If Not IsNull(!PaidCancelledDateTime) Then
               varPaidCancelledDateTime = !PaidCancelledDateTime
            End If
            If Not IsNull(!ProfessionalPaymentBillID) Then
               varProfessionalPaymentBillID = !ProfessionalPaymentBillID
            End If
            If Not IsNull(!CancelledProfessionalPaymentBillID) Then
               varCancelledProfessionalPaymentBillID = !CancelledProfessionalPaymentBillID
            End If
            If Not IsNull(!IsOPDBill) Then
               varIsOPDBill = !IsOPDBill
            End If
            If Not IsNull(!IsLabBill) Then
               varIsLabBill = !IsLabBill
            End If
            If Not IsNull(!IsPharmacyBill) Then
               varIsPharmacyBill = !IsPharmacyBill
            End If
            If Not IsNull(!IsInwardPaymentBill) Then
               varIsInwardPaymentBill = !IsInwardPaymentBill
            End If
            If Not IsNull(!IsMedicalTestBill) Then
               varIsMedicalTestBill = !IsMedicalTestBill
            End If
            If Not IsNull(!IsGSBill) Then
               varIsGSBill = !IsGSBill
            End If
            If Not IsNull(!IsHSTBill) Then
               varIsHSTBill = !IsHSTBill
            End If
            If Not IsNull(!IsRBill) Then
               varIsRBill = !IsRBill
            End If
            If Not IsNull(!IsIncomeBill) Then
               varIsIncomeBill = !IsIncomeBill
            End If
            If Not IsNull(!IsExpenceBill) Then
               varIsExpenceBill = !IsExpenceBill
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!FeeCancelled) Then
               varFeeCancelled = !FeeCancelled
            End If
            If Not IsNull(!FeeReturned) Then
               varFeeReturned = !FeeReturned
            End If
            If Not IsNull(!FeePaid) Then
               varFeePaid = !FeePaid
            End If
            If Not IsNull(!FeeToPay) Then
               varFeeToPay = !FeeToPay
            End If
            If Not IsNull(!FeeToGive) Then
               varFeeToGive = !FeeToGive
            End If
            If Not IsNull(!BilledDate) Then
               varBilledDate = !BilledDate
            End If
            If Not IsNull(!BillledTime) Then
               varBillledTime = !BillledTime
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varProfessionalChargesID = 0
    varPatientServiceCharge = False
    varProfessionalCharge = False
    varPatientServiceID = 0
    varStaffID = 0
    varUserID = 0
    varForBHTID = 0
    varForOPDBillID = 0
    varForLabBillID = 0
    varForMedicalTestBillID = 0
    varForHSTBillID = 0
    varForRBillID = 0
    varForExpenceBillID = 0
    varForIncomeBillID = 0
    varServiceProfessionalChargesID = 0
    varFee = 0
    varComments = Empty
    varCancelled = False
    varCancelledUserID = 0
    varCancelledDate = Empty
    varCancelledTime = Empty
    varCancelledDateTime = Empty
    varDate = Empty
    varTime = Empty
    varPaidFee = 0
    varPaid = False
    varPaidUserID = 0
    varPaidDate = Empty
    varPaidTime = Empty
    varPaidComments = Empty
    varPaidCancelled = False
    varPaidCancelledDate = Empty
    varPaidCancelledTime = Empty
    varPaidCancelledUserID = 0
    varPaidCancelledDateTime = Empty
    varProfessionalPaymentBillID = 0
    varCancelledProfessionalPaymentBillID = 0
    varIsOPDBill = False
    varIsLabBill = False
    varIsPharmacyBill = False
    varIsInwardPaymentBill = False
    varIsMedicalTestBill = False
    varIsGSBill = False
    varIsHSTBill = False
    varIsRBill = False
    varIsIncomeBill = False
    varIsExpenceBill = False
    varupsize_ts = Empty
    varFeeCancelled = 0
    varFeeReturned = 0
    varFeePaid = 0
    varFeeToPay = 0
    varFeeToGive = 0
    varBilledDate = Empty
    varBillledTime = Empty
End Sub

Public Property Let ProfessionalChargesID(ByVal vProfessionalChargesID As Long)
    Call clearData
    varProfessionalChargesID = vProfessionalChargesID
    Call loadData
End Property

Public Property Get ProfessionalChargesID() As Long
    ProfessionalChargesID = varProfessionalChargesID
End Property

Public Property Let PatientServiceCharge(ByVal vPatientServiceCharge As Boolean)
    varPatientServiceCharge = vPatientServiceCharge
End Property

Public Property Get PatientServiceCharge() As Boolean
    PatientServiceCharge = varPatientServiceCharge
End Property

Public Property Let ProfessionalCharge(ByVal vProfessionalCharge As Boolean)
    varProfessionalCharge = vProfessionalCharge
End Property

Public Property Get ProfessionalCharge() As Boolean
    ProfessionalCharge = varProfessionalCharge
End Property

Public Property Let PatientServiceID(ByVal vPatientServiceID As Long)
    varPatientServiceID = vPatientServiceID
End Property

Public Property Get PatientServiceID() As Long
    PatientServiceID = varPatientServiceID
End Property

Public Property Let StaffID(ByVal vStaffID As Long)
    varStaffID = vStaffID
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let UserID(ByVal vUserID As Long)
    varUserID = vUserID
End Property

Public Property Get UserID() As Long
    UserID = varUserID
End Property

Public Property Let ForBHTID(ByVal vForBHTID As Long)
    varForBHTID = vForBHTID
End Property

Public Property Get ForBHTID() As Long
    ForBHTID = varForBHTID
End Property

Public Property Let ForOPDBillID(ByVal vForOPDBillID As Long)
    varForOPDBillID = vForOPDBillID
End Property

Public Property Get ForOPDBillID() As Long
    ForOPDBillID = varForOPDBillID
End Property

Public Property Let ForLabBillID(ByVal vForLabBillID As Long)
    varForLabBillID = vForLabBillID
End Property

Public Property Get ForLabBillID() As Long
    ForLabBillID = varForLabBillID
End Property

Public Property Let ForMedicalTestBillID(ByVal vForMedicalTestBillID As Long)
    varForMedicalTestBillID = vForMedicalTestBillID
End Property

Public Property Get ForMedicalTestBillID() As Long
    ForMedicalTestBillID = varForMedicalTestBillID
End Property

Public Property Let ForHSTBillID(ByVal vForHSTBillID As Long)
    varForHSTBillID = vForHSTBillID
End Property

Public Property Get ForHSTBillID() As Long
    ForHSTBillID = varForHSTBillID
End Property

Public Property Let ForRBillID(ByVal vForRBillID As Long)
    varForRBillID = vForRBillID
End Property

Public Property Get ForRBillID() As Long
    ForRBillID = varForRBillID
End Property

Public Property Let ForExpenceBillID(ByVal vForExpenceBillID As Long)
    varForExpenceBillID = vForExpenceBillID
End Property

Public Property Get ForExpenceBillID() As Long
    ForExpenceBillID = varForExpenceBillID
End Property

Public Property Let ForIncomeBillID(ByVal vForIncomeBillID As Long)
    varForIncomeBillID = vForIncomeBillID
End Property

Public Property Get ForIncomeBillID() As Long
    ForIncomeBillID = varForIncomeBillID
End Property

Public Property Let ServiceProfessionalChargesID(ByVal vServiceProfessionalChargesID As Long)
    varServiceProfessionalChargesID = vServiceProfessionalChargesID
End Property

Public Property Get ServiceProfessionalChargesID() As Long
    ServiceProfessionalChargesID = varServiceProfessionalChargesID
End Property

Public Property Let Fee(ByVal vFee As Double)
    varFee = vFee
End Property

Public Property Get Fee() As Double
    Fee = varFee
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledUserID(ByVal vCancelledUserID As Long)
    varCancelledUserID = vCancelledUserID
End Property

Public Property Get CancelledUserID() As Long
    CancelledUserID = varCancelledUserID
End Property

Public Property Let CancelledDate(ByVal vCancelledDate)
    varCancelledDate = vCancelledDate
End Property

Public Property Get CancelledDate()
    CancelledDate = varCancelledDate
End Property

Public Property Let CancelledTime(ByVal vCancelledTime)
    varCancelledTime = vCancelledTime
End Property

Public Property Get CancelledTime()
    CancelledTime = varCancelledTime
End Property

Public Property Let CancelledDateTime(ByVal vCancelledDateTime)
    varCancelledDateTime = vCancelledDateTime
End Property

Public Property Get CancelledDateTime()
    CancelledDateTime = varCancelledDateTime
End Property

Public Property Let Time(ByVal vTime)
    varTime = vTime
End Property

Public Property Get Time()
    Time = varTime
End Property

Public Property Let PaidFee(ByVal vPaidFee As Double)
    varPaidFee = vPaidFee
End Property

Public Property Get PaidFee() As Double
    PaidFee = varPaidFee
End Property

Public Property Let Paid(ByVal vPaid As Boolean)
    varPaid = vPaid
End Property

Public Property Get Paid() As Boolean
    Paid = varPaid
End Property

Public Property Let PaidUserID(ByVal vPaidUserID As Long)
    varPaidUserID = vPaidUserID
End Property

Public Property Get PaidUserID() As Long
    PaidUserID = varPaidUserID
End Property

Public Property Let PaidDate(ByVal vPaidDate)
    varPaidDate = vPaidDate
End Property

Public Property Get PaidDate()
    PaidDate = varPaidDate
End Property

Public Property Let PaidTime(ByVal vPaidTime)
    varPaidTime = vPaidTime
End Property

Public Property Get PaidTime()
    PaidTime = varPaidTime
End Property

Public Property Let PaidComments(ByVal vPaidComments As String)
    varPaidComments = vPaidComments
End Property

Public Property Get PaidComments() As String
    PaidComments = varPaidComments
End Property

Public Property Let PaidCancelled(ByVal vPaidCancelled As Boolean)
    varPaidCancelled = vPaidCancelled
End Property

Public Property Get PaidCancelled() As Boolean
    PaidCancelled = varPaidCancelled
End Property

Public Property Let PaidCancelledDate(ByVal vPaidCancelledDate)
    varPaidCancelledDate = vPaidCancelledDate
End Property

Public Property Get PaidCancelledDate()
    PaidCancelledDate = varPaidCancelledDate
End Property

Public Property Let PaidCancelledTime(ByVal vPaidCancelledTime)
    varPaidCancelledTime = vPaidCancelledTime
End Property

Public Property Get PaidCancelledTime()
    PaidCancelledTime = varPaidCancelledTime
End Property

Public Property Let PaidCancelledUserID(ByVal vPaidCancelledUserID As Long)
    varPaidCancelledUserID = vPaidCancelledUserID
End Property

Public Property Get PaidCancelledUserID() As Long
    PaidCancelledUserID = varPaidCancelledUserID
End Property

Public Property Let PaidCancelledDateTime(ByVal vPaidCancelledDateTime)
    varPaidCancelledDateTime = vPaidCancelledDateTime
End Property

Public Property Get PaidCancelledDateTime()
    PaidCancelledDateTime = varPaidCancelledDateTime
End Property

Public Property Let ProfessionalPaymentBillID(ByVal vProfessionalPaymentBillID As Long)
    varProfessionalPaymentBillID = vProfessionalPaymentBillID
End Property

Public Property Get ProfessionalPaymentBillID() As Long
    ProfessionalPaymentBillID = varProfessionalPaymentBillID
End Property

Public Property Let CancelledProfessionalPaymentBillID(ByVal vCancelledProfessionalPaymentBillID As Long)
    varCancelledProfessionalPaymentBillID = vCancelledProfessionalPaymentBillID
End Property

Public Property Get CancelledProfessionalPaymentBillID() As Long
    CancelledProfessionalPaymentBillID = varCancelledProfessionalPaymentBillID
End Property

Public Property Let IsOPDBill(ByVal vIsOPDBill As Boolean)
    varIsOPDBill = vIsOPDBill
End Property

Public Property Get IsOPDBill() As Boolean
    IsOPDBill = varIsOPDBill
End Property

Public Property Let IsLabBill(ByVal vIsLabBill As Boolean)
    varIsLabBill = vIsLabBill
End Property

Public Property Get IsLabBill() As Boolean
    IsLabBill = varIsLabBill
End Property

Public Property Let IsPharmacyBill(ByVal vIsPharmacyBill As Boolean)
    varIsPharmacyBill = vIsPharmacyBill
End Property

Public Property Get IsPharmacyBill() As Boolean
    IsPharmacyBill = varIsPharmacyBill
End Property

Public Property Let IsInwardPaymentBill(ByVal vIsInwardPaymentBill As Boolean)
    varIsInwardPaymentBill = vIsInwardPaymentBill
End Property

Public Property Get IsInwardPaymentBill() As Boolean
    IsInwardPaymentBill = varIsInwardPaymentBill
End Property

Public Property Let IsMedicalTestBill(ByVal vIsMedicalTestBill As Boolean)
    varIsMedicalTestBill = vIsMedicalTestBill
End Property

Public Property Get IsMedicalTestBill() As Boolean
    IsMedicalTestBill = varIsMedicalTestBill
End Property

Public Property Let IsGSBill(ByVal vIsGSBill As Boolean)
    varIsGSBill = vIsGSBill
End Property

Public Property Get IsGSBill() As Boolean
    IsGSBill = varIsGSBill
End Property

Public Property Let IsHSTBill(ByVal vIsHSTBill As Boolean)
    varIsHSTBill = vIsHSTBill
End Property

Public Property Get IsHSTBill() As Boolean
    IsHSTBill = varIsHSTBill
End Property

Public Property Let IsRBill(ByVal vIsRBill As Boolean)
    varIsRBill = vIsRBill
End Property

Public Property Get IsRBill() As Boolean
    IsRBill = varIsRBill
End Property

Public Property Let IsIncomeBill(ByVal vIsIncomeBill As Boolean)
    varIsIncomeBill = vIsIncomeBill
End Property

Public Property Get IsIncomeBill() As Boolean
    IsIncomeBill = varIsIncomeBill
End Property

Public Property Let IsExpenceBill(ByVal vIsExpenceBill As Boolean)
    varIsExpenceBill = vIsExpenceBill
End Property

Public Property Get IsExpenceBill() As Boolean
    IsExpenceBill = varIsExpenceBill
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let FeeCancelled(ByVal vFeeCancelled As Double)
    varFeeCancelled = vFeeCancelled
End Property

Public Property Get FeeCancelled() As Double
    FeeCancelled = varFeeCancelled
End Property

Public Property Let FeeReturned(ByVal vFeeReturned As Double)
    varFeeReturned = vFeeReturned
End Property

Public Property Get FeeReturned() As Double
    FeeReturned = varFeeReturned
End Property

Public Property Let FeePaid(ByVal vFeePaid As Double)
    varFeePaid = vFeePaid
End Property

Public Property Get FeePaid() As Double
    FeePaid = varFeePaid
End Property

Public Property Let FeeToPay(ByVal vFeeToPay As Double)
    varFeeToPay = vFeeToPay
End Property

Public Property Get FeeToPay() As Double
    FeeToPay = varFeeToPay
End Property

Public Property Let FeeToGive(ByVal vFeeToGive As Double)
    varFeeToGive = vFeeToGive
End Property

Public Property Get FeeToGive() As Double
    FeeToGive = varFeeToGive
End Property

Public Property Let BilledDate(ByVal vBilledDate)
    varBilledDate = vBilledDate
End Property

Public Property Get BilledDate()
    BilledDate = varBilledDate
End Property

Public Property Let BillledTime(ByVal vBillledTime)
    varBillledTime = vBillledTime
End Property

Public Property Get BillledTime()
    BillledTime = varBillledTime
End Property


