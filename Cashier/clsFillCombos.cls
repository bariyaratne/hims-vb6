VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFillCombos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim rsFill As New ADODB.Recordset
    Dim temSQL As String
    
    
    
Public Sub FillAnyCombo(ComboToFill As DataCombo, table As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then temSQL = temSQL & " Where Deleted <> 1 OR Deleted is Null "
    temSQL = temSQL & " Order by " & table
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = table
        .BoundColumn = table & "ID"
    End With
End Sub

Public Sub FillBoolCombo(ComboToFill As DataCombo, table As String, ListField As String, BoolField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & BoolField & " = 1 "
    Else
        temSQL = temSQL & " Where " & BoolField & " = 1 "
    End If
    temSQL = temSQL & " Order by " & ListField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub


Public Sub FillBoolList(ComboToFill As DataList, table As String, ListField As String, BoolField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & BoolField & " = 1 "
    Else
        temSQL = temSQL & " Where " & BoolField & " = 1 "
    End If
    temSQL = temSQL & " Order by " & ListField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub


Public Sub FillDblBoolCombo(ComboToFill As DataCombo, table As String, ListField As String, BoolField1 As String, BoolField2 As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & BoolField1 & " = 1 AND " & BoolField2 & " = 1 "
    Else
        temSQL = temSQL & " Where " & BoolField1 & " = 1 AND " & BoolField2 & " = 1 "
    End If
    temSQL = temSQL & " Order by " & ListField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub

Public Sub FillDblBoolComboNegPos(ComboToFill As DataCombo, table As String, ListField As String, BoolField1 As String, BoolFieldVal1 As Integer, BoolField2 As String, BoolFieldVal2 As Integer, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & BoolField1 & " = " & BoolFieldVal1 & " AND " & BoolField2 & " = " & BoolFieldVal2 & " "
    Else
        temSQL = temSQL & " Where " & BoolField1 & " = " & BoolFieldVal2 & " AND " & BoolField2 & " = " & BoolFieldVal2 & " "
    End If
    temSQL = temSQL & " Order by " & ListField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub


Public Sub FillSpecificField(ComboToFill As DataCombo, table As String, DisplayField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then temSQL = temSQL & " Where Deleted = 0 "
    temSQL = temSQL & " Order by " & DisplayField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = DisplayField
        .BoundColumn = table & "ID"
    End With
End Sub

Public Sub FillSpecificIDField(ComboToFill As DataCombo, table As String, IDField As String, DisplayField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then temSQL = temSQL & " Where Deleted = 0 "
    temSQL = temSQL & " Order by " & DisplayField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = DisplayField
        .BoundColumn = IDField
    End With
End Sub

Public Sub FillSpecificIDFielterField(ComboToFill As DataCombo, table As String, IDField As String, FilterValue As Long, DisplayField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then temSQL = temSQL & " Where Deleted = 0 "
    If FilterValue <> 0 Then
        temSQL = temSQL & " AND " & IDField & " = " & FilterValue & " "
    End If
    temSQL = temSQL & " Order by " & DisplayField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = DisplayField
        .BoundColumn = table & "ID"
    End With
End Sub

Public Sub FillSpecificFieldBoolCombo(ComboToFill As DataCombo, table As String, ListField As String, DisplayField As String, BoolField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & BoolField & " = 1 "
    Else
        temSQL = temSQL & " Where " & BoolField & " = 1 "
    End If
    temSQL = temSQL & " Order by " & DisplayField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = DisplayField
        .BoundColumn = table & "ID"
    End With
End Sub

Public Sub FillLongCombo(ComboToFill As DataCombo, table As String, ListField As String, LongField As String, LongValue As Long, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & LongField & " = " & LongValue
    Else
        temSQL = temSQL & " Where " & LongField & " =  " & LongValue
    End If
    temSQL = temSQL & " Order by " & ListField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub

Public Sub FillLongBoolCombo(ComboToFill As DataCombo, table As String, ListField As String, LongField As String, LongValue As Long, BoolField As String, Optional DoNotIncludeDeleted As Boolean)
    temSQL = "Select * from tbl" & table
    If DoNotIncludeDeleted = True Then
        temSQL = temSQL & " Where Deleted = 0 AND " & LongField & " = " & LongValue & " AND " & BoolField & " = 1 "
    Else
        temSQL = temSQL & " Where " & LongField & " =  " & LongValue & " AND " & BoolField & " = 1 "
    End If
    temSQL = temSQL & " Order by " & ListField
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub


Public Sub FillSQLCombo(ComboToFill As DataCombo, table As String, ListField As String, strSQL As String)
    temSQL = strSQL
    With rsFill
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With ComboToFill
        Set .RowSource = rsFill
        .ListField = ListField
        .BoundColumn = table & "ID"
    End With
End Sub
