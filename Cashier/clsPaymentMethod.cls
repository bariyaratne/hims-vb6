VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPaymentMethod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varPaymentMethodID As Long
    Private varPaymentMethod As String
    Private varCanPay As Boolean
    Private varCanReceive As Boolean
    Private varFinalPay As Boolean
    Private varSettlingPay As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblPaymentMethod Where PaymentMethodID = " & varPaymentMethodID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !PaymentMethod = varPaymentMethod
        !CanPay = varCanPay
        !CanReceive = varCanReceive
        !FinalPay = varFinalPay
        !SettlingPay = varSettlingPay
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varPaymentMethodID = !NewID
        Else
            varPaymentMethodID = !PaymentMethodID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPaymentMethod WHERE PaymentMethodID = " & varPaymentMethodID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!PaymentMethod) Then
               varPaymentMethod = !PaymentMethod
            End If
            If Not IsNull(!CanPay) Then
               varCanPay = !CanPay
            End If
            If Not IsNull(!CanReceive) Then
               varCanReceive = !CanReceive
            End If
            If Not IsNull(!FinalPay) Then
               varFinalPay = !FinalPay
            End If
            If Not IsNull(!SettlingPay) Then
               varSettlingPay = !SettlingPay
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPaymentMethodID = 0
    varPaymentMethod = Empty
    varCanPay = False
    varCanReceive = False
    varFinalPay = False
    varSettlingPay = False
End Sub

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    Call clearData
    varPaymentMethodID = vPaymentMethodID
    Call loadData
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let PaymentMethod(ByVal vPaymentMethod As String)
    varPaymentMethod = vPaymentMethod
End Property

Public Property Get PaymentMethod() As String
    PaymentMethod = varPaymentMethod
End Property

Public Property Let CanPay(ByVal vCanPay As Boolean)
    varCanPay = vCanPay
End Property

Public Property Get CanPay() As Boolean
    CanPay = varCanPay
End Property

Public Property Let CanReceive(ByVal vCanReceive As Boolean)
    varCanReceive = vCanReceive
End Property

Public Property Get CanReceive() As Boolean
    CanReceive = varCanReceive
End Property

Public Property Let FinalPay(ByVal vFinalPay As Boolean)
    varFinalPay = vFinalPay
End Property

Public Property Get FinalPay() As Boolean
    FinalPay = varFinalPay
End Property

Public Property Let SettlingPay(ByVal vSettlingPay As Boolean)
    varSettlingPay = vSettlingPay
End Property

Public Property Get SettlingPay() As Boolean
    SettlingPay = varSettlingPay
End Property


